#include "Helpers.h"



#include <dirent.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <libxml/parser.h>






int splitString(vector<string>& l, const string& s, char ch) {
    l.clear();
	size_t a = 0, e = 0;
	do {
		e = s.find(ch, a);
		if (e == string::npos) break;
		l.push_back(s.substr(a, e-a));
		a=e+1;
	} while (true);
	l.push_back(s.substr(a, s.length()-a));
	return l.size();
}

void toUpperCase(string &s) {
	for(unsigned int i=0; i < s.length(); i++) {
		if ((s[i]>='a') && (s[i]<='z'))
			s[i] = 'A' + (s[i]-'a');
	}
}
void toLowerCase(string &s){
	for(unsigned int i=0; i < s.length(); i++) {
		if ((s[i]>='A') && (s[i]<='Z'))
			s[i] = 'a' + (s[i]-'A');
	}
}


bool isNumber(const string& s, bool neg) {
	unsigned int i=0;
	if ((s[i] == '-') && neg) i++;
	for (; i < s.length(); i++) {
		if ( (s[i] < '0') || (s[i] > '9')) return false;
	}
	return true;
}




string trim(string& s)
{
    int a = 0;
    int e = s.length();
    while ((a < e) && ((s[a]==' ')||(s[a]=='\n')||(s[a]=='\t')||(s[a]=='\r')))
    {
        a++;
    }
    e -= a;
    while ((e > 0) && ((s[a+e-1]==' ')||(s[a+e-1]=='\n')||(s[a+e-1]=='\t')||(s[a+e-1]=='\r')))
    {
        e--;
    }

    return s.substr(a,e);
}
void trim_inplace(string& s)
{
    int a = 0;
    int e = s.length();
    while ((a < e) && ((s[a]==' ')||(s[a]=='\n')||(s[a]=='\t')||(s[a]=='\r')))
    {
        a++;
    }
    s.replace(0,a,"");
    a = s.length();
    e = 0;
    while ((e < a) && ((s[a-e-1]==' ')||(s[a-e-1]=='\n')||(s[a-e-1]=='\t')||(s[a-e-1]=='\r')))
    {
        e++;
    }
    s.replace(a-e,e,"");
}


// TODO --> file test
bool DirectoryExists( const char* pzPath )
{
    DIR *pDir;
    bool bExists = false;

    pDir = opendir (pzPath);

    if (pDir != NULL){
        bExists = true;
        closedir (pDir);
    }
    return bExists;
}



bool FileExists(const string& fileName)
{
    return g_file_test(fileName.c_str(), G_FILE_TEST_EXISTS) && g_file_test(fileName.c_str(), G_FILE_TEST_IS_REGULAR);
}

bool FileNotExists(const string& fileName)
{
    return !g_file_test(fileName.c_str(), G_FILE_TEST_EXISTS) || !g_file_test(fileName.c_str(), G_FILE_TEST_IS_REGULAR);
}


bool isFileWritable(const string& fileName)
{
/*    gchar *dir;
    dir = g_path_get_dirname(fileName.c_str());
    cout << dir << endl;
    //                      Eigentü, Gruppe, Sonst
    bool r = (g_access(dir, 0x220) == 0);
    g_free(dir);
    return r;*/
            FILE* f = fopen(fileName.c_str(), "a");
            if (f)
            {
                fclose(f);
                return true;
            }
            return false;

}

/** @brief getFirstElement
  *
  * @todo: document this function
  */
xmlpp::Element* getFirstElement(xmlpp::Element* parent, const std::string& name)
{
    xmlpp::Node::NodeList nl = parent->get_children("circuit");
    if (nl.size() > 0)
        return (xmlpp::Element*)*(nl.begin());
    else return NULL;
    //(xmlpp::Element *)(*(e->get_children("circuit").begin()));
}





bool xmlParseTestMemory(const Glib::ustring& xml)
{
    xmlDocPtr doc; /* the resulting document tree */
    doc = xmlReadMemory(xml.c_str(), xml.bytes(), "noname.xml", NULL, 0);
    if (doc != NULL)
    {
        xmlFreeDoc(doc);
        return true;
    }
    return false;
}

bool xmlParseTestFile(const Glib::ustring& xml)
{
    xmlDocPtr doc; /* the resulting document tree */
    doc = xmlReadFile(xml.c_str(), NULL, 0);
    if (doc != NULL)
    {
        xmlFreeDoc(doc);
        return true;
    }
    return false;
}


