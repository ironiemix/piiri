/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef PROPERTYDIALOG2_H
#define PROPERTYDIALOG2_H

#include <gtkmm.h>
#include <string>

#include "model/Complex2.h"

class PropertyDialog2 : public Gtk::Dialog
{
    public:

        PropertyDialog2(const string& t, Gtk::Window& parent, Complex2& c);
        virtual ~PropertyDialog2();

        int run();

        void writeback(bool checkOnly = false);
        void changed(bool c) {change = c;}
        bool changed() {return change;}
        bool hasChanged();


        void setUiText(std::string, ui_text);
        ui_text_map& getUiTextMap() {return m_uiText;};

        //typedef sigc::signal<std::pair<std::string,std::string>, std::string&> type_signal_property_title;
        //type_signal_property_title signal_property_title() {return m_signal_property_title;}

    protected:
        //type_signal_property_title m_signal_property_title;

        //Gtk::Widget *createWidget(std::string &name, Complex2 *c);
        ui_text getUiText(std::string& path);

    private:
        typedef std::map<TypedBase*, Gtk::Widget*> wmap;
        wmap widgets;
        bool change;
        bool uiCreated;

        ui_text_map m_uiText;

        Complex2& m_rComplex;
        // http://developer.gnome.org/gtkmm/2.24/classGtk_1_1Notebook.html
        Gtk::Notebook tabbing;
        Gtk::VBox main;
};

#endif // PROPERTYDIALOG_H
