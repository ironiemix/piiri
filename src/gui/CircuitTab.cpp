#include "gui/CircuitTab.h"

#include "model/GeneralUndoItem.h"

#include <glib.h>
#include <glib/gstdio.h>
#include <fstream>

#include "gui/ViewWindow.h"
#include "gui/DiagramWindow.h"
#include "gui/TableWindow.h"

#include "gui/PropertyDialog2.h"

CircuitTab::CircuitTab(Gtk::Label* tab_label, Glib::RefPtr<Gtk::UIManager> uim) :
 m_pTabLabel(tab_label),
 inputNumberModifier(-1),
 m_refUIManager(uim),
 m_ClockScale(20, 5000, 20),
 m_ClockCombo(),
 m_ClockCheck("Takt (ms): "),
 m_ClockScaleButton(10, 0),
 m_MoveActive(),
 m_Back(Gtk::Stock::GO_BACK),
 m_MoveImage(App::etc_piiri_path + "/move.png"),
 m_NoMoveImage(App::etc_piiri_path + "/nomove.png"),

 m_pTLCircuit(new ToplevelCircuit()),
 m_pEdCircuit(m_pTLCircuit),
 m_pVisual(new ToplevelCircuitVisual(m_pEdCircuit)),
 m_cview(false, m_pVisual),

 tcb(this),
 m_undo(10, (unsigned int)this/* id, nicht pointer! */),
 m_changed(false),
 editActive(true)
{
    //m_cview.signal_realize().connect(sigc::mem_fun(*this, &CircuitTab::on_cwidget_realize));

    m_Toolbar.set_toolbar_style(Gtk::TOOLBAR_ICONS);
    m_Tools.pack_start(m_Toolbar, Gtk::PACK_EXPAND_WIDGET);


    m_Name.add_label("-");
	m_Toolbar.append(m_Name);

	m_Back.set_sensitive(false);
	m_Toolbar.append(m_Back, sigc::mem_fun(*this, &CircuitTab::on_Back));



    // Bewegung erlaubtn -toolbutton
    m_Toolbar.append(m_MoveActive, sigc::mem_fun(*this, &CircuitTab::on_action_MoveActivate));
    Gtk::SeparatorToolItem* sti = Gtk::manage(new Gtk::SeparatorToolItem());
    m_Toolbar.append(*sti);
    m_MoveActive.set_active(true);
    m_MoveImage.show();
    m_NoMoveImage.show();



	m_cview.signal_circuitstate().connect(sigc::mem_fun(*this, &CircuitTab::on_circuitstate));

    signal_key_release_event().connect(sigc::mem_fun(*this, &CircuitTab::on_key_release_event));
    signal_key_press_event().connect(sigc::mem_fun(*this, &CircuitTab::on_key_press_event));

	m_pTLCircuit->signal_inputs_changed().connect(sigc::mem_fun(*this, &CircuitTab::on_clock_menu));
	m_pTLCircuit->signal_circuit_notification().connect(sigc::mem_fun(*this, &CircuitTab::on_circuit_notification));





	Gtk::Adjustment* adj = m_ClockScaleButton.get_adjustment();
	adj->set_upper(5000);
	adj->set_lower(20);
	adj->set_step_increment(10);
	m_ClockScaleButton.set_value(1000);

	m_ClockScale.set_update_policy(Gtk::UPDATE_DISCONTINUOUS);
	m_ClockScale.set_size_request(100,-1);
	m_ClockScale.set_value(1000);
	m_ClockScale.set_visible(false);

	m_ClockCombo.signal_changed().connect(sigc::mem_fun(*this, &CircuitTab::on_clock_changed));
	m_ClockScaleButton.signal_value_changed().connect(sigc::mem_fun(*this, &CircuitTab::on_clock_scale_changed));
	m_ClockScale.signal_value_changed().connect(sigc::mem_fun(*this, &CircuitTab::on_clock_scale_changed));
	m_ClockCheck.signal_clicked().connect(sigc::mem_fun(*this, &CircuitTab::on_clock_activate));
	Gtk::HBox* clock_hbox = Gtk::manage(new Gtk::HBox());

	// clock_hbox->add(m_ClockScale);
	//clock_hbox->add(*Gtk::manage(new Gtk::Label("Takt (ms): ")));
	clock_hbox->add(m_ClockCheck);
	clock_hbox->add(m_ClockCombo);
	clock_hbox->add(m_ClockScaleButton);
	Gtk::ToolItem* clock_tool_item = Gtk::manage(new Gtk::ToolItem());
	clock_tool_item->add(*clock_hbox);
	m_Toolbar.append(*clock_tool_item);
    clock_hbox->set_tooltip_markup("<b>Takt-Geber</b>\nLegen Sie ein Taktsignal auf einen beliebigen Eingang der Schaltung.");




    m_ScrolledWindowCanvas.set_shadow_type(Gtk::SHADOW_IN);
	m_ScrolledWindowCanvas.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	m_ScrolledWindowCanvas.set_border_width(0);
	m_ScrolledWindowCanvas.add(m_cview);

	pack_end(m_ScrolledWindowCanvas, Gtk::PACK_EXPAND_WIDGET);
    pack_start(m_Tools, Gtk::PACK_SHRINK);

	m_cview.drag_dest_set(App::getMW().listTargets);


	show_all_children();

}


CircuitTab::~CircuitTab()
{
    stopClock();
    closeSubwindows();

    if (m_pTLCircuit) delete m_pTLCircuit;
    m_pEdCircuit = m_pTLCircuit = NULL;

    if (m_pVisual) delete m_pVisual;
    m_pVisual = NULL;

    while (!m_visualStack.empty())
    {
        ToplevelCircuitVisual* pv = m_visualStack.top();
        m_visualStack.pop();
        delete pv;
    }

}

void CircuitTab::updateAll()
{
    list<Updatable *>::iterator it;
	m_cview.update();
	for (it = subwindows.begin(); it != subwindows.end(); it++)
	{
		(*it)->updateAll();
	}
	App::getMW().updateUI();
}




bool CircuitTab::command_open(const std::string& fn)
{
    if (fn.empty())
    {
		m_undo.clear();
        m_undo.addItem(new GeneralUndoItem("Neue Schaltung", this, m_undo));
		changed(false);
        return true;
    }

	if (openFile(fn))
	{
        App::getMW().stopStepTimer();

		m_pTLCircuit->setFilename(fn);
		gchar* bn = g_path_get_basename(fn.c_str());
		string name(bn);
		g_free(bn);


		m_pTLCircuit->setName(name);
		m_pTabLabel->set_text(name);
		m_pTabLabel->set_tooltip_text(fn);

		m_undo.clear();
        m_undo.addItem(new GeneralUndoItem("Schaltung geladen", this, m_undo));


		changed(false);

		updateAll();

        App::getMW().startStepTimer();

		return true;
	}

	return false;
}

/** @brief openContainerView
  *
  * @todo: document this function
  */
void CircuitTab::openContainerView(EditableCircuit* pobj)
{
    // TODO subfenster?

    // ist pobj ein kind von m_pTLCircuit
    CircuitContainer* pc = pobj;
    while (pc)
    {
        if (pc == m_pEdCircuit) break;
        pc = pc->getParent();
    }
    if (pc==NULL) return;

    m_cview.deselectAll();

    ToplevelCircuitVisual* pvis = new ToplevelCircuitVisual(pobj);

    m_cview.setVisual(pvis);

    m_visualStack.push(m_pVisual);
    m_pVisual = pvis;
    m_pEdCircuit = pobj;

    std::string markup("<b>Eingebetteten Modus schließen</b>\n");
    markup += "Aktuelle Komponente: \n"+m_pEdCircuit->getNameRecursive(true);
    m_Back.set_tooltip_markup(markup);
    dynamic_cast<Gtk::Label*>(m_Name.get_child())->set_text(">"+m_pEdCircuit->getNameRecursive(false));

	m_Back.set_sensitive(true);

    updateAll();
}

/** @brief on_Back
  *
  * @todo: document this function
  */
void CircuitTab::on_Back()
{
    closeContainerView(); // nur eines
}

/** @brief closeContainerView
  *
  * @todo: document this function
  */
void CircuitTab::closeContainerView(bool all)
{
    ToplevelCircuitVisual* pv = NULL;

    m_cview.deselectAll();

    closeContainerView_loop:

    if (!m_visualStack.empty())
    {
        pv = m_visualStack.top();
        //cout << m_pVisual->getObj()->getName() << endl;
        m_visualStack.pop();
        delete m_pVisual;
        m_pVisual = pv;

        if (all) goto closeContainerView_loop;
    }

    // wenn pv != NULL ist, dann wurde zumindest ein schritt durchgeführt!
    if (pv)
    {
        m_cview.setVisual(m_pVisual);
        m_pEdCircuit = m_pVisual->getObj();
    }


    if (m_visualStack.empty())
    {
        m_Back.set_sensitive(false);
        m_Back.set_tooltip_markup("");
        dynamic_cast<Gtk::Label*>(m_Name.get_child())->set_text("");
    }
    else
    {
        std::string markup("<b>Eingebetteten Modus schließen</b>\n");
        markup += m_pEdCircuit->getNameRecursive(false);
        m_Back.set_tooltip_markup(markup);
        dynamic_cast<Gtk::Label*>(m_Name.get_child())->set_text(">"+m_pEdCircuit->getNameRecursive(false));
    }

    updateAll();
}





bool CircuitTab::openFile(const string& filename)
{
    if (m_pTLCircuit->loadFile(filename))
    {
        Complex2* pc = m_pTLCircuit->getProperties().get_complex("plan");

        m_cview.setPlan(
            pc->get_string("file"),
            pc->get_int("x"), pc->get_int("y"),
            pc->get_double("scale"),
            pc->get_double("alpha"),
            pc->get_bool("over"),
            pc->get_bool("show")
        );

        m_cview.setZoom(m_pTLCircuit->m_pExtraProperties->get_double("zoom"));

        /*std::cout << m_pTLCircuit->m_pExtraProperties->get_bool("clock_active") << std::endl;
        std::cout << m_pTLCircuit->m_pExtraProperties->get_string("clock_input") << std::endl;
        std::cout << m_pTLCircuit->m_pExtraProperties->get_int("clock_intervall") << std::endl;*/

        m_ClockCheck.set_active(m_pTLCircuit->m_pExtraProperties->get_bool("clock_active"));
        m_ClockCombo.set_active_text(m_pTLCircuit->m_pExtraProperties->get_string("clock_input"));
        m_ClockScaleButton.set_value(m_pTLCircuit->m_pExtraProperties->get_int("clock_intervall"));

        m_pTLCircuit->update();

		return true;
    }
	return false;
}



void CircuitTab::changed(bool c)
{

	if (m_changed != c)
	{
		m_changed = c;
		string s = m_pTabLabel->get_text();
		if (c)
		{
			if (s[s.size()-1] != '*')
			{
				m_pTabLabel->set_text(s + " *");
			}
		}
		else
		{
			if (s[s.size()-1] == '*')
			{
				s = s.substr(0, s.size()-2);
				m_pTabLabel->set_text(s);
			}
		}
	}
	if (!c) m_undo.setSaved();
}



void CircuitTab::init()
{
    /*
	Complex *c = m_pCircuit->getProps()->set_complex("plan");
	c->set_bool("show", circuitArea.planShow);
	c->set_bool("over", circuitArea.planOver);
	c->set_double("alpha", circuitArea.planAlpha);
	c->set_double("scale", circuitArea.planScale);
	c->set_string("file", circuitArea.planFile);
	c->set_int("x", circuitArea.planX);
	c->set_int("y", circuitArea.planY);

	m_pCircuit->init();
	on_clock_menu()
	*/

}
/** @brief getClockMs
  *
  * @todo: document this function
  */
int CircuitTab::getClockMs()
{
	double d;

	if (m_ClockScale.get_visible())
	{
        d = m_ClockScale.get_value();
	}
	else
	{
	    istringstream is(m_ClockScaleButton.get_text());
	    is >> d;
	}

    if (d < 20) d = 20;

    return d;
}



/**
 * Clock-Steuerung
 */
void CircuitTab::on_clock_scale_changed()
{
    m_pTLCircuit->m_pExtraProperties->set_int("clock_intervall", m_ClockScaleButton.get_value());

	if (m_ClockCheck.get_active())
	{
		startClock(m_ClockCombo.get_active_text(), getClockMs());
	}
}

/**
 * Clock-Steuerung
 */
void CircuitTab::on_clock_changed()
{
    //std::cout << "on_clock_changed: " << m_ClockCombo.get_active_text() << std::endl;
    m_pTLCircuit->m_pExtraProperties->set_string("clock_input", m_ClockCombo.get_active_text());

	if (m_ClockCheck.get_active())
	{
		startClock(m_ClockCombo.get_active_text(), getClockMs());
	}
}

/**
 * Clock-Steuerung
 */
void CircuitTab::on_clock_activate()
{
    m_pTLCircuit->m_pExtraProperties->set_bool("clock_active", m_ClockCheck.get_active());

	if (m_ClockCheck.get_active())
	{
		startClock(m_ClockCombo.get_active_text(), getClockMs());
	}
	else
	{
		stopClock();
	}
}

/**
 * callback für on_inputs_changed des CCirquit:
 * mögliche inputs in clock-combo setzen
 */
void CircuitTab::on_clock_menu()
{
    //std::cout << "on_clock_menu" << std::endl;

	string sel = m_ClockCombo.get_active_text();
	bool hasit = false;
	m_ClockCombo.clear_items(); // --> remove_all ab gtkmm 2.8
	type_inputs::iterator iit;
	for (iit = m_pTLCircuit->getInputs().begin(); iit != m_pTLCircuit->getInputs().end(); iit++)
	{
		if ((*iit)->getName() == sel) hasit = true;
		m_ClockCombo.append_text((*iit)->getName());
	}

	if (hasit)
	{
	    m_ClockCombo.set_active_text(sel);
	}
	else
	{
		stopClock();
	}
}

/**
 * Clock starten.
 */
void CircuitTab::startClock(const string& in, int interval)
{
	if (in != "")
	{
		string old = tcb.m_In;

		tcb.m_In = in;

		if ((old != "") && (tcb.m_In != old)) m_pTLCircuit->setInput(old, false);
		tcb.c.disconnect();

		tcb.m_pObj = m_pTLCircuit;
		tcb.m_Interval = 1500;
		tcb.m_Interval = interval; //atoi(sp[2].c_str());
		tcb.m_StopClock = false;
		tcb.m_pTab = this;

		tcb.c = Glib::signal_timeout().connect(sigc::mem_fun(tcb, &TimerCallback::timer_callback), tcb.m_Interval);

		if (!m_ClockCheck.get_active()) m_ClockCheck.set_active(true);

		updateAll();
	}
}

/**
 * Clock stoppen.
 */
void CircuitTab::stopClock()
{
	if (tcb.m_In != "") m_pTLCircuit->setInput(tcb.m_In, false);
	if (tcb.c.connected()) tcb.c.disconnect();
	if (m_ClockCheck.get_active()) m_ClockCheck.set_active(false);
	updateAll();
}

/** @brief setMoveActivation
  *
  * @todo: document this function
  */
void CircuitTab::command_edit_active(bool a)
{
    editActive = a;
    m_cview.editActive = a;
    App::getMW().updateUI();
    App::getMW().update_paste_status();
}

/** @brief componentDrop
  *
  * @todo: document this function
  */
void CircuitTab::componentDrop(const std::string id, int x, int y)
{
    bool _dummy;
    CircuitChild* item = m_pEdCircuit->addComponent(id, "");
    CircuitChildVisual* vis = m_cview.getVisual()->adjoinVisual(item, _dummy); //=m_pVisual->adjoin...!!
    if (item && vis)
    {
        m_cview.update();

        int m_drag_dx = vis->getSizeW() / 2;
        int m_drag_dy = vis->getSizeH() / 2;

//        cout << "drop:"<<endl;
//        cout << "                 ("<<x<<", "<<y<<")"<<endl;

        m_cview.mouseToCircuit(x,y, x,y);
//        cout << "mouseToCircuit   ("<<x<<", "<<y<<")"<<endl;

        //m_cview.getVisual()->parentToComponent(x,y, x,y);
        //cout << "parentToComponent("<<x<<", "<<y<<")"<<endl;

        m_cview.getVisual()->componentToArea(CA_INNER,x,y, x,y);
//        cout << "componentToArea  ("<<x<<", "<<y<<")"<<endl;

        vis->setPos(x-m_drag_dx, y-m_drag_dy);


        m_cview.deselectAll();
        m_cview.selObj.push_back(vis);
        vis->selected = true;
        on_circuitstate(CSC_SELECTED);

        App::getMW().on_selection_changed();

        m_undo.addItem(new GeneralUndoItem("Komponente hinzufügen", this, m_undo));
        changed(true);

        updateAll();
    }
    else
    {
        cerr << "Komponente oder Visual wurde nicht erzeugt." << endl;
    }
}


/**
 * Signal-Handler für Statusveränderungen in der Schaltung:
 * Alles neuzeichnen (vor Allem Unterfenster!). kommt vom CircuitWidget
 */
void CircuitTab::on_circuitstate(int a)
{

	if (a == CSC_SWITCHED)
	{
	    updateAll();
	}
	else if (a == CSC_MOVED)
	{
        m_undo.addItem(new GeneralUndoItem("Komponente bewegen", this, m_undo));
		changed(true);
        App::getMW().updateUI();
	}
	else if (a == CSC_CONNECTED)
	{
        m_undo.addItem(new GeneralUndoItem("Verbindungen", this, m_undo));
		changed(true);
        App::getMW().updateUI();
	}
	else if (a == CSC_CONNECTING)
	{
	}
	else if (a == CSC_MOVING)
	{
	}

}



bool CircuitTab::askSave()
{
	bool ok = true;
	if (changed())
	{
		ok = false;

		Gtk::MessageDialog dialog(App::getMW(), "<b>Die Schaltung wurde geändert</b>\n\nSollen die Änderungen verworfen werden?", true, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_NONE, true);

		dialog.add_button("Änderungen verwerfen",    Gtk::RESPONSE_YES);
		dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);

		dialog.show_all_children();
        dialog.set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

		int res = dialog.run();
		dialog.hide();

		ok = (res == Gtk::RESPONSE_YES);
	}
	return ok;
}




bool CircuitTab::saveFile(const string& filename)
{

	// http://developer.gnome.org/libxml++/stable/classxmlpp_1_1Document.html
	// http://developer.gnome.org/libxml++/stable/classxmlpp_1_1Element.html

//	Complex *c = m_pTLCircuit->getProperties().get_complex("plan");
	/*c->set_bool("show", m_cview.planShow);
	c->set_bool("over", m_cview.planOver);
	c->set_double("alpha", m_cview.planAlpha);
	c->set_double("scale", m_cview.planScale);
	c->set_string("file", m_cview.planFile);
	c->set_int("x", m_cview.planX);
	c->set_int("y", m_cview.planY);*/

	return m_pTLCircuit->saveFile(filename);
}

bool CircuitTab::command_save_as()
{
	string s = m_pTLCircuit->getFilename();
	m_pTLCircuit->setFilename("");
	bool b = command_save();
	if (m_pTLCircuit->getFilename() == "")
	{
		m_pTLCircuit->setFilename(s);
	}
	return b;
}

bool CircuitTab::command_save()
{

	std::string filename;

	if (m_pTLCircuit->getFilename() == "")
	{
		Gtk::FileChooserDialog dialog(App::getMW(), "Schaltung speichern...", Gtk::FILE_CHOOSER_ACTION_SAVE);

		dialog.set_current_folder(App::cir_path);
		dialog.set_do_overwrite_confirmation(false);
		//dialog.set_create_folders(false);

		//Add response buttons the the dialog:
		dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
		dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);

		//Add filters, so that only certain file types can be selected:
		Gtk::FileFilter filter_cir;
		filter_cir.set_name("Schaltungen (*.cir)");
		filter_cir.add_pattern("*.cir");
		dialog.add_filter(filter_cir);

		//Show the dialog and wait for a user response:
        int result;
        do
        {
            result = dialog.run();
            if (result == Gtk::RESPONSE_OK)
            {
                filename = dialog.get_filename();
                if (!g_str_has_suffix(filename.c_str(),".cir"))
                {
                    filename += ".cir";
                }
                if (g_file_test(filename.c_str(), G_FILE_TEST_EXISTS))
                {
                    if (App::question("Datei existiert bereits", "Die unten genannte Datei existiert bereits.\nSoll Sie überschrieben werden?", filename, dialog))
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else break;
        } while (true);


		if (result != Gtk::RESPONSE_OK)
		{
			filename = "";
		}
	}
	else filename = m_pTLCircuit->getFilename();


	if (!filename.empty())
	{
		if (saveFile(filename))
		{
            gchar* bn = g_path_get_basename(filename.c_str());
			string name(bn);
			m_pTabLabel->set_text(name);
			m_pTabLabel->set_tooltip_text(filename);
			m_pTLCircuit->setName(name);
			m_pTLCircuit->setFilename(filename);
			changed(false);
			updateAll();
            g_free(bn);
			return true;
		}
	}

	return false;
}



/** @brief addIO
  *
  * @todo: document this function
  */
void CircuitTab::command_addIO(IO_TYPE t, const string& prae)
{
    if (t == IO_TYPE_IN)
    {
        m_pEdCircuit->addIn("", 1, prae);
    }
    else
    {
        m_pEdCircuit->addOut("", 1, prae);
    }
	m_undo.addItem(new GeneralUndoItem((t==IO_TYPE_IN) ? "Input hinzufügen" : "Output hinzufügen", this, m_undo));
	changed(true);
	updateAll();
}

void CircuitTab::command_zoom(ZOOM_ACTION z)
{
    switch (z)
    {
        case ZA_IN:
            m_cview.zoomIn();
        break;

        case ZA_OUT:
            m_cview.zoomOut();
        break;

        case ZA_100:
            m_cview.setZoom(1.0);
        break;
    }
    m_pTLCircuit->m_pExtraProperties->set_double("zoom", m_cview.getZoom());
}


/** @brief closeSubwindows
  *
  * @todo: document this function
  */
void CircuitTab::closeSubwindows(Updatable::SUBWINDOW_TYPE t)
{
	m_cview.update();
	for (list<Updatable *>::iterator it = subwindows.begin(); it != subwindows.end(); it++)
	{
	    if ((t == Updatable::SW_ALL) || (t == (*it)->getWindowType()))
		(*it)->hide();
		delete (*it);
	}
	subwindows.clear();
}

/** @brief viewSelectedElement
  *
  * @todo: document this function
  */
void CircuitTab::command_show()
{
	string name = "";
	CircuitContainer* pCont;
	type_visual_list::iterator it;

	// alle markierten Container-Komponenten öffnen.
	for (it = m_cview.selObj.begin(); it != m_cview.selObj.end(); it++)
	{
		pCont = dynamic_cast<CircuitContainer*>((*it)->getObj());
        if (pCont) openViewWindow(pCont);
	}

	m_cview.on_selection_changed();
	updateAll();
}

void CircuitTab::command_selectall()
{
    m_cview.deselectAll();
    type_elements_iterator it;
    bool _dummy;
    for (it = m_pEdCircuit->getChildren().begin(); it != m_pEdCircuit->getChildren().end(); it++)
    {
        CircuitChildVisual* vis = m_pVisual->adjoinVisual((*it), _dummy);
        m_cview.selObj.push_back(vis);
        vis->selected = true;
    }
	m_cview.on_selection_changed();
	updateAll();
}

void CircuitTab::command_embed()
{
    /*
	type_visual_list::iterator it;
//	bool c = false;

	if (m_cview.selObj.size()==0) return;

	for (it = m_cview.selObj.begin(); it != m_cview.selObj.end(); it++)
	{
	    CircuitChildVisual* pvis = (*it);
	    CircuitContainer* pcont = dynamic_cast<CircuitContainer*>(pvis->getObj());
	    if (pcont)
	    {
	        EditableCircuit* ped = (EditableCircuit*)CircuitContainer::createComponent(m_pEdCircuit, "container", pcont->getName());


            ped->clone(pcont->getChildren(), NULL);

            m_cview.getVisual()->m_childVisuals.erase(pcont); // Visual aus liste entfernen.
            delete pvis; // Visual löschen.
            m_pEdCircuit->removeComponent(pcont);

	        m_pEdCircuit->addComponent(ped);
	    }
	}
*/
    std::cerr << "Embed nicht implementiert" << std::endl;
}

bool CircuitTab::cloneSelectionVirtually(type_elements& to_clone, CircuitContainer& cloned)
{
	std::cout << "Klone " << to_clone.size() << " Elemente.";

	if (to_clone.size() == 0) return false;

	m_pEdCircuit->clone(to_clone, cloned);

	std::cout  << "Neu: " << cloned.getChildren().size() << " Kinderlein" << std::endl;

	return true;
}

void CircuitTab::command_clone()
{
    command_copy();
    command_paste();
}

void CircuitTab::command_cut()
{
    command_copy();
    command_delete();
}

void CircuitTab::command_copy()
{

	if (m_cview.selObj.size()==0) return;

    std::cout << "CircuitTab::command_copy [1]" << std::endl;

    /*
    //Build a string representation of the stuff to be copied:
    //Ideally you would use XML, with an XML parser here:
    Glib::ustring strData;
    strData = "Test String";*/

    Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();

    //Targets:
    std::vector<Gtk::TargetEntry> listTargets;

    listTargets.push_back( Gtk::TargetEntry(App::target_string) );


    //Store the copied data until it is pasted:
    //(Must be done after the call to refClipboard->set, because that call
    //may trigger a call to on_clipboard_clear.)

    // Markierung in XML umwandeln

	type_visual_list::iterator it;

	type_elements to_clone;
	for (it = m_cview.selObj.begin(); it != m_cview.selObj.end(); it++)
	{
	    to_clone.push_back((*it)->getObj());
	}


    // Temporären Container erstellen
    CircuitContainer c("piri_clipboard_container", NULL, false);
    // ... und mit Klon befüllen
    cloneSelectionVirtually(to_clone, c);

    // Dom-Parser anschmeißen
    xmlpp::DomParser dom;
    xmlpp::Document *doc = dom.get_document();
    if (!doc) return;
    xmlpp::Element *root = doc->create_root_node("piiriClipboard");
    if (!root) return;

    std::cout << "CircuitTab::command_copy [2]" << std::endl;


    // Container in xml umwandeln
    c.saveXml(root);

    // xml als string ablegen, bis es abgeholt wird.
    App::m_ClipboardStore = doc->write_to_string_formatted();

    std::cout << "- clipboard ----------------" << std::endl;
    std::cout << App::m_ClipboardStore << std::endl;
    std::cout << "----------------------------" << std::endl;


    refClipboard->set(
                      listTargets,
                      sigc::ptr_fun(&App::on_clipboard_get),
                      sigc::ptr_fun(&App::on_clipboard_clear)
                      );

}

void CircuitTab::command_paste()
{
    //Tell the clipboard to call our method when it is ready:
    Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();

    refClipboard->request_contents(App::target_string, sigc::mem_fun(*this, &CircuitTab::on_clipboard_received) );
}


void CircuitTab::on_clipboard_received(const Gtk::SelectionData& selection_data)
{
    //std::cout << "on_clipboard_received" << std::endl;

    const std::string target = selection_data.get_target();

    //It should always be this, because that' what we asked for when calling
    //request_contents().
    if(target == App::target_string)
    {
        Glib::ustring text_representation = selection_data.get_data_as_string();

        #ifdef XML_USE_EXCEPTIONS
        try
        #else
        if (xmlParseTestMemory(text_representation))
        #endif
        {
            xmlpp::DomParser dom;
            //dom.set_throw_messages(false);
            dom.parse_memory(text_representation);
            xmlpp::Document *doc = dom.get_document();
            if (!doc) return;
            xmlpp::Element *root = doc->get_root_node();
            if (!root) return;
            m_pEdCircuit->loadXml(root);

            // Die neuen selektieren?!
            m_cview.deselectAll();
            type_elements_iterator it;
            bool neu;
            for (it = m_pEdCircuit->getChildren().begin(); it != m_pEdCircuit->getChildren().end(); it++)
            {
                CircuitChildVisual* vis = m_pVisual->adjoinVisual((*it), neu);
                if (neu && vis)
                {
                    m_cview.selObj.push_back(vis);
                    vis->selected = true;
                }
            }

            m_undo.addItem(new GeneralUndoItem("Einfügen", this, m_undo));
            changed(true);

            on_circuitstate(CSC_SELECTED);
            m_cview.on_selection_changed();
            m_pTLCircuit->update();
            updateAll();
        }
        #ifdef XML_USE_EXCEPTIONS
        catch (xmlpp::exception& e)
        {
        #else
        else
        {
        #endif
            std::cerr << "Im Clipboard sind keine Daten, die eingefügt werden können." << std::endl;
            App::fetchError("Clipboard", App::getMW());
        }


    }
    else
    {
        std::cerr << "on_clipboard_received, falsches target" << std::endl;
    }
}




/** @brief openViewWindow
  *
  * @todo: document this function
  */
void CircuitTab::openViewWindow(CircuitContainer *pObj)
{
    // Fenster erzeugen und öffnen.
    ViewWindow* nwin = new ViewWindow(*this, pObj);
    subwindows.push_back(nwin);
    nwin->show();
}


/** @brief table
  *
  * @todo: document this function
  */
void CircuitTab::command_table()
{
    CircuitChild *pobj = NULL;

    if (m_cview.selObj.size() == 1)
    {
        pobj = m_cview.selObj.front()->getObj();
    }
    else if (m_cview.selObj.size() == 0)
    {
        pobj = m_pEdCircuit;
    }
    openTableWindow(pobj);
}


void CircuitTab::command_diagram()
{
    DiagramWindow *win = new DiagramWindow(*this, m_pTLCircuit);
    subwindows.push_back(win);
    win->updateAll();
    win->show();
}



/** @brief openTableWindow
  *
  * @todo: document this function
  */
void CircuitTab::openTableWindow(CircuitChild *pobj)
{
    if (pobj)
    {
        if (pobj->getOutputs().size() == 0)
        {
            App::message("Keine Wertetabelle", "Für eine Komponenten ohne Ausgänge wird keine Wertetabelle angezeigt.", "", App::getMW());
            return;
        }
        if (pobj->getInputPinCount() > 10)
        {
            App::message("Keine Wertetabelle", "Für Komponenten mit mehr als 10 Pins am Eingang wird keine Wertetabelle angezeigt. Das wäre eine riesige Tabelle!", "", App::getMW());
            return;
        }

        TableWindow *twin = new TableWindow(*this, pobj);
        subwindows.push_back(twin);
        twin->updateAll();
        twin->show();
    }
}

/** @brief on_subwindow_close
  *
  * @todo: document this function
  */
void CircuitTab::on_subwindow_close(Updatable* w)
{
    subwindows.remove(w);
    delete w;
}

/** @brief del
  *
  * @todo: document this function
  */
void CircuitTab::command_delete()
{

    // Am einfachsten: beim löschen alle fenster schließen:
    // aber eig. nur view windows
    //closeSubwindows(Updatable::SW_VIEW);

    type_visual_list::iterator it;
    type_forwardings::iterator ioit;
    bool c = false;

    //CircuitChildVisual* vis;
    CircuitChild *obj;

    // alternativ:: immer erstes nehmen und _liste abbauen_
    /*it = m_cview.selObj.begin();
    if (it != m_cview.selObj.end())
    {
        vis = (*it);
        obj = vis->getObj();
        if (m_cview.getVisual()->m_childVisuals.erase(obj)==0) std::cout << "Konnte nicht gelöscht werden" << std::endl; // Visual aus liste entfernen.
        //vis->m_pObj=NULL;
        m_pCircuit->removeComponent(obj);
        delete vis; // Visual löschen.
        c = true;
    }*/



    for (it = m_cview.selObj.begin(); it != m_cview.selObj.end(); it++)
    {
        obj = (*it)->getObj();


// offene fenster nach referenzen durchsuchen
        list<Updatable *> lu;
        for (list<Updatable *>::iterator _it = subwindows.begin(); _it != subwindows.end(); _it++)
        {
            Updatable *pup = (*_it);
            //if (pup->getWindowType() == Updatable::SW_VIEW)
            {
                CircuitChild *o = pup->getObj();
                do
                {
                    if (obj == o)
                    {
                        lu.push_back(pup);
                        break;
                    }
                    o = o->getParent();
                } while (o != NULL);
            }
            /*else if (pup->getWindowType() == Updatable::SW_TABLE)
            {
                if (obj == pup->getObj())
                {
                    lu.push_back(pup);
                }
            }*/
        }
        // gemerkte fenster schließen
        for (list<Updatable *>::iterator _it = lu.begin(); _it != lu.end(); _it++)
        {
            subwindows.remove((*_it));
            (*_it)->hide();
            delete (*_it);
        }
        // ENDE fenster schließen


        m_cview.getVisual()->m_childVisuals.erase(obj); // Visual aus liste entfernen.
        delete (*it); // Visual löschen.
        m_pEdCircuit->removeComponent(obj);
        c = true;
    }

    for (ioit = m_cview.selIO.begin(); ioit != m_cview.selIO.end(); ioit++)
    {
        if ((*ioit)->getOwner() == m_pEdCircuit)
        {
            m_pEdCircuit->removeIO((*ioit));
            c = true;
        }
    }

    m_pTLCircuit->update();
    m_cview.deselectAll();
    m_cview.on_selection_changed();

    if (c)
    {
        //new GeneralUndoItem("Lö", this, m_undo);
        m_undo.addItem(new GeneralUndoItem("Löschen", this, m_undo));
        changed(true);
    }
    updateAll();
}

/** @brief isNew
  *
  * @todo: document this function
  */
bool CircuitTab::isNew()
{
    return (!changed() && m_pTLCircuit->getFilename().empty());
}


/** @brief rename
  *
  * @todo: document this function
  */
void CircuitTab::command_rename()
{
	Gtk::Dialog dialog(App::t("Umbenennen"), App::getMW(), true, true);
	Gtk::VBox* vb = dialog.get_vbox();
	Gtk::Entry entry;
	Gtk::Label l(App::t("Geben Sie den neuen Namen ein:"));

	CircuitChildVisual *pv = NULL;
	CircuitIO *pio = NULL;

	if      ((m_cview.selObj.size() == 1) && (m_cview.selObj.front()->getParentObj() == m_pEdCircuit)) pv = m_cview.selObj.front();
	else if ((m_cview.selIO.size() == 1)  && (m_cview.selIO.front()->getOwner() == m_pEdCircuit))     pio = m_cview.selIO.front();

	if      (pv) entry.set_text(pv->getObj()->getName());
	else if (pio) entry.set_text(pio->getName());

	vb->pack_start(l, Gtk::PACK_EXPAND_WIDGET);
	vb->pack_start(entry, Gtk::PACK_EXPAND_WIDGET);

	dialog.add_button(Gtk::Stock::APPLY, Gtk::RESPONSE_OK);
	dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	dialog.show_all_children();

	// Enter in Textfeld triggert Dialog-Antwort
	entry.signal_activate().connect(sigc::bind(
		sigc::mem_fun(&dialog, &Gtk::Dialog::response),
	Gtk::RESPONSE_OK));


	dialog.set_default_response(Gtk::RESPONSE_OK);


	// TODO stimmt das?
	bool ret = false;
	int ok;
	while (!ret && ((ok = dialog.run()) == Gtk::RESPONSE_OK))
	{
		if (pv)
		{
			ret = m_pEdCircuit->changeElementName(pv->getObj(), entry.get_text());
		}
		else if (pio)
		{
			ret = m_pEdCircuit->changeIOName(pio, entry.get_text());
		}
		if (!ret) App::error(App::t("Name schon vorhanden"), App::t("Ein Name kann nur einmal vergeben werden. Der eingegebene Name existiert bereits."), "", &App::getMW());
	}
	if (ok == Gtk::RESPONSE_OK)
	{

        m_undo.addItem(new GeneralUndoItem(App::t("Umbenennen"), this, m_undo));
	    changed(true);
	}

	m_cview.on_selection_changed();
	updateAll();
}




/** @brief getSelectedObject
  *
  * @todo: document this function
  */
CircuitChild* CircuitTab::getSelectedObject()
{
	if (m_cview.selObj.size() > 0)
		return (*m_cview.selObj.begin())->getObj();
    else return NULL;
}



/** @brief command_reload
  *
  * @todo: document this function
  */
void CircuitTab::command_reload()
{
    //App::message("Zurücksetzen deaktiviert",".", "Zur Zeit deaktiviert wegen Bauarbeiten.", App::getMW());

	string filename = m_pTLCircuit->getFilename();

	if (!filename.empty())
	{
	    if (!changed() || App::question("Soll die Schaltung neu geladen werden?", "Alle ungesicherten Änderungen werden verloren gehen.", filename))
        {
            clear();
            command_open(filename);
            updateAll();
        }
	}
	else
	{
		App::message("Zurücksetzen nicht möglich","Die Schaltung wurde bisher nicht gespeichert, ein Zurücksetzen ist daher nicht möglich.", "", App::getMW());
	}

}

void CircuitTab::command_undo()
{
    bool done, saved;

    // Container-Vews sichern
    std::stack<std::string> names;
    std::stack<ToplevelCircuitVisual*> visuals(m_visualStack);
    names.push(std::string(m_pVisual->getObj()->getName()));
    while (!visuals.empty())
    {
        names.push(std::string(visuals.top()->getObj()->getName()));
        visuals.pop();
    }
    names.pop();


	done = m_undo.undo(saved);
	if (done) changed(!saved);


    // Container-Vews wiederherstellen
    while (!names.empty())
    {
        EditableCircuit* pc = dynamic_cast<EditableCircuit*>(m_pEdCircuit->findChildByName(names.top()));
        if (pc)
        {
            openContainerView(pc);
        }
        names.pop();
    }


	updateAll();
}

/** @brief command_node
  *
  * @todo: document this function
  */
void CircuitTab::command_node()
{
	type_forwardings::iterator ioit, ioit2;
	CircuitIO *pio, *p1, *p2;
	int x,y;
	//CircuitChild* pe;
	NodeComponent* pe;
	string s;
	CircuitChildVisual *vis1, *vis2;

	for (ioit = m_cview.selIO.begin(); ioit != m_cview.selIO.end(); ioit++)
	{
		pio = (*ioit);
		if (pio)
		{
		    bool inOrMainOut = ((pio->getOwner()!=m_pEdCircuit) && (pio->getType() == IO_TYPE_IN))
                                || ( (pio->getOwner()==m_pEdCircuit) && (pio->getType() == IO_TYPE_OUT));

		    // Prüfen ob überhaupt verbindungen da sind
		    if (inOrMainOut)
		    {
                if (pio->getFrom()==NULL) return;
		    }
            else
            {
                if (pio->getForwardings().size() == 0) return;
            }

            // Node-Komponente erzeugen
            pe = (NodeComponent*)m_pEdCircuit->addComponent("node","");
            pe->setPinCount(pio->getPinCount());

            // Eingang bzw Main ausgang hat nur eine leitung
            if (inOrMainOut)
            {
                p2 = pio;
                p1 = p2->getFrom();
                if (p1)
                {
                    if (p1->getOwner() == m_pEdCircuit) vis1 = m_pVisual;
                    else vis1 = m_pVisual->m_childVisuals[p1->getOwner()];
                    if (p2->getOwner() == m_pEdCircuit) vis2 = m_pVisual;
                    else vis2 = m_pVisual->m_childVisuals[p2->getOwner()];

                    x = vis1->getAbsPosX() + vis2->getAbsPosX();
                    y = vis1->getAbsPosY() + vis2->getAbsPosY();
                    if (p1->getType() == IO_TYPE_OUT) x += vis1->getSizeW();
                    if (p2->getType() == IO_TYPE_OUT) x += vis2->getSizeW();
                    x += p1->ps_getFromPosX() + p2->ps_getToPosX();
                    y += p1->ps_getFromPosY() + p2->ps_getToPosY();

                    p1->removeForwarding(p2);
                    p1->addForwarding(pe->getInput("i"));
                    pe->getOutput("o")->addForwarding(p2);
                }
            }
            else // main eingang und andere ausgänge haben mehrere leitungen
            {
                type_forwardings& cop = pio->getForwardings();
                type_forwardings a(cop);

                p1 = pio;

                if (p1->getOwner() == m_pEdCircuit) vis1 = m_pVisual;
                else vis1 = m_pVisual->m_childVisuals[p1->getOwner()];

                // die punkte am ziel werden gemittelt (um mit dem startpunkt die mitte zu finden für die position des knotens)
                x = y = 0;
                for (ioit2 = cop.begin(); ioit2 != cop.end(); ioit2++)
                {
                    p2 = (*ioit2);
                    if (p2->getOwner() == m_pEdCircuit) vis2 = m_pVisual;
                    else vis2 = m_pVisual->m_childVisuals[p2->getOwner()];
                    x += p2->ps_getToPosX();
                    y += p2->ps_getToPosY();
                    x += vis2->getAbsPosX();
                    y += vis2->getAbsPosY();
                    if (p2->getType() == IO_TYPE_OUT) x += vis2->getSizeW();
                }
                x /= cop.size();
                y /= cop.size();

                x += p1->ps_getFromPosX();
                y += p1->ps_getFromPosY();

                x += vis1->getAbsPosX();
                y += vis1->getAbsPosY();
                if (p1->getType() == IO_TYPE_OUT) x += vis1->getSizeW();

                p1->disconnect(true, false, false);

                s = "i";
                p1->addForwarding(pe->getInput(s));

                for (ioit2 = a.begin(); ioit2 != a.end(); ioit2++)
                {
                    p2 = (*ioit2);
                    s="o";
                    pe->getOutput(s)->addForwarding(p2);
                }

            }

            bool _dummy;
            CircuitChildVisual *pv = m_cview.getVisual()->adjoinVisual(pe, _dummy);
            updateAll();
            x -= pv->getSizeW();
            y -= pv->getSizeH();
            x /= 2;
            y /= 2;

            //m_pVisual->parentToComponent(x,y,x,y);
            m_pVisual->componentToArea(CA_INNER, x,y,x,y);

            pv->setPos(x, y);

            //###if (p1) p1->list_forward();
            m_pTLCircuit->update();

		}
	}

	m_undo.addItem(new GeneralUndoItem("Ecke einfügen", this, m_undo));
	changed(true);

	m_cview.deselectAll();
	m_cview.on_selection_changed();
	updateAll();
}

/** @brief command_properties
  *
  * @todo: document this function
  */
void CircuitTab::command_properties()
{
	CircuitChild *pobj = NULL;

    if (m_cview.selIO.size() == 1)
    {
        //
        Gtk::Dialog dialog(App::t("Anzahl Pins"), App::getMW(), true, true);
        Gtk::VBox* vb = dialog.get_vbox();
        Gtk::Entry entry;
        Gtk::Label l(App::t("Anzahl Pins für diesen Anschluss:"));

        vb->pack_start(l, Gtk::PACK_EXPAND_WIDGET);
        vb->pack_start(entry, Gtk::PACK_EXPAND_WIDGET);

        dialog.add_button(Gtk::Stock::APPLY, Gtk::RESPONSE_OK);
        dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
        dialog.show_all_children();

        // Enter in Textfeld triggert Dialog-Antwort
        entry.signal_activate().connect(sigc::bind(
            sigc::mem_fun(&dialog, &Gtk::Dialog::response),
            Gtk::RESPONSE_OK));

        ostringstream os;
        os << m_cview.selIO.front()->getPinCount();
        entry.set_text(os.str());

        dialog.set_default_response(Gtk::RESPONSE_OK);

        if (dialog.run() == Gtk::RESPONSE_OK)
        {
            istringstream is(entry.get_text());
            unsigned short pc;
            is >> pc;
            if ((pc<1) || (pc>16)) pc = 1;
            m_cview.selIO.front()->setPinCount(pc);

            m_undo.addItem(new GeneralUndoItem("Einstellungen ändern", this, m_undo));
            changed(true);
            updateAll();
        }
    } // IO properties
    else
    {
        if (m_cview.selObj.size() == 1)
        {
            pobj = (*m_cview.selObj.begin())->getObj();
        }
        else if (m_cview.selObj.size() == 0)
        {
            pobj = m_pEdCircuit;
        }
        else
        {
            App::message("Noch nicht möglich","Eigenschaften können derzeit nicht von mehreren Komponenten gleichzeitig bearbeitet werden.","", App::getMW());
        }

        if (pobj)
        {
            PropertyDialog2 d(pobj->getName(), App::getMW(), pobj->getProperties());

            provideUiText(pobj, d);

            if (d.run() == Gtk::RESPONSE_OK)
            {
                try
                {
                    //if (d.hasChanged())
                    {
                        //cout << "hasChanged, writeback!" << endl;
                        d.writeback();

                        m_undo.addItem(new GeneralUndoItem("Einstellungen ändern", this, m_undo));
                        changed(true);

                        pobj->on_propertyChanged();
                        if (pobj == m_pTLCircuit)
                        {
                            Complex2* c = pobj->getProperties().get_complex("plan");

                            //std::cout << ((void*)c) << std::endl;
                            //c->dump();

                            //std::cout << "::: '" << c->get_string("file") << "'" << std::endl;

                            m_cview.setPlan(
                                c->get_string("file"),
                                c->get_int("x"), c->get_int("y"),
                                c->get_double("scale"),
                                c->get_double("alpha"),
                                c->get_bool("over"),
                                c->get_bool("show")
                            );
                        }

                        updateAll();
                    }
                }
                catch (std::string& s)
                {
                    std::cerr << s << std::endl;
                    std::cout << s << std::endl;
                }
            }
        }
    } // selIO.size == 1 else
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void CircuitTab::provideUiText(CircuitChild* obj, PropertyDialog2& d)
{
    obj->provideUiText(d.getUiTextMap());
}



/** @brief command_disconnect
  *
  * @todo: document this function
  */
void CircuitTab::command_disconnect(DISCONNECT_ACTION d)
{
	type_visual_list::iterator it;
	type_forwardings::iterator ioit;

	if ((d == DA_ALL) || (d == DA_IN) || (d == DA_OUT))
	{
	    bool in = ((d == DA_ALL) || (d == DA_IN));
	    bool out = ((d == DA_ALL) || (d == DA_OUT));
        for (it = m_cview.selObj.begin(); it != m_cview.selObj.end(); it++)
        {
            (*it)->getObj()->disconnect(in, out);
        }
	}
	else if (d == DA_SELECTED)
	{
        for (ioit = m_cview.selIO.begin(); ioit != m_cview.selIO.end(); ioit++)
        {
            // disconnect(to, from)
            if ((*ioit)->getOwner() == m_pEdCircuit) // wenn io der Editierten schaltung
            {
                (*ioit)->disconnect((*ioit)->getType() == IO_TYPE_IN, (*ioit)->getType() == IO_TYPE_OUT, true);
            }
            else
            {
                (*ioit)->disconnect((*ioit)->getType() == IO_TYPE_OUT, (*ioit)->getType() == IO_TYPE_IN, true);
            }
        }
	}

	m_cview.deselectAll();
	m_cview.on_selection_changed();
	m_pTLCircuit->update();

	m_undo.addItem(new GeneralUndoItem("Verbindungen Trennen", this, m_undo));
	changed(true);

	updateAll();
}



/** @brief clear
  *
  * @todo: document this function
  */
void CircuitTab::clear()
{
    closeContainerView(true);

    stopClock();
    closeSubwindows();
    m_cview.clear();
    // TODO
    m_pTLCircuit->clear();
    init();
    m_cview.setVisual(m_pVisual);
    m_cview.on_selection_changed();
    updateAll();
}


/** @brief command_order
  *
  * @todo: document this function
  */
void CircuitTab::command_order(ORDER_ACTION o)
{
	type_visual_list::reverse_iterator it;

    bool c = false;
	bool b = false;

	std::string undotext;

	for (it = m_cview.selObj.rbegin(); it != m_cview.selObj.rend(); it++)
	{
		CircuitChild* obj = (*it)->getObj();
		if (!obj) continue;

		switch (o)
		{
		    case OA_UP:
                b = m_pEdCircuit->up(obj);
                undotext = "Komponente nach unten";
		    break;
		    case OA_DOWN:
                b = m_pEdCircuit->down(obj);
                undotext = "Komponente nach oben";
		    break;
		    case OA_TOPMOST:
                b = m_pEdCircuit->topmost(obj);
                undotext = "Komponente ganz nach oben";
		    break;
		    case OA_BOTTOMMOST:
                b = m_pEdCircuit->bottommost(obj);
                undotext = "Komponente ganz nach unten";
		    break;
		}


		if(b)
		{
		    c = true;
		}
	}

    if (c)
    {
        m_undo.addItem(new GeneralUndoItem(undotext, this, m_undo));
        changed(true);
    }

	App::fetchError("Beim Ändern der Anordnung", App::getMW());
	updateAll();
}



// http://developer.gnome.org/gdk/stable/gdk-Keyboard-Handling.html
// <gdk/gdkkeysyms.h>
bool CircuitTab::on_key_press_event(GdkEventKey* event)
{

    if ((event->hardware_keycode >= 10) && (event->hardware_keycode <= 19))
    {
        if (event->hardware_keycode == 19) inputNumberModifier = 0;
        else inputNumberModifier = event->hardware_keycode - 9;
    }


    CircuitInput *in = getInputFromKey(event->keyval);
    if (in)
    {
        bool shift = (event->state & Gdk::SHIFT_MASK);
        if (shift)
        {
            in->setValue(!in->getValue());
            m_pEdCircuit->update(); // TODO ? ist das richtig TL?
            updateAll();
        }
        else
        {
            if (in->getValue() != 1)
            {
                in->setValue(1);
                m_pEdCircuit->update(); // TODO ? ist das richtig TL?
                updateAll();
            }
        }
        return true;
    }

    return false;
}

bool CircuitTab::on_key_release_event(GdkEventKey* event)
{

    bool shift = (event->state & Gdk::SHIFT_MASK);

    CircuitInput *in = getInputFromKey(event->keyval);

    if (in)
    {
        if (!shift)
        {
            if (in->getValue()!=0)
            {
                in->setValue(0);
                m_pEdCircuit->update(); // TODO ? ist das richtig TL?
                updateAll();
            }
            return true;
        }
    }

    if ((event->hardware_keycode >= 10) && (event->hardware_keycode <= 19))
    {
        inputNumberModifier = -1;
    }


    return false;
}


CircuitInput* CircuitTab::getInputFromKey(guint keyval)
{

    if ((keyval >= 65) && (keyval <= 122))
    {
        char _ch, ch = (char)gdk_keyval_to_lower(keyval);
        CircuitInput *_in=NULL;
        type_inputs_iterator iit;
        for (iit = m_pEdCircuit->getInputs().begin(); iit != m_pEdCircuit->getInputs().end(); iit++ )
        {
            _in = (*iit);
            _ch = _in->getName().at(0);

            if ((_ch == ch)|(_ch == ch-32))
            {
                if (inputNumberModifier >= 0)
                {
                    if ((_in->getName().length()>1) && (_in->getName().at(1) == (inputNumberModifier+48)))
                    {
                        return _in;
                    }
                }
                else
                {
                    return _in;
                }
            }
        }
    }

    return NULL;
}

void CircuitTab::on_circuit_notification(int noti, const std::string& msg, gpointer data)
{
    if (noti == CircuitChild::CN_TIMEOUT_ERROR)
    {
        stopClock();
        ostringstream os;
        os << "<b>" << msg << " (" << CircuitChild::timeoutDelta << ")</b>\nDie Schaltung nimmt keinen <a href=\"piiri:help:circ\">stabilen Zustand</a> an.";
        App::getMW().open_infobar(Gtk::MESSAGE_WARNING, os.str());
    }
    else if (CircuitChild::CN_UPDATE)
    {
        updateAll();
    }
}




















/** @brief getCurrentUndoAction
  *
  * @todo: document this function
  */
std::string CircuitTab::getCurrentUndoAction()
{
    return m_undo.getCurrentAction();
    //return "";
}





/** @brief activate
  *
  * @todo: document this function
  */
void CircuitTab::tabActivate()
{
}





void CircuitTab::on_action_MoveActivate()
{
    bool ma = m_MoveActive.get_active();

    if (ma) m_MoveActive.set_icon_widget(m_MoveImage);
    else m_MoveActive.set_icon_widget(m_NoMoveImage);

    command_edit_active(ma);
}
