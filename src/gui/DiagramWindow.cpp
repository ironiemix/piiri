/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "DiagramWindow.h"
#include <iostream>
#include <fstream>
#include <time.h>



#define DIAGRAM_LINE_COLOR 0.2, 0.2, 0.2
#define DIAGRAM_BACKGROUND_COLOR 0.1, 0.1, 0.1
#define DIAGRAM_INPUTS_COLOR 0.5, 0.9, 0.5
#define DIAGRAM_OUTPUTS_COLOR 0.9, 0.5, 0.9

/**
 * class DiagramWindow
 *
 *
 */
DiagramWindow::DiagramWindow(CircuitTab& tab, CircuitContainer* c) :
 Updatable(tab, c, Updatable::SW_VIEW),
 m_Rec(false),
 m_VBox(false, 3),
 m_HBox(false, 3)
{
	// Fenster einrichten
	set_title(App::name + " | Impulsdiagramm");
	set_default_size(600, 300);

	// signale fürs Markieren (Maus).
	m_Area.signal_button_release_event().connect(sigc::mem_fun(*this, &DiagramWindow::on_popup_menu));
	m_Area.signal_button_press_event().connect(sigc::mem_fun(*this, &DiagramWindow::on_doubleclick));

	signal_key_release_event().connect(sigc::mem_fun(*this, &DiagramWindow::on_my_key_release_event));

	// Action-Gruppe für alle Aktionen (Toolbar / Menü) des Fensters
	refActionGroup = Gtk::ActionGroup::create();

	// Zoom-Aktionen hat jedes Fenster
	/*CREATE_ACTION2_KEY(DiagramWindow,ZoomIn,Gtk::Stock::ZOOM_IN,"","Anzeige vergrößern", Gtk::AccelKey('+',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(DiagramWindow,ZoomOut,Gtk::Stock::ZOOM_OUT,"","Anzeige verkleinern", Gtk::AccelKey('-',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(DiagramWindow,Zoom100,Gtk::Stock::ZOOM_100,"","Normale Anzeige", Gtk::AccelKey('#',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(DiagramWindow,Refresh,Gtk::Stock::REFRESH,"","Schaltung aktualisieren", Gtk::AccelKey("F5"))*/

	CREATE_ACTION2_KEY(DiagramWindow, Reset, Gtk::Stock::CLEAR, "Zurücksetzen","Zurücksetzen", Gtk::AccelKey("F6"))
	CREATE_ACTION2(DiagramWindow, Rec, Gtk::Stock::MEDIA_RECORD, "Aufzeichnung starten","Aufzeichnung starten")
	CREATE_ACTION2(DiagramWindow, Stop, Gtk::Stock::MEDIA_STOP, "Aufzeichnung stoppen","Aufzeichnung stoppen")
	CREATE_ACTION2(DiagramWindow, Save, Gtk::Stock::SAVE_AS, "Signaldaten abspeichern","Signaldaten abspeichern")



	// UI-Manager erzeugen und Gruppe hinzufügen.
	refUIManager = Gtk::UIManager::create();
	refUIManager->insert_action_group(refActionGroup);
	add_accel_group(refUIManager->get_accel_group());

    //std_context = m_Statusbar.get_context_id(App::name);


    //------------------
	// Toolbar und Popup-Menü definieren.
	Glib::ustring ui_info =
		"<ui>"
		"  <toolbar  name='ToolBar'>"
		"    <toolitem action='Reset'/>"
		"    <toolitem action='Rec'/>"
		"    <toolitem action='Stop'/>"
		"    <separator/>"
		"    <toolitem action='Save'/>"
		"  </toolbar>"
		"  <popup name='PopupMenu'>"
		"  </popup>"
		"</ui>";

	refUIManager->add_ui_from_string(ui_info);

	// Toolbar anlegen
	Gtk::Widget* pToolBar = refUIManager->get_widget("/ToolBar");
	((Gtk::Toolbar*)pToolBar)->set_toolbar_style(Gtk::TOOLBAR_ICONS);

	add(m_VBox);

	m_VBox_Toolbar.pack_start(*pToolBar, Gtk::PACK_EXPAND_WIDGET);
	m_VBox.pack_start(m_VBox_Toolbar, Gtk::PACK_SHRINK);
	m_VBox.pack_start(m_HBox, Gtk::PACK_EXPAND_WIDGET);
	m_VBox.pack_start(m_Statusbar, Gtk::PACK_SHRINK);

	m_ScrolledWindowCanvas.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	m_ScrolledWindowCanvas.set_border_width(3);
	m_ScrolledWindowCanvas.add(m_Area);


	m_Area.signal_expose_event().connect(sigc::mem_fun(*this, &DiagramWindow::on_area_expose_event));

	m_HBox.pack_start(m_ScrolledWindowCanvas);




    m_Statusbar.push("PiiRi Impulsdiagramm");

    m_ICConnection = m_pObj->signal_input_changed().connect(sigc::mem_fun(*this, &DiagramWindow::on_in_ch));
    m_OCConnection = m_pObj->signal_output_changed().connect(sigc::mem_fun(*this, &DiagramWindow::on_out_ch));



	show_all_children();


    on_action_Stop();
}



DiagramWindow::~DiagramWindow()
{
    m_ICConnection.disconnect();
    m_OCConnection.disconnect();
    clear_lines();
}

/** @brief clear_lines
  *
  * @todo: document this function
  */
void DiagramWindow::clear_lines()
{
    type_line_list::iterator it;
    for (it = m_LineList.begin(); it != m_LineList.end(); it++)
    {
        delete (*it);
    }
    m_LineList.clear();
}
/** @brief reset_lines
  *
  * @todo: document this function
  */
void DiagramWindow::reset_lines()
{
    clear_lines();
    init_lines();
}

/** @brief init_lines
  *
  * @todo: document this function
  */
void DiagramWindow::init_lines()
{
    //m_LastTime = g_get_monotonic_time();

    m_timer.start();

    ostringstream os;

    // erste Werte holen
    type_inputs_iterator iit;
    type_outputs_iterator oit;
    for (iit = m_pObj->getInputs().begin(); iit != m_pObj->getInputs().end(); iit++)
    {
        for (int i = 0; i < (*iit)->getPinCount(); i++)
        {
            os.str("");
            os << (*iit)->getName();
            if ((*iit)->getPinCount() > 1) os << " " << i;
            add_point(os.str(), (*iit)->getValue() & (1<<i), IO_TYPE_IN);
        }
    }

    for (oit = m_pObj->getOutputs().begin(); oit != m_pObj->getOutputs().end(); oit++)
    {
        for (int i = 0; i < (*oit)->getPinCount(); i++)
        {
            os.str("");
            os << (*oit)->getName();
            if ((*oit)->getPinCount() > 1) os << " " << i;
            add_point(os.str(), (*oit)->getValue() & (1<<i), IO_TYPE_OUT);
        }
    }

    m_Area.set_size_request(-1, m_LineList.size()*30+30);
}




/** DiagramWindow Members */


/** @brief on_area_expose_event
  *
  * @todo: document this function
  */
bool DiagramWindow::on_area_expose_event(GdkEventExpose* event)
{
	if(m_Area.is_realized())
	{
	    Glib::RefPtr<Gdk::Window> window = m_Area.get_window();

		Gtk::Allocation allocation = m_Area.get_allocation();
		const int width = allocation.get_width();
		const int height = allocation.get_height();

		Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();

		draw_me(cr, width, height);

	} // realized
	return true;
}


/** @brief draw_me
  *
  * @todo: document this function
  */
void DiagramWindow::draw_me(Cairo::RefPtr<Cairo::Context> cr, int width, int height)
{
    Cairo::TextExtents extents;

    cr->rectangle(0, 0, width, height);
    cr->set_source_rgb(DIAGRAM_BACKGROUND_COLOR);
    cr->fill();




    if (!m_LineList.empty())
    {
        type_line_list::iterator it;
        DiagramLine::iterator dl_it;
        double x=0,y=20;
        double h = 20, pad=10;
        double x_scale = 1;



        int l_rand = 20, r_rand = 10;
        // TODO l_rand auf max der textbreite berechnen

        cr->set_font_size(12);
        long whole_interval = 0;
        for (it = m_LineList.begin(); it != m_LineList.end(); it++)
        {
            DiagramLine* p_dl = (*it);
            cr->get_text_extents(p_dl->name, extents);
            if (extents.width > l_rand) l_rand = extents.width;
            if (!p_dl->empty())
            {
                long interval = p_dl->back()->time;
                if (interval > whole_interval) whole_interval = interval;
            }
        }
        l_rand += 10;

        if (whole_interval < 5000) whole_interval = 5000;


        x_scale = (double)(width-(l_rand+r_rand)) / (double)whole_interval;

        cr->set_line_width(3);

        for (it = m_LineList.begin(); it != m_LineList.end(); it++)
        {


            y += h;
            x = l_rand;

            // hintergrund einer linie
            cr->rectangle(l_rand-3, y+(pad/3), width-(l_rand+r_rand)+6, -(h+2*(pad/3)));
            cr->set_source_rgb(DIAGRAM_LINE_COLOR);
            cr->fill();


            cr->move_to(x,y);

            DiagramLine* p_dl = (*it);


            if (p_dl->type == IO_TYPE_IN) cr->set_source_rgb(DIAGRAM_INPUTS_COLOR);
            else                          cr->set_source_rgb(DIAGRAM_OUTPUTS_COLOR);

            cr->set_font_size(12);
            cr->get_text_extents(p_dl->name, extents);
            cr->rel_move_to(-extents.width-5, 0);
            cr->show_text(p_dl->name);

            cr->move_to(x,y);

            bool lv = false;
            for (dl_it = p_dl->begin(); dl_it != p_dl->end(); dl_it++)
            {
                DiagramPoint* p_dp = (*dl_it);

                x = l_rand + ((double)p_dp->time)*x_scale;
                cr->line_to(x, y - (lv ? h : 0)); // alten wert ausfahren in richtiger länge
                lv = p_dp->value;
                cr->line_to(x, y - (lv ? h : 0)); // auf neues niveau gehen
            }
            x = l_rand + ((double)whole_interval)*x_scale;
            cr->line_to(x, y - (lv ? h : 0)); // auf aktuellem niveau bis zum ende zeichnen

            cr->stroke();

            y += pad;
        } // for lines
    }
    else
    {
        cr->set_source_rgb(0.6, 0.6, 0.6);
        cr->set_font_size(24);
        std::string a("Klicken Sie Aufzeichnung starten,");
        std::string b("um Signale aufzuzeichnen.");
        cr->get_text_extents(a, extents);
        cr->move_to((width-extents.width)/2, height/2+extents.y_bearing);
        cr->show_text(a);
        cr->get_text_extents(b, extents);
        cr->move_to((width-extents.width)/2, height/2-extents.y_bearing);
        cr->show_text(b);
    }

}




/**
 * Allgemeines anzeigen des Kontextmenüs.
 */
bool DiagramWindow::on_popup_menu(GdkEventButton* event)
{
	if(event->button == 3)
	{
	    contextMenu(event->button);
	    return true;
	}
	return false; // TODO --> richtig?
}

/** @brief contextMenu
  *
  * @todo: document this function
  */
void DiagramWindow::contextMenu(int button)
{
    Gtk::Menu* pMenuPopup;
    pMenuPopup = (Gtk::Menu*)refUIManager->get_widget("/PopupMenu");
    pMenuPopup->popup(button, gtk_get_current_event_time());
}



/**
 * Doppelklick auf Schaltung wird hier her geführt: zum Öffnen einer Schaltung.
 */
bool DiagramWindow::on_doubleclick(GdkEventButton* event)
{
	if (event->button == 1)
	{
		bool doubleClick = false;
		if (((Gdk::EventType)event->type) == Gdk::DOUBLE_BUTTON_PRESS ) doubleClick = true;
		if (doubleClick)
		{
            // aktion
		}
		return true;
	}
	return false; // TODO--> richtig?
}


/**
 * Action-Handler für Zoom-In.
 */
void DiagramWindow::on_action_ZoomIn()
{
}

/**
 * Action-Handler für Zoom-100%.
 */
void DiagramWindow::on_action_Zoom100()
{
}

/**
 * Action-Handler für Zoom-Out.
 */
void DiagramWindow::on_action_ZoomOut()
{
}

/**
 * Action-Handler für Aktualisieren.
 */
void DiagramWindow::on_action_Refresh()
{
}

/**
 * Action-Handler für Reset.
 */
void DiagramWindow::on_action_Reset()
{

    reset_lines();
}


/**
 * Action-Handler für Rec.
 */
void DiagramWindow::on_action_Rec()
{
    m_Rec = true;
    reset_lines();
    DISABLE_ACTION("Rec")
    ENABLE_ACTION("Stop")
    ENABLE_ACTION("Reset")
    DISABLE_ACTION("Save")
}

/**
 * Action-Handler für Stop.
 */
void DiagramWindow::on_action_Stop()
{
    m_Rec = false;
    DISABLE_ACTION("Stop")
    ENABLE_ACTION("Rec")
    DISABLE_ACTION("Reset")
    ENABLE_ACTION("Save")
}



/**
 * Action-Handler für Stop.
 */
void DiagramWindow::on_action_Save()
{
	std::string filename;

	if (!m_LineList.empty())
	{
		Gtk::FileChooserDialog dialog(*this, "Signale speichern...", Gtk::FILE_CHOOSER_ACTION_SAVE);

		dialog.set_current_folder(App::home_piiri_path);
		dialog.set_do_overwrite_confirmation(false);
		//dialog.set_create_folders(false);

		//Add response buttons the the dialog:
		dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
		dialog.add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);

		//Add filters, so that only certain file types can be selected:
//		Gtk::FileFilter filter_cir;
//		filter_cir.set_name("Schaltungen (*.cir)");
//		filter_cir.add_pattern("*.cir");
//		dialog.add_filter(filter_cir);

		//Show the dialog and wait for a user response:
        int result;
        do
        {
            result = dialog.run();
            if (result == Gtk::RESPONSE_OK)
            {
                filename = dialog.get_filename();
                /*if (!g_str_has_suffix(filename.c_str(),".cir"))
                {
                    filename += ".cir";
                }*/
                if (g_file_test(filename.c_str(), G_FILE_TEST_EXISTS))
                {
                    if (App::question("Datei existiert bereits", "Die unten genannte Datei existiert bereits.\nSoll Sie überschrieben werden?", filename, dialog))
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            else break;
        } while (true);


		if (result != Gtk::RESPONSE_OK)
		{
			filename = "";
		}
	}
	else
	{
	    App::message("Signale speichern", "Keine Signale zum Speichern vorhanden.", "Klicken Sie 'Aufzeichnung starten', um Signale aufzuzeichnen.", *this);
	}


	if (!filename.empty())
	{
	    if (g_str_has_suffix(filename.c_str(),".png"))
        {
            int w,h;
            Glib::RefPtr<Gdk::Window> window = m_Area.get_window();
            window->get_size(w, h);

            //Cairo::RefPtr<Cairo::Surface> sf = window->create_similar_surface(Cairo::CONTENT_COLOR, w, h);
            //Cairo::RefPtr<Cairo::Surface> sf = Cairo::Surface::create(, Cairo::CONTENT_COLOR, w, h);
            Cairo::RefPtr<Cairo::ImageSurface> sf = Cairo::ImageSurface::create(Cairo::FORMAT_RGB24, w, h);
            Cairo::RefPtr<Cairo::Context> con = Cairo::Context::create(sf);
            draw_me(con, w,h);
            sf->write_to_png(filename);

            // funktioniert alles nicht gescheit

            //Glib::RefPtr<Gdk::Pixmap> pm = m_Area.get_snapshot(clip);
            //Glib::RefPtr<Gdk::Pixbuf> pb = Gdk::Pixbuf::create((Glib::RefPtr< Gdk::Drawable >)pm, 0,0, w, h);

            //Glib::RefPtr<Gdk::Pixbuf> pb = Gdk::Pixbuf::create((Glib::RefPtr< Gdk::Drawable >)window, 0,0, w, h);


            //Glib::RefPtr<Gdk::Pixmap> pm = window->get_offscreen_pixmap();
            //Glib::RefPtr<Gdk::Pixbuf> pb = Gdk::Pixbuf::create((Glib::RefPtr< Gdk::Drawable >)pm, 0,0, w, h);

            //Cairo::RefPtr<Cairo::Context> dcon = window->create_cairo_context();
        }
        else if (g_str_has_suffix(filename.c_str(),".csv"))
        { // cvs datei
            /**
            zeit, x0, x1, x2, y0, y1
            3345, 0,  0, 0, 0
            */
            ofstream os(filename.c_str());
            if (os.is_open())
            {
                type_line_list::iterator it;
                DiagramLine::iterator dl_it;

                /*long whole_interval = 0;
                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);
                    if (!p_dl->empty())
                    {
                        long interval = p_dl->back()->time;
                        if (interval > whole_interval) whole_interval = interval;
                    }
                }*/

                //       time           -1 0 1 (-1 = nicht geändert)
                std::map<int, std::list<int> > zeilen;

                os << "Zeit, ";
                // IO Namen ausgeben (kopfzeile)
                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);
                    os << ((p_dl->type==IO_TYPE_IN)?"IN":"OUT") << " " << p_dl->name << ", ";
                } // for lines
                os << std::endl;

                // Datenstruktur ändern
                //
                int c = 0;
                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);

                    // liste mit so vielen -1en wie es io gibt
                    std::list<int> l(m_LineList.size(), -1);
                    std::list<int>::iterator _i = l.begin();
                    for (int i=0; i<c; i++) _i++;

                    for (dl_it = p_dl->begin(); dl_it != p_dl->end(); dl_it++)
                    {
                        DiagramPoint* p_dp = (*dl_it);

                        (*_i) = (p_dp->value ?  1 : 0 );

                        zeilen[p_dp->time] = l;
                    }

                    c++;
                }

                // schreiben
                std::list<int>::iterator _i, _i2;
                std::map<int, std::list<int> >::iterator zit;
                std::map<int, std::list<int> >::iterator zit_old;
                for (zit = zeilen.begin(); zit != zeilen.end(); zit++)
                {
                    os << (*zit).first << ", "; // zeit

                    if (zit != zeilen.begin()) // nicht wenn erste zeile (muss speziell behandelt werden)
                    {
                        for (_i  = (*zit).second.begin(),
                             _i2 = (*zit_old).second.begin();
                             _i != (*zit).second.end();
                             _i++, _i2++)
                        {
                            if ((*_i) == -1)
                            {
                                (*_i) = (*_i2);
                            }
                        }
                    }
                    else
                    {
                        for (_i  = (*zit).second.begin();
                             _i != (*zit).second.end();
                             _i++)
                        {
                            if ((*_i) == -1)
                            {
                                (*_i) = 0;
                            }
                        }
                    }

                    for (_i = (*zit).second.begin(); _i != (*zit).second.end(); _i++)
                    {
                        os << (*_i) << ", ";
                    }
                    os << std::endl;
                    zit_old = zit;
                }


/*
                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);
                    os << ((p_dl->type==IO_TYPE_IN)?"IN":"OUT") << " " << p_dl->name << std::endl;
                    for (dl_it = p_dl->begin(); dl_it != p_dl->end(); dl_it++)
                    {
                        DiagramPoint* p_dp = (*dl_it);
                        os << p_dp->time << " " << (p_dp->value?"1":"0") << std::endl;
                    }
                } // for lines
*/
                os.close();
            }
        }
        else
        { // text datei
            ofstream os(filename.c_str());
            if (os.is_open())
            {
                type_line_list::iterator it;
                DiagramLine::iterator dl_it;

                long whole_interval = 0;
                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);
                    if (!p_dl->empty())
                    {
                        long interval = p_dl->back()->time;
                        if (interval > whole_interval) whole_interval = interval;
                    }
                }

                for (it = m_LineList.begin(); it != m_LineList.end(); it++)
                {
                    DiagramLine* p_dl = (*it);
                    os << ((p_dl->type==IO_TYPE_IN)?"IN":"OUT") << " " << p_dl->name << std::endl;
                    for (dl_it = p_dl->begin(); dl_it != p_dl->end(); dl_it++)
                    {
                        DiagramPoint* p_dp = (*dl_it);
                        os << p_dp->time << " " << (p_dp->value?"1":"0") << std::endl;
                    }
                } // for lines

                os.close();
            }
        }
	}
}


/**
 * Test callback.
 */
void DiagramWindow::on_action_all()
{
	cout << "Unhandled Aktion!" << endl;
}

/**
 * Cir-Widget und alle Unterfenster aktualisieren.
 */
void DiagramWindow::updateAll()
{
}


/**
 */
bool DiagramWindow::on_my_key_release_event(GdkEventKey* event)
{
	switch (event->keyval)
	{
		case 65383: // context menu
            contextMenu(0);
            return true;
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;

		default:
			break;
	}

	return false;
}

/** @brief on_out_ch
  *
  * @todo: document this function
  */
void DiagramWindow::on_out_ch(const std::string name, bool value)
{
    CircuitOutput* out;
    out = m_pObj->getOutput(name);
    if (out == NULL) return;

    ostringstream os;
    for (int i = 0; i < out->getPinCount(); i++)
    {
        os.str("");
        os << out->getName();
        if (out->getPinCount() > 1) os << " " << i;
        add_point(os.str(), out->getValue() & (1<<i), IO_TYPE_OUT);
    }
//    add_point(name, value, IO_TYPE_OUT);
}

/** @brief on_in_ch
  *
  * @todo: document this function
  */
void DiagramWindow::on_in_ch(const std::string name, bool value)
{
    CircuitInput* in;
    in = m_pObj->getInput(name);
    if (in == NULL) return;

    ostringstream os;
    for (int i = 0; i < in->getPinCount(); i++)
    {
        os.str("");
        os << in->getName();
        if (in->getPinCount() > 1) os << " " << i;
        add_point(os.str(), in->getValue() & (1<<i), IO_TYPE_IN);
    }
    //add_point(name, value, IO_TYPE_IN);
}


/** @brief add_point
  *
  * @todo: document this function
  */
void DiagramWindow::add_point(const std::string name, bool value, IO_TYPE type)
{
    if (!m_Rec) return;

    DiagramPoint* dp = new DiagramPoint;
    dp->time = getImpulseTime();
    dp->value = value;

    DiagramLine* dl = NULL;
    type_line_list::iterator it;
    for (it = m_LineList.begin(); it != m_LineList.end(); it++)
    {
        if (((*it)->type == type) && ((*it)->name == name))
        {
            dl = (*it);
            break;
        }
    }

    if (!dl)
    {
        dl = new DiagramLine(name, type);
        m_LineList.push_back(dl);
    }

    if (dl)
    {
        dl->push_back(dp);
        //std::cout << dp->time << "\t" << ((type==IO_TYPE_IN)?"IN":"OUT") << "  " << m_pObj->getName() << "." << name << "\t= " << value << std::endl;
    }
    else delete dp;

    m_Area.queue_draw();
}







/** @brief getImpulseTime
  *
  * @todo: document this function
  */
long DiagramWindow::getImpulseTime()
{
    unsigned long mis;
    double s = m_timer.elapsed(mis);
    //std::cout << s << " " << mis << std::endl;
    mis = (unsigned long)s*1000000 + mis;
    return mis;
}

