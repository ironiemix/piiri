#include "gui/NewProject.h"
#include "model/App.h"

#include <sigc++/sigc++.h>

NewProject::NewProject() :
    Gtk::Dialog(App::t("Neues Projekt erstellen"), App::getMW(), true, true),
    l("", 0.5, 0.5),
    lname(App::t("Projekt-Name")+":", 1.0, 0.0),
    lfile(App::t("Projekt-Datei")+":", 1.0, 0.0),
    lfolder(App::t("Projekt-Ordner")+":", 1.0, 0.0),
    ldescr(App::t("Projekt-Beschreibung")+":", 1.0, 0.0),
    table(5, 2)
{
    set_default_size(600, -1);
    add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
	set_border_width(5);

	Gtk::VBox* vb = get_vbox();

	l.set_use_markup(true);
	l.set_markup("<big>"+App::t("Neues Projekt erstellen")+"</big>\n");


	//filebox.pack_start(efile, Gtk::PACK_EXPAND_WIDGET);
	//filebox.pack_start(bfile, Gtk::PACK_SHRINK);

	table.set_spacings(5);

	vb->pack_start(l, Gtk::PACK_EXPAND_WIDGET);
	vb->pack_start(table, Gtk::PACK_EXPAND_WIDGET);

	int n=0;

    table.attach(lname, 0,1, n,n+1, Gtk::FILL, Gtk::FILL|Gtk::SHRINK);
    table.attach(ename, 1,2, n,n+1, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::SHRINK);n++;

    table.attach(lfile, 0,1, n,n+1, Gtk::FILL, Gtk::FILL|Gtk::SHRINK);
    table.attach(efile, 1,2, n,n+1, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::SHRINK);n++;

    table.attach(lfolder, 0,1, n,n+1, Gtk::FILL, Gtk::FILL|Gtk::SHRINK);
    table.attach(efolder, 1,2, n,n+1, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::SHRINK);n++;

    table.attach(ldescr, 0,1, n,n+1, Gtk::FILL, Gtk::FILL|Gtk::SHRINK);
    table.attach(edescr, 1,2, n,n+1, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::SHRINK);n++;

    efolder.set_sensitive(false);
    efile.set_sensitive(false);
    edescr.set_sensitive(false);

    on_project_name_changed();

    ename.signal_changed().connect(sigc::mem_fun(*this, &NewProject::on_project_name_changed));

	show_all_children();
}

NewProject::~NewProject()
{
}



void NewProject::on_project_name_changed()
{
    efile.set_text(getProjectFile());
    efolder.set_text(getProjectFolder());
}



/** @brief getProjectFolder
  *
  * @todo: document this function
  */
string NewProject::getProjectFolder()
{
    return App::home_piiri_path + "/projekte/" + getProjectName();
}

/** @brief getProjectFile
  *
  * @todo: document this function
  */
string NewProject::getProjectFile()
{
    return getProjectFolder() + "/" + getProjectName() + ".piiri";
}

/** @brief getProjectName
  *
  * @todo: document this function
  */
string NewProject::getProjectName()
{
    return ename.get_text();
}

