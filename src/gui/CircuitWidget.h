/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SCHALTUNG_CIRCUIT_WIDGET_H
#define SCHALTUNG_CIRCUIT_WIDGET_H

#include <gtkmm.h>
#include <gtkmm/drawingarea.h>
#include <vector>
#include <map>
#include <string>

#include "circuit/ToplevelCircuit.h"
//#include "Components.h"
#include "circuit/CircuitVisual.h"

#define KEY_UP 65362
#define KEY_DOWN 65364
#define KEY_LEFT 65361
#define KEY_RIGHT 65363
#define KEY_CONTEXT 65383
#define KEY_ESC 65307



enum Operation {
	OPERATION_NONE,
	OPERATION_CONNECT,
	OPERATION_RECTSELECT,
	OPERATION_MOVING,
	OPERATION_PUTTING
};

enum CircuitStateChanged {
	CSC_SWITCHED,
	CSC_MOVED,
	CSC_SELECTED,
	CSC_CONNECTED,
	CSC_MOVING,
	CSC_CONNECTING
};



//#ifndef ABS
//#endif



class CircuitWidget : public Gtk::DrawingArea
{
private:
	CircuitContainerVisual *m_pVisual;

	int _mx,_my,_dx,_dy, _old_mx, _old_my, last_moved_x, last_moved_y;
	bool rubber, view_only, is_moving; //, is_connecting;

	sigc::connection m_size_changed_connection;

public:
	string planFile;
	Cairo::RefPtr<Cairo::ImageSurface> planImage;
	bool planOver, planShow;
	double planAlpha, planScale;
	int planX, planY;

	bool editActive;

	double zoom;

	Operation op;

	void setPlan(const string& f, int x, int y, double scale, double alpha, bool over, bool show);


	Cairo::RefPtr<Cairo::Context> getCarioContext() {return get_window()->create_cairo_context();}



public:
	CircuitWidget(bool view_only=false, CircuitContainerVisual* v=NULL);
	virtual ~CircuitWidget();


	void update();

	CircuitContainerVisual *getVisual() {return m_pVisual;}
	void setVisual(CircuitContainerVisual *v);

	void clear();

	void on_selection_changed();
	void on_circuitstate(CircuitStateChanged n);

	type_visual_list selObj;
	type_forwardings selIO;
	void deselectAll();
	void deselectIOs();
	void deselectComponents();

	void startMoving(CircuitChildVisual *vis); // TODO: gebraucht?


	void zoomIn();
	void zoomOut();
	void setZoom(double z);
	double getZoom() {return zoom;};

	bool getViewOnly() {return view_only;};

	void mouseToCircuit(int x, int y, int& cx, int& cy);


protected:
	//Override default signal handler:
	virtual bool on_expose_event(GdkEventExpose* event);
	//virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);

	//virtual bool on_my_event(GdkEvent* event);
	virtual bool on_my_press_event(GdkEventButton* event);
	virtual bool on_my_motion_event(GdkEventMotion* event);
	virtual bool on_my_release_event(GdkEventButton* event);
	virtual void on_my_size_allocate(Gtk::Allocation& allocation);
	virtual void on_my_realize();
    virtual bool on_my_key_press_event(GdkEventKey* event);
    virtual bool on_focus(Gtk::DirectionType direction);

	virtual void on_circuit_size_changed(int w, int h);

	Cairo::RefPtr<Cairo::ImageSurface> buffer;
	void saveImgBuf(Cairo::RefPtr<Cairo::Context> cr);
	void restoreImgBuf(Cairo::RefPtr<Cairo::Context> cr);

public:
	typedef sigc::signal<void, CircuitStateChanged> type_signal_circuitstate;
	type_signal_circuitstate signal_circuitstate() {return m_signal_circuitstate;}

	typedef sigc::signal<void> type_signal_selection_changed;
	type_signal_selection_changed signal_selection_changed() {return m_signal_selection_changed;}

protected:
	type_signal_selection_changed m_signal_selection_changed;
	type_signal_circuitstate m_signal_circuitstate;




};


#endif // SCHALTUNG_CIRCUIT_WIDGET_H
