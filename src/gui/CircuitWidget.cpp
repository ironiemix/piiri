/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "gui/CircuitWidget.h"

#include <cairomm/context.h>

#include "model/UndoManager.h"
#include "model/GeneralUndoItem.h"
#include "model/App.h"

#include <time.h>
#include <algorithm>

// http://library.gnome.org/devel/gdk/stable/gdk-Event-Structures.html
// http://developer.gnome.org/gtkmm/stable/
// http://developer.gnome.org/gtkmm-tutorial/stable/


#define START_X 0
#define START_Y 0

/***********/



CircuitWidget::CircuitWidget(bool vo, CircuitContainerVisual *v) :
m_pVisual(v),
rubber(false),
view_only(vo),
is_moving(false),
planFile(""),
planOver(true), planShow(false),
planAlpha(0.1), planScale(1.0),
planX(20), planY(20),
editActive(true),
zoom(1.0),
op(OPERATION_NONE)
{
	buffer = (Cairo::RefPtr<Cairo::ImageSurface>)NULL;

	if (view_only) editActive = false;

	add_events(
		Gdk::POINTER_MOTION_MASK |
		Gdk::BUTTON_PRESS_MASK |
		Gdk::BUTTON_RELEASE_MASK
		);


	signal_button_press_event().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_press_event));
	signal_button_release_event().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_release_event));
	if (!view_only) signal_motion_notify_event().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_motion_event));
	signal_size_allocate().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_size_allocate));
	signal_realize().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_realize));
	signal_key_press_event().connect(sigc::mem_fun(*this, &CircuitWidget::on_my_key_press_event));
	signal_focus().connect(sigc::mem_fun(*this, &CircuitWidget::on_focus));

	m_size_changed_connection = m_pVisual->signal_size_changed().connect(sigc::mem_fun(*this, &CircuitWidget::on_circuit_size_changed));


	set_can_focus();

	set_name("CircuitWidget");

/*	#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
	//Connect the signal handler if it isn't already a virtual method override:
	signal_draw().connect(sigc::mem_fun(*this, &Clock::on_draw), false);
	#endif //GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
	*/
}



CircuitWidget::~CircuitWidget()
{
    m_signal_selection_changed.clear();
    m_signal_circuitstate.clear();
}


void CircuitWidget::setPlan(const string& f, int x, int y, double scale, double alpha, bool over, bool show)
{
	//cout << "plan " << f << ", " << x << ", " << y << ", " << scale << ", " << alpha << ", " << over << ", " << show << endl;


	planFile = f;
	planX = x;
	planY = y;
	planScale = scale;
	planAlpha = alpha;
	planOver = over;
	planShow = show;
	//Circuit *pcir = (Circuit*)m_pVisual->getObj();
	//if (FileExists(App::plan_path+planFile))
	if (!planFile.empty())
	{
        try
        {
            string s;
            s = App::plan_path + "/" + planFile;
            FILE* f = fopen(s.c_str(), "r");
            if (f)
            {
                fclose(f);
                planImage = Cairo::ImageSurface::create_from_png(s);
            }
            else
            {
                planImage.clear(); // = NULL
                cerr << "Die Datei für den Plan konnte nicht geladen werden." << endl;
            }
        }
        catch (...)
        {
            planImage.clear(); // = NULL
            cerr << "Die Datei für den Plan konnte nicht geladen werden." << endl;
        }
	}
}




void CircuitWidget::on_my_realize()
{
	update();
}

/**
 * TODO - gerade nicht benutzt - oder?
 */
void CircuitWidget::on_my_size_allocate(Gtk::Allocation& allocation)
{
}



#define SELECT_IO(var)				deselectAll();\
	if (!var->isSelected())\
	{\
		deselectAll();\
		var->setSelected(true);\
		selIO.push_front(var);\
		on_selection_changed();\
		update();\
	}



bool CircuitWidget::on_my_press_event(GdkEventButton* event)
{
	CircuitChildVisual *visual = NULL;
	CircuitInput *pin = NULL;
	CircuitOutput *pout = NULL;

	if (op == OPERATION_MOVING)
	{
	    op = OPERATION_NONE;
	    return false;
	}

	grab_focus();

	if (get_current_modal_grab() != this) add_modal_grab();


	int mx,my;
	int rx,ry;
	get_pointer(mx, my);

	_mx = mx;
	_my = my;

	op = OPERATION_RECTSELECT;

	COMPONENT_AREA area;

	// wo sind wir im Visual der Toplevelschaltung dieses fensters
	// nicht diese umrechnung verwenden: sie verwendet die CirObj position!! aber das toplevel element liegt immer bei 0 0
	// evtl. visual->getAbsPos verwenden?
	//m_pVisual->parentToComponent(mx/zoom, my/zoom,  rx, ry);
	rx = mx/zoom;
	ry = my/zoom;

	//cout << mx/zoom << ", " << my/zoom << " " << rx << ", " << ry << endl;

	area = m_pVisual->areaOnComponent(rx, ry);

	m_pVisual->componentToArea(area,  rx, ry,  rx, ry);

	if ((area == CA_IN) && (!view_only))
	{
		//cout << "Main IN ";
		// rx und ry sind koordinaten rel zum input-bereich!

		// Main IN markieren oder manin IN toggeln
		pin = m_pVisual->getInputFromPoint(rx,ry);
		if (pin)
		{
			if ((rx > 20) && ((event->button == 1) || (event->button == 3)))
			{
				//is_connecting = true;
				if (event->button == 1) op = OPERATION_CONNECT;
				else op = OPERATION_NONE;
				SELECT_IO(pin)
			}
			else if ((event->button == 1) || (event->button == 2))
			{
				deselectAll();

				if (event->button == 1)
                    pin->setValueB(!pin->getValueB());
                else
                    pin->setValueB(true); // TODO beim loslassen ausschalten!

				on_selection_changed();
				m_pVisual->getObj()->update();
				update();
				m_signal_circuitstate.emit(CSC_SWITCHED);
				return true;
			}
		}

	}
	else if ((area == CA_OUT) && (!view_only))
	{
		pout = m_pVisual->getOutputFromPoint(rx,ry);
		if (pout && ((event->button == 1) || (event->button == 3)))
		{
			SELECT_IO(pout)
		}
	}
	else if (area == CA_INNER)
	{
		// cout << "Main INNER ";
		// rx und ry sind koordinaten rel zum inner-bereich!
		// wir sind im innern und schauen, ob wir eine komponente unterm zeiger haben
		// Element suchen
		visual = m_pVisual->getVisualFromPoint(rx, ry, rx, ry, area);
		if (visual != NULL)
		{
			if (!(event->state & Gdk::CONTROL_MASK))
			{
				if (((selObj.size() + selIO.size()) == 1) && !(visual->selected))
				{
					deselectAll();
					on_selection_changed();
				}
			}
			//cout << visual->getObj()->getName() << " ";
			// wir haben eine komponente!
			// zum bereich relative koordinaten holen
			//cout << " : " << rx << "," << ry << endl;
			visual->parentToComponent(rx, ry,  rx, ry);
			//cout << "C: " << rx << "," << ry << endl;
			visual->componentToArea(area,  rx, ry,  rx, ry);
			//cout << "A: " << rx << "," << ry << endl;

			_dx = mx;
			_dy = my;

			last_moved_x = last_moved_y = 0;

			if ((area == CA_INNER) || (area == CA_TITLE))
			{
                //else if (area == CA_TITLE)
                //{
                    //cout << "title ";
                    if (!visual->selected)
                    {
                        visual->selected = true;
                        selObj.push_front(visual);
                        on_selection_changed();
                    }
                    if ((event->button == 1) && editActive) op = OPERATION_MOVING;
                    else op = OPERATION_NONE;
                //}
                if ((area == CA_INNER) && (!view_only))
                {
                    /*
                    //cout << "inner " << endl;
                    if (event->button == 1)
                    {
                        visual->on_inner_click(
                            rx,
                            ry,
                            this
                        );
                        //return true;
                    }
                    */
                }
			}
			else if ((area == CA_IN) && (!view_only))
			{
				//cout << "in ";
				pin = visual->getInputFromPoint(rx,ry);
				if (pin && ((event->button == 1) || (event->button == 3)))
				{
					SELECT_IO(pin)
				}
			}
			else if ((area & CA_OUT) && (!view_only))
			{
				//is_connecting = true;
				//cout << "out ";
				pout = visual->getOutputFromPoint(rx,ry);
				if (pout && ((event->button == 1) || (event->button == 3)))
				{
					SELECT_IO(pout)
                    if (event->button == 1) op = OPERATION_CONNECT;
                    else op = OPERATION_NONE;
				}
			}
			//cout << endl;
			//cout << "(" << rx << ", " << ry << ")";


		} // wenn komponente angeklickt
		else
		{
			deselectAll();
			on_selection_changed();
		}
	}
	else
	{
		deselectAll();
		on_selection_changed();
	}

	update();

	return false;
} // on press event



void CircuitWidget::on_selection_changed()
{
	m_signal_selection_changed.emit();
}

void CircuitWidget::on_circuitstate(CircuitStateChanged n)
{
	m_signal_circuitstate.emit(n);
}



bool CircuitWidget::on_my_release_event(GdkEventButton* event)
{
    remove_modal_grab();
	//CircuitObject *obj;
	//list<CircuitObject*> l;

	int mx,my;
	int x1,y1,x2,y2;

	get_pointer(mx, my);

	if (rubber)
	{
		restoreImgBuf(get_window()->create_cairo_context());
		rubber = false;

		if (!view_only && (op == OPERATION_CONNECT))
		{
			//is_connecting = false;

			// Über welchen in/out haben wir angehalten?

			// IO-Verbindung (ziel ist entweder Main out oder input

			COMPONENT_AREA area;
			CircuitIO *start = NULL, *end = NULL;

			// start-Punkte umrechnen in main-comp. koord.
			//m_pVisual->parentToComponent(_mx/zoom,_my/zoom,  x1, y1);
			x1=_mx/zoom; // TEST
			y1=_my/zoom;

			// wo sind wir in d. main-komp.?
			area = m_pVisual->areaOnComponent(x1,y1);
			// koord. zu dieser komp umrechnen
			m_pVisual->componentToArea(area,  x1, y1,  x1, y1);
			if (area == CA_IN)
			{
				// main-out: input holen
				start = m_pVisual->getInputFromPoint(x1, y1);
			}
			else if (area == CA_INNER)
			{
				// main-inner: zunächst komponente holen
				CircuitChildVisual *vis = m_pVisual->getVisualFromPoint(x1,y1,x1,y1, area);
				if (vis)
				{
					vis->parentToComponent(x1,y1, x1,y1);
					// komponente gefunden
					if (area & CA_OUT)
					{
						vis->componentToArea(CA_OUT, x1,y1, x1,y1);
						// wir sind auf dem in-bereich der komponente
						// wir holen uns den input
						start = vis->getOutputFromPoint(x1, y1);
					}
				}
			}


			// end-Punkte umrechnen in main-comp. koord.
			//m_pVisual->parentToComponent(mx/zoom,my/zoom,  x2, y2);
			x2 = mx/zoom; // TEST
			y2 = my/zoom;
			// wo sind wir in d. main-komp.?
			area = m_pVisual->areaOnComponent(x2,y2);
			// koord. zu dieser komp umrechnen
			m_pVisual->componentToArea(area,  x2, y2,  x2, y2);
			if (area == CA_OUT)
			{
				// main-out: output holen
				end = m_pVisual->getOutputFromPoint(x2, y2);
			}
			else if (area == CA_INNER)
			{
				// main-inner: zunächst komponente holen
				CircuitChildVisual *vis = m_pVisual->getVisualFromPoint(x2,y2,x2,y2, area);
				if (vis)
				{
					vis->parentToComponent(x2,y2, x2,y2);
					// komponente gefunden
					if (area & CA_IN)
					{
						vis->componentToArea(CA_IN, x2,y2, x2,y2);
						// wir sind auf dem in-bereich der komponente
						// wir holen uns den input
						end = vis->getInputFromPoint(x2, y2);
					}
				}
			}

			// falls da einer ist:
			if (start && end)
			{
				// ende darf nicht main input sein
				// anfang darf nicht main output sein
				// input an output nur wenn beide von main sind
				// out an out nur wenn ziel von main ist
				// in an in nur wenn start in main ist



				#define ismainin(o) ((o->getOwner() == m_pVisual->getObj()) && (o->getType() == IO_TYPE_IN))
				#define ismainout(o) ((o->getOwner() == m_pVisual->getObj()) && (o->getType() == IO_TYPE_OUT))
				#define isnmainin(o) ((o->getOwner() != m_pVisual->getObj()) && (o->getType() == IO_TYPE_IN))
				#define isnmainout(o) ((o->getOwner() != m_pVisual->getObj()) && (o->getType() == IO_TYPE_OUT))
				#define ismain(o) ((o->getOwner() == m_pVisual->getObj()))
				#define isin(o) (o->getType() == IO_TYPE_IN)
				#define isout(o) (o->getType() == IO_TYPE_OUT)

				if (  // Nur gültige verbindungen zulassen
					// anfang darf nicht gleich ende sein
					(start != end)

					&&
					// anfang und ende dürfen nicht beides inputs vom selben typ im selben element sein
					((start->getType() != end->getType()) || (start->getOwner() != end->getOwner()))

					&&
					(
					(ismainin(start) && ismainout(end))
					|| (isnmainout(start) && isnmainin(end))
					|| (ismainin(start) && isnmainin(end))
					|| (isnmainout(start) && ismainout(end))
					)
					)
				{
					//deselectAll();
					//on_selection_changed();

                    if (start->getPinCount() == end->getPinCount())
                    {
                        on_circuitstate(CSC_CONNECTING);

                        start->addForwarding(end);

                        start->getOwner()->list_push(start->getOwner());
                        //->getOwner()->update();
                        m_pVisual->getObj()->update();
                        update();
                        on_circuitstate(CSC_CONNECTED);
                    }

				}
			}
		} // ein IO markiert
		else if (selObj.size() == 0)
		{
			// Mehrere Componenten markieren

			//selObj.clear();
			COMPONENT_AREA area;

			// Punkte der bounding rubber box umrechnen in main-comp. inner koord.
			x1 = _mx/zoom;
			y1 = _my/zoom;
			x2 = mx/zoom;
			y2 = my/zoom;
			//m_pVisual->parentToComponent(_mx/zoom,_my/zoom,  x1, y1);
			m_pVisual->componentToArea(CA_INNER,  x1, y1,  x1, y1);
			//m_pVisual->parentToComponent(mx/zoom,my/zoom,  x2, y2);
			m_pVisual->componentToArea(CA_INNER,  x2, y2,  x2, y2);

			m_pVisual->getVisualFromPoint(x1,y1, x2,y2, area, &selObj);
			if (selObj.size() > 0)
			{
				type_visual_list::iterator it;
				for(it = selObj.begin(); it != selObj.end(); it++)
				{
					(*it)->selected = true;
				}
				on_selection_changed();
				update();
			}
		}


	} // rubber
	else if (!view_only)
	{
	    if (is_moving && (op == OPERATION_MOVING))
	    {
            on_circuitstate(CSC_MOVED);
            is_moving = false;
	    }
		else
		{ // schauen wir wo wir losgelassen haben

			x1 = mx/zoom;
			y1 = my/zoom;

			COMPONENT_AREA area;
            m_pVisual->componentToArea(CA_INNER,  x1, y1, x1, y1);
            CircuitChildVisual* visual = m_pVisual->getVisualFromPoint(x1, y1, x1, y1, area);
            if (visual != NULL)
            {
                visual->parentToComponent(x1, y1,  x1, y1);
                visual->componentToArea(area,  x1, y1,  x1, y1);

                if (area == CA_INNER)
                {
                    if ((event->button == 1) && !view_only)
                    {
                        //std::cout << "INNER klick" << std::endl;
                        visual->on_inner_click(
                            x1,
                            y1,
                            this
                        );
                        //return true;
                    }
                } // if inner
                else if (area == CA_IN)
                {/*
                    if (event->button == 2) // TODO
                    {
                        CircuitInput *pin = m_pVisual->getInputFromPoint(x1,y1);
                        if (pin)
                        {
                            pin->setValue(true); // TODO beim loslassen ausschalten!
                            //on_selection_changed();
                            m_pVisual->getObj()->update();
                            update();
                            m_signal_circuitstate.emit(CSC_SWITCHED);
                            return true;
                        }
                    }
                    */
                } // in
            } // visual
		}
	} // rubber else


	op = OPERATION_NONE;


	if(event->button == 3)
	{
		// Popup wird vom fenser behandelt!
		return false; //It has been handled.
	}
	else return false;


	update();
	return false;
}



bool CircuitWidget::on_my_motion_event(GdkEventMotion* event)
{
	int mx,my;
	bool xend=false, yend=false;
	//Gdk::ModifierType mod;
	get_pointer(mx, my);

	if ((selObj.size() > 0) && (op == OPERATION_MOVING) && editActive)
	{
		list<CircuitChildVisual*>::iterator it;
		int x,y, move_x, move_y;

		move_x = (mx - _dx)/zoom;
		move_y = (my - _dy)/zoom;

        // threshold
        if ((move_x*move_x > 9) || (move_y*move_y > 9))
        {


            for (it = selObj.begin(); it != selObj.end(); it++)
            {
                x = move_x + (*it)->getPosX() - last_moved_x;
                y = move_y + (*it)->getPosY() - last_moved_y;
                if (x<0)
                {
                    xend = true;
                }
                if (y<0)
                {
                    yend = true;
                }
            }

            if (xend) move_x = last_moved_x = 0;
            if (yend) move_y = last_moved_y = 0;

            if (!is_moving)
            {
                on_circuitstate(CSC_MOVING);
                is_moving = true;
            }

            for (it = selObj.begin(); it != selObj.end(); it++)
            {
                x = move_x + (*it)->getPosX() - last_moved_x;
                y = move_y + (*it)->getPosY() - last_moved_y;

                (*it)->setPos(x, y);
            }

            last_moved_x = move_x;
            last_moved_y = move_y;

        } // threshold

		//selectedObj->setPos(mx-_dx,my-_dy);
		update();
	}
	else if (!view_only && (op == OPERATION_CONNECT))
	{
		Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
		if (!rubber)
		{
			saveImgBuf(cr);
			rubber = true;
		}
		else
		{
			restoreImgBuf(cr);
		}

		// http://cairographics.org/documentation/cairomm/reference/classCairo_1_1Context.html
		// http://cairographics.org/samples/

		cr->save();
		cr->set_line_width(2);
		cr->set_source_rgba(0.6,0.5,0.0, 0.6);
		cr->move_to(_mx,_my);
		cr->line_to(mx,my);
		cr->stroke();
		cr->restore();
	}
	else if ( (op == OPERATION_RECTSELECT))
	{
		Cairo::RefPtr<Cairo::Context> cr = get_window()->create_cairo_context();
		if (!rubber)
		{
			saveImgBuf(cr);
			rubber = true;
		}
		else
		{
			restoreImgBuf(cr);
		}

		// http://cairographics.org/documentation/cairomm/reference/classCairo_1_1Context.html
		// http://cairographics.org/samples/

		cr->save();
		cr->set_line_width(2);
		cr->set_source_rgba(0.6,0.5,0.0, 0.6);
		cr->rectangle(_mx,_my, mx-_mx,my-_my);
		cr->stroke();
		cr->restore();
	}

	return true;
}

void CircuitWidget::startMoving(CircuitChildVisual *vis)
{
    if (!editActive) return;

	int mx,my;
	get_pointer(mx, my);

    deselectAll();

    selObj.push_back(vis);
    vis->selected = true;
    on_circuitstate(CSC_SELECTED);
    on_selection_changed();

    int x,y;

    update();
    x = vis->getAbsPosX();
    y = vis->getAbsPosY();

    _dx = (x + vis->getSizeW()/2) * zoom;
    _dy = (y + vis->getSizeH()/2) * zoom;

    last_moved_x = last_moved_y = 0;
    op = OPERATION_MOVING;
	grab_focus();
	if (get_current_modal_grab() != this) add_modal_grab();
}


void CircuitWidget::zoomIn()
{
	setZoom(zoom * 1.1);
}

void CircuitWidget::zoomOut()
{
	setZoom(zoom * 0.9);
}

void CircuitWidget::setZoom(double z)
{
	zoom = z;

	set_size_request(m_pVisual->getSizeW()*zoom, m_pVisual->getSizeH()*zoom);

	queue_draw();
}



//bool CircuitWidget::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)

#define PAINT_PLAN 	\
                    {\
                    cr->set_source(planImage, 0, 0);\
					Cairo::RefPtr<Cairo::Pattern> pat = cr->get_source();\
					Cairo::Matrix matrix = Cairo::identity_matrix();\
					matrix.scale(1.0/planScale, 1.0/planScale);\
	int ax=0,ay=0;\
	m_pVisual->parentToComponent(ax,ay, ax,ay);\
	m_pVisual->componentToArea(CA_INNER, ax,ay, ax,ay);\
					matrix.translate(-planX+ax, -planY+ay);\
					pat->set_matrix(matrix);\
					cr->paint_with_alpha(planAlpha);\
                    }



bool CircuitWidget::on_expose_event(GdkEventExpose* event)
{
//    clock_t start = clock();

//  // This is area we draw on the window
	Glib::RefPtr<Gdk::Window> window = get_window();
	if(is_realized())
	{
		Gtk::Allocation allocation = get_allocation();
		const int width = allocation.get_width();
		const int height = allocation.get_height();

		Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();

		cr->scale(zoom, zoom);

		// clip to the area indicated by the expose event so that we only redraw
		// the portion of the window that needs to be redrawn
		cr->rectangle(event->area.x/zoom, event->area.y/zoom, event->area.width/zoom, event->area.height/zoom);
		cr->clip();



		cr->rectangle(0, 0, width/zoom, height/zoom);
		cr->set_source_rgb(MAINCIR_BACK_COL);
		cr->fill();
		//m_pVisual->setCC(cr);

		if (m_pVisual->getObj() != NULL)
		{
			if (planShow && planImage)
			{
				if (!planOver)
				{
					PAINT_PLAN
				}
			}

			m_pVisual->paintAll(cr, START_X, START_Y, true);

			if (m_pVisual->isEmpty() && !getViewOnly())
			{
                Cairo::TextExtents extents;
                cr->set_source_rgb(0.6, 0.6, 0.6);
                cr->set_font_size(24);
                std::string a(App::t("Bauen Sie Ihre Schaltung auf, indem Sie"));
                std::string b(App::t("Komponenten in dieses Feld ziehen."));
                cr->get_text_extents(a, extents);
                cr->move_to((width-extents.width)/2, height/2+extents.y_bearing);
                cr->show_text(a);
                cr->get_text_extents(b, extents);
                cr->move_to((width-extents.width)/2, height/2-extents.y_bearing);
                cr->show_text(b);
			}

			if (planShow && planImage)
			{
				if (planOver)
				{
					PAINT_PLAN
				}
			}

		}

	}
	//std::cout << "[on_expose_event] Zeit: " << ((double)((clock()-start))/(CLOCKS_PER_SEC/1000)) << "ms" << std::endl;
	return true;
}


void CircuitWidget::setVisual(CircuitContainerVisual* v)
{
    if (m_size_changed_connection.connected()) m_size_changed_connection.disconnect();
	m_pVisual = v;
	m_size_changed_connection = m_size_changed_connection = m_pVisual->signal_size_changed().connect(sigc::mem_fun(*this, &CircuitWidget::on_circuit_size_changed));
}


void CircuitWidget::update()
{
	if (get_window())
	{
		m_pVisual->updateSize(get_window()->create_cairo_context(), START_X, START_Y);
		queue_draw();
	}
}

void CircuitWidget::saveImgBuf(Cairo::RefPtr<Cairo::Context> cr)
{
	buffer.clear(); // Cairo::RefPtr!!
	if (buffer == (Cairo::RefPtr<Cairo::ImageSurface>)NULL)
	{
		Gtk::Allocation allocation = get_allocation();
		const int width = allocation.get_width();
		const int height = allocation.get_height();
		buffer = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);
	}

	Cairo::RefPtr<Cairo::Context> bufcr = Cairo::Context::create(buffer);
	bufcr->set_source(cr->get_target(), 0, 0);
	bufcr->paint();
}

void CircuitWidget::restoreImgBuf(Cairo::RefPtr<Cairo::Context> cr)
{
	if (buffer != (Cairo::RefPtr<Cairo::ImageSurface>)NULL)
	{
		cr->set_source(buffer, 0, 0);
		cr->paint();
	}
}


void CircuitWidget::deselectAll()
{
	deselectComponents();
	deselectIOs();
}

void CircuitWidget::deselectIOs()
{
	type_forwardings::iterator ioit;
	for (ioit = selIO.begin(); ioit != selIO.end(); ioit++)
	{
		(*ioit)->setSelected(false);
	}
	selIO.clear();
}

void CircuitWidget::deselectComponents()
{
	type_visual_list::iterator it;
	for (it = selObj.begin(); it != selObj.end(); it++)
	{
		(*it)->selected = false;
	}
	selObj.clear();
}


void CircuitWidget::on_circuit_size_changed(int w, int h)
{
	set_size_request(w*zoom,h*zoom);
}

void CircuitWidget::clear()
{
	deselectAll();
	type_visual_map::iterator it;
	for (it = m_pVisual->m_childVisuals.begin(); it != m_pVisual->m_childVisuals.end(); it++)
	{
		delete (*it).second;
	}
	m_pVisual->m_childVisuals.clear();

	planFile = "";
	planX = planY = 0;
	planScale = 1.0;
	planAlpha = 0.3;
	planImage.clear();
	planShow = false;
	planOver = false;
}


/**
 */
bool CircuitWidget::on_my_key_press_event(GdkEventKey* event)
{
	/**
	http://developer.gnome.org/gdk/stable/gdk-Keyboard-Handling.html
	<gdk/gdkkeysyms.h>
	struct GdkEventKey {
	  GdkEventType type;
	  GdkWindow *window;
	  gint8 send_event;
	  guint32 time;
	  guint state;
	  guint keyval;
	  gint length;
	  gchar *string;
	  guint16 hardware_keycode;
	  guint8 group;
	  guint is_modifier : 1;
	};
	 */

    //cout << event->keyval << endl;



	switch (event->keyval)
	{
		case KEY_ESC:
            if (get_current_modal_grab() == this) remove_modal_grab();
            op = OPERATION_NONE;
			break;
		case 2:
			break;
		case 3:
			break;

		default:
			break;
	}

	//cout << event->wi << endl;
	//cout << "CW:" << event->keyval << endl;
	if (editActive && !view_only && (selObj.size() > 0))
	{
        int dx,dy;
        switch (event->keyval)
        {
            case KEY_RIGHT: //>
                dx = 10;
                dy = 0;
                break;
            case KEY_LEFT: //<
                dx = -10;
                dy = 0;
                break;
            case KEY_UP: //^
                dx = 0;
                dy = -10;
                break;
            case KEY_DOWN: //v
                dx = 0;
                dy = 10;
                break;

            default:
                dx = dy = 0;
                break;
        }
        if (dx || dy)
        {
            if (event->state & Gdk::MOD1_MASK)
            {
                dx /= 10;
                dy /= 10;
            }
            on_circuitstate(CSC_MOVING);

            type_visual_list::iterator it;
            for (it = selObj.begin(); it != selObj.end(); it++)
            {
                (*it)->setPos((*it)->getPosX() + dx, (*it)->getPosY() + dy);
            }

            on_circuitstate(CSC_MOVED);
            update();
        }
        return true; // true: wird nicht mehr im parent behandelt
	}
	else if (selIO.size()==1)
	{
	    #define IS_UP (event->keyval == KEY_UP)
	    #define IS_DOWN (event->keyval == KEY_DOWN)
        if (IS_UP || IS_DOWN)
        {
            CircuitIO *io = selIO.front();
            CircuitChild *pobj = io->getOwner();

            bool shift = (event->state & Gdk::SHIFT_MASK);

            if (io->getType() == IO_TYPE_IN)
            {
                CircuitInput *pin = (CircuitInput *)io;

                type_inputs_iterator start = pobj->getInputIterator(pin->getName());
                type_inputs_iterator it = start;

                if (it == pobj->getInputs().end()) return false;

                if (IS_DOWN)
                {
                    it++;
                    if (it == pobj->getInputs().end()) return false;
                    if (!shift) pin->setSelected(false);
                    io = (*it);
                }
                else if (IS_UP)
                {
                    if (it == pobj->getInputs().begin()) return false;
                    it--;
                    if (!shift) pin->setSelected(false);
                    io = (*it);
                }

                if (shift)
                {
                    std::swap((*it),(*start));
                    // on_inputs_changed ??
                }
            }
            else if (io->getType() == IO_TYPE_OUT)
            {
                CircuitOutput *pout = (CircuitOutput *)io;

                type_outputs_iterator start = pobj->getOutputIterator(pout->getName());
                type_outputs_iterator it = start;

                if (it == pobj->getOutputs().end()) return false;

                if (IS_DOWN)
                {
                    it++;
                    if (it == pobj->getOutputs().end()) return false;
                    if (!shift) pout->setSelected(false);
                    io = (*it);
                }
                else if (IS_UP)
                {
                    if (it == pobj->getOutputs().begin()) return false;
                    it--;
                    if (!shift) pout->setSelected(false);
                    io = (*it);
                }

                if (shift)
                {
                    std::swap((*it),(*start));
                }
            }

            if (!shift)
            {
                io->setSelected(true);
                selIO.clear();
                selIO.push_back(io);
                on_selection_changed();
            }
            update();
            return true;
        }
	}

	return false;
}


bool CircuitWidget::on_focus(Gtk::DirectionType direction)
{
    if (!has_focus())
    {
        grab_focus();
        return false;
    }
    else
    {
        if ((direction==Gtk::DIR_TAB_FORWARD) || (direction==Gtk::DIR_TAB_BACKWARD))
        {
            return Gtk::DrawingArea::on_focus(direction);
        }
    }
    return true;
}


/** @brief mouseToCircuit
  *
  * @todo: document this function
  */
void CircuitWidget::mouseToCircuit(int x, int y, int& cx, int& cy)
{
    cx = x/zoom;
    cy = y/zoom;
}





