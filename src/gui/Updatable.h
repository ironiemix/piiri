/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef UPDATABLE_H
#define UPDATABLE_H

#include <gtkmm.h>

#include "circuit/CircuitChild.h"


class CircuitTab;

class Updatable : public Gtk::Window
{
    public:
        enum SUBWINDOW_TYPE {
            SW_ALL,SW_VIEW, SW_TABLE
        };

    protected:
        SUBWINDOW_TYPE window_type;
        CircuitChild *m_pObj;
        CircuitTab& m_rTab;

        virtual bool on_delete_event(GdkEventAny* event);

    public:

        Updatable(CircuitTab& tab, CircuitChild *obj, SUBWINDOW_TYPE t);
        CircuitChild *getObj() {return m_pObj;};
        virtual void updateAll() = 0;
        SUBWINDOW_TYPE getWindowType() {return window_type;};
};

#endif // UPDATABLE_H
