/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "Updatable.h"
#include "CircuitTab.h"


Updatable::Updatable(CircuitTab& tab, CircuitChild *obj, SUBWINDOW_TYPE t) :
window_type(t),
m_pObj(obj),
m_rTab(tab)
{
    signal_delete_event().connect(sigc::mem_fun(*this, &Updatable::on_delete_event));
};


bool Updatable::on_delete_event(GdkEventAny* event)
{
    hide();
    m_rTab.on_subwindow_close(this);
	return true;
}
