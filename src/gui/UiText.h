#ifndef UITEXT_H_INCLUDED
#define UITEXT_H_INCLUDED

#include <string>
#include <map>

typedef std::pair<std::string,std::string> ui_text;
typedef std::map<std::string, ui_text> ui_text_map;


#endif // UITEXT_H_INCLUDED
