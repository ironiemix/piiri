/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "ViewWindow.h"
#include <iostream>
#include "TableWindow.h"


/**
 * class ViewWindow
 *
 * Anzeigefenster.
 *
 * Hier kann man nur Komponenten Markieren und Öffnen. Zoom.
 *
 */
ViewWindow::ViewWindow(CircuitTab& tab, CircuitContainer* c) :
 Updatable(tab, c, Updatable::SW_VIEW),
 m_pVisual(new CircuitContainerVisual(c)),
 circuitArea(true, m_pVisual),
 m_VBox(false, 3),
 m_HBox(false, 3)
{
	// Zum Visual zugehörige Circuit-Komponente holen.
    //circuitArea.signal_realize().connect(sigc::mem_fun(*this, &ViewWindow::on_cwidget_realize));

	// Fenster einrichten
	set_title(App::name + " | " + m_pObj->getNameRecursive() );
	set_default_size(500, 300);

	// signale fürs Markieren (Maus).
	circuitArea.signal_button_release_event().connect(sigc::mem_fun(*this, &ViewWindow::on_popup_menu));
	circuitArea.signal_button_press_event().connect(sigc::mem_fun(*this, &ViewWindow::on_doubleclick));
	// wird aufgerufen, wenn die Auswahl sich geändert hat: dann müssen die verfügbaren Aktionen gewählt werden.
	circuitArea.signal_selection_changed().connect(sigc::mem_fun(*this, &ViewWindow::on_selection_changed));

	signal_key_release_event().connect(sigc::mem_fun(*this, &ViewWindow::on_my_key_release_event));

	// Action-Gruppe für alle Aktionen (Toolbar / Menü) des Fensters
	refActionGroup = Gtk::ActionGroup::create();

	// Zoom-Aktionen hat jedes Fenster
	CREATE_ACTION2_KEY(ViewWindow,ZoomIn,Gtk::Stock::ZOOM_IN,"","Anzeige vergrößern", Gtk::AccelKey('+',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(ViewWindow,ZoomOut,Gtk::Stock::ZOOM_OUT,"","Anzeige verkleinern", Gtk::AccelKey('-',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(ViewWindow,Zoom100,Gtk::Stock::ZOOM_100,"","Normale Anzeige", Gtk::AccelKey('#',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(ViewWindow,Refresh,Gtk::Stock::REFRESH,"","Schaltung aktualisieren", Gtk::AccelKey("F5"))
	CREATE_ACTION(ViewWindow,Show, "Anzeigen","<b>Komponente anzeigen</b>")

    CREATE_ACTION(ViewWindow,Table,"Wertetabelle","<b>Wertetabelle anzeigen</b>")


	// UI-Manager erzeugen und Gruppe hinzufügen.
	refUIManager = Gtk::UIManager::create();
	refUIManager->insert_action_group(refActionGroup);
	add_accel_group(refUIManager->get_accel_group());

    std_context = m_Statusbar.get_context_id(App::name);


    //------------------
	// Toolbar und Popup-Menü definieren.
	Glib::ustring ui_info =
		"<ui>"
		"  <toolbar  name='ToolBar'>"
		"    <toolitem action='Refresh'/>"
		"    <separator/>"
		"    <toolitem action='ZoomIn'/>"
		"    <toolitem action='ZoomOut'/>"
		"    <toolitem action='Zoom100'/>"
		"  </toolbar>"
		"  <popup name='PopupMenu'>"
		"    <menuitem action='Show'/>"
		"    <menuitem action='Table'/>"
		"  </popup>"
		"</ui>";

	refUIManager->add_ui_from_string(ui_info);

	// Toolbar anlegen
	Gtk::Widget* pToolBar = refUIManager->get_widget("/ToolBar");
	((Gtk::Toolbar*)pToolBar)->set_toolbar_style(Gtk::TOOLBAR_ICONS);


	add(m_VBox);
	m_VBox_Toolbar.pack_start(*pToolBar, Gtk::PACK_EXPAND_WIDGET);
	m_VBox.pack_start(m_VBox_Toolbar, Gtk::PACK_SHRINK);
	m_VBox.pack_start(m_HBox, Gtk::PACK_EXPAND_WIDGET);
	m_VBox.pack_start(m_Statusbar, Gtk::PACK_SHRINK);
	m_ScrolledWindowCanvas.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	m_ScrolledWindowCanvas.set_border_width(3);
	m_ScrolledWindowCanvas.add(circuitArea);
	m_HBox.pack_start(m_ScrolledWindowCanvas);

	view_context = m_Statusbar.get_context_id("view");
    m_Statusbar.push("PiiRi Komponenten-Ansicht", view_context);


	on_selection_changed();

	show_all_children();
}

/** @brief on_cwidget_realize
  *
  * @todo: document this function
  */
/*void ViewWindow::on_cwidget_realize()
{
    m_pVisual->setCC(circuitArea.getCarioContext());
}
*/

ViewWindow::~ViewWindow()
{
    //cout << "~ViewWindow()" << endl;
	delete m_pVisual;
	m_pVisual = NULL;
}



/** ViewWindow Members */



/**
 * Allgemeines anzeigen des Kontextmenüs.
 */
bool ViewWindow::on_popup_menu(GdkEventButton* event)
{
	if(event->button == 3)
	{
	    contextMenu(event->button);
	    return true;
	}
	return false; // TODO --> richtig?
}

/** @brief contextMenu
  *
  * @todo: document this function
  */
void ViewWindow::contextMenu(int button)
{
    Gtk::Menu* pMenuPopup;
    pMenuPopup = (Gtk::Menu*)refUIManager->get_widget("/PopupMenu");
    pMenuPopup->popup(button, gtk_get_current_event_time());
}



/**
 * Doppelklick auf Schaltung wird hier her geführt: zum Öffnen einer Schaltung.
 */
bool ViewWindow::on_doubleclick(GdkEventButton* event)
{
	if (event->button == 1)
	{
		bool doubleClick = false;
		if (((Gdk::EventType)event->type) == Gdk::DOUBLE_BUTTON_PRESS ) doubleClick = true;
		if (doubleClick) on_action_Show();
		return true;
	}
	return false; // TODO--> richtig?
}

/**
 * Aktualisieren der Aktionen, je nach Auswahl.
 * TODO: Sollte teilweise nur in MainWindow passieren!
 */
void ViewWindow::on_selection_changed()
{

	if (circuitArea.selObj.size() > 0)
	{
		ENABLE_ACTION("Show")
		ENABLE_ACTION("Rename")
		ENABLE_ACTION("Delete")
		ENABLE_ACTION("Disconnect")
		ENABLE_ACTION("Edit")
		if (circuitArea.selObj.size() == 1)
		{
		    ENABLE_ACTION("Properties")
		}
		else
		{
		    DISABLE_ACTION("Properties")
		}
		DISABLE_ACTION("Node")

		ENABLE_ACTION("SubMenuAnordnung")
		ENABLE_ACTION("Up")
		ENABLE_ACTION("Down")
		ENABLE_ACTION("Bottommost")
		ENABLE_ACTION("Topmost")
	}
	else if (circuitArea.selIO.size() == 1)
	{
		DISABLE_ACTION("SubMenuAnordnung")
		DISABLE_ACTION("Up")
		DISABLE_ACTION("Down")
		DISABLE_ACTION("Bottommost")
		DISABLE_ACTION("Topmost")

		DISABLE_ACTION("Properties")
		DISABLE_ACTION("Edit")
		DISABLE_ACTION("Show")
		ENABLE_ACTION("Disconnect")
		ENABLE_ACTION("Node")
		if (circuitArea.selIO.front()->getOwner() == m_pObj)
		{
			ENABLE_ACTION("Rename")
			ENABLE_ACTION("Delete")
		}
		else
		{
			DISABLE_ACTION("Rename")
			DISABLE_ACTION("Delete")
		}
	}
	else
	{
		ENABLE_ACTION("Properties")
		DISABLE_ACTION("Show")
		DISABLE_ACTION("Rename")
		DISABLE_ACTION("Delete")
		DISABLE_ACTION("Disconnect")
		DISABLE_ACTION("Edit")
		DISABLE_ACTION("Node")

		DISABLE_ACTION("SubMenuAnordnung")
		DISABLE_ACTION("Up")
		DISABLE_ACTION("Down")
		DISABLE_ACTION("Bottommost")
		DISABLE_ACTION("Topmost")
	}

    // Statusbar

    ostringstream msg;

    m_Statusbar.pop(std_context);
    if (circuitArea.selObj.size() > 0)
    {
        if (circuitArea.selObj.size() == 1)
        {
            CircuitChild* po = circuitArea.selObj.front()->getObj();
            msg << "Komponente " << po->getName() << ": " << po->getCirName();
            string d = po->getProperties().get_string("description");
            msg << " (" << d << ") ";
            msg << po->getInfo();
        }
        else
        {
            msg << circuitArea.selObj.size() << " Komponenten";
        }
        m_Statusbar.push(msg.str(),std_context);
    }
    else if (circuitArea.selIO.size() > 0)
    {
        CircuitIO *pio = circuitArea.selIO.front();
        CircuitChild* po = pio->getOwner();

        if (pio->getType() == IO_TYPE_IN) msg << "Eingang ";
        else msg << "Ausgang ";

        if (pio->getOwner()->getParent() != NULL)
            msg << po->getName() << ".";

        msg << pio->getName();

        m_Statusbar.push(msg.str(),std_context);
    }
    else
    {

    }
}

/**
 * Action-Handler für Zoom-In.
 */
void ViewWindow::on_action_ZoomIn()
{
	circuitArea.zoomIn();
}

/**
 * Action-Handler für Zoom-100%.
 */
void ViewWindow::on_action_Zoom100()
{
	circuitArea.setZoom(1.0);
}

/**
 * Action-Handler für Zoom-Out.
 */
void ViewWindow::on_action_ZoomOut()
{
	circuitArea.zoomOut();
}

/**
 * Action-Handler für Aktualisieren.
 */
void ViewWindow::on_action_Refresh()
{
    if (circuitArea.selObj.size() > 0)
    {
        CircuitChild* pObj;
        type_visual_list::iterator it;
        for (it = circuitArea.selObj.begin(); it != circuitArea.selObj.end(); it++)
        {
            pObj = (*it)->getObj();
            pObj->update();
        }
    }
    else
    {
        m_pObj->update();
    }
    circuitArea.update();
}

/**
 * Action-Handler für Komponente Anzeigen.
 */
void ViewWindow::on_action_Show()
{
	CircuitContainer* pCont;
	type_visual_list::iterator it;

	// alle markierten Container-Komponenten öffnen.
	for (it = circuitArea.selObj.begin(); it != circuitArea.selObj.end(); it++)
	{
		pCont = dynamic_cast<CircuitContainer*>((*it)->getObj());
        if (pCont) m_rTab.openViewWindow(pCont);
	}
}

/**
 * Action-Handler für Komponente Anzeigen.
 */
void ViewWindow::on_action_Table()
{
    CircuitChild* pobj = NULL;

    if (circuitArea.selObj.size() == 1)
    {
        pobj = circuitArea.selObj.front()->getObj();
    }
    else if (circuitArea.selObj.size() == 0)
    {
        pobj = m_pObj;
    }
    m_rTab.openTableWindow(pobj);
}
/**
 * Test callback.
 */
void ViewWindow::on_action_all()
{
	cout << "Unhandled Aktion!" << endl;
}

/**
 * Cir-Widget und alle Unterfenster aktualisieren.
 */
void ViewWindow::updateAll()
{
	circuitArea.update();
}


/**
 */
bool ViewWindow::on_my_key_release_event(GdkEventKey* event)
{
	switch (event->keyval)
	{
		case 65383: // context menu
            contextMenu(0);
            return true;
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;

		default:
			break;
	}

	return false;
}




