/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "gui/MainWindow.h"


#include <dirent.h>
#include <fstream>

#include "model/App.h"
#include "gui/PropertyDialog2.h"
#include "model/LogicSynth.h"
#include "circuit/CircuitVisual.h"
#include "gui/CircuitTab.h"
#include "gui/NewProject.h"

#include <gtkmm/accelmap.h>
#include <glib.h>
#include <glib/gstdio.h>


#ifdef _WIN32
#include <windows.h>
#endif

#ifdef _WIN32

    #if defined(DELETE) && !defined(GTKMM_MACRO_SHADOW_DELETE)
    enum { GTKMM_MACRO_DEFINITION_DELETE = DELETE };
    #undef DELETE
    enum { DELETE = GTKMM_MACRO_DEFINITION_DELETE };
    #define DELETE DELETE
    #define GTKMM_MACRO_SHADOW_DELETE 1
    #endif


#endif

//#include <libintl.h> // wozu?? ==> internationalisation?


#define CTAB(code) CircuitTab *ct = dynamic_cast<CircuitTab *>(m_Tabbing.get_nth_page(m_Tabbing.get_current_page()));if (ct){code}

bool MainWindow::dequeEmpty = false;

/**
 * class MainWindow
 *
 * Hauptfenster mit zusätzlich einem Menü, weiteren Aktionen zum Manipulieren
 * der Schaltung und eine Toolpalette für die Schaltungskomponenten.
 *
 * Hier wird nicht auf einem allgemeinen CircuitObject gearbeitet,
 * sondern mit einem Circuit.
 */
MainWindow::MainWindow() :
    m_VPaned(),m_HPaned(),
    m_CirList(1),
    m_Message_Label("", Gtk::ALIGN_LEFT),
    m_pProject(NULL)
{
    /// Window Icons -------------------------------------
    std::vector<Glib::RefPtr<Gdk::Pixbuf> > pixbufs;
    Glib::RefPtr<Gdk::Pixbuf> buf;

    buf = Gdk::Pixbuf::create_from_file(App::etc_piiri_path + "/64x64.png");
    pixbufs.push_back(buf);
    buf = Gdk::Pixbuf::create_from_file(App::etc_piiri_path + "/14x14.png");
    pixbufs.push_back(buf);
    buf = Gdk::Pixbuf::create_from_file(App::etc_piiri_path + "/192x192.png");
    pixbufs.push_back(buf);

    set_icon_list(pixbufs);
    /// ---------------------------------------------------

    /// Window-Signals:
    /// Fenster Schließen, Fenster Status ändern
	signal_delete_event().connect(sigc::mem_fun(*this, &MainWindow::on_delete_event));
	signal_window_state_event().connect(sigc::mem_fun(*this, &MainWindow::on_my_window_state_event));


	/// Action-Gruppe für alle Aktionen (Toolbar / Menü) des Fensters
	refActionGroup = Gtk::ActionGroup::create();
    /// UI-Manager anlegen
	refUIManager = Gtk::UIManager::create();
	refUIManager->insert_action_group(refActionGroup);
	add_accel_group(refUIManager->get_accel_group());


	/// Datei-Menü-Aktionen erzeugen
	refActionGroup->add( Gtk::Action::create("MenuFile", App::t("_Datei") ));
	CREATE_ACTION2(MainWindow, New, Gtk::Stock::NEW,"","Neue Schaltung")
	CREATE_ACTION2(MainWindow, Open, Gtk::Stock::OPEN,"",("Schaltung öffnen"))
	CREATE_ACTION2(MainWindow, Quit, Gtk::Stock::QUIT,"",("Beenden"))
	CREATE_ACTION_KEY(MainWindow, ReloadCircuits, App::t("Palette neuladen"), ("Reload component palette."), Gtk::AccelKey("<shift>F5"))
	CREATE_ACTION2(MainWindow,     Save, Gtk::Stock::SAVE,"","Schaltung speichern.")
	CREATE_ACTION2(MainWindow,     SaveAs, Gtk::Stock::SAVE_AS,"","Schaltung unter einem bestimmten Namen speichern.")
	CREATE_ACTION2_KEY(MainWindow, Reload, Gtk::Stock::REVERT_TO_SAVED,"","Zurücksetzen zum gespeicherten Zustand.", Gtk::AccelKey("<control>r"))


	/// Projekt-Menü-Aktionen erzeugen
	refActionGroup->add( Gtk::Action::create("MenuProject", App::t("_Projekt")) );
	CREATE_ACTION(MainWindow, ProjectNew, App::t("Neues Projekt ..."),".")
	CREATE_ACTION(MainWindow, ProjectOpen, App::t("Projekt öffnen ..."),".")
	CREATE_ACTION(MainWindow, ProjectClose, App::t("Projekt schließen"),".")
	CREATE_ACTION(MainWindow, ProjectSave, App::t("Projekt speichern"),".")
	CREATE_ACTION(MainWindow, ProjectImport, App::t("Komponenten importieren ..."),".")
	refActionGroup->add( Gtk::Action::create("RecentProjectsMenu", App::t("Zuletzt geöffnete Projekte")) );


	/// Ansicht-Menü-Aktionen erzeugen
	refActionGroup->add( Gtk::Action::create("MenuAnsicht", App::t("_Ansicht")) );
	CREATE_ACTION2_KEY(MainWindow, Refresh,Gtk::Stock::REFRESH,"","Schaltung aktualisieren", Gtk::AccelKey("F5"))
	CREATE_ACTION(MainWindow,      Show, App::t("Anzeigen"),"<b>Komponente anzeigen</b>")
    CREATE_ACTION(MainWindow,      Table,App::t("Wertetabelle"),"<b>Wertetabelle anzeigen</b>")
	CREATE_ACTION2_KEY(MainWindow, ZoomIn,Gtk::Stock::ZOOM_IN,"","Anzeige vergrößern", Gtk::AccelKey('+',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(MainWindow, ZoomOut,Gtk::Stock::ZOOM_OUT,"","Anzeige verkleinern", Gtk::AccelKey('-',(Gdk::ModifierType)0,""))
	CREATE_ACTION2_KEY(MainWindow, Zoom100,Gtk::Stock::ZOOM_100,"","Normale Anzeige", Gtk::AccelKey('#',(Gdk::ModifierType)0,""))
	// Anordnung
	refActionGroup->add( Gtk::Action::create("SubMenuAnordnung", App::t("Anordnung")) );
	CREATE_ACTION2(MainWindow, Anordnung, Gtk::Stock::DND_MULTIPLE, App::t("Anordnung"), "Anordnung")
	CREATE_ACTION2(MainWindow, Up, Gtk::Stock::GO_UP, "", "")
	CREATE_ACTION2(MainWindow, Down, Gtk::Stock::GO_DOWN, "", "")
	CREATE_ACTION2(MainWindow, Topmost, Gtk::Stock::GOTO_TOP, "", "")
	CREATE_ACTION2(MainWindow, Bottommost, Gtk::Stock::GOTO_BOTTOM, "", "")


	/// Aktion-Menü-Aktionen erzeugen
	refActionGroup->add( Gtk::Action::create("MenuAction", App::t("A_ktion")) );
	CREATE_ACTION(MainWindow,      Synth,App::t("Schaltungs-Synthese")+" [BETA]","Schaltung aus logischer Funktion erzeugen.")
	CREATE_ACTION(MainWindow,      Diagram,App::t("Impulsdiagramm")+" [BETA]","Impulsdiagramm aufzeichnen.")
	CREATE_ACTION(MainWindow,      Editor,App::t("Mit Texteditor öffnen"),"Im Texteditor öffnen.")
	CREATE_ACTION2_KEY(MainWindow, Edit, Gtk::Stock::EDIT,App::t("Komponente bearbeiten"),"Schaltung der Komponente zum Bearbeiten öffnen.", Gtk::AccelKey("F4"))
	CREATE_ACTION(MainWindow,      Embed,App::t("Komponente einbetten"),"Komponente in eingebettete Komponente umwandlen.")
	CREATE_ACTION2_KEY(MainWindow, Properties, Gtk::Stock::PROPERTIES, "", "Einstellungen für die Komponente.", Gtk::AccelKey("F3"))
	CREATE_ACTION2_KEY(MainWindow, Stop, Gtk::Stock::STOP, "", "", Gtk::AccelKey("Esc"))


	/// Bearbeiten-Menü
	refActionGroup->add( Gtk::Action::create("MenuEdit", App::t("_Bearbeiten")   ) );
	CREATE_ACTION2_KEY(MainWindow, Undo, Gtk::Stock::UNDO,App::t("Rückgängig"),"Rückgängig", Gtk::AccelKey("<control>z"))

	CREATE_ACTION2_KEY(MainWindow, SelectAll,Gtk::Stock::SELECT_ALL,"","",   Gtk::AccelKey("<control>a"))
	CREATE_ACTION2_KEY(MainWindow, Cut,      Gtk::Stock::CUT,"","",   Gtk::AccelKey("<control>x"))
	CREATE_ACTION2_KEY(MainWindow, Copy,     Gtk::Stock::COPY,"","",  Gtk::AccelKey("<control>c"))
	CREATE_ACTION2_KEY(MainWindow, Paste,    Gtk::Stock::PASTE,"","", Gtk::AccelKey("<control>v"))
	CREATE_ACTION2_KEY(MainWindow, Clone,    Gtk::Stock::COPY,App::t("Duplizieren"),"Duplizieren.", Gtk::AccelKey("<control>d"))
	CREATE_ACTION2_KEY(MainWindow, Delete,   Gtk::Stock::DELETE,"","Markiertes Objekt löschen.", Gtk::AccelKey("Delete"))

	CREATE_ACTION_KEY(MainWindow, Rename, App::t("Umbenennen")+" ...","Markiertes Objekt umbenennen.", Gtk::AccelKey("F2"));
	CREATE_ACTION_KEY(MainWindow,  Node, App::t("Ecke einfügen"),"In der Mitte der Verbindung eine Eck-Komponente einfügen.", Gtk::AccelKey("<control><shift>n"))
	CREATE_ACTION_KEY(MainWindow, AddIn, App::t("Eingang hinzufügen"),"Neuen Eingang hinzufügen.", Gtk::AccelKey("<control><shift>i"))
	CREATE_ACTION_KEY(MainWindow, AddOut, App::t("Ausgang hinzufügen"),"Neuen Ausgang hinzufügen.", Gtk::AccelKey("<control><shift>o"))
	// Trennen:
	refActionGroup->add( Gtk::Action::create("Disconnect", Gtk::Stock::DISCONNECT) );
	CREATE_ACTION_KEY(MainWindow, DisconnectAll, "Alle","Alle Verbindungen vom markierten Objekt entfernen.", Gtk::AccelKey("<shift>Delete"))
	CREATE_ACTION(MainWindow, DisconnectIn, "Eingänge","tooltip")
	CREATE_ACTION(MainWindow, DisconnectOut, "Ausgänge","tooltip")
	CREATE_ACTION(MainWindow, DisconnectSel, "Gewählter Anschluss","tooltip")


	/// Hilfe-Menü
	refActionGroup->add( Gtk::Action::create("MenuHelp", App::t("_Hilfe")) );
	CREATE_ACTION2(MainWindow,      Settings, Gtk::Stock::PREFERENCES, "","Programm-Einstellungen")
	CREATE_ACTION2_KEY(MainWindow, Help, Gtk::Stock::HELP,"","", Gtk::AccelKey("F1"))
	CREATE_ACTION2(MainWindow,     Info, Gtk::Stock::INFO,"","")


	// ============================================


	/// Menüs und Toolbar definieren.
	Glib::ustring ui_info =
		"<ui>"
		"  <menubar name='MenuBar'>"
		"    <menu action='MenuFile' name='FileMenu'>"
		"      <menuitem action='New'/>"
		"      <menuitem action='Open'/>"
		"      <menuitem action='Save'/>"
		"      <menuitem action='SaveAs'/>"
		"      <menuitem action='Reload'/>"
		"      <separator/>"
		"      <menuitem action='ReloadCircuits'/>"
		"      <separator/>"
		"      <menuitem action='Settings'/>"
		"      <separator/>"
		"      <menuitem action='Quit'/>"
		"    </menu>"


		"    <menu action='MenuEdit'>"
		"      <menuitem action='Undo'/>"
		"      <separator/>"
		"      <menuitem action='Cut'/>"
		"      <menuitem action='Copy'/>"
		"      <menuitem action='Paste'/>"
		"      <menuitem action='Clone'/>"
		"      <separator/>"
		"      <menuitem action='SelectAll'/>"
		"      <menuitem action='Delete'/>"
		"      <menuitem action='Rename'/>"
		"      <menuitem action='Node'/>"
		"      <menu action='Disconnect'>"
		"        <menuitem action='DisconnectAll'/>"
		"        <menuitem action='DisconnectIn'/>"
		"        <menuitem action='DisconnectOut'/>"
		"        <menuitem action='DisconnectSel'/>"
		"      </menu>"
		"      <separator/>"
		"      <menuitem action='AddIn'/>"
		"      <menuitem action='AddOut'/>"
		"    </menu>"



		"    <menu action='MenuAction'>"
		"      <menuitem action='Show'/>"
		"      <menuitem action='Edit'/>"
		"      <menuitem action='Editor'/>"
//		"      <menuitem action='Embed'/>"
		"      <menuitem action='Synth'/>"
		"      <menuitem action='Table'/>"
		"      <menuitem action='Diagram'/>"
		"      <menuitem action='Properties'/>"
		"    </menu>"

		"    <menu action='MenuAnsicht' name='AnsichtMenu'>"
		"      <menuitem action='Refresh'/>"
		"      <separator/>"
		"      <menu action='SubMenuAnordnung'>"
        "         <menuitem action='Up'/>"
        "         <menuitem action='Down'/>"
        "         <menuitem action='Topmost'/>"
        "         <menuitem action='Bottommost'/>"
		"      </menu>"
		"      <separator/>"
		"      <menuitem action='Zoom100'/>"
		"      <menuitem action='ZoomIn'/>"
		"      <menuitem action='ZoomOut'/>"
		"    </menu>"

		"    <menu action='MenuProject' name='ProjectMenu'>"
		"      <menu name='RecentProjects' action='RecentProjectsMenu'><placeholder/></menu>"
		"      <menuitem action='ProjectOpen'/>"
		"      <menuitem action='ProjectNew'/>"
		"      <menuitem action='ProjectSave'/>"
		"      <menuitem action='ProjectClose'/>"
		"      <menuitem action='ProjectImport'/>"
		"    </menu>"


		"    <menu action='MenuHelp'>"
		"      <menuitem action='Help'/>"
		"      <menuitem action='Info'/>"
		"    </menu>"
		"  </menubar>"


		"  <toolbar  name='ToolBar'>"
		"    <toolitem action='New'/>"
		"    <toolitem action='Open'/>"
		"    <toolitem action='Save'/>"
		"    <toolitem action='SaveAs'/>"
		"    <toolitem action='Reload'/>"
		"    <toolitem action='Undo' name='Undo'/>"
		"    <separator/>"
		"    <toolitem action='ZoomIn'/>"
		"    <toolitem action='ZoomOut'/>"
		"    <toolitem action='Zoom100'/>"
		"    <separator/>"
		/*"    <toolitem action='Anordnung'/>"*/
		"    <separator expand='true'/>"
		"  </toolbar>"

		"  <popup action='SubMenuAnordnung' name='AnordnungMenu'>"
		"     <menuitem action='Up'/>"
		"     <menuitem action='Down'/>"
		"     <menuitem action='Topmost'/>"
		"     <menuitem action='Bottommost'/>"
		"  </popup>"


		"  <popup name='PopupMenu'>"
		"    <menuitem action='Show'/>"
		"    <separator/>"
		"    <menu action='Disconnect'>"
		"      <menuitem action='DisconnectAll'/>"
		"      <menuitem action='DisconnectIn'/>"
		"      <menuitem action='DisconnectOut'/>"
		"      <menuitem action='DisconnectSel'/>"
		"    </menu>"
		"    <menu action='SubMenuAnordnung'>"
        "      <menuitem action='Up'/>"
        "      <menuitem action='Down'/>"
        "      <menuitem action='Topmost'/>"
        "      <menuitem action='Bottommost'/>"
		"    </menu>"
		"    <separator/>"
		"    <menuitem action='Cut'/>"
		"    <menuitem action='Copy'/>"
		"    <menuitem action='Paste'/>"
		"    <menuitem action='Clone'/>"
		"    <separator/>"
		"    <menuitem action='Delete'/>"
		"    <menuitem action='Disconnect'/>"
		"    <menuitem action='Node'/>"
		"    <menuitem action='Rename'/>"
		"    <separator/>"
		"    <menuitem action='Properties'/>"
		"    <menuitem action='Edit'/>"
		"  </popup>"

		"  <popup name='ViewPopupMenu'>"
		"    <menuitem action='Show'/>"
		"  </popup>"
		"</ui>";

	refUIManager->add_ui_from_string(ui_info);



	/// Menü und Toolbar daraus erzeugen
	Gtk::Widget* pMenuBar=NULL;
	Gtk::Toolbar* pToolBar=NULL;
	Gtk::Frame* pFrame = NULL;


	pMenuBar = refUIManager->get_widget("/MenuBar");
	pToolBar = dynamic_cast<Gtk::Toolbar*>(refUIManager->get_widget("/ToolBar"));





	if (pToolBar)
	{
	    pToolBar->set_toolbar_style(Gtk::TOOLBAR_ICONS);
	    // ++ interessanter code, desshalb mal lassen
        //Gtk::MenuToolButton *mtb;
        //Gtk::Menu *mn;
        //mtb = new Gtk::MenuToolButton(Gtk::Stock::NEW);
        //refActionGroup->get_action("New")->connect_proxy(*mtb);
        //pToolBar->insert(*mtb, 0);
        //mn = ((Gtk::MenuItem*)refUIManager->get_widget("/MenuBar/FileMenu/NewMenu"))->get_submenu();
        //mtb->set_menu(*mn);
	}

    /// Menü der kürzlich geöffneten Projekte holen, für späteren Zugriff
    Gtk::MenuItem* pmn = dynamic_cast<Gtk::MenuItem*>(refUIManager->get_widget("/MenuBar/ProjectMenu/RecentProjects"));
    if (pmn) m_pRecentProjectsMenu = pmn->get_submenu();
    else m_pRecentProjectsMenu = NULL;


	/// Fensterkomponenten anlegen
	set_default_size(800, 600);
	set_position(Gtk::WIN_POS_CENTER);

	add(m_VBox);
	if (pMenuBar) m_VBox_Toolbar.pack_start(*pMenuBar, Gtk::PACK_EXPAND_WIDGET);
	if (pToolBar) m_VBox_Toolbar.add(*pToolBar);
	m_VBox.pack_start(m_VBox_Toolbar, Gtk::PACK_SHRINK);

    //Gtk::Image* ib_image;
    //ib_image = Gtk::manage (new Gtk::Image(Gtk::Stock::DIALOG_ERROR, Gtk::ICON_SIZE_DIALOG));
    m_InfobarImage.show();
    m_InfobarImage.set_alignment(Gtk::ALIGN_LEFT);
    m_Infobar.set_message_type(Gtk::MESSAGE_INFO);
    Gtk::Container* infoBarContainer = dynamic_cast<Gtk::Container*>(m_Infobar.get_content_area());
    if (infoBarContainer)
    {
        infoBarContainer->add(m_InfobarImage);
        infoBarContainer->add(m_Message_Label);
    }
    m_Infobar.add_button(Gtk::Stock::CLOSE, Gtk::RESPONSE_CLOSE);

    //m_Infobar.add_button(App::t("Weitere Informationen"), Gtk::RESPONSE_ACCEPT);
    m_Message_Label.signal_activate_link().connect(sigc::mem_fun(*this, &MainWindow::on_infobar_link), false);
    //m_Infobar.add_button("Mehr...", Gtk::RESPONSE_YES);
	m_VBox.pack_start(m_Infobar, Gtk::PACK_SHRINK);
	m_Infobar.signal_response().connect(sigc::mem_fun(*this, &MainWindow::on_infobar_response));


    /// Toolpalette füllen
	init_palette();

	m_VBox.pack_start(m_HPaned, Gtk::PACK_EXPAND_WIDGET);
	m_VBox.pack_start(m_Statusbar, Gtk::PACK_SHRINK);

    /// Drag & Drop einrichten
    listTargets.push_back( Gtk::TargetEntry("STRING") );
    listTargets.push_back( Gtk::TargetEntry("application/x-gtk-tool-palette-item") );

	m_CirList.drag_source_set(listTargets);
    m_CirList.signal_drag_data_get().connect(sigc::mem_fun(*this, &MainWindow::on_list_drag_data_get));



	m_ScrolledWindowPalette.set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);
	m_ToolPalette.set_size_request(200,100);
	m_ScrolledWindowPalette.add(m_ToolPalette);

	m_HPaned.pack1(m_VPaned, Gtk::SHRINK);


	m_CirList.set_size_request(-1,100);
	m_CirList.set_column_title(0, App::t("Komponenten"));
	m_CirList.signal_row_activated().connect(sigc::mem_fun(*this, &MainWindow::on_circuit_row_activated));
    m_CirList.signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::on_cirlist_button_release_event));
    m_ScrolledWindowCircuits.add(m_CirList);



    pFrame = Gtk::manage(new Gtk::Frame()); // Fram drum herum
    pFrame->set_shadow_type(Gtk::SHADOW_IN);
    pFrame->add(m_ScrolledWindowPalette);
	m_VPaned.pack1(*pFrame, Gtk::FILL); // EXPAND, FILL , SHRINK

    pFrame = Gtk::manage(new Gtk::Frame()); // Fram drum herum
    pFrame->set_shadow_type(Gtk::SHADOW_IN);
    pFrame->add(m_ScrolledWindowCircuits);
	m_VPaned.pack2(*pFrame, Gtk::FILL); // EXPAND, FILL , SHRINK



    pFrame = Gtk::manage(new Gtk::Frame());  // Fram drum herum
    pFrame->set_shadow_type(Gtk::SHADOW_IN);
    pFrame->add(m_Tabbing);
    m_HPaned.pack2(*pFrame, Gtk::EXPAND);

    m_Tabbing.signal_page_added().connect(sigc::mem_fun(*this, &MainWindow::on_notebook_added));
    m_Tabbing.signal_page_removed().connect(sigc::mem_fun(*this, &MainWindow::on_notebook_removed));

    m_Tabbing.signal_switch_page().connect(sigc::mem_fun(*this, &MainWindow::on_notebook_switch_page));

    m_Tabbing.set_scrollable();
    Gtk::Button* butt = Gtk::manage(new Gtk::Button());

    Gtk::Widget* image;
    image = Gtk::manage (new Gtk::Image(Gtk::Stock::ADD, Gtk::ICON_SIZE_BUTTON));
    image->show();
    butt->add(*image);

    butt->set_focus_on_click(false);
    butt->set_relief(Gtk::RELIEF_NONE);
    Glib::RefPtr<Gtk::RcStyle> style = Gtk::RcStyle::create();
    style->set_xthickness(0);
    style->set_ythickness(0);
    butt->modify_style(style);
    butt->show_all();
    butt->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_notebook_new));
    m_Tabbing.set_action_widget(butt, Gtk::PACK_START);
    butt->show();
    m_Tabbing.show_all_children();




	std_context = m_Statusbar.get_context_id("main");
	deque_context = m_Statusbar.get_context_id("deque");
    m_Statusbar.push("PiiRi " + App::t("Hauptfenster"), std_context);


    Gtk::ImageMenuItem *i;
    /*i = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::ADD));
    m_CirMenu.append(*i);
    i->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_cirmenu_add) );*/
    i = Gtk::manage(new Gtk::ImageMenuItem(App::t("Umbenennen")));
    m_CirMenu.append(*i);
    i->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_cirmenu_ren) );
    i = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::DELETE));
    m_CirMenu.append(*i);
    i->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_cirmenu_del) );
    i = Gtk::manage(new Gtk::ImageMenuItem(Gtk::Stock::OPEN));
    m_CirMenu.append(*i);
    i->signal_activate().connect(sigc::mem_fun(*this, &MainWindow::on_cirmenu_edi) );
    m_CirMenu.show_all();


	App::fetchError("Beim Erzeugen des Hauptfensters", *this);

	updateRecentProjectsMenu();

    updateWindowTitle();

	show_all_children();

    /// Infobar muss wieder versteckt werden
    m_Infobar.hide();

    /// nach 2 Sekunden verschiedenes erledigen
    m_MsgTimeoutConnection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::start_msg_timeout), 2000);

    /// Clipboard Changes
    Gtk::Clipboard::get()->signal_owner_change().connect(sigc::mem_fun(*this, &MainWindow::on_clipboard_owner_change) );
    update_paste_status();


    /// Vorigen Fenster-Status wiederherstellen
    if (App::m_pInternSettings->get_bool("maximized"))
    {
        maximize();
    }


    if (CircuitChild::isStepSim())
    {
        startStepTimer();
    }


}

MainWindow::~MainWindow()
{
    if (m_MsgTimeoutConnection.connected()) m_MsgTimeoutConnection.disconnect();
    App::m_ClipboardStore.clear();
}

/// ================ MainWindow Members ======================


/** @brief on_step_timer
  *
  * @todo: document this function
  */
bool MainWindow::on_step_timer()
{
    //ostringstream msg;
    if (!CircuitChild::getDeque().empty())
    {
        if (MainWindow::dequeEmpty)
        {
            //msg << "Stapel = " << CircuitChild::getDeque().size();
            m_Statusbar.pop(deque_context);
            m_Statusbar.push("Schritt wird simuliert ...", deque_context);

            MainWindow::dequeEmpty = false;
        }
        CircuitChild::step();
        CTAB(
            ct->updateAll();
        );
    }
    else
    {
        if (!MainWindow::dequeEmpty)
        {
            //msg << "Schritt fertig.";
            m_Statusbar.pop(deque_context);
            m_Statusbar.push("Schritt fertig.", deque_context);

            MainWindow::dequeEmpty = true;
        }
    }
    return true;
}

/** @brief stopStepTimer
  *
  * @todo: document this function
  */
void MainWindow::stopStepTimer()
{
    if (stepTimerConnection.connected())
    {
        stepTimerConnection.disconnect();
    }
}

/** @brief startStepTimer
  *
  * @todo: document this function
  */
void MainWindow::startStepTimer()
{
    if (CircuitChild::isStepSim() && !stepTimerConnection.connected())
    {
        stepTimerConnection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::on_step_timer), CircuitChild::steptime);
    }
}



/// ---------------- Infobar ----------------------

/** @brief open_infobar
  *
  * @todo: document this function
  */
/*void MainWindow::open_infobar(Gtk::MessageType t, std::string text, Glib::SignalProxy0< void > more, bool autohide)
{
    /// Noch nicht implementiert
    open_infobar(t, text, autohide);
}
*/
/** @brief open_infobar
  *
  * @todo: document this function
  */
void MainWindow::open_infobar(Gtk::MessageType t, std::string text, bool autohide)
{
    m_InforbarMessages.push(InfobarMessage(t,text));
    set_infobar(m_InforbarMessages.top());
}

void MainWindow::set_infobar(const InfobarMessage& ibm)
{
    //m_Infobar.set_response_sensitive(Gtk::RESPONSE_OK, false);
    m_Message_Label.set_markup(ibm.getText());
    Gtk::StockID mt;
    switch (ibm.getTyp())
    {
        case Gtk::MESSAGE_INFO:     mt = Gtk::Stock::DIALOG_INFO;     break;
        case Gtk::MESSAGE_ERROR:    mt = Gtk::Stock::DIALOG_ERROR;    break;
        case Gtk::MESSAGE_QUESTION: mt = Gtk::Stock::DIALOG_QUESTION; break;
        case Gtk::MESSAGE_WARNING:  mt = Gtk::Stock::DIALOG_WARNING;  break;
        case Gtk::MESSAGE_OTHER:    mt = Gtk::Stock::DIALOG_INFO;     break;
    }

    m_InfobarImage.set(mt, Gtk::ICON_SIZE_BUTTON);
    m_Infobar.show();
}
/** @brief on_infobar_link
  *
  * @todo: document this function
  */
bool MainWindow::on_infobar_link(const Glib::ustring& uri)
{
    if ((uri.length()>6) && (uri.substr(0,6) == "piiri:"))
    {
        Glib::ustring command(uri.substr(6));
        std::cout << "uri-click: " << command << std::endl;
        if (command == "new-project")
        {
            on_infobar_close();
            on_action_ProjectNew();
            return true;
        }
        else if (command == "example")
        {
            on_infobar_close();
            on_action_Open();
            return true;
        }
        else if (command == "help:circ")
        {
            on_infobar_close();
            open_infobar(Gtk::MESSAGE_INFO,
                         "Eine Schaltung muss nach einer kurzen Simulationszeit einen stabilen Zustand erreichen.\n"
                         "Dieses Problem entsteht, wenn eine zirkuläre Schaltung sich nicht 'einpendelt', sondern\n"
                         "bei jedem Durchlauf den Zustand ändert.");
            return true;
        }

        return false;
    }


    #ifdef _WIN32
    ShellExecute(NULL, "open", uri.c_str(), NULL, NULL, SW_SHOWNORMAL);
    return true;
    #else
/*
GdkScreen *screen;
GError *error;

if (gtk_widget_has_screen (widget))
screen = gtk_widget_get_screen (widget);
else
screen = gdk_screen_get_default ();

error = NULL;
gtk_show_uri (screen, " http://google.com" ;,
gtk_get_current_event_time (),
&error);

*/
    return gtk_show_uri(NULL, uri.c_str(), gtk_get_current_event_time(), NULL);
    #endif
}



/** @brief on_infobar_close
  *
  * Beim Schließen wird das oberste element entfernt und
  * das nächste angezeigt.
  * ist der stapel leer, so wird der Infobar geschlossen.
  */
void MainWindow::on_infobar_close()
{
    if (!m_InforbarMessages.empty())
    {
        m_InforbarMessages.pop();
    }

    if (m_InforbarMessages.empty())
    {
        m_Infobar.hide();
    }
    else
    {
        set_infobar(m_InforbarMessages.top());
    }
}

/** @brief on_infobar_response
  *
  * @todo: document this function
  */
void MainWindow::on_infobar_response(int r)
{
    if (r == Gtk::RESPONSE_CLOSE)
        on_infobar_close();
}

/// ----------------  ----------------------


/** @brief start_msg_timeout
  *
  * @todo: document this function
  */
bool MainWindow::start_msg_timeout()
{
    if (App::m_pInternSettings)
    {
        std::string ver(App::m_pInternSettings->get_string("current_version"));
        if (ver != App::version)
        {
            open_infobar(Gtk::MESSAGE_INFO, "<big>" + App::t("Willkommen bei PiiRi") + " " + App::version + "</big>\n" +
                         App::t("Vermutlich haben Sie") + " <b>PiiRi " + App::version + "</b>" +
                         App::t(" zum ersten Mal gestartet.") + " " +
                         App::t("Bitte informieren Sie sich über") + " <a href='http://stefan.xornet.de/interessen/computer/piiri/#changelog'>"+App::t("wichtige Änderungen")+"</a>.\n\n" +
                         App::t("Vorgeschlagene Aktionen:") + " <a href='piiri:new-project'>"+App::t("Neues Projekt")+"</a> | <a href='piiri:example'>"+App::t("Beispiel öffnen")+"</a>"
                         );
            App::m_pInternSettings->set_string("current_version", App::version);
            App::m_pInternSettings->set_int("start_times", 0);
        }

        if (App::settings.get_bool("update_msg")) {
            // http://developer.gnome.org/glibmm/2.28/classGio_1_1SocketClient.html#adc739baf06362ca1d8b9085c29824cd8
            // http://developer.gnome.org/glibmm/2.28/classGio_1_1IOStream.html#ac15dc0ec5596a9bb5ee174ea70a2b59d
            // https://live.gnome.org/Vala/GIONetworkingSample
            Glib::RefPtr< Gio::SocketClient> sockc = Gio::SocketClient::create();
            //sockc->set_timeout(5);
            Glib::RefPtr<Gio::SocketConnection> scon = sockc->connect_to_host("beyer.xornet.de", 80);
            if (scon)
            {
                Glib::RefPtr<Gio::OutputStream> os = scon->get_output_stream();
                if (os)
                {
                    std::string http("GET /piiri.version HTTP/1.1\r\nHost: beyer.xornet.de\r\nConnection: close\r\n\r\n");
                    os->write(http);
                    os->flush();
                    os->close();
                }
                else
                {
                    std::cerr << "Kein OutStream" << std::endl;
                }

                Glib::RefPtr<Gio::InputStream> is = scon->get_input_stream();
                if (is)
                {
                    Glib::RefPtr< Gio::DataInputStream > dis = Gio::DataInputStream::create(is);
                    //Gio::DataStreamNewlineType dsnlt = dis->get_newline_type();
                    //char lbch = 13;
                    //if (dsnlt == Gio::DATA_STREAM_NEWLINE_TYPE_LF) lbch = 10;
                    std::string str, msg;
                    bool head = true;

                    //while (dis->read_line(str)) {if (str[0] == lbch) break;};
                    while (dis->read_line(str))
                    {
                        trim_inplace(str);
                        //std::cout << "[" << str << "]" << std::endl;

                        if (head) {
                            if (str == "") head = false;
                        } else {
                            msg += str+"\n";
                        }
                    }
                    is->close();

                    if (!msg.empty()) {
                        open_infobar(Gtk::MESSAGE_INFO, "<big>Neuigkeiten:</big>\n" + msg + "\n<i>Sie können diese Meldungen in den Einstellungen deaktivieren.</i>");
                        //App::message("Neuigkeiten", msg, "Sie können diese Meldungen in den Einstellungen deaktivieren.",*this);
                    }
                }
                else
                {
                    std::cerr << "Kein InStream" << std::endl;
                }

                scon->close();
            }
            else
            {
                std::cerr << "Keine Verbindung" << std::endl;
            }
            // piiri.version
            App::fetchError("piiri.version", *this);
        } // update msg


        int st = App::m_pInternSettings->get_int("start_times");
        std::cout << App::t("Programm zum") << " " << st << ". " + App::t("Mal gestartet.") << std::endl;
        int n = (st<=50)?10:100;
        if (((st % n) <= 1) && (st>1))
        {
            open_infobar(Gtk::MESSAGE_INFO, App::t("Vielen Dank, dass Sie PiiRi benutzen. Bitte erzählen Sie mir von Ihren Eindrücken:") + " <a href='mailto:piiri@xornet.de'>piiri@xornet.de</a>.");
        }
        //else std::cout << "nope" << std::endl;
    }
    return false;
}

/// ---------------- Fenster-Signale ----------------------

bool MainWindow::on_delete_event(GdkEventAny* event)
{
	on_action_Quit();
	return true;
}

/** @brief on_my_window_state_event
  *
  * @todo: document this function
  */
bool MainWindow::on_my_window_state_event(GdkEventWindowState* event)
{
    if (event->changed_mask & GDK_WINDOW_STATE_MAXIMIZED)
    {
        if (event->new_window_state & GDK_WINDOW_STATE_MAXIMIZED)
        {
            App::m_pInternSettings->set_bool("maximized", true);
        }
        else
        {
            App::m_pInternSettings->set_bool("maximized", false);
        }
    }

/*
typedef enum {
  GDK_WINDOW_STATE_WITHDRAWN  = 1 << 0,
  GDK_WINDOW_STATE_ICONIFIED  = 1 << 1,
  GDK_WINDOW_STATE_MAXIMIZED  = 1 << 2,
  GDK_WINDOW_STATE_STICKY     = 1 << 3,
  GDK_WINDOW_STATE_FULLSCREEN = 1 << 4,
  GDK_WINDOW_STATE_ABOVE      = 1 << 5,
  GDK_WINDOW_STATE_BELOW      = 1 << 6
} GdkWindowState;
*/
    return true;
}


/// ---------------- Notebook ----------------------

void MainWindow::on_notebook_added(Widget* page, guint page_num)
{
    CircuitTab* ct = dynamic_cast<CircuitTab*>(page);

    m_ToolPalette.add_drag_dest(ct->getCView(), Gtk::DEST_DEFAULT_HIGHLIGHT, Gtk::TOOL_PALETTE_DRAG_ITEMS, Gdk::ACTION_COPY);
    ct->getCView().drag_dest_set(listTargets);

    ct->getCView().signal_drag_data_received().connect(sigc::mem_fun(*this, &MainWindow::on_drag_received) );

	// signale fürs Markieren (Maus).
	ct->getCView().signal_button_release_event().connect(sigc::mem_fun(*this, &MainWindow::on_popup_menu));
	ct->getCView().signal_button_press_event().connect(sigc::mem_fun(*this, &MainWindow::on_doubleclick));
	// wird aufgerufen, wenn die Auswahl sich geändert hat: dann müssen die verfügbaren Aktionen gewählt werden.
	ct->getCView().signal_selection_changed().connect(sigc::mem_fun(*this, &MainWindow::on_selection_changed));

    update_paste_status();

    //cout << "on_notebook_added" << endl;
}

void MainWindow::on_notebook_removed(Widget* page, guint page_num)
{
    //cout << "on_notebook_removed" << endl;
    //if (m_Tabbing.get_n_pages() == 0)
    updateUI();
    update_paste_status();
}

void MainWindow::on_notebook_switch_page(GtkNotebookPage* page, guint page_num)
{
    CTAB(
        ct->tabActivate();
    );

    updateUI();
}

void MainWindow::on_notebook_close(CircuitTab* ct)
{
    closeTab(ct);
    updateUI();
}

bool MainWindow::closeTab(CircuitTab* ct)
{
    int num = m_Tabbing.page_num(*ct);
    if (num == -1) return false;

    if (ct->askSave())
    {
        m_Tabbing.remove_page(num);
        delete ct;
        return true;
    }
    return false;
}


bool MainWindow::closeAllTabs()
{
    int numtabs = m_Tabbing.get_n_pages();
    for (int i=numtabs-1; i>=0; i--)
    {
        CircuitTab *ct = dynamic_cast<CircuitTab *>(m_Tabbing.get_nth_page(i));
        if (ct)
        {
            if (!closeTab(ct))
            {
                return false;
            }
        }
    }
    return true;
}


void MainWindow::on_notebook_new()
{
    openCircuit("");
}


/// ----------------  ----------------------



/**
 * Tool-Palette füllen.
 */
void MainWindow::init_palette()
{
	// Pointer für alle Aktionen
	Gtk::ToolButton* button;
	Gtk::ToolItem* item;

	// Gruppen erstellen
	Gtk::ToolItemGroup* group_io = Gtk::manage(new Gtk::ToolItemGroup(App::t("Ein- / Ausgang")));
	m_ToolPalette.add(*group_io);
	group_io->set_collapsed(true);
	group_io->set_header_relief(Gtk::RELIEF_NONE);

	Gtk::ToolItemGroup* group_gatter = Gtk::manage(new Gtk::ToolItemGroup(App::t("Logik-Gatter")));
	m_ToolPalette.add(*group_gatter);
	group_gatter->set_collapsed(true);
	group_gatter->set_header_relief(Gtk::RELIEF_NONE);

	Gtk::ToolItemGroup* group_special = Gtk::manage(new Gtk::ToolItemGroup(App::t("Spezial-Komponenten")));
	m_ToolPalette.add(*group_special);
	group_special->set_collapsed(true);
	group_special->set_header_relief(Gtk::RELIEF_NONE);

	/*group_cir = Gtk::manage(new Gtk::ToolItemGroup("Schaltungen"));
	m_ToolPalette.add(*group_cir);
	group_cir->set_collapsed(true);
	group_cir->set_header_relief(Gtk::RELIEF_NONE);*/


	// Toolpalette füllen

	ADD_TO_PALETTE_IO(in, "x", FALSE);
	ADD_TO_PALETTE_IO(out, "y", TRUE);



	addToGatterPalette("and",group_gatter);
	addToGatterPalette("or",group_gatter);
	addToGatterPalette("nand",group_gatter);
	addToGatterPalette("nor",group_gatter);
	addToGatterPalette("xor",group_gatter);
	addToGatterPalette("not",group_gatter);


	addToSpecialPalette(App::t("Ecke"),                  "node",     App::t("Um eine Leitung um die Ecke zu führen."), group_special);
	addToSpecialPalette(App::t("Adapter"),               "adapter",  App::t("Adapter zwischen Einzel- und Mehrfachleitungen."), group_special);
	addToSpecialPalette(App::t("7-Segment-Anzeige"),     "digit2",   App::t("Jedes Segment wird einzeln angesteuert (7 Eingänge)."), group_special);
	addToSpecialPalette(App::t("7-Segment-Anzeige"),     "digit",    App::t("Mit 4-Bit-Decoder: Als Eingang dient eine 4 Bit breite Binärzahl."), group_special);
	addToSpecialPalette(App::t("Buchstaben-Anzeige"),    "letter",   App::t("ASCII-Code Decoder."), group_special);
	addToSpecialPalette(App::t("Taktgeber"),             "clock",    App::t("Intervall ist über Eigenschaften einstellbar."), group_special);
	addToSpecialPalette(App::t("Konstante Eins"),        "one",      "", group_special);
	addToSpecialPalette(App::t("Konstante Null"),        "zero",     App::t("Hinweis")+": "+App::t("Ein offener Eingang liefert i.d.R. auch eine logische Null."), group_special);
	addToSpecialPalette(App::t("Schalter"),              "switch",   App::t("Anzahl der Schalter ist über die Eigenschaften einstellbar."), group_special);

	addToSpecialPalette(App::t("LED"),                   "led",      App::t("Die Farbe ist über die Eigenschaften einstellbar: rot, violett, blau, grün, gelb, orange."), group_special);
	addToSpecialPalette(App::t("Text"),                  "text",     App::t("Text und Schriftgröße sind über die Eigenschaften einstellbar."), group_special);
	addToSpecialPalette(App::t("Bitanzeigen"),           "indicator",App::t("Die Anzahl der Anschlüsse ist über die Eigenschaften einstellbar."), group_special);
    addToSpecialPalette(App::t("Eingebettete Schaltung"),"container",App::t("Eine integrierte Schaltung direkt in diese Schaltung einbetten. Für diese Komponente muss dann keine Datei geladen werden."), group_special);

	if (App::settings.get_bool_path("palette/function_component"))
	{
        addToSpecialPalette(App::t("Logische Funktion"), "function", App::t("Funktionsterm ist über die Eigenschaften einstellbar: &amp;(and) |(or) !(not) ^(xor) und Klammersetzung."), group_special);
	}
	if (App::settings.get_bool_path("palette/ram_component"))
	{
        addToSpecialPalette(App::t("Speicher"),          "ram",      "", group_special);
	}
	if (App::settings.get_bool_path("palette/iow_component"))
	{
        addToSpecialPalette(App::t("IOWarrior Schnittstelle"),"iow", App::t("Für den IOWarrior 40 von <i>Code Mercenaries</i>."), group_special);
	}

	//addToSpecialPalette("RS-FF","rs","<b>RS-FlipFlop</b>\nDiese Komponente ist nur verfügbar, wenn sie im cir-Ordner als rs.cir liegt.", group_special);

	App::fetchError("Beim Erzeugen der Palette", *this);


	on_action_ReloadCircuits();



}

static bool sort_predicate(const string& a, const string& b)
{
  return a < b;
}





void MainWindow::on_action_ReloadCircuits()
{
    reloadCircuits();
}


/** @brief reloadCircuits
  *
  * @todo: document this function
  */
void MainWindow::reloadCircuits()
{
    // http://developer.gnome.org/gtkmm-tutorial/unstable/sec-treeview-examples.html.de


    m_CirList.clear_items();

	DIR *dp;
	struct dirent *ep;
	string cir;
	typedef std::vector<string> cirs_items;
	cirs_items cirs;

	dp = opendir (App::cir_path.c_str());
	if (dp != NULL)
	{
		while ( (ep = readdir(dp)) ) {
			cir = ep->d_name;
			if((ep->d_name[0] == '.') || (cir.length()<=4) || !(cir.substr(cir.length()-4, 4) == ".cir")) continue; // dot- und Fremd-Files überspringen
			cirs.push_back(cir);
		}
		closedir(dp);
	}

	if (cirs.size() > 0)
	{

        std::sort(cirs.begin(), cirs.end(), &sort_predicate);

        cirs_items::iterator it2;
        for (it2 = cirs.begin(); it2 != cirs.end(); it2++)
        {
            cir = (*it2);
            //cout << cir << endl;
            cir = cir.substr(0, cir.length()-4);
            //addToCirPalette(cir, cir, cir, group_cir);  // label id tip   // TODO namen und beschreibung
            //ADD_TO_PALETTE_CIR(cir.substr(0,8), cir, cir)  // label id tip   // TODO namen und beschreibung
            //button->set_label(); // nur 8 Zeichen anzeigen
            m_CirList.append_text(cir);
        }
	}
	else
	{
        //addToCirPalette("","","", group_cir);
	}


	//group_cir->show_all_children();

	App::fetchError("Beim Laden der Schaltungspalette", *this);
}





/**
 * Action-Handler für Umbenennen.
 */
void MainWindow::on_action_Rename()
{
    CTAB(ct->command_rename();)
	App::fetchError("Beim Ändern des Namens", *this);
}

/**
 * Action-Handler für Löschen.
 */
void MainWindow::on_action_Delete()
{
    CTAB(ct->command_delete();)
	App::fetchError("Beim Löschen", *this);
}

/**
 * Action-Handler für Trennen.
 */
void MainWindow::on_action_DisconnectAll()
{
    CTAB(ct->command_disconnect(CircuitTab::DA_ALL);)
	App::fetchError("Beim Trennen", *this);
}
void MainWindow::on_action_DisconnectIn()
{
    CTAB(ct->command_disconnect(CircuitTab::DA_IN);)
	App::fetchError("Beim Trennen", *this);
}
void MainWindow::on_action_DisconnectOut()
{
    CTAB(ct->command_disconnect(CircuitTab::DA_OUT);)
	App::fetchError("Beim Trennen", *this);
}
void MainWindow::on_action_DisconnectSel()
{
    CTAB(ct->command_disconnect(CircuitTab::DA_SELECTED);)
	App::fetchError("Beim Trennen", *this);
}

/**
 * Action-Handler für Schaltung der Komponente bearbeiten.
 */
void MainWindow::on_action_Edit()
{
    CTAB(

	CircuitContainer* pobj = dynamic_cast<CircuitContainer*>(ct->getSelectedObject());
    if (pobj)
    {
        if (!pobj->getFilename().empty())
        {
            string s;
            s = pobj->getFilename();
            openCircuit(s);
        }
        else if (pobj->getCirName() == "container")
        {
            ct->openContainerView((EditableCircuit*)pobj);
        }
    }
    else
    {
        App::message("Bearbeiten nicht möglich","Logische Gatter und andere interne Komponenten\nkönnen nicht bearbeitet werden.","", *this);
    }

    ) // current tab

	App::fetchError("Beim Bearbeiten", *this);

}

/** @brief on_action_Editor
  *
  * @todo: document this function
  */
void MainWindow::on_action_Editor()
{
    CTAB(
        std::string editor(App::settings.get_string("editor"));
        if (editor.empty())
        {
            App::message("Texteditor", "Bitte geben Sie unter Menü-->Datei-->Einstellungen den zu verwendenden Texteditor an.", "", *this);
        }
        else
        {
            std::string fn(ct->getTLCircuit().getFilename());
            if (fn.empty())
            {
                App::message("Texteditor", "Keine Datei zum Öffnen.", "Schaltung muss erst gespeichert werden.", *this);
            }
            else
            {
                if (App::startProgramm(editor, fn))
                {
                    App::message("Texteditor", "Konnte nicht gestartet werden...", "Programm "+editor, *this);
                }
            }
        }
    )
}



/**
 * Action-Handler für Komponenten Einstellungen
 *
 */
void MainWindow::on_action_Properties()
{
    CTAB(ct->command_properties();)
	App::fetchError("Beim Einstellungen Ändern", *this);
}




/**
 * Action-Handler für Hilfe
 */
void MainWindow::on_action_Help()
{
    string keys, path, key;
    vector<Glib::RefPtr<Gtk::Action> > al;
    al = refActionGroup->get_actions();
    std::vector<Glib::RefPtr<Gtk::Action> >::iterator it;
    Gtk::AccelKey ac;

    Gtk::Dialog d("Tastenkürzel", *this, true);
    d.add_button(Gtk::Stock::CLOSE, Gtk::BUTTONS_CLOSE);
    Gtk::ListViewText lvt(2);
    lvt.set_column_title(0, "Befehl");
    lvt.set_column_title(1, "Kürzel");
    int nr;

    for (it = al.begin(); it != al.end(); it++)
    {
        path = (*it)->get_accel_path();
        // string Gtk::AccelGroup::name(guint  	accelerator_key, Gdk::ModifierType accelerator_mods);
        if (Gtk::AccelMap::lookup_entry(path, ac))
        {
            key = ac.get_abbrev();
            if (!key.empty())
            {
                if (key == "plus")
                {
                    key="+";
                }
                else if (key == "minus")
                {
                    key="-";
                }
                else if (key == "numbersign")
                {
                    key="#";
                }

                nr = lvt.append_text(path);
                lvt.set_text(nr,1,key);
            }
        }
    }

    d.get_vbox()->add(lvt);
    d.show_all_children();
    d.run();
}

void MainWindow::on_action_Settings()
{

    //App::settings.dump();

    PropertyDialog2 d("Einstellungen", *this, App::settings);

    App::provideUiText(d);

    if (d.run() == Gtk::RESPONSE_OK)
    {
        try
        {
            if (d.hasChanged())
            {
                d.writeback();
                App::saveSettings();
                App::updateLiveSettings();
            }
        }
        catch (std::string& s)
        {
            std::cerr << s << std::endl;
            std::cout << s << std::endl;
        }
    }
    App::fetchError("Einstellungen Speichern", *this);
}

/**
 * Action-Handler für Info
 */
void MainWindow::on_action_Info()
{
	Gtk::MessageDialog dialog(*this, "<span size='xx-large'>" + App::name + "</span>",
		true /* use_markup */, Gtk::MESSAGE_INFO,
		Gtk::BUTTONS_OK);

	dialog.set_secondary_text(
"<b>Didaktische Schaltungssimulation</b>\n\
\n\
© 2012 <a href='http://stefan.xornet.de/'>Stefan Beyer</a> GNU GPL v3 - keine Garantie.\n\
Kontakt: <a href='mailto:piiri@xornet.de'>piiri@xornet.de</a>.\n\
Weitere Infos: <a href=\"http://beyer.xornet.de/interessen/computer/piiri/\">beyer.xornet.de/interessen/computer/piiri/</a>.\n\
\n\
Version " + App::version,
true);

    // Links in labels mit callback verklüpfen
    sigc::connection c1, c2;
    Gtk::VBox* b = dialog.get_message_area();
    if (b)
    {
        std::vector<Gtk::Widget*> ch = b->get_children();
        Gtk::Label* l;
        l = dynamic_cast<Gtk::Label*>(ch.back());
        if (l) c1 = l->signal_activate_link().connect(sigc::mem_fun(*this, &MainWindow::on_infobar_link), false);
        l = dynamic_cast<Gtk::Label*>(ch.front());
        if (l) c2 = l->signal_activate_link().connect(sigc::mem_fun(*this, &MainWindow::on_infobar_link), false);
    }

    //int result =
    dialog.run();

    if (c1.connected()) c1.disconnect();
    if (c2.connected()) c2.disconnect();

	App::fetchError("Beim Info Anzeigen", *this);
}



/**
 * Action-Handler für Ecke Einfügen.
 */
void MainWindow::on_action_Node()
{
    CTAB(ct->command_node();)
	App::fetchError("Beim Einfügen einer Ecke", *this);
}

void MainWindow::on_action_AddIn()
{
    on_palette_io_click("in");
}
void MainWindow::on_action_AddOut()
{
    on_palette_io_click("out");
}

// http://grooveshark.com/s/Chinese/3PtunT?src=5


/**
 * Action-Handler für Schaltung Öffnen.
 */
void MainWindow::on_action_Open()
{
    Gtk::FileChooserDialog dialog("Schaltung öffnen...", Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);
    dialog.set_current_folder(App::cir_path);

    //Add response buttons the the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    //Add filters, so that only certain file types can be selected:
    Gtk::FileFilter filter_cir;
    filter_cir.set_name("Schaltungen (*.cir)");
    filter_cir.add_pattern("*.cir");
    dialog.add_filter(filter_cir);

    //Show the dialog and wait for a user response:
    int result;
    result = dialog.run();
    dialog.hide();

	if (result == Gtk::RESPONSE_OK)
	{
		{
			//Notice that this is a std::string, not a Glib::ustring.
			std::string filename = dialog.get_filename();

			//if (!(filename.substr(filename.length()-4, 4) == ".cir")) filename = filename + ".cir";
            //std::cout << "open " << filename << std::endl;
			openCircuit(filename);
		}
	} // askSave

	App::fetchError("Beim Öffnen einer Schaltung", *this);
}



CircuitTab * MainWindow::openCircuit(const string& fn, const std::string container_id)
{
    CircuitTab *ptab = NULL;

    //std::cout << "openCircuit " << fn << std::endl;
    int n=0;


    if (m_Tabbing.get_n_pages() > 0)
    {
        if (!fn.empty()) // für neuen tab (fn="") ist das uninteressant
        {
            // Schaltung schon geöffnet? Dann Tab aktivieren
            std::vector<CircuitTab*> tabs = getOpenTabs();
            for (std::vector<CircuitTab*>::iterator it = tabs.begin(); it != tabs.end(); it++)
            {
                if ((*it)->getTLCircuit().getFilename() == fn)
                {
                    ptab = (*it);
                    m_Tabbing.set_current_page(n);
                    return ptab;
                }
                n++;
            }
        }

        // letzten tab überprüfen: wenn er neu und unberührt ist: weg
        ptab = dynamic_cast<CircuitTab *>(m_Tabbing.get_nth_page(m_Tabbing.get_n_pages()-1));
        if (ptab && ptab->isNew()) closeTab(ptab);
    }


    Gtk::HBox* label = Gtk::manage(new Gtk::HBox);
    Gtk::Label* text = Gtk::manage(new Gtk::Label(App::t("Neue Schaltung")));
    Gtk::Button* butt = Gtk::manage(new Gtk::Button());

    label->pack_start(*text);

    Gtk::Widget* image;
    image = Gtk::manage (new Gtk::Image(Gtk::Stock::CLOSE, Gtk::ICON_SIZE_MENU));
    image->show();
    butt->add(*image);
    butt->set_focus_on_click(false);
    butt->set_relief(Gtk::RELIEF_NONE);
    Glib::RefPtr<Gtk::RcStyle> style = Gtk::RcStyle::create();
    style->set_xthickness(0);
    style->set_ythickness(0);
    butt->modify_style(style);
    //butt->set_size_request(16,16);

    label->pack_start(*butt, false, false, 0);
    label->show_all();

    // Tab inhalt
	ptab = new CircuitTab(text, refUIManager);

    // Close-Signal wird mit Tab-Pointer gebunden
    butt->signal_clicked().connect(sigc::bind<CircuitTab*>(sigc::mem_fun(*this, &MainWindow::on_notebook_close), ptab));


    n = m_Tabbing.append_page(*ptab, *label);



    m_Tabbing.show_all_children();
    m_Tabbing.set_current_page(n);
    //bool b; = true;
    ptab->command_open(fn);

	App::fetchError("Beim Öffnen einer Schaltung (openCircuit)", *this);


    if (m_pProject) m_pProject->save();
    App::updateLock();


	return ptab;
}




/**
 * Action-Handler für neue Schaltung.
 */
void MainWindow::on_action_New()
{
    openCircuit("");
}





/**
 * Action-Handler für Beenden.
 */
void MainWindow::on_action_Quit()
{
	if (closeProject())
	{
		hide();
	}
}








void MainWindow::on_action_Synth()
{
    LogicSynth ls(*this);
    if (ls.run() == Gtk::RESPONSE_OK)
    {
        // current oder neuer Tab? - Frage??? -> undo?
        CircuitTab *ct = openCircuit("");
        ls.execute(ct->getEdCircuit());
        ct->changed(true);
        ct->updateAll();
    }
}

void MainWindow::on_action_Diagram()
{
    if (CircuitChild::isNormalSim())
    {
        CTAB(ct->command_diagram();)
        App::fetchError("Diagramm-Fenster", *this);
    }
    else
    {
        open_infobar(Gtk::MESSAGE_WARNING, "Impulsdiagramme können nur mit der Normal-Simulation erstellt werden.");
    }
}

void MainWindow::on_action_Stop()
{
    cout << "Stop" << endl;
}

/**
 * Klick auf Toolpalette - In/Out zur Schaltung hinzufügen.
 */
void MainWindow::on_palette_io_click(const string& gatter, Gtk::ToolItem* item /*unused*/)
{

	string prae;
	string name;

	bool in = (gatter == "in");

    prae = in ? entry_in->get_text() : entry_out->get_text();

    CTAB(
        ct->command_addIO(in ? IO_TYPE_IN : IO_TYPE_OUT, prae);
    )
	App::fetchError("Beim Hinzufügen eines Anschlusses", *this);
}



void MainWindow::on_action_recent()
{
    cout << "on_action_recent"<< endl;
	// http://developer.gnome.org/gtkmm/2.24/classGtk_1_1RecentManager-members.html
	// in 2.4 gibt das noch nicht:
	//cout << recent_manager->get_current_uri() << endl;
	//cout << recent_manager->get_current_uri() << endl;

	//std::vector<Glib::RefPtr<Gtk::RecentInfo>> items;
	//for (std::vector<Glib::RefPtr<Gtk::RecentInfo>> it = recent_manager->get_items().begin();
	//it != recent_manager->get_items().end();
	//it++)
	//{
	//}
	/**
	 *
struct GtkRecentData {
  gchar *display_name;
  gchar *description;

  gchar *mime_type;

  gchar *app_name;
  gchar *app_exec;

  gchar **groups;

  gboolean is_private;
};
	 */
	// zur zeit deaktiviert
	/*
	GList *pl;
	pl = gtk_recent_manager_get_items(recent_manager->gobj());
	GList *plf = g_list_first(pl);
	GtkRecentInfo *pri;

	Glib::RefPtr<Gtk::RecentInfo> ri;

	while(plf = g_list_next(plf))
	{
		pri = (GtkRecentInfo*)plf->data;
		ri = Glib::wrap(pri);
		cout << ri->get_uri() << endl;
	}
	*/
}




void MainWindow::on_circuit_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{
    on_cirmenu_edi();
}

/** @brief on_list_drag_data_get
  *
  * @todo: document this function
  */
void MainWindow::on_list_drag_data_get(const Glib::RefPtr<Gdk::DragContext>&, Gtk::SelectionData& selection_data, guint, guint)
{
    //cout << "on_list_drag_data_get" << endl;
    selection_data.set_text(getCirListSelection());
}





void MainWindow::on_drag_received(const Glib::RefPtr<Gdk::DragContext>& context, int x, int y, const Gtk::SelectionData& selection_data, guint info, guint time)
{
    //cout << "on_drag_data_received" << endl;

    string target = selection_data.get_target();
    string id;

    if (target == "application/x-gtk-tool-palette-item")
    {
        Gtk::ToolItem* drag_item = 0;
        drag_item = m_ToolPalette.get_drag_item(selection_data);

        // Create a drop indicator when a tool button was found:
        Gtk::ToolButton* button = dynamic_cast<Gtk::ToolButton*>(drag_item);
        if(!button) return;
        if(button->get_label() == "in") return;
        if(button->get_label() == "out") return;

        id = button->get_label();
    }
    else if (target == "STRING")
    {
        id = selection_data.get_text();
    }


    if (!id.empty())
    {
        CircuitTab* ct = getCurrentTab();
        if (!ct) return;
        ct->componentDrop(id, x,y);

        context->drag_finish(true, false, time);
    }
    App::fetchError("Beim Gatter aus Palette Hinzufügen (DnD)", *this);
    //Gtk::DrawingArea::on_drag_data_received(context, x, y, selection_data, info, time);

}

CircuitWidget* MainWindow::getCurrentWidget()
{
    CircuitTab* ct = getCurrentTab();
    if (!ct) return NULL;
    return &ct->getCView();
}

CircuitTab* MainWindow::getCurrentTab()
{
    int n = m_Tabbing.get_current_page();
    if (n == -1) return NULL;
    CircuitTab* ct = dynamic_cast<CircuitTab*>(m_Tabbing.get_nth_page(n));
    return ct;
}

ToplevelCircuit* MainWindow::getCurrentCircuit()
{
    CircuitWidget* cw = getCurrentWidget();
    if (!cw) return NULL;
    return (ToplevelCircuit *)cw->getVisual()->getObj();
}




/**
Popup für Schalungs-Liste
*/
bool MainWindow::on_cirlist_button_release_event(GdkEventButton* event)
{
    if (event->button == 3)
    {
        m_CirMenu.popup(event->button, event->time);
        return true;
    }
    return false;
}

void MainWindow::on_cirmenu_add()
{
    string cir(getCirListSelection());
/*TODO*/
//on_palette_gatter_click(cir);
}

void MainWindow::on_cirmenu_ren()
{
    string cir(getCirListSelection());

	Gtk::Dialog dialog("Schaltung umbenennen", *this, true, true);
	Gtk::VBox* vb = dialog.get_vbox();
	Gtk::Entry entry;
    string fn_old = App::getCirFilename(cir);
	Gtk::Label l;
    l.set_markup("Geben Sie den neuen Namen ein:\n\n\(Diese Datei wird umbenannt:\n<i>"+fn_old+"</i>)");

	entry.set_text(cir);

	vb->pack_start(l, Gtk::PACK_EXPAND_WIDGET);
	vb->pack_start(entry, Gtk::PACK_EXPAND_WIDGET);

	dialog.add_button(Gtk::Stock::APPLY, Gtk::RESPONSE_OK);
	dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	dialog.show_all_children();

	// Enter in Textfeld triggert Dialog-Antwort
	entry.signal_activate().connect(sigc::bind(
		sigc::mem_fun(&dialog, &Gtk::Dialog::response),
	Gtk::RESPONSE_OK));


	dialog.set_default_response(Gtk::RESPONSE_OK);
	// TODO stimmt das?
	bool ret = false;
	int ok;
	while (!ret && ((ok = dialog.run()) == Gtk::RESPONSE_OK))
	{
	    string fn_new = App::getCirFilename(entry.get_text());
        ret = FileNotExists(fn_new);
        if (ret)
        {
            if (g_rename(fn_old.c_str(), fn_new.c_str())!=0)
            {
                App::error("Fehler","Datei konnte nicht umbenannt werden.","", *this);
            }
        }
        else
        {
            App::error("Fehler","Name schon vorhanden.","", *this);
        }
	}
    on_action_ReloadCircuits();
	App::fetchError("Beim Ändern des Namens einer Schaltung", *this);
}

void MainWindow::on_cirmenu_del()
{
    string cir(getCirListSelection());
    string fn = App::getCirFilename(cir);
    if (App::question("Schaltung löschen?","Soll die Schaltung <b>" + cir + "</b> wirklich gelöscht werden?\n\nDabei wird folgende Datei von der Festplatte gelöscht:",fn))
    {
        g_unlink(fn.c_str());
        on_action_ReloadCircuits();
    }
}

void MainWindow::on_cirmenu_edi()
{
    string cir(getCirListSelection());
    openCircuit(App::getCirFilename(cir));
    //string s(App::getCirFilename(cir));
    //m_signal_open.emit(s, false);
}


string MainWindow::getCirListSelection()
{
    std::vector<int> selvec = m_CirList.get_selected();
    string s("");
    if (selvec.size()>0)
    {
        int sel = selvec[0];
        s = m_CirList.get_text(sel, 0);
    }
    return s;
}





void MainWindow::contextMenu(int button)
{
    Gtk::Menu* pMenuPopup;
    pMenuPopup = dynamic_cast<Gtk::Menu*>(refUIManager->get_widget("/PopupMenu"));
    if (pMenuPopup) pMenuPopup->popup(0/*button*/, gtk_get_current_event_time());
}

bool MainWindow::on_my_key_release_event(GdkEventKey* event)
{
	switch (event->keyval)
	{
		case 65383: // context menu
            contextMenu(0);
            return true;
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;

		default:
			break;
	}

	return false;
}



Gtk::ToolButton* MainWindow::addToPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe)
{
    Gtk::ToolButton* button;
    button = Gtk::manage(new Gtk::ToolButton());
    button->set_label(id);
    button->set_tooltip_markup("<b>"+name+"</b>\n"+tip);
    gruppe->insert(*button);
    return button;
}

// Logik-Gatter werden ohne weiteren Schnick-Schnack
Gtk::ToolButton* MainWindow::addToGatterPalette(const string& name, Gtk::ToolItemGroup* gruppe)
{
    // name = id
    Gtk::ToolButton* b;
    b = addToPalette(name,name,name, gruppe);
    Gtk::Image* img = Gtk::manage(new Gtk::Image(App::etc_piiri_path + "/" + name +".png"));
    b->set_icon_widget(*img);
    return b;
}
// Spezial-Komponenten werden an die entsprechende Gruppe weitergereicht
Gtk::ToolButton* MainWindow::addToSpecialPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe)
{
    Gtk::ToolButton* b;
    b = addToPalette(name,id,tip, gruppe);
    Gtk::Image* img = Gtk::manage(new Gtk::Image(App::etc_piiri_path + "/" + id +".png"));
    b->set_icon_widget(*img);
    return b;
}
// Schaltungen aus dem cir-Ordner werden auch an die entsprechende Gruppe weitergereicht
Gtk::ToolButton* MainWindow::addToCirPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe)
{
    Gtk::ToolButton* b;
    b = addToPalette(name,id,tip, gruppe);
    Gtk::Image* img = Gtk::manage(new Gtk::Image(App::etc_piiri_path + "/" + id +".png"));
    b->set_icon_widget(*img);
    return b;
}

void MainWindow::on_selection_changed()
{
    updateUI();
}

/** @brief updateUI
  *
  * @todo: document this function
  */
void MainWindow::updateUI()
{
    CircuitWidget* cw = getCurrentWidget();

	if (cw && (cw->selObj.size() > 0))
	{
		ENABLE_ACTION("Show")
		ENABLE_ACTION("Rename")
		ENABLE_ACTION("Delete")
		ENABLE_ACTION("Clone")
		ENABLE_ACTION("Copy")
		ENABLE_ACTION("Cut")
		ENABLE_ACTION("Disconnect")
		DISABLE_ACTION("DisconnectSel")
		ENABLE_ACTION("DisconnectAll")
		ENABLE_ACTION("DisconnectOut")
		ENABLE_ACTION("DisconnectIn")
		ENABLE_ACTION("Edit")
		if (cw->selObj.size() == 1)
		{
		    ENABLE_ACTION("Properties")
		}
		else
		{
		    DISABLE_ACTION("Properties")
		}
		DISABLE_ACTION("Node")

		ENABLE_ACTION("SubMenuAnordnung")
		ENABLE_ACTION("Up")
		ENABLE_ACTION("Down")
		ENABLE_ACTION("Bottommost")
		ENABLE_ACTION("Topmost")
	}
	else if (cw && (cw->selIO.size() == 1))
	{
		DISABLE_ACTION("SubMenuAnordnung")
		DISABLE_ACTION("Up")
		DISABLE_ACTION("Down")
		DISABLE_ACTION("Bottommost")
		DISABLE_ACTION("Topmost")

		DISABLE_ACTION("Edit")
		DISABLE_ACTION("Show")
		ENABLE_ACTION("Disconnect")
		ENABLE_ACTION("DisconnectSel")
		DISABLE_ACTION("DisconnectAll")
		DISABLE_ACTION("DisconnectOut")
		DISABLE_ACTION("DisconnectIn")
		ENABLE_ACTION("Node")
		if (cw->selIO.front()->getOwner() == getCurrentCircuit())
		{
			ENABLE_ACTION("Rename")
			ENABLE_ACTION("Delete")
            ENABLE_ACTION("Properties")
		}
		else
		{
			DISABLE_ACTION("Rename")
			DISABLE_ACTION("Delete")
            DISABLE_ACTION("Properties")
		}
	}
	else
	{
	    if (cw)
	    {
            ENABLE_ACTION("Properties")
            ENABLE_ACTION("Table")
            ENABLE_ACTION("Refresh")
            ENABLE_ACTION("Zoom100")
            ENABLE_ACTION("ZoomIn")
            ENABLE_ACTION("ZoomOut")
            ENABLE_ACTION("AddIn")
            ENABLE_ACTION("AddOut")
            ENABLE_ACTION("Save")
            ENABLE_ACTION("SaveAs")
            ENABLE_ACTION("Reload")
            ENABLE_ACTION("Anordnung")
            m_ToolPalette.set_sensitive(true);
	    }
	    else
	    {
            DISABLE_ACTION("Properties")
            DISABLE_ACTION("Table")
            DISABLE_ACTION("Refresh")
            DISABLE_ACTION("Zoom100")
            DISABLE_ACTION("ZoomIn")
            DISABLE_ACTION("ZoomOut")
            DISABLE_ACTION("AddIn")
            DISABLE_ACTION("AddOut")
            DISABLE_ACTION("Save")
            DISABLE_ACTION("SaveAs")
            DISABLE_ACTION("Reload")
            DISABLE_ACTION("Anordnung")
            m_ToolPalette.set_sensitive(false);
	    }
		DISABLE_ACTION("Show")
		DISABLE_ACTION("Rename")
		DISABLE_ACTION("Delete")
		DISABLE_ACTION("Clone")
		DISABLE_ACTION("Copy")
		DISABLE_ACTION("Cut")
		DISABLE_ACTION("Disconnect")
		DISABLE_ACTION("Edit")
		DISABLE_ACTION("Node")

		DISABLE_ACTION("SubMenuAnordnung")
		DISABLE_ACTION("Up")
		DISABLE_ACTION("Down")
		DISABLE_ACTION("Bottommost")
		DISABLE_ACTION("Topmost")
	}


	CTAB(
        Glib::RefPtr<Gtk::Action> a = App::getMW().getActionGroup()->get_action("Undo");
        if (a)
        {
            std::string t = ct->getCurrentUndoAction();
            //Gtk::Action *a;
            a->set_sensitive((t != ""));
            a->set_label(App::t("Rückgängig")+": " + t);
            a->set_short_label(App::t("Rückgängig"));
            a->set_tooltip(App::t("Rückgängig")+": " + t);
        }

		if (!ct->getEditActive())
        {
            DISABLE_ACTION("Rename")
            DISABLE_ACTION("Delete")
            DISABLE_ACTION("Clone")
            DISABLE_ACTION("Cut")
            DISABLE_ACTION("Paste")
            DISABLE_ACTION("Disconnect")
            DISABLE_ACTION("Edit")
            DISABLE_ACTION("Node")
        }

    )


	if (m_pProject)
	{
        ENABLE_ACTION("ProjectClose")
        ENABLE_ACTION("ProjectSave")
        ENABLE_ACTION("ProjectImport")
	}
	else
	{
        DISABLE_ACTION("ProjectClose")
        DISABLE_ACTION("ProjectSave")
        DISABLE_ACTION("ProjectImport")
	}


    // Statusbar

    ostringstream msg;


    m_Statusbar.pop(std_context);
    if (cw && (cw->selObj.size() > 0))
    {
        if (cw->selObj.size() == 1)
        {
            CircuitChild *po = cw->selObj.front()->getObj();
            if (!po) std::cerr << "Schlimm schlimm!" << std::endl;
            else
            {
                msg << "Komponente " << po->getName() << ": " << po->getCirName();
                string d = po->getProperties().get_string("description");
                msg << " (" << d << ") ";
                msg << po->getInfo();
            }
        }
        else
        {
            msg << cw->selObj.size() << " Komponenten";
        }
        m_Statusbar.push(msg.str(),std_context);
    }
    else if (cw && (cw->selIO.size() > 0))
    {
        CircuitIO *pio = cw->selIO.front();
        CircuitChild *po = pio->getOwner();

        if (pio->getType() == IO_TYPE_IN) msg << "Eingang ";
        else msg << "Ausgang ";

        if (pio->getOwner()->getParent() != NULL)
            msg << po->getName() << ".";

        msg << pio->getName();

        m_Statusbar.push(msg.str(),std_context);
    }
    else
    {

    }
}


/**
 * Allgemeines anzeigen des Kontextmenüs.
 */
bool MainWindow::on_popup_menu(GdkEventButton* event)
{
	if(event->button == 3)
	{
	    contextMenu(event->button);
	    return true;
	}
	return false; // TODO --> richtig?
}




/**
 * Doppelklick auf Schaltung wird hier her geführt: zum Öffnen einer Schaltung.
 */
bool MainWindow::on_doubleclick(GdkEventButton* event)
{
	if (event->button == 1)
	{
		bool doubleClick = false;
		if (((Gdk::EventType)event->type) == Gdk::DOUBLE_BUTTON_PRESS ) doubleClick = true;
		if (doubleClick) on_action_Show();
		return true;
	}
	return false; // TODO--> richtig?
}



/// ---------------- Projekt -------------------

void MainWindow::on_action_ProjectNew()
{
    NewProject dialog;

    if (dialog.run() == Gtk::RESPONSE_OK)
    {
        if (!dialog.getProjectName().empty())
        {
            if (closeProject())
            {
                Project p(dialog.getProjectFile(), false);
                p.setName(dialog.getProjectName());
                //p.setFolder(dialog.getProjectName());
                if (p.save())
                {
                    openProject(dialog.getProjectFile());
                }
            }
        }
    }
    App::fetchError("Projekt erzeugen", *this);
}

/** @brief openProject
  *
  * @todo: document this function
  */
void MainWindow::openProject(const std::string pfn)
{
    if (FileExists(pfn))
    {
        if (closeProject())
        {
            setProject(new Project(pfn));

            // tabs öffnen
            vector<string> tabs = m_pProject->getOpenTabs();
            for (std::vector<std::string>::iterator it = tabs.begin(); it != tabs.end(); it++)
            {
                if ((*it).empty() || ((*it)==".")) continue;
                string name(App::cir_path + "/" + (*it));
                openCircuit(name);
            }
            //? updateWindowTitle();
            //? updateUI();
        }
    }
    else std::cerr << "Projektdatei nicht vorhanden." << std::endl;

    App::fetchError("Projekt öffnen", App::getMW());
}

/** @brief setProject
  *
  * @todo: document this function
  */
void MainWindow::setProject(Project* prj)
{
    m_pProject = prj;
    if (m_pProject)
    {
        App::cir_path = m_pProject->getProjectFolderPath() + "/cir";
        App::plan_path = m_pProject->getProjectFolderPath() + "/plan";
        on_action_ReloadCircuits();
        App::addToRecentProjects(m_pProject->getProjectFilePath());
        updateRecentProjectsMenu();
    }
    updateWindowTitle();
    updateUI();
}



/** @brief updateRecentProjectsMenu
  *
  * @todo: document this function
  */
void MainWindow::updateRecentProjectsMenu()
{
    vector<string> rps = App::getRecentProjects();
    vector<Gtk::Widget*> ch = m_pRecentProjectsMenu->get_children();
    for (vector<Gtk::Widget*>::iterator it = ch.begin(); it != ch.end(); it++)
    {
        m_pRecentProjectsMenu->remove(*(*it));
    }

    Gtk::MenuItem *pmi;

    for (vector<string>::reverse_iterator it = rps.rbegin(); it != rps.rend(); it++)
    {
        pmi = Gtk::manage(new Gtk::MenuItem((*it)));
        pmi->show();
        pmi->signal_activate().connect( sigc::bind(  sigc::mem_fun(*this, &MainWindow::on_recent_project_activate), (*it)  )  );
        m_pRecentProjectsMenu->add(*pmi);
    }
}

/** @brief on_recent_project_activate
  *
  * @todo: document this function
  */
void MainWindow::on_recent_project_activate(const string& pfn)
{
    openProject(pfn);
}




void MainWindow::on_action_ProjectOpen()
{
    Gtk::FileChooserDialog dialog(*this, "Projekt öffnen...", Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_current_folder(App::home_piiri_path + "/projekte");

    //Add response buttons the the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    if (dialog.run() == Gtk::RESPONSE_OK)
    {
        openProject(dialog.get_filename());
    }
}
void MainWindow::on_action_ProjectSave()
{
    if (m_pProject)
    {
        m_pProject->save();
    }
}
void MainWindow::on_action_ProjectClose()
{
    closeProject();
}
void MainWindow::on_action_ProjectImport()
{
    if (!m_pProject) return;

    Gtk::FileChooserDialog dialog(*this, "Komponente importieren ...", Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_current_folder(App::home_piiri_path);

    //Add response buttons the the dialog:
    dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
    dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

    if (dialog.run() == Gtk::RESPONSE_OK)
    {
        string sfn(dialog.get_filename());
        m_pProject->importToProject(sfn);
    }

    on_action_ReloadCircuits();
}




/** @brief getOpenTabs
  *
  * @todo: document this function
  */
std::vector<CircuitTab*> MainWindow::getOpenTabs()
{
    std::vector<CircuitTab*> arr;

    int numtabs = m_Tabbing.get_n_pages();
    for (int i=0; i<numtabs; i++)
    {
        CircuitTab* ct = dynamic_cast<CircuitTab *>(m_Tabbing.get_nth_page(i));
        if (ct)
        {
            //std::string s = ct->getTLCircuit().getFilename();
            //if (!s.empty()) arr.push_back(s);
            arr.push_back(ct);
        }
    }
    return arr;
}


/** @brief closeProject
  *
  * @todo: document this function
  */
bool MainWindow::closeProject()
{
    if (m_pProject) m_pProject->save();

    bool tc = closeAllTabs();

    if (tc && m_pProject)
    {
        delete m_pProject;
        m_pProject = NULL;

        App::cir_path = App::home_piiri_path + "/cir";
        App::plan_path = App::home_piiri_path + "/plan";
        on_action_ReloadCircuits();
    }

    updateWindowTitle();
    updateUI();

    return tc;
}



/** @brief updateWindowTitle
  *
  * @todo: document this function
  */
void MainWindow::updateWindowTitle()
{
    if (m_pProject)
    {
        set_title(App::name + " [" + m_pProject->getName() + "]");
    }
    else
    {
        set_title(App::name + " []");
    }
}






///---------------- Actions for TABs ---------


void MainWindow::on_action_SaveAs()
{
    CTAB(
    ct->command_save_as();
    )
        reloadCircuits();
	App::fetchError("Beim Speichern unter", *this);
}

void MainWindow::on_action_Save()
{
    CTAB(
        ct->command_save();
    )
    reloadCircuits();
	App::fetchError("Beim Speichern", *this);
}


void MainWindow::on_action_Reload()
{
    CTAB(
        ct->command_reload();
    )
	App::fetchError("Beim Zurücksetzen", *this);
}


void MainWindow::on_action_Undo()
{
    // std::cout << "undo" << std::endl;

    CTAB(
        ct->command_undo();
    )

	App::fetchError("Beim rückgängig Machen", App::getMW());
}


void MainWindow::on_action_ZoomIn()
{
    CTAB(
        ct->command_zoom(CircuitTab::ZA_IN);
    )
}

void MainWindow::on_action_Zoom100()
{
    CTAB(
        ct->command_zoom(CircuitTab::ZA_100);
    )
}

void MainWindow::on_action_ZoomOut()
{
    CTAB(
        ct->command_zoom(CircuitTab::ZA_OUT);
    )
}

void MainWindow::on_action_Anordnung()
{
    Gtk::Menu *pmn = dynamic_cast<Gtk::Menu*>(refUIManager->get_widget("/AnordnungMenu"));
    if (!pmn) return;
    pmn->popup(1, gtk_get_current_event_time());
}

void MainWindow::on_action_Up()
{
    CTAB(
        ct->command_order(CircuitTab::OA_UP);
    )
}

void MainWindow::on_action_Down()
{
    CTAB(
        ct->command_order(CircuitTab::OA_DOWN);
    )
}

void MainWindow::on_action_Topmost()
{
    CTAB(
        ct->command_order(CircuitTab::OA_TOPMOST);
    )
}

void MainWindow::on_action_Bottommost()
{
    CTAB(
        ct->command_order(CircuitTab::OA_BOTTOMMOST);
    )
}

/**
 * Action-Handler für Aktualisieren.
 */
void MainWindow::on_action_Refresh()
{
    //CTAB( ct->command_step(); )

}

/**
 * Action-Handler für Komponente Anzeigen.
 */
void MainWindow::on_action_Show()
{
    CTAB( ct->command_show(); )
}

/**
 * Action-Handler für Komponente Anzeigen.
 */
void MainWindow::on_action_Table()
{
    if (CircuitChild::isNormalSim())
    {
        CTAB(ct->command_table();)
        App::fetchError("Tabellen-Fenster", *this);
    }
    else
    {
        open_infobar(Gtk::MESSAGE_WARNING, "Wertetabellen können nur mit der Normal-Simulation erstellt werden.");
    }
}

/// START CLIPBOARD

void MainWindow::on_action_Clone()
{
    CTAB(ct->command_clone();)
	App::fetchError("Beim Duplizieren", *this);
}

void MainWindow::on_action_Cut()
{
    CTAB(ct->command_cut();)
	App::fetchError("Beim Ausschneiden", *this);

    update_paste_status();
}

void MainWindow::on_action_Copy()
{
    CTAB(ct->command_copy();)
	App::fetchError("Beim Kopieren", *this);

    update_paste_status();
}

void MainWindow::on_action_Paste()
{
    CTAB(ct->command_paste();)
	App::fetchError("Beim Einfügen", *this);

    update_paste_status();
}

void MainWindow::on_clipboard_owner_change(GdkEventOwnerChange*)
{
    //std::cout << "on_clipboard_owner_change" << std::endl;
    update_paste_status();
}

void MainWindow::update_paste_status()
{
    //std::cout << "update_paste_status" << std::endl;
    //Disable the paste button if there is nothing to paste.

    Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();

    //Discover what targets are available:
    refClipboard->request_targets(sigc::mem_fun(*this, &MainWindow::on_clipboard_received_targets) );
}

void MainWindow::on_clipboard_received_targets(const Glib::StringArrayHandle& targets_array)
{
    CTAB(
        //std::cout << "on_clipboard_received_targets" << std::endl;
        // Get the list of available clipboard targets:
        std::vector<std::string> targets = targets_array;

        /*for (std::vector<std::string>::iterator it=targets.begin(); it != targets.end(); it++)
        {
            std::cout<< "T: " << (*it) << std::endl;
        }*/

        const bool bPasteIsPossible = std::find(targets.begin(), targets.end(), App::target_string) != targets.end();

        // Enable/Disable the Paste button appropriately:
        if (bPasteIsPossible) ENABLE_ACTION("Paste")
        else DISABLE_ACTION("Paste")
        if (!ct->getEditActive()) DISABLE_ACTION("Paste")
    )
    else
    {
        DISABLE_ACTION("Paste")
    }
}



/// ENDE CLIPBOARD

void MainWindow::on_action_SelectAll()
{
    CTAB(ct->command_selectall();)
}

void MainWindow::on_action_Embed()
{
    CTAB(ct->command_embed();)
	App::fetchError("Beim Einbetten", *this);
}
