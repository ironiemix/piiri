/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gui/gui.h"
#include "circuit/ToplevelCircuit.h"
#include "gui/CircuitWidget.h"
#include "model/Project.h"

#include <sigc++/sigc++.h>


#include <vector>




#define DISABLE_ACTION(name) {Glib::RefPtr<Gtk::Action> _a_ = refActionGroup->get_action(name); if (_a_) _a_->set_sensitive(false);}
#define ENABLE_ACTION(name) {Glib::RefPtr<Gtk::Action> _a_ = refActionGroup->get_action(name); if (_a_) _a_->set_sensitive(true);}


class CircuitTab;
class App;

class InfobarMessage {
    Gtk::MessageType m_typ;
    std::string m_text;

    public:

    InfobarMessage(Gtk::MessageType ty, std::string& tx)
     : m_typ(ty), m_text(tx)
    {
        //std::cout << "InfobarMessage" << std::endl;
    }

    InfobarMessage(const InfobarMessage& ibm)
     : m_typ(ibm.m_typ), m_text(ibm.m_text)
    {
        //std::cout << "copy InfobarMessage" << std::endl;
    }

    ~InfobarMessage()
    {
        //std::cout << "~InfobarMessage" << std::endl;
    }

    const std::string& getText() const
    {
        return m_text;
    }

    const Gtk::MessageType getTyp() const
    {
        return m_typ;
    }
};

/**
    Hauptfenster
*/
class MainWindow : public Gtk::Window
{
    public:
        MainWindow();
        virtual ~MainWindow();

        std::vector<CircuitTab*> getOpenTabs();
        void reloadCircuits();

    protected:
        Gtk::VBox m_VBox_Toolbar;
        Gtk::VBox m_VBox;
        Gtk::HBox m_HBox;

        Gtk::Statusbar m_Statusbar;
        unsigned std_context;
        unsigned deque_context;

        void contextMenu(int button);
        virtual bool on_my_key_release_event(GdkEventKey* event);


        void on_notebook_added(Widget* page, guint page_num);
        void on_notebook_removed(Widget* page, guint page_num);
        void on_notebook_switch_page(GtkNotebookPage* page, guint page_num);

        void on_notebook_close(CircuitTab* ct);
        void on_notebook_new();

    public:
        std::vector<Gtk::TargetEntry> listTargets;
        Glib::RefPtr<Gtk::ActionGroup> getActionGroup() {return refActionGroup;};
    private:
        Glib::RefPtr<Gtk::UIManager> refUIManager;
        Glib::RefPtr<Gtk::ActionGroup> refActionGroup;
        Glib::RefPtr<Gtk::ActionGroup> refAnordnung;

        //Child widgets:
        Gtk::ToolPalette m_ToolPalette;
        Gtk::ScrolledWindow m_ScrolledWindowPalette;
        Gtk::ScrolledWindow m_ScrolledWindowCircuits;

        Gtk::VPaned m_VPaned;
        Gtk::HPaned m_HPaned;
        Gtk::ListViewText m_CirList;



        sigc::connection m_MsgTimeoutConnection;

        Gtk::Entry* entry_in;
        Gtk::Entry* entry_out;

        Gtk::Menu m_CirMenu;

        Gtk::Notebook m_Tabbing;

        Gtk::InfoBar m_Infobar;
        std::stack<InfobarMessage> m_InforbarMessages;
        Gtk::Label m_Message_Label;
        Gtk::Image m_InfobarImage;

        Project* m_pProject;

        static bool dequeEmpty;


        CircuitTab* getCurrentTab();
        ToplevelCircuit* getCurrentCircuit();
        CircuitWidget* getCurrentWidget();

        void init_palette();
        void updateWindowTitle();

        virtual void on_palette_io_click(const string& gatter, Gtk::ToolItem* item = NULL);

        virtual void on_list_drag_data_get(const Glib::RefPtr<Gdk::DragContext>&, Gtk::SelectionData& selection_data, guint, guint);
    public:
        virtual void on_drag_received(const Glib::RefPtr<Gdk::DragContext>& context,
                                       int x, int y, const Gtk::SelectionData& selection_data, guint info, guint time);

    private:
        void updateRecentProjectsMenu();

        Gtk::Menu* m_pRecentProjectsMenu;

        void on_recent_project_activate(const string& pfn);

        virtual void on_action_recent(); // gebraucht?

        virtual bool on_delete_event(GdkEventAny* event);
        virtual bool on_my_window_state_event(GdkEventWindowState* event);
        virtual bool on_infobar_link(const Glib::ustring& uri);

        string getCirListSelection();
        void on_circuit_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
        bool on_cirlist_button_release_event(GdkEventButton* event);
        void on_cirmenu_add();
        void on_cirmenu_ren();
        void on_cirmenu_del();
        void on_cirmenu_edi();

        virtual void on_infobar_close();
        bool start_msg_timeout();
        void on_infobar_response(int);

        virtual void on_action_New();
        virtual void on_action_Open();

        virtual void on_action_ProjectNew();
        virtual void on_action_ProjectOpen();
        virtual void on_action_ProjectSave();
        virtual void on_action_ProjectClose();
        virtual void on_action_ProjectImport();

        virtual void on_action_ReloadCircuits();
        virtual void on_action_Quit();
        virtual void on_action_Help();
        virtual void on_action_Info();
        virtual void on_action_Settings();

        virtual void on_action_SelectAll();
        virtual void on_action_Rename();
        virtual void on_action_Delete();
        virtual void on_action_DisconnectAll();
        virtual void on_action_DisconnectIn();
        virtual void on_action_DisconnectOut();
        virtual void on_action_DisconnectSel();
        virtual void on_action_Edit();
        virtual void on_action_Node();
        virtual void on_action_Properties();

        virtual void on_action_AddIn();
        virtual void on_action_AddOut();

        virtual void on_action_Synth();
        virtual void on_action_Diagram();
        virtual void on_action_Stop();
        virtual void on_action_Refresh();
        virtual void on_action_Show();
        virtual void on_action_Table();



        // tab
        virtual void on_action_Save();
        virtual void on_action_SaveAs();
        virtual void on_action_Reload();

        virtual void on_action_Undo();
        virtual void on_action_ZoomIn();
        virtual void on_action_ZoomOut();
        virtual void on_action_Zoom100();
        virtual void on_action_Anordnung();
        virtual void on_action_Up();
        virtual void on_action_Down();
        virtual void on_action_Topmost();
        virtual void on_action_Bottommost();
        virtual void on_action_Cut();
        virtual void on_action_Copy();
        virtual void on_action_Paste();
        virtual void on_action_Clone();
        virtual void on_action_Editor();
        virtual void on_action_Embed();


        bool on_step_timer();
        sigc::connection stepTimerConnection;

        bool closeTab(CircuitTab* ct);
        bool closeAllTabs();

        void openProject(const std::string pfn);
        bool closeProject();


        Gtk::ToolButton* addToPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe);
        // Logik-Gatter werden ohne weiteren Schnick-Schnack
        Gtk::ToolButton* addToGatterPalette(const string& name, Gtk::ToolItemGroup* gruppe);
        // Spezial-Komponenten werden an die entsprechende Gruppe weitergereicht
        Gtk::ToolButton* addToSpecialPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe);
        // Schaltungen aus dem cir-Ordner werden auch an die entsprechende Gruppe weitergereicht
        Gtk::ToolButton* addToCirPalette(const string& name, const string& id, const string& tip, Gtk::ToolItemGroup* gruppe);



	// Input und Output hinzufügen: button und entry. def="x"|"y"
	// enty-on_activate wird auch an on_palette_io_click weitergeleitet
	#define ADD_TO_PALETTE_IO(name,def, nr)\
		{button = Gtk::manage(new Gtk::ToolButton());\
		button->set_label(#name);\
		group_io->insert(*button); \
		gtk_container_child_set (GTK_CONTAINER (group_io->gobj()), GTK_WIDGET (button->gobj()), "new-row", nr, NULL);\
		\
		entry_##name = Gtk::manage(new Gtk::Entry());\
		entry_##name->set_text(def);\
		entry_##name->set_width_chars(5);\
		item = Gtk::manage(new Gtk::ToolItem());\
		item->add(*entry_##name);\
		\
		entry_##name->signal_activate().connect(sigc::bind<std::string, Gtk::ToolItem*>(sigc::mem_fun(*this, &MainWindow::on_palette_io_click), #name, item)); \
		\
		group_io->insert(*item);\
		button->signal_clicked().connect(sigc::bind<std::string, Gtk::ToolItem*>(sigc::mem_fun(*this, &MainWindow::on_palette_io_click), #name, item));\
		\
		}

//		gtk_container_child_set (GTK_CONTAINER (group_io->gobj()), GTK_WIDGET (item->gobj()), NULL);
//		gtk_container_child_set (GTK_CONTAINER (group_io->gobj()), GTK_WIDGET (item->gobj()), "homogeneous", TRUE, "expand", TRUE, NULL);



    public:
        CircuitTab* openCircuit(const string& fn, const std::string container_id = std::string());
        void setProject(Project* prj);

        void startStepTimer();
        void stopStepTimer();

        // Copy & Paste
        void on_clipboard_owner_change(GdkEventOwnerChange* event);
        virtual void update_paste_status(); //Disable the paste button if there is nothing to paste.
        void on_clipboard_received_targets(const Glib::StringArrayHandle& targets_array);

        virtual bool on_popup_menu(GdkEventButton* event);
        virtual bool on_doubleclick(GdkEventButton* event);
        virtual void on_selection_changed();

        void open_infobar(Gtk::MessageType t, std::string text, bool autohide=false);
        //void open_infobar(Gtk::MessageType t, std::string text, Glib::SignalProxy0< void > more, bool autohide=false);
        void set_infobar(const InfobarMessage& ibm);

        virtual void updateUI();

        Project* getCurrentProject() {return m_pProject;};



}; // class MainWindow








#endif // MAINWINDOW_H
