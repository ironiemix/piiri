/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef TABLEWINDOW_H
#define TABLEWINDOW_H

#include <gtkmm.h>
#include <string>
#include <vector>
//#include "CircuitChild.h"
#include "Updatable.h"
#include "CircuitTab.h"

using namespace std;


class TableWindow : public Updatable
{
    public:
        TableWindow(CircuitTab& tab, CircuitChild* obj);
        virtual ~TableWindow();
        virtual void updateAll();


/*class ModelColumns : public Gtk::TreeModelColumnRecord
{
public:

  ModelColumns()
    { add(m_col_text); add(m_col_number); }

  Gtk::TreeModelColumn<Glib::ustring> m_col_text;
  Gtk::TreeModelColumn<int> m_col_number;
};*/


    protected:
        //CircuitWindow *parent;
        vector<Gtk::TreeModelColumn<string>* > columns;
        Gtk::TreeModelColumnRecord *tmcr;
        Glib::RefPtr<Gtk::ListStore> listStore;
    private:

        void clear();

        Gtk::ListViewText *m_pList;
        string inout;
        Gtk::VBox m_Vbox;
        Gtk::Toolbar m_Toolbar;
        Gtk::ScrolledWindow m_Scroll;



};

#endif // TABLEWINDOW_H
