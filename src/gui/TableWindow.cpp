/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "TableWindow.h"

TableWindow::TableWindow(CircuitTab& tab, CircuitChild *obj) :
        Updatable(tab, obj, Updatable::SW_TABLE),
        tmcr(NULL),
        m_pList(NULL),
        m_Vbox(),
        m_Toolbar()
{
    set_default_size(300,200);
    Gtk::ToolButton *b = Gtk::manage(new Gtk::ToolButton(Gtk::Stock::REFRESH));
    m_Toolbar.append(*b, sigc::mem_fun(*this, &TableWindow::updateAll));

	m_Vbox.pack_start(m_Toolbar, Gtk::PACK_SHRINK);

    m_pList = Gtk::manage(new Gtk::ListViewText(1));


    m_Scroll.add(*m_pList);
    m_Vbox.add(m_Scroll);
    add(m_Vbox);

    show_all_children();
}

TableWindow::~TableWindow()
{
    //cout << "~TableWindow()" << endl;
    clear();
}


/** MainWindow Members */


void TableWindow::updateAll()
{
    type_inputs::reverse_iterator iit;
    type_outputs::reverse_iterator oit;
    Gtk::TreeModelColumn<string> *tmc;


    bool recreateList = false;

	set_title("PiiRi Wertetabelle: " + m_pObj->getNameRecursive());


    // Gesamtanzahl der Bits durch addieren der PinCount-Werte
    unsigned int inbits = m_pObj->getInputPinCount();
    unsigned int outbits = m_pObj->getOutputPinCount();

    // haben sich inputs oder outputs geänder?
    string s;
    for (iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++)
    {
        s += "|"+(*iit)->getNameEx();
    }
    s += "|";
    for (oit = m_pObj->getOutputs().rbegin(); oit != m_pObj->getOutputs().rend(); oit++)
    {
        s += "|"+(*oit)->getNameEx();
    }
    if (s != inout)
    {
        recreateList = true;
        inout = s;
    }

// http://magazin.c-plusplus.de/artikel/GTKmm%20Tutorial%20Teil%205



    //Glib::RefPtr<Gtk::TreeModel>


    //Glib::RefPtr<Gtk::ListStore> ls(m_pList->get_model());


    //ls.swap((Gtk::ListStore*)((Gtk::TreeModel*)m_pList->get_model()));



    // falls nötig, neue liste erzeugen
    if (recreateList)
    {
        //Glib::RefPtr<Gtk::TreeModel> tm = m_pList->get_model();
        //Gtk::TreeModel* ptm = (tm.operator->());
        //Gtk::ListStore *pls = (Gtk::ListStore *)ptm;



        //if (m_pList)
        {


            // allte columns löschen
            m_pList->remove_all_columns();
            m_pList->unset_model();
            clear();


            // neue spalten erzeugen
            tmcr = new Gtk::TreeModelColumnRecord();

            ostringstream os;

            for (iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++)
            {
                for (int i = (*iit)->getPinCount()-1; i >= 0 ; i--)
                {
                    os.str("");
                    os << (*iit)->getName();
                    if ((*iit)->getPinCount() > 1) os << " " << i;
                    tmc = new Gtk::TreeModelColumn<string>();
                    tmcr->add(*tmc);
                    m_pList->append_column(os.str(), *tmc);
                    columns.push_back(tmc);
                }
            }

            tmc = new Gtk::TreeModelColumn<string>();
            tmcr->add(*tmc);
            m_pList->append_column("", *tmc);
            columns.push_back(tmc);

            for (oit = m_pObj->getOutputs().rbegin(); oit != m_pObj->getOutputs().rend(); oit++)
            {
                for (int i = (*oit)->getPinCount()-1; i >= 0 ; i--)
                {
                    os.str("");
                    os << (*oit)->getName();
                    if ((*oit)->getPinCount() > 1) os << " " << i;
                    tmc = new Gtk::TreeModelColumn<string>();
                    tmcr->add(*tmc);
                    m_pList->append_column(os.str(), *tmc);
                    columns.push_back(tmc);
                }
            }

            m_pList->set_model(listStore = Gtk::ListStore::create(*tmcr));
        }
    }

    listStore->clear();

    /// Werte durchsimulieren

    // aktuellen input-zustand sichern
    unsigned int savedIn = m_pObj->getInputValue();
    //std::cout << "savedIN: " << savedIn << std::endl;





    for (unsigned int i=0; i < (1u<<inbits); i++)
    {
        // neue zeile
        Gtk::TreeModel::Row row = *(listStore->append());

        // Simulation
        m_pObj->setInputValue(i);
        m_pObj->update();
        unsigned int result = m_pObj->getOutputValue();
        // std::cout << "IN: " << i  << " " << m_pObj->getInputValue() << " OUT: " << result << std::endl;

        // Ausgabe in liste
        bool b;
        int col = 0;
        // unsigned in rückwärts durchlaufen ist nicht so einfach!
        for (unsigned int n = 0; n < inbits; n++)
        {
            b = (bool)(i & (1<<(inbits-n-1)));
            tmc = columns[col];
            row[*tmc] = b ? "1" : "0";
            col++;
        }

        col++;
        for (unsigned int n = 0; n < outbits; n++)
        {
            b = (bool)(result & (1<<(outbits-n-1)));
            tmc = columns[col];
            row[*tmc] = b ? "1" : "0";
            col++;
        }

    }


    m_pObj->setInputValue(savedIn);
    m_pObj->update();
}

/** @brief clear
  *
  * @todo: document this function
  */
void TableWindow::clear()
{
    Gtk::TreeModelColumn<string> *tmc;
    if (tmcr) delete tmcr;
    while (columns.size() > 0)
    {
        tmc = columns.back();
        delete tmc;
        columns.pop_back();
    }
}


