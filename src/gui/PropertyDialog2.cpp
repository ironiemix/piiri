/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <iostream>
#include <sstream>

#include "PropertyDialog2.h"
#include "model/App.h"


/** PropertyDialog2 */




PropertyDialog2::PropertyDialog2(const string& t, Gtk::Window& parent, Complex2& c) :
Gtk::Dialog("Einstellungen für "+t, parent, true, true),
change(false),
uiCreated(false),
m_rComplex(c),
tabbing(),
main(true, 5)
{

    set_size_request(500,-1);
	set_position(Gtk::WIN_POS_CENTER_ON_PARENT);

	main.add(tabbing);
	get_vbox()->add(main);

	add_button(Gtk::Stock::OK,     Gtk::RESPONSE_OK);
	add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
}


/** @brief run
  *
  * @todo: document this function
  */
int PropertyDialog2::run()
{
    if (!uiCreated)
    {
        uiCreated = true;

        std::string path;

        { // abschirmen von w
            Gtk::Widget& w = m_rComplex.create_widget(path, getUiTextMap());
            tabbing.append_page(w, App::t("Allgemein"));
        }


        Complex2::iterator it;
        TypedBase* pv;
        string n;
        ui_text title;
        Complex2* pc;

        for (it = m_rComplex.begin(); it != m_rComplex.end(); it++)
        {
            n = (*it).first;
            pv = (*it).second;
            if (pv->get_type() == TypedBase::COMPLEX)
            {
                pc = ((PropValue2<Complex2>*)pv)->get_pointer();
                if (pc->editAllowed())
                {
                    path = "/"+n;
                    Gtk::Widget& w = pc->create_widget(path, getUiTextMap());
                    title = getUiText(path);
                    tabbing.append_page(w, title.first);
                }
            }
        }
    }
	show_all_children();

    return Dialog::run();
}


/** @brief getPropTitle
  *
  * @todo: document this function
  */
std::pair<std::string,std::string> PropertyDialog2::getUiText(std::string& path)
{
    ui_text_map::iterator t = m_uiText.find(path);
    if (t == m_uiText.end()) return ui_text(path,path);

    return m_uiText[path];
}
/** @brief setUiText
  *
  * @todo: document this function
  */
void PropertyDialog2::setUiText(std::string path, ui_text text)
{
    m_uiText[path] = text;
}


bool PropertyDialog2::hasChanged()
{
	writeback(true);
	return changed();
}

/**
 * bei c == NULL wird der Complex2-Member verwendet
 */
void PropertyDialog2::writeback(bool checkOnly)
{
    bool res;

    res = m_rComplex.read_widget(checkOnly);
	changed(res);


/*	Complex2::iterator it;
	PropValue *pv;
	string name;
	istringstream is;
	Complex2 *pc;

	if (c == NULL) c = &m_rComplex;

	for (it = c->begin(); it != c->end(); it++)
	{
		name = (*it).first;
		pv = (*it).second;

		if (pv->getType() == PropValue::Complex2)
		{
			pc = pv->get_Complex2();
			if (pc->editAllowed())
			{
                writeback(pc, checkOnly);
			}
		}
		else if (pv->getType() == PropValue::INT)
		{
			Gtk::Entry *b = dynamic_cast<Gtk::Entry *>(widgets[pv]);
			if (b)
			{
				int n;
				n = atoi(b->get_text().c_str());
				if (pv->get_int() != n)
				{
					changed(true);
					if(!checkOnly) pv->set_int(n);
					else return;
				}
			}
			else
			{
				//cout << "pv-widget-fail: " << name << endl;
			}
		}
		else if (pv->getType() == PropValue::DOUBLE)
		{
			Gtk::Entry *b = dynamic_cast<Gtk::Entry *>(widgets[pv]);
			if (b)
			{
				is.str(b->get_text());
				is.clear();
				double n;
				is >> n;
				if (pv->get_double() != n)
				{
					changed(true);
					if(!checkOnly) pv->set_double(n);
					else return;
				}
			}
			else
			{
				//cout << "pv-widget-fail: " << name << endl;
			}
		}
		else if (pv->getType() == PropValue::STRING)
		{
			Gtk::Entry *b = dynamic_cast<Gtk::Entry *>(widgets[pv]);
			if (b)
			{
				string s = b->get_text();
				if (pv->get_string() != s)
				{
					changed(true);
					if(!checkOnly) pv->set_string(s);
					else return;
				}
			}
			else
			{
				//cout << "pv-widget-fail: " << name << endl;
			}
		}
		else if (pv->getType() == PropValue::BOOL)
		{
			Gtk::CheckButton *b = dynamic_cast<Gtk::CheckButton *>(widgets[pv]);
			if (b)
			{
				if (pv->get_bool() != b->get_active())
				{
					changed(true);
					if(!checkOnly) pv->set_bool(b->get_active());
					else return;
				}
			}
			else
			{
				//cout << "pv-widget-fail: " << name << endl;
			}
		}
		else if (pv->getType() == PropValue::SELECTION)
		{
			Gtk::ComboBoxText* b = dynamic_cast<Gtk::ComboBoxText*>(widgets[pv]);
			if (b)
			{
			    string s = b->get_active_text();
				if (pv->get_selection_value() != s)
				{
					changed(true);
					if(!checkOnly) pv->set_selection_value(s);
					else return;
				}
			}
			else
			{
				//cout << "pv-widget-fail: " << name << endl;
			}
		}
	}
	*/
}


PropertyDialog2::~PropertyDialog2()
{
    //m_signal_property_title.clear();
}

