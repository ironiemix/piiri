#include "gui/RecoverDlg.h"

#include "model/App.h"
#include "model/UndoManager.h"
#include "model/GeneralUndoItem.h"
#include "Helpers.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

RecoverDlg::RecoverDlg(std::vector<std::pair<std::string,std::string> >& recFiles):
    Gtk::Dialog("Wiederherstellung [BETA]")
{
	add_button("Wiederherstellen", Gtk::RESPONSE_OK);
	add_button("Abbrechen", Gtk::RESPONSE_CANCEL);

	Gtk::VBox *vb = get_vbox();
    vb->set_spacing(5);
    vb->set_border_width(5);
    set_border_width(5);

    Gtk::Label *txt = Gtk::manage(new Gtk::Label());
    txt->set_use_markup();
    txt->set_markup("<big>Wiederherstellung BETA</big>\n\n"
                    "Das Programm wurde nicht ordnungsgemäß beendet:\n"
                    "Es wird versucht, die zuletzt geöffneten Schaltungen wiederherzustellen.\n\n"

                    "Die wiederhergestellten Schaltungen werden als\n"
                    "'recovered_[projektname].[originalname].cir' im projektlosen\n"
                    "cir-Ordner (" + App::home_piiri_path + "/cir) abgelegt.\n"
                    "Nach der Wiederherstellung sollten Sie die Schaltung unter richtigem\n"
                    "Namen in den entsprechenden Projektordner speichern.\n"

                    "<b>Wenn Sie <i>Abbrechen</i> klicken, können Sie die Schaltungen\n"
                    "<u>NICHT mehr</u> wiederherstellen!</b>\n\n"

                    "Folgende Dateien werden wiederhergestellt:\n"
                    );
	vb->add(*txt);


    //txt = Gtk::manage(new Gtk::Label());
	Gtk::ListViewText* l = Gtk::manage(new Gtk::ListViewText(2));
	l->set_column_title(0, "Erzeugte Datei");
	l->set_column_title(1, "Quelle");

    for (std::vector<std::pair<std::string,std::string> >::iterator it = recFiles.begin(); it != recFiles.end(); it++)
    {
        // (best,recoverd)
        std::size_t pos = (*it).first.find_last_of('/');
        std::string from((*it).first.substr(pos+1));
        std::string to;
        #define CIR_RECOVER_MAX_DEST_FILE_LENGTH 40
        if ((*it).second.length() > CIR_RECOVER_MAX_DEST_FILE_LENGTH)
        {
            to = "..."+(*it).second.substr((*it).second.length()-CIR_RECOVER_MAX_DEST_FILE_LENGTH);
        }
        else
        {
            to = (*it).second;
        }
        //files += from + " => " + to + "\n";

        int n = l->append_text(to);
        l->set_text(n,1,from);
    }


	vb->add(*l);


	show_all();

}

RecoverDlg::~RecoverDlg()
{
    //dtor
}


bool RecoverDlg::run()
{
    int ret;

    ret = Gtk::Dialog::run();

    hide();

    if (ret == Gtk::RESPONSE_OK)
    {
        return true;
    }

    //m_Filenames.clear();

    return false;
}
