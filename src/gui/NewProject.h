#ifndef NEWPROJECT_H
#define NEWPROJECT_H

#include <gtkmm.h>
#include <string>


class NewProject : public Gtk::Dialog
{
    public:
        NewProject();
        virtual ~NewProject();

        std::string getProjectName();
        std::string getProjectFile();


    protected:
        Gtk::Label l;
        Gtk::Label lname;
        Gtk::Entry ename;
        Gtk::Label lfile;
        Gtk::Entry efile;
        Gtk::Label lfolder;
        Gtk::Entry efolder;
        Gtk::Label ldescr;
        Gtk::Entry edescr;
        Gtk::Table table;
    private:

        void on_project_name_changed();
        std::string getProjectFolder();
};

#endif // NEWPROJECT_H
