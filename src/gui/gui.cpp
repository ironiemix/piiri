/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "gui/gui.h"

#include "model/Complex2.h"
#include "gui/ViewWindow.h"
#include "gui/CircuitTab.h"


/** TimerCallback Members */

bool TimerCallback::timer_callback()
{
	m_pObj->setInput(m_In, !m_pObj->getInputValue(m_In));
	m_pObj->update();
	m_pTab->updateAll();

	// never disconnect timer handler
	return 1;
}


/** Properties Members */

/*
Properties::Properties(Gtk::Window& wnd) :
Glib::ObjectBase("plan_properties"),
Gtk::Dialog("Einstellungen", wnd, true, true),

PROPERTY_INIT(plan_show, false),
PROPERTY_INIT(plan_over, false),
PROPERTY_INIT(plan_x, 0),
PROPERTY_INIT(plan_y, 0),
PROPERTY_INIT(plan_scale, 1.0),
PROPERTY_INIT(plan_alpha, 0.8),
PROPERTY_INIT(plan_file, ""),


show("Plan Anzeigen"),
over(grp, "Über der Schaltung"),
under(grp, "Unter der Schaltung"),
alpha(0, 1.05, 0.05),
scale(0.1, 2)
{
	add_button("Ok", Gtk::RESPONSE_OK);
	add_button("Abbrechen", Gtk::RESPONSE_CANCEL);

	Gtk::VBox *vb = get_vbox();


	show.signal_clicked().connect(sigc::mem_fun(*this, &Properties::on_plan_show_activate));

	Gtk::Label *lbl1 = Gtk::manage(new Gtk::Label("File"));
	Gtk::Label *lbl2 = Gtk::manage(new Gtk::Label("Transparenz"));
	Gtk::Label *lbl3 = Gtk::manage(new Gtk::Label("X-Versatz:"));
	Gtk::Label *lbl4 = Gtk::manage(new Gtk::Label("Y-Versatz:"));
	Gtk::Label *lbl5 = Gtk::manage(new Gtk::Label("Skalierung:"));


	file.set_current_folder(App::plan_path); // "./plan"
	Gtk::FileFilter filter_png;
	filter_png.set_name("Plan-Grafik (*.png)");
	filter_png.add_pattern("*.png");
	file.set_filter(filter_png);
	file.set_local_only(true);

	xvers.set_max_length(5);
	yvers.set_max_length(5);
	xvers.set_width_chars(5);
	yvers.set_width_chars(5);

	Gtk::HBox* p_vers = Gtk::manage(new Gtk::HBox());
	Gtk::HBox& vers = *p_vers;
	vers.add(*lbl3);
	vers.add(xvers);
	vers.add(*lbl4);
	vers.add(yvers);



	Gtk::HBox* p_zoom = Gtk::manage(new Gtk::HBox());
	Gtk::HBox& zoom = *p_zoom;
	scale.set_range(0.1, 4.0);
	zoom.add(*lbl5);
	zoom.add(scale);

	vb->add(show);
	vb->add(over);
	vb->add(under);
	vb->add(*lbl1);
	vb->add(file);
	vb->add(*lbl2);
	vb->add(alpha);
	vb->add(vers);
	vb->add(zoom);

	show_all_children();

}

int Properties::run()
{

	if (property_plan_over()) over.set_active(true);
	else under.set_active(true);


	string currentfile = App::plan_path + property_plan_file();
	file.select_filename(currentfile);

	ostringstream os;
	os << property_plan_x();
	xvers.set_text(os.str());
	os.str("");
	os << property_plan_y();
	yvers.set_text(os.str());


	alpha.set_value(property_plan_alpha());
	scale.set_value(property_plan_scale());

	show.set_active(property_plan_show());

	on_plan_show_activate();

	// Dialog ausführen
	int ret = Gtk::Dialog::run();

	if (ret == Gtk::RESPONSE_OK)
	{
		// Werte auslesen
		gchar * fn = g_path_get_basename(file.get_filename().c_str());
		string fnstr(fn);

		property_plan_file(fnstr);
		property_plan_x(atoi(xvers.get_text().c_str()));
		property_plan_y(atoi(yvers.get_text().c_str()));


		property_plan_scale(scale.get_value());
		property_plan_alpha(alpha.get_value());
		property_plan_over(over.get_active());
		property_plan_show(show.get_active());

		g_free(fn);
	}

	return ret;
}

void Properties::on_plan_show_activate()
{
	bool ps = show.get_active();
	under.set_sensitive(ps);
	over.set_sensitive(ps);
	file.set_sensitive(ps);
	xvers.set_sensitive(ps);
	yvers.set_sensitive(ps);
	alpha.set_sensitive(ps);
	scale.set_sensitive(ps);
}


*/

