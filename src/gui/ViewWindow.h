/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef VIEWWINDOW_H
#define VIEWWINDOW_H

#include <gtkmm.h>
#include <gtkmm/accelmap.h>
#include <vector>
#include <string>

#include "model/App.h"
#include "gui/CircuitWidget.h"
#include "circuit/CircuitVisual.h"
//#include "Components.h"

#include "gui/Updatable.h"


class ViewWindow : public Updatable
{
    public:
        ViewWindow(CircuitTab& tab, CircuitContainer* c);
        virtual ~ViewWindow();

    private:
        unsigned view_context;





    public:
        void updateAll();

        virtual void on_action_all();
        virtual bool on_popup_menu(GdkEventButton* event);
        virtual bool on_doubleclick(GdkEventButton* event);
        virtual void on_selection_changed();
        void on_close();


        //void closeSubwindows();


    protected:
        //vector<CircuitWindow*> subwindows;
        //vector<Updatable*> subwindows;
        //CircuitWindow* parent;
        CircuitContainerVisual *m_pVisual;
        CircuitWidget circuitArea;

        Gtk::VBox m_VBox_Toolbar;
        Gtk::VBox m_VBox;
        Gtk::HBox m_HBox;
        Gtk::ScrolledWindow m_ScrolledWindowCanvas;
        Gtk::Statusbar m_Statusbar;
        unsigned std_context;


        Glib::RefPtr<Gtk::UIManager> refUIManager;
        Glib::RefPtr<Gtk::ActionGroup> refActionGroup;
        Glib::RefPtr<Gtk::ActionGroup> refAnordnung;

        virtual void on_action_ZoomIn();
        virtual void on_action_ZoomOut();
        virtual void on_action_Zoom100();
        virtual void on_action_Refresh();
        virtual void on_action_Show();

        virtual void on_action_Table();

        //virtual void on_cwidget_realize();

        void contextMenu(int button);
        virtual bool on_my_key_release_event(GdkEventKey* event);




}; // class ViewWindow


#endif // VIEWWINDOW_H
