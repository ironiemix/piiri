/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef SCHALTUNG_GUI_H
#define SCHALTUNG_GUI_H


//#include <gtkmm/button.h>
//#include <gtkmm/window.h>



#include <glib-object.h>

//#include "Components.h"
#include "circuit/CircuitChild.h"

#include <gtkmm.h>



class CircuitTab;

class TimerCallback
{
public:
    TimerCallback(CircuitTab* ptab):m_pTab(ptab) {}
	CircuitChild* m_pObj;
	CircuitTab* m_pTab;
	std::string m_In;
	int m_Interval;
	bool m_StopClock;

	sigc::connection c;

	bool timer_callback();
};

/******************************************/


class Splash : public Gtk::Window
{
public:
	Splash() : o(0.8)
	{
		set_type_hint(Gdk::WINDOW_TYPE_HINT_SPLASHSCREEN );
		set_default_size(300, 200);
		set_position(Gtk::WIN_POS_CENTER);
		show_all_children();
		set_keep_above(true);
		set_opacity(o);
		show();
		// sigc::connection c =
		Glib::signal_timeout().connect(sigc::mem_fun(this, &Splash::callb), 3000);
	}
	bool callb()
	{
		Glib::signal_timeout().connect(sigc::mem_fun(this, &Splash::fade), 100);
		return 0;
	}
	bool fade()
	{
		o *= 0.3;
		set_opacity(o);
		if (o < 0.01)
		{
			hide();
			return 0;
		}
		return 1;
	}
	bool on_expose_event(GdkEventExpose* ev)
	{
	Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create_from_file("../etc/piiri/splash.png");
	image->render_to_drawable(get_window(), get_style()->get_black_gc(),
	0, 0,        32, 40, image->get_width(), image->get_height(), // draw the whole image (from 0,0 to the full width,height) at 0,0 in the window
	Gdk::RGB_DITHER_NONE, 0, 0);
	return true;
	}

private:
	double o;
}; // Splash









/**
 * PROPERTY(einstellung, int)
 * -->
 *  protected:
 *  Glib::Property<int> the_property_einstellung;
 *  public:
 *  int property_einstellung() {return the_property_einstellung.get_value();}
 *  void property_einstellung(int v) {the_property_einstellung.set_value(v);}
 */
#define PROPERTY(type, name) \
protected:\
Glib::Property<type> the_property_##name;\
public:\
type property_##name() {return the_property_##name.get_value();}\
void property_##name(type v) {the_property_##name.set_value(v);}

/**
 * PROPERTY_INIT(einstellung, wert)
 * -->
 *  the_property_einstellung(*this, "einstellung", wert)
 */

#define PROPERTY_INIT(name, def) the_property_##name(*this, #name, def)
/*
class Properties : public Gtk::Dialog
{
public:
	Properties(Gtk::Window& wnd);
	~Properties() {};

	//Glib::Property<bool> property_plan_show;

	PROPERTY(bool, plan_show)
	PROPERTY(bool, plan_over)
	PROPERTY(int, plan_x)
	PROPERTY(int, plan_y)
	PROPERTY(double, plan_scale)
	PROPERTY(double, plan_alpha)
	PROPERTY(string, plan_file)

public:
	int run();

private:
	Gtk::CheckButton show;
	Gtk::RadioButtonGroup grp;
	Gtk::RadioButton over, under;
	Gtk::Entry xvers, yvers;
	Gtk::FileChooserButton file;
	Gtk::HScale alpha;
	Gtk::SpinButton scale;

	virtual void on_plan_show_activate();

}; // Properties

*/



#endif // SCHALTUNG_GUI_H
