#ifndef CIRCUITTAB_H
#define CIRCUITTAB_H

#include <gtkmm.h>
#include <string>
#include <list>
#include <stack>

#include "model/App.h"
#include "gui/CircuitWidget.h"
#include "circuit/CircuitVisual.h"
//#include "Components.h"
#include "gui/gui.h"
#include "circuit/ToplevelCircuit.h"
#include "model/UndoManager.h"
#include "gui/Updatable.h"

class CircuitTab : public Gtk::VBox
{
    public:
        CircuitTab(Gtk::Label* tab_label, Glib::RefPtr<Gtk::UIManager> uim);
        virtual ~CircuitTab();

        enum ZOOM_ACTION {
            ZA_IN, ZA_OUT, ZA_100
        };
        enum ORDER_ACTION {
            OA_UP, OA_DOWN, OA_TOPMOST, OA_BOTTOMMOST
        };
        enum DISCONNECT_ACTION {
            DA_ALL, DA_IN, DA_OUT, DA_SELECTED
        };


        void updateAll();
        CircuitWidget& getCView() {return m_cview;};

        void componentDrop(const std::string id, int x, int y);
        bool askSave();
        void changed(bool c);

        // Commands
        bool command_open(const std::string& fn);
        void openContainerView(EditableCircuit* pobj);
        void closeContainerView(bool all = false);
        bool command_save();
        bool command_save_as();
        void command_undo();
        void command_addIO(IO_TYPE t, const string& prae);
        void command_zoom(ZOOM_ACTION z);
        void command_order(ORDER_ACTION o);
        void command_show();
        void command_step();
        void command_cut();
        void command_copy();
        void command_paste();
        void command_clone();
        void command_embed();
        void command_table();
        void command_diagram();
        void command_delete();
        void command_rename();
        void command_disconnect(DISCONNECT_ACTION d);
        void command_properties();
        void command_selectall();
        void command_node();
        void command_reload();
        void command_edit_active(bool a);


        ToplevelCircuit &getTLCircuit() {return *m_pTLCircuit;};
        EditableCircuit &getEdCircuit() {return *m_pEdCircuit;};

        void openTableWindow(CircuitChild *pObj);
        void openViewWindow(CircuitContainer *pObj);

        void on_subwindow_close(Updatable* w); // wird vom unterfenster beim schließen aufgerufen

        bool isNew();
        CircuitChild* getSelectedObject();
        bool getEditActive() {return editActive;};
        //void setUndoTitle(const string& t);
        std::string getCurrentUndoAction();

        void tabActivate();

        bool saveFile(const string& filename);
        bool openFile(const string& filename);
        void clear();

        // Clipboard
        void on_clipboard_received(const Gtk::SelectionData& selection_data);

    protected:
        static void provideUiText(CircuitChild* obj, PropertyDialog2& d);

    private:
        Gtk::Label* m_pTabLabel;
        list<Updatable*> subwindows;
        int inputNumberModifier;

        /*Glib::RefPtr<Gtk::UIManager> refUIManager;
        Glib::RefPtr<Gtk::ActionGroup> refActionGroup;
        Glib::RefPtr<Gtk::ActionGroup> refAnordnung;*/
        Glib::RefPtr<Gtk::UIManager> m_refUIManager;

        Gtk::ScrolledWindow m_ScrolledWindowCanvas;
        Gtk::HScale m_ClockScale;
        Gtk::ComboBoxText m_ClockCombo; // http://developer.gnome.org/gtkmm/2.24/classGtk_1_1ComboBoxText.html
        Gtk::CheckButton m_ClockCheck; // http://developer.gnome.org/gtkmm/2.24/classGtk_1_1CheckButton.html
        Gtk::SpinButton  m_ClockScaleButton;
        Gtk::HBox m_Tools;
        Gtk::Toolbar m_Toolbar;
        Gtk::ToggleToolButton m_MoveActive;
        Gtk::ToolButton m_Back;
        Gtk::ToolItem m_Name;
        Gtk::Image m_MoveImage;
        Gtk::Image m_NoMoveImage;


        ToplevelCircuit* m_pTLCircuit;
        EditableCircuit* m_pEdCircuit;
        ToplevelCircuitVisual* m_pVisual;
        std::stack<ToplevelCircuitVisual*> m_visualStack;
        CircuitWidget m_cview;

        TimerCallback tcb;
        UndoManager m_undo;

        bool m_changed;
        bool editActive;

        bool timer_callback();
        void init();
        bool changed() {return m_changed;}
        void setUnsaved() {changed(true);}
        void startClock(const string& in, int interval);
        void stopClock();
        void closeSubwindows(Updatable::SUBWINDOW_TYPE t = Updatable::SW_ALL);

        bool cloneSelectionVirtually(type_elements& to_clone, CircuitContainer& cloned);

        virtual void on_clock_scale_changed();
        virtual void on_clock_changed();
        virtual void on_clock_activate();
        virtual void on_clock_menu();
        virtual void on_action_MoveActivate();
        virtual void on_Back();






        void on_circuitstate(int a);
        virtual void on_circuit_notification(int, const std::string&, gpointer);
        bool on_key_press_event(GdkEventKey* event);
        bool on_key_release_event(GdkEventKey* event);
        CircuitInput *getInputFromKey(guint keyval);

        int getClockMs();
};

#endif // CIRCUITTAB_H
