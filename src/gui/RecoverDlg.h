#ifndef RECOVERDLG_H
#define RECOVERDLG_H

#include <gtkmm.h>
#include <string>
#include <vector>


class RecoverDlg : public Gtk::Dialog
{
    public:
        RecoverDlg(std::vector<std::pair<std::string,std::string> >& recFiles);
        virtual ~RecoverDlg();

        bool run();

        /*std::vector<std::string>& getFilenames()
        {
            return m_Filenames;
        }*/
    protected:
    private:

    //std::vector<std::string> m_Filenames;
};

#endif // RECOVERDLG_H
