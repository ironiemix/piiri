/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef DIAGRAMWINDOW_H
#define DIAGRAMWINDOW_H

#include <gtkmm.h>
#include <gtkmm/accelmap.h>
#include <vector>
#include <string>

#include "model/App.h"
#include "gui/Updatable.h"


struct DiagramPoint
{
    long time;
    bool value;
};

class DiagramLine : public std::vector<DiagramPoint*>
{
    public:
        DiagramLine(const std::string& n, IO_TYPE t):
          name(n), type(t)
        {

        };

        ~DiagramLine()
        {
            clear_content();
        }

        void clear_content()
        {
            iterator it;
            for (it = begin(); it != end(); it++)
            {
                delete (*it);
            }
            clear();
        }

        std::string name;
        IO_TYPE type;
};

typedef std::vector<DiagramLine*> type_line_list;


class DiagramWindow : public Updatable
{
    public:
        DiagramWindow(CircuitTab& tab, CircuitContainer* c);
        virtual ~DiagramWindow();

    private:
        unsigned view_context;

    public:
        void updateAll();

        virtual void on_action_all();
        virtual bool on_popup_menu(GdkEventButton* event);
        virtual bool on_doubleclick(GdkEventButton* event);
        void on_close();

        void clear_lines();
        void init_lines();
        void reset_lines();

    protected:
        sigc::connection m_ICConnection, m_OCConnection;
        //long m_StartTime, m_LastTime;
        Glib::Timer m_timer;
        bool m_Rec;

        type_line_list m_LineList;

        Gtk::DrawingArea m_Area;

        bool on_area_expose_event(GdkEventExpose* event);

        void draw_me(Cairo::RefPtr<Cairo::Context> cr, int w, int h);


        Gtk::VBox m_VBox_Toolbar;
        Gtk::VBox m_VBox;
        Gtk::HBox m_HBox;

        Gtk::ScrolledWindow m_ScrolledWindowCanvas;
        Gtk::Statusbar m_Statusbar;
        unsigned std_context;


        Glib::RefPtr<Gtk::UIManager> refUIManager;
        Glib::RefPtr<Gtk::ActionGroup> refActionGroup;
        Glib::RefPtr<Gtk::ActionGroup> refAnordnung;

        virtual void on_action_ZoomIn();
        virtual void on_action_ZoomOut();
        virtual void on_action_Zoom100();
        virtual void on_action_Refresh();
        virtual void on_action_Reset();
        virtual void on_action_Rec();
        virtual void on_action_Stop();
        virtual void on_action_Save();

        void contextMenu(int button);
        virtual bool on_my_key_release_event(GdkEventKey* event);


        void on_in_ch(const std::string name, bool value);
        void on_out_ch(const std::string name, bool value);

        void add_point(const std::string name, bool value, IO_TYPE type);

        long getImpulseTime();



}; // class DiagramWindow


#endif // DIAGRAMWINDOW_H
