/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef GENERALUNDOITEM_H
#define GENERALUNDOITEM_H

#include "UndoItem.h"

#include <string>

class CircuitTab;

class GeneralUndoItem : public UndoItem
{
    public:
        GeneralUndoItem(string name, CircuitTab *tab, UndoManager& m);
        virtual ~GeneralUndoItem();
        virtual void undo();
        virtual void clear();

        string undoFilename();
    protected:
        string filename;
        CircuitTab *pTab;
};

#endif // GENERALUNDOITEM_H
