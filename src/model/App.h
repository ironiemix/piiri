/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef APP_H
#define APP_H

#include <string>
#include <gtkmm.h>
#include <gio/gio.h>

#include "gui/MainWindow.h"
#include "gui/PropertyDialog2.h"
#include "model/Complex2.h"

#include <vector>
#include <set>
#include <map>

#define MAX_RECENT_PROJECTS 5

#define WIRING_PIIRI 0
#define WIRING_4590  1



class App
{
    public:

        // Der Pfad zu den PiiRi Daten im Benutzerverzeichnis (z.B. ~/piiri
        static std::string home_piiri_path;

        // etc-Pfad ../etc/piiri
        static std::string etc_piiri_path;

        // automatisch ermittelter piiri-Path - dort liegen die Einstellungen, die dann weiteres enthalten
        static std::string system_piiri_path;

        // aktueller Pfad zum cir-Ordner
        static std::string cir_path;

        // aktueller Pfad zum plan-Ordner
        static std::string plan_path;

        // Version des Programms
        static std::string version;

        // Name des Programms
        static std::string name;

        // Umleitung des Error-Streams
        static std::ostringstream err_os;

        // Auffangen von fehlenden Komponenten beim laden einer Schaltung
        static std::set<std::string> missingComponents;
        static std::set<std::string> getMissingComponents();

        // Verdrahtungsart
        static unsigned int wiringType;

        // Error-Buffer auslesen
        static const std::string getErrString(bool reset=true);

        // richtigen Aufbau des PiiRi-Heimverzeichnisses sicherstellen, Dateien und Ordner anlegen
        static void ensureHomeDir();

        // Programm starten
        static void init(int argc, char *argv[]);

        // Aufräumarbeiten
        static void end();

        // Status-Datei aktualisieren (für Wiederherstellung)
        static void updateLock();

        // Einstellungen laden und speichern
        static bool loadSettings();
        static bool saveSettings();

        // Pfade aktualisieren
        static void updatePaths();
        static void updateLiveSettings();

        // Dateinamen einer Komponente aus ihrem Namen erzeugen
        static std::string getCirFilename(const std::string& cir);

        // Programmeinstellungen
        static Complex2 settings;
        // Zeiger auf interne, nicht editierbare Einstellungen
        static Complex2* m_pInternSettings;

        // Kürzlich geöffnete Projekte
        static void addToRecentProjects(const std::string& pfn);
        static std::vector<std::string> getRecentProjects();

        // Text für Einstellungsdialog bereitstellen
        static void provideUiText(PropertyDialog2& d);

        // Verschiedene Dialoge erzeugen
        static bool question(const string& title, const string& msg, const string& info, Gtk::Window& parent);
        static bool question(const string& title, const string& msg, const string& info);
        static void message(const string& title, const string& msg, const string& info, Gtk::Window& parent);
        static void error(const string& title, const string& msg, const string& info, Gtk::Window* parent = NULL);
        static void error(const string& title, const string& msg, const string& info, Gtk::Window& parent);
        static void fetchError(const string& title, Gtk::Window& parent);
        static void fetchError(const string& title);

        // Externes Programm starten (plattformabhängig)
        static int startProgramm(std::string& prg, std::string& param);

        // HAuptfenster abrufen
        static MainWindow& getMW() {return *pWindow;};
        static MainWindow* getMWp() {return pWindow;};


        static const std::string& t(const std::string& id);
        static void loadLang(const std::string& filename);
        static std::map<std::string,std::string> texts;
        static std::string emptyString;

        static const char target_string[];
        static std::string m_ClipboardStore; //Keep copied stuff here, until it is pasted. This could be a big complex data structure.
        static void on_clipboard_get(Gtk::SelectionData& selection_data, guint info);
        static void on_clipboard_clear();

    protected:

    private:
        App() {}; // keine Instanz erzeugen
        virtual ~App() {};

        // Zeiger auf das Hauptfenster
        static MainWindow *pWindow;
};

#endif // APP_H
