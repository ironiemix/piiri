#ifndef LOGICPARSER_H
#define LOGICPARSER_H


#include <string>
#include <map>
#include <vector>
#include <stdio.h>
#include <cstdio>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

// #define LOGICPARSER_DEBUG 1


class LogicBin;

class LogicBin
{
    public:
        typedef map<string,bool> VarAssignment;

        LogicBin(string& func, LogicBin* p = NULL);
        ~LogicBin();
        virtual bool eval(VarAssignment& var);

        char getOperation() {return op;}
        LogicBin *getLHO() {return lho;}
        LogicBin *getRHO() {return rho;}
        LogicBin *getParent() {return parent;}
        string& getVar() {return var;}

        int depth();
        int layer();

        static bool eval(string func, VarAssignment& var, string& err);
        static int *logic_parser_levels(string& s);

    protected:
        LogicBin *lho, *rho, *parent;
        char op;
        string var;

        virtual bool parse(string& func);


    public:

        static char const opAND;
        static char const opOR;
        static char const opNOT;
        static char const opXOR;
        //static vector<char> ops;
        static int const ops_count = 4;
        static char const ops[ops_count];

}; // class LogicBin





#endif // LOGICPARSER_H
