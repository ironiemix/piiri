#ifndef PROJECT_H
#define PROJECT_H

#include <string>
#include <vector>
#include <libxml++/libxml++.h>

class Project
{
    public:
        Project(const std::string& fn, bool load = true);
        virtual ~Project();

        bool save();

        void setName(const std::string &n);
        //void setFolder(const std::string &fo);

        const std::vector<std::string> getOpenTabs();
        const std::vector<unsigned long> getOpenTabIds();

        void importToProject(const std::string& fn);

        const std::string & getName();
        const std::string getProjectFolderPath();
        const std::string getProjectFilePath();
        void setFilename(const std::string &fn);
        void setDir(const std::string &dr);
        const std::string & getFilename();
        //const std::string & getFolder();
        const std::string & getDir();

    protected:
    private:

        xmlpp::DomParser dom;

        std::string filename;
        std::string folder;
        std::string dir;
        std::string name;

        bool loadProjectFile();
        bool saveProjectFile();





};

#endif // PROJECT_H
