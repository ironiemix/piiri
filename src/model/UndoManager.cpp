/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "UndoManager.h"
#include "UndoItem.h"

#include <iostream>



//UndoManager *UndoManager::instance;


UndoManager::UndoManager(unsigned int _size, unsigned int id) :
max_size(_size),
next_number(0),
window_id(id)
{
}

UndoManager::~UndoManager()
{
    //cout << "~UndoManager()" << endl;
	clear();
}

void UndoManager::clear()
{
	UndoItem *i;
	while(!items.empty())
	{
		i = items.front();
		i->clear();
		items.pop_front();
		delete i;
	}
}

void UndoManager::addItem(UndoItem *item)
{
   // delete item;

    //cout << "[undo] add item " << item->getName() << endl;
	if (items.size() >= max_size)
	{
		UndoItem* u = items.back();
		items.pop_back();
		delete u; // kein clear? ja, denn ein neues undoitem verwendet bereits die datei!
	}

	items.push_front(item);

}


bool UndoManager::undo(bool& b)
{
    //cout << "undo" << endl;
	if (!items.empty())
	{
	    UndoItem* u;

		u = items.front();
		items.pop_front();
		u->clear();
		delete u;

        if (!items.empty())
        {
            u = items.front();
            u->undo();
            b = u->isSaved(); // b ist bool-ref
            //updateWindow();
            return true;
        }
	}
	return false;
}



void UndoManager::othersUnsaved()
{
    if (items.size() <= 1) return;

    UndoItemsType::iterator it;
    it = items.begin();
    it++; // erstens überspringen
    for (; it != items.end(); it++)
    {
        (*it)->setUnsaved();
    }
}

void UndoManager::setSaved()
{
    // cout << "setSaved" << endl;
    if (items.size() == 0) return;

    UndoItem* u = items.front();
    if (u)
    {
        u->setSaved();
        othersUnsaved();
    }
}

/** @brief nextNumber
  *
  * @todo: document this function
  */
void UndoManager::nextNumber()
{
    next_number++;
    if (next_number >= max_size) next_number = 0;
}


/** @brief prevNumber
  *
  * @todo: document this function
  */
void UndoManager::prevNumber()
{
    if (next_number == 0) next_number = max_size - 1;
    else next_number--;
}

/** @brief getCurrentAction
  *
  * @todo: document this function
  */
std::string UndoManager::getCurrentAction()
{
   string s;
	if (items.size() > 1)
	{
		UndoItem* u = items.front();
		s = u->getName();
	}
	else
	{

	}
	return s;
}



