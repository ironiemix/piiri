#include "Project.h"

#include "App.h"
#include <stdio.h>
#include <glib.h>
#include <glib/gstdio.h>
#include "gui/CircuitTab.h"

Project::Project(const std::string& fn, bool load)
{
    gchar *bn = g_path_get_basename(fn.c_str());
    gchar *dn = g_path_get_dirname(fn.c_str());
    string base(bn);
    string dir(dn);
    g_free(bn);
    g_free(dn);

    setFilename(base);
    setDir(dir);

    if (load && !fn.empty()) loadProjectFile();
}


Project::~Project()
{
    //dtor
}

/** @brief setName
  *
  * @todo: document this function
  */
void Project::setName(const std::string &n)
{
    name = n;
}

/** @brief setFolder
  *
  * @todo: document this function
  *
void Project::setFolder(const std::string &fo)
{
    folder = fo;
}*/

/** @brief setFilename
  *
  * @todo: document this function
  */
void Project::setFilename(const std::string &fn)
{
    filename = fn;
}

/** @brief getName
  *
  * @todo: document this function
  */
const std::string& Project::getName()
{
    return name;
}

/** @brief getFolder
  *
  * @todo: document this function
  *
const std::string& Project::getFolder()
{
    return folder;
}
*/

/** @brief getFilename
  *
  * @todo: document this function
  */
const std::string& Project::getFilename()
{
    return filename;
}


/** @brief getDir
  *
  * @todo: document this function
  */
const std::string & Project::getDir()
{
    return dir;
}

/** @brief setDir
  *
  * @todo: document this function
  */
void Project::setDir(const std::string &dr)
{
    dir = dr;
}

/** @brief getProjectFilePath
  *
  * @todo: document this function
  */
const std::string Project::getProjectFilePath()
{
    return getDir() + "/" + getFilename();
}

/** @brief getProjectFolderPath
  *
  * @todo: document this function
  */
const std::string Project::getProjectFolderPath()
{
    // ***
    return getDir(); // + "/" + getFolder();
}










/** @brief save
  *
  * @todo: document this function
  */
bool Project::save()
{
    bool b = saveProjectFile();
    if (!b)
    {
        cerr << "Projekt konnte nicht gespeichert werden." << endl;
    }
    return b;
}

/** @brief saveProjectFile
  *
  * @todo: document this function
  */
bool Project::saveProjectFile()
{
    if (!DirectoryExists(getProjectFolderPath().c_str()))
    {
        if(g_mkdir(getProjectFolderPath().c_str(), 0711) == 0)
        {
            if (!DirectoryExists((getProjectFolderPath()+"/cir").c_str())) g_mkdir((getProjectFolderPath()+"/cir").c_str(), 0711);
            if (!DirectoryExists((getProjectFolderPath()+"/plan").c_str())) g_mkdir((getProjectFolderPath()+"/plan").c_str(), 0711);
        }
        else
        {
            std::cerr << "Projekt-Ordner konnte nicht erstellt werden!" << std::endl;
        }
    }


    //dom.parse_memory("");
	xmlpp::Document *doc = dom.get_document();
    if (!doc) return false;
	xmlpp::Element *root = doc->create_root_node("PiiRiProject");
    if (!root) return false;

	//root->set_attribute("name", getName());

	// Settings
	//xmlpp::Element *settings = root->add_child("settings");
    //if (!settings) return false;

	xmlpp::Element *e;

	e = root->add_child("name");
	e->add_child_text(getName());

	e = root->add_child("file");
	e->add_child_text(getProjectFilePath());

	//e = root->add_child("folder");
	//e->add_child_text(getFolder());

	e = root->add_child("tabs");
	std::vector<CircuitTab*> tabs = App::getMW().getOpenTabs();

	xmlpp::Element *_e;
	ostringstream os;
	for (std::vector<CircuitTab*>::iterator it = tabs.begin(); it != tabs.end(); it++)
	{
        _e = e->add_child("tab");

		gchar* bn = g_path_get_basename((*it)->getTLCircuit().getFilename().c_str());
		string name(bn);
        _e->add_child_text(name);

        // tab-id
        os << (unsigned long)(void*)(*it);
        _e->set_attribute("id", os.str());

		g_free(bn);

		os.str("");
	}


    // std::cout << getProjectFilePath() << std::endl;
	if (isFileWritable(getProjectFilePath()))
	{
        doc->write_to_file_formatted(getProjectFilePath());
        return true;
	}

	return false;

}

/** @brief loadProjectFile
  *
  * @todo: document this function
  */
bool Project::loadProjectFile()
{
    dom.parse_file(getProjectFilePath());

    xmlpp::Document *doc = dom.get_document();
    if (!doc) return false;
    xmlpp::Element *root = doc->get_root_node();
    if (!root) return false;

    xmlpp::Element *e;

    e  = (xmlpp::Element *)(*root->get_children("name").begin());
    if (!e) return false;
    if (e->get_child_text()) setName(e->get_child_text()->get_content());

    /*e  = (xmlpp::Element *)(*root->get_children("folder").begin());
    if (!e) return false;
    if (e->get_child_text()) setFolder(e->get_child_text()->get_content());
    */

    /*e  = (xmlpp::Element *)(*root->get_children("file").begin());
    if (!e) return false;
    setFilename(e->get_child_text()->get_content());*/

    return true;
}

/** @brief getOpenTabs
  *
  * @todo: document this function
  */
const std::vector<std::string> Project::getOpenTabs()
{
    std::vector<std::string> arr;

	{ // wichtig!!
        xmlpp::Document *doc = dom.get_document();
        if (!doc) goto ende;
        xmlpp::Element *root = doc->get_root_node();
        if (!root) goto ende;
        xmlpp::Element *tabse  = (xmlpp::Element *)(*root->get_children("tabs").begin());
        if (!tabse) goto ende;


        xmlpp::Node::NodeList tabs = tabse->get_children("tab");
        xmlpp::Node::NodeList::iterator it;
        xmlpp::Element *e;

        for (it = tabs.begin(); it != tabs.end(); it++)
        {
            e = (xmlpp::Element *)(*it);
            arr.push_back(e->get_child_text()->get_content());
        }
	}

	ende:
    return arr;
}



/** @brief getOpenTabIds
  *
  * @todo: document this function
  */
const std::vector<unsigned long> Project::getOpenTabIds()
{
    std::vector<unsigned long> arr;

	{ // wichtig!!
        xmlpp::Document *doc = dom.get_document();
        if (!doc) goto ende;
        xmlpp::Element *root = doc->get_root_node();
        if (!root) goto ende;
        xmlpp::Element *tabse  = (xmlpp::Element *)(*root->get_children("tabs").begin());
        if (!tabse) goto ende;


        xmlpp::Node::NodeList tabs = tabse->get_children("tab");
        xmlpp::Node::NodeList::iterator it;
        xmlpp::Element *e;

        unsigned long id;
        for (it = tabs.begin(); it != tabs.end(); it++)
        {
            e = (xmlpp::Element *)(*it);
            istringstream is(e->get_attribute_value("id"));
            is >> id;
            std::cout << id << std::endl;
            arr.push_back(id);
        }
	}

	ende:
    return arr;
}




/** @brief importToProject
  *
  * @todo: document this function
  */
void Project::importToProject(const std::string& fn)
{

    gchar *bn = g_path_get_basename(fn.c_str());
    gchar *dn = g_path_get_dirname(fn.c_str());
    string base(bn);
    string dir(dn);
    g_free(bn);
    g_free(dn);

    cout << "[" << getName() << "] Import: " << fn << endl;

    std::string from(fn);
    std::string to(App::cir_path+"/"+base);

    //std::cout << "::: " << from << " --> " << to << endl;

    GFile* srcf = g_file_new_for_path(from.c_str());
    GFile* destf = g_file_new_for_path(to.c_str());
    GError *err = NULL;
    if (g_file_copy (
                 srcf,
                 destf,
                 (GFileCopyFlags)(G_FILE_COPY_TARGET_DEFAULT_PERMS), //flags G_FILE_COPY_OVERWRITE
                 //G_FILE_COPY_OVERWRITE, //flags
                 NULL, //cancellable
                 NULL, //&progressCallback, // progress callback
                 NULL, // progress callback data
                 &err
                 ))
    {
    }
    else
    {
        App::message("Fehler", err->message, "", App::getMW());
        g_error_free (err);
        err = NULL;
    }
    g_object_unref(srcf);
    g_object_unref(destf);




    ToplevelCircuit c;
    c.loadFile(fn);
    App::getErrString();

    set<string> missing = App::getMissingComponents();
    if (missing.size() > 0)
    {
        /*Gtk::Dialog dialog("Komponenten importieren", *this, true, true);
        dialog.add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
        Gtk::Label l("Folgende Komponenten müssen ebenfalls importiert werden:");
        dialog.get_vbox()->add(l);
        */

        //string s;
        for (set<string>::iterator it = missing.begin(); it != missing.end(); it++)
        {
            //cout << (base + "/" + (*it) + ".cir") << endl;
            importToProject(dir + "/" + (*it) + ".cir");
            //dialog.get_vbox()->add(*(Gtk::manage(new Gtk::CheckButton((*it)))));
            //s += (*it)+"\n";
        }
        /*
        dialog.show_all_children();
        dialog.run();*/
    }

}



