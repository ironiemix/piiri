#include "LogicSynth.h"

#include <string>

using namespace std;

LogicSynth::LogicSynth(Gtk::Window& parent) :
    Gtk::Dialog("Schaltungs-Synthese [BETA]", parent, true, true),
    m_Minimize("Als disjunktiven Normalform minimieren.")
{

	add_button("Ok", Gtk::RESPONSE_OK);
	add_button("Abbrechen", Gtk::RESPONSE_CANCEL);

	Gtk::VBox *vb = get_vbox();
    vb->set_spacing(5);
    set_border_width(10);

    //m_Term.set_text("(!a&b)|(!b&a)");
    //m_Term.set_text("!b&a");
    //m_Term.set_text("a|b&c^d");
    //m_Term.set_text("(a&b)|!(!a)");
    //m_Term.set_text("(!a&b&c)|(!b&a&c)|(!c&a)");

    Gtk::Label *txt = Gtk::manage(new Gtk::Label());

    txt->set_use_markup();
    txt->set_markup("<big>Schaltungs-Synthese BETA</big>\n\n"
                    "<i>Der unten angegebene Term wird analysiert und eine entsprechende\n"
                    "Schaltung wird entworfen.</i>\n\n"
                    "Verwenden Sie diese logischen Operatoren:\n"
                    "\tor:\t\t|\n\tand:\t&amp;\n\txor:\t\t^\n\tnot:\t\t!\n"
                    "Die Variablen, die Sie verwenden, werden automatisch als Eingänge\n"
                    "angelegt. Der Ausgang wird y heißen.\n\n"
                    "Geben Sie einen logischen (boolschen) Term ein:");


	vb->add(*txt);

	vb->add(m_Term);
	m_Minimize.set_sensitive(false);
	vb->add(m_Minimize);

	show_all();
}

LogicSynth::~LogicSynth()
{

}

int LogicSynth::run()
{
    int ret;

    ret = Gtk::Dialog::run();

    return ret;
}

bool LogicSynth::execute(EditableCircuit& obj)
{
    string term(m_Term.get_text());
    LogicBin lb(term);
    synth(obj, lb);
    return true;
}



/*
void synth_move(CircuitObject *obj, int y)
{
    if (obj->getPosY() != 0) return;
    if (obj == NULL) return;
    if (obj->getParent() == NULL) return;
    obj->setPosY(obj->getPosY()+y);
    if (obj->getCirName() == "not")
    {
        if (obj->getInput("a") == NULL) return;
        if (obj->getInput("a")->getFrom()) synth_move(obj->getInput("a")->getFrom()->getParent(), y);
    }
    else
    {
        if (obj->getInput("a") == NULL) return;
        if (obj->getInput("b") == NULL) return;
        if (obj->getInput("a")->getFrom()) synth_move(obj->getInput("a")->getFrom()->getParent(), y);
        if (obj->getInput("b")->getFrom()) synth_move(obj->getInput("b")->getFrom()->getParent(), y+80);
    }
}
*/




void LogicSynth::synth(EditableCircuit& obj, LogicBin& bin)
{
    CircuitIO *io, *out;

    max_layer = bin.depth();

    io = _synth(obj, bin);

    out = obj.getOutput("y");
    if (!out) out = obj.addOut("y");

    io->addForwarding(out);

    //synth_move(io->getParent(), 1);


}

int _get_number_in_layer(LogicBin *pbin, LogicBin *bin, int l, bool& finished)
{
    if (pbin == NULL) return 0;
    if (pbin->getOperation() == 0) return 0;
    if (pbin == bin)
    {
        finished = true;
        return 0;
    }

    if (pbin->layer() == l) return 1;

    int a = _get_number_in_layer(pbin->getLHO(), bin, l, finished);
    if (finished) return a;
    int b = _get_number_in_layer(pbin->getRHO(), bin, l, finished);
    return a+b;
}

int get_number_in_layer(LogicBin *bin, int l)
{
    if (!bin) return 0;

    LogicBin *pbin = bin;
    while (pbin->getParent())
    {
        pbin = pbin->getParent();
    }
    bool finished = false;
    return _get_number_in_layer(pbin, bin, l, finished);
}




CircuitIO *LogicSynth::_synth(EditableCircuit& cobj, LogicBin& bin)
{
    CircuitChild *obj = NULL;
    CircuitIO *io = NULL, *lio = NULL, *rio = NULL;

    int x=0,y=0;

    int layer = bin.layer();
    int number = get_number_in_layer(&bin, layer);

    x = (max_layer-layer) * 80;
    y = number * 80  + 10;

    switch (bin.getOperation())
    {
        case 0:
            io = cobj.getInput(bin.getVar());
            if (!io)
            {
                io = cobj.addIn(bin.getVar());
            }
        break;
        case '&':
        case '|':
        case '^':
        {
            string comp;
            switch (bin.getOperation())
            {
                case '&':
                    comp = "and";
                break;
                case '|':
                    comp = "or";
                break;
                case '^':
                    comp = "xor";
            }
            obj = cobj.addComponent(comp, "", x, y);
            if (obj)
            {
                io = obj->getOutput("x");
                if (bin.getLHO()) lio = _synth(cobj, *(bin.getLHO()));
                if (bin.getRHO()) rio = _synth(cobj, *(bin.getRHO()));
                if (lio) lio->addForwarding(obj->getInput("a"));
                if (rio) rio->addForwarding(obj->getInput("b"));
            }
        }
        break;
        case '!':
            obj = cobj.addComponent("not", "", x, y);
            if (obj)
            {
                io = obj->getOutput("x");
                if (bin.getRHO()) rio = _synth(cobj, *(bin.getRHO()));
                if (rio) rio->addForwarding(obj->getInput("a"));
            }
        break;
    }

    /*if (obj)
    {
        cout << obj->getName() << "\t " << number << ". auf L " << layer << endl;
    }*/

    return io;
}

