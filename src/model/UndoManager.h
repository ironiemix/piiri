/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef UNDOMANAGER_H
#define UNDOMANAGER_H

#include <sigc++/sigc++.h>
#include <deque>
#include <string>
using namespace std;

class UndoItem;

class UndoManager
{
    public:
        UndoManager(unsigned int max_size, unsigned int id);
        virtual ~UndoManager();


        void addItem(UndoItem *item);

        void setSaved();
        bool undo(bool& b);
        void clear();


        unsigned int max_size;
        unsigned int next_number;
        unsigned int window_id;



        void othersUnsaved();

        void nextNumber();
        void prevNumber();

        std::string getCurrentAction();

    protected:
        typedef deque<UndoItem*> UndoItemsType;
        UndoItemsType items;
};

#endif // UNDOMANAGER_H
