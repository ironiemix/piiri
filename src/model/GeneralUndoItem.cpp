/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "GeneralUndoItem.h"

#include <iostream>
#include <sstream>
#include <glib.h>
#include <glib/gstdio.h>

using namespace std;

#include "UndoManager.h"
#include "gui/CircuitTab.h"
#include "App.h"


GeneralUndoItem::GeneralUndoItem(string name, CircuitTab *tab, UndoManager& m) :
UndoItem(name, m),
pTab(tab)
{
    filename = GeneralUndoItem::undoFilename();
    //cout << "[GeneralUndoItem] " << name << " --> " << filename << endl;
    if (name != "L")
    {
        if (pTab->saveFile(filename))
        {
            //cout << "OK" << endl;
            manager.nextNumber();
        }
        else
        {
            cout << "ERROR: undo file nicht schreibbar (" << filename << ")" << endl;
        }
    }
}

GeneralUndoItem::~GeneralUndoItem()
{
    //cout << "~GeneralUndoItem (" << filename << ")" << endl;
    // kein clear? ja, denn ein neues undoitem verwendet bereits die datei!
}

void GeneralUndoItem::clear()
{
    g_unlink(filename.c_str());
}



string GeneralUndoItem::undoFilename()
{
	ostringstream os;
	os << App::home_piiri_path << "/undo-" << manager.window_id << "-" << manager.next_number << ".tmp";
    return string(os.str());
}



void GeneralUndoItem::undo()
{
	pTab->clear();
	pTab->openFile(filename); // cout << "loaded" << endl;
    manager.prevNumber();
}
