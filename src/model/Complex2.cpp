/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include "Complex2.h"
//#include "PropValue2.h"

#include "Helpers.h"

// alternative
// http://developer.gnome.org/glibmm/2.28/classGio_1_1Settings.html

std::ostream& operator<<(std::ostream& os, SelectionValue2 const& rhs)
{
    if (rhs.getSaveIndex())
    {
        os << rhs.get_value_index();
    }
    else
    {
        os << rhs.get_value();
    }
    return os;
}

std::istream& operator>>(std::istream& is, SelectionValue2& rhs)
{
    if (rhs.getSaveIndex())
    {
        unsigned int n;
        is >> n;
        rhs.set_value_index(n);
    }
    else
    {
        std::string s;
        getline(is, s);
        rhs.set_value(s);
    }
    return is;
}


bool Complex2::saveNew;
bool Complex2::loadNew;

// Complex2 Members

/**
Liest die Werte des Complex2-Objektes aus einem XML-Element.
Skalare Typen werden direkt eingefügt. Complex2-Elemente werden rekursiv verarbeitet.

    @param element XML-Element, das die Werte enthält.
*/
/*
void Complex2::readFromXml(const xmlpp::Element *element)
{
	xmlpp::Node::NodeList nl;
	xmlpp::Node::NodeList::iterator it;
	string name;
	const xmlpp::Element *e;
	string type;

	nl = element->get_children();

	for (it = nl.begin(); it != nl.end(); it++)
	{
		e = dynamic_cast<const xmlpp::Element*>((*it));
		if (e)
		{
			type = e->get_attribute_value("type");
			if (Complex2::loadNew)
			{
			    name = e->get_attribute_value("name");
			}
			else
			{
                name = e->get_name();
			}

			try
			{
                if (type == "complex")
                {
                    Complex2 *pc = this->set_complex(name);
                    pc->readFromXml(e);
                }
                else
                {
                    string s;

                    if (e->has_child_text())
                        s = e->get_child_text()->get_content();
                    else
                        s = "";

                    if (type == "int")
                    {
                        int i;
                        i = atoi(s.c_str());
                        this->set_int(name, i);
                    }
                    else if (type == "double")
                    {
                        istringstream is(s);
                        double d;
                        is >> d;
                        this->set_double(name, d);
                    }
                    else if (type == "bool")
                    {
                        bool b;
                        if ((s=="true")) b = true;
                        else b = false;
                        this->set_bool(name, b);
                    }
                    else if (type == "string")
                    {
                        this->set_string(name, s);
                    }
                    else if (type == "selection")
                    {
                        PropValue::SelectionValue* sv = this->set_selection(name);
                        if (sv) sv->set_value(s);
                    }
                    else
                    {
                        cout << "Complex2::readFromXml: ACHTUNG: Unbekannter type='" << type << "'." << endl;
                    }
                } // if complex else
			}
			catch (string& s)
			{
			    cout << s << endl;
			    cerr << s << endl;
			}
			catch (...)
			{
			    cout << "ydgydf" << endl;
			}
		}
	}
}
*/

/**
Bildet die Werte des Complex2-Objektes in einer XML-Struktur ab.
Skalare Typen werden direkt eingefügt. Complex2-Elemente werden rekursiv verarbeitet.

    @param element XML-Element, in dem die Werte erzeugt werden sollen.
*/
/*
void Complex2::writeToXml(xmlpp::Element *parent)
{
	xmlpp::Element *e;
	PropValue *pv;
	string s, name;
	Complex2 *pc;

	//parent->set_name(_name);
	parent->set_attribute("type", "complex");

	Complex2::iterator it;
	for (it = begin(); it != end(); it++)
	{
		name = (*it).first;
		pv = (*it).second;

        if (Complex2::saveNew)
        {
            e = parent->add_child("PropValue");
            e->set_attribute("name", name);
        }
        else
        {
            e = parent->add_child(name);
        }

		if (pv->getType() == PropValue::COMPLEX)
		{
			pc = get_complex(name);
			pc->writeToXml(e);
		}
		else
		{
			ostringstream os;

			if (pv->getType() == PropValue::BOOL)
			{
				e->set_attribute("type", "bool");
				bool b;
				b = get_bool(name);
				os << (b ? "true" : "false");
			}
			else if (pv->getType() == PropValue::INT)
			{
				e->set_attribute("type", "int");
				int i;
				i = get_int(name);
				os << i;
			}
			else if (pv->getType() == PropValue::DOUBLE)
			{
				e->set_attribute("type", "double");
				double d;
				d = get_double(name);
				os << d;
			}
			else if (pv->getType() == PropValue::STRING)
			{
				e->set_attribute("type", "string");
				os << get_string(name);
			}
			else if (pv->getType() == PropValue::SELECTION)
			{
				e->set_attribute("type", "selection");
				PropValue::SelectionValue* sv = get_selection(name);
				if (sv) os << sv->get_value();
			}

			e->add_child_text(os.str());
		}
	}

}
*/

void Complex2::clear()
{
    iterator it;
    TypedBase* pv;

    for (it = begin(); it != end(); it++)
    {
        pv = (*it).second;
        //(()pv)->clear();
        delete pv;
    }
    ComplexBase2::clear();
}

void Complex2::dump()
{
    iterator it;
    TypedBase* pv;
    cout << "Complex2-Dump {" << endl;
    for (it = begin(); it != end(); it++)
    {
        pv = (*it).second;
        cout << "[" << (*it).first << "]: ";
        pv->dump();
    }
    cout << "}" << endl;
}


/** @brief clone
  *
  * @todo: document this function
  */
void Complex2::clone(Complex2& src)
{
    iterator it, it2;
    TypedBase* pv;
    std::string name;
    for (it = src.begin(); it != src.end(); it++)
    {
        name = (*it).first;
        pv = (*it).second;

        it2 = find(name);
        if (it2 != end())
        {
            (*it2).second->clone(pv);
        }
        else
        {
            std::cerr << "Nicht existentes Property " << name << " kann nicht geklont werden!" << std::endl;
        }

        /*switch (pv->get_type())
        {
            case TypedBase::INT:
            set_int(name, ((PropValue2<int>*)pv)->get_value());
            break;

            default:
            break;
        }*/
    }
}




Gtk::Widget& Complex2::create_widget(std::string& path, ui_text_map& text_map)
{
    Complex2::iterator it;
    TypedBase* pv;
	std::string n, npath;
    ui_text title;
    ui_text_map::iterator t;

	Gtk::VBox *box = Gtk::manage(new Gtk::VBox());
	Gtk::Table *table = Gtk::manage(new Gtk::Table(size(), 2));
	table->set_spacings(5);
	box->add(*table);
    int num =0;
	for (it = begin(); it != end(); it++)
	{
		n = (*it).first;
		pv = (*it).second;
        npath = path+"/"+n;

        t = text_map.find(npath);
        if (t == text_map.end()) title = ui_text(npath,npath);
        else title = text_map[npath];

		if (pv->get_type() != TypedBase::COMPLEX)
		{
		    // Label
            Gtk::Label *l = Gtk::manage(new Gtk::Label(title.first+":", 1.0, 0.5));
            l->set_tooltip_markup(title.second);
            table->attach(*l, 0,1,num,num+1, Gtk::FILL, Gtk::FILL|Gtk::SHRINK); // l r t b

            Gtk::Widget& w = pv->create_widget(npath, text_map);
            table->attach(w, 1,2,num,num+1, Gtk::FILL|Gtk::EXPAND, Gtk::FILL|Gtk::SHRINK); // l r t b
            widgets[pv] = &w;

            num++;
		}
	}
	return *box;
}

bool Complex2::read_widget(bool checkOnly)
{

    Complex2::iterator it;
    bool ret=false;
    TypedBase* pv;
	std::string n;

	for (it = begin(); it != end(); it++)
	{
		n = (*it).first;
		pv = (*it).second;
		//std::cout << "pname: " << n << std::endl;
		if (pv->get_type() == TypedBase::COMPLEX)
		{
            //std::cout << "  complex" << std::endl;
            PropValue2<Complex2>* pc = (PropValue2<Complex2>*)pv;
            if (pc->get_pointer()->editAllowed())
            {
                //std::cout << "  call read_widget" << std::endl;
                if (pc->get_pointer()->read_widget(checkOnly)) ret = true;

                //pc->get_pointer()->dump();
                //std::cout << ((void*)pc->get_pointer()) << std::endl;
            }
		}
		else
		{
            //std::cout << "  normal" << std::endl;
            //std::cout << "  call read_widget" << std::endl;

            Gtk::Widget* w = widgets[pv];

            if (pv->read_widget(*w, checkOnly)) ret = true;
		}
	}
	return ret;
}



Gtk::Widget& SelectionValue2::create_widget(std::string& path, ui_text_map& text_map)
{
    Gtk::ComboBoxText* b = Gtk::manage(new Gtk::ComboBoxText());
    type_property_selection_list::iterator it;
    for (it = get_options().begin(); it != get_options().end(); it++)
    {
        b->append_text((*it));
    }
    b->set_active_text(get_value());
    return *b;
}
/** @brief read_widget
  *
  * @todo: document this function
  */
bool SelectionValue2::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    //std::cout << "SelectionValue2::read_widget" << std::endl;
    Gtk::ComboBoxText* w = dynamic_cast<Gtk::ComboBoxText*>(&_w);
    if (w)
    {
        std::string s = w->get_active_text();
        if (get_value() != s)
        {
            if(!checkOnly) set_value(s);
            return true;
        }
    }
    else throw (std::string("[PropValue2] Falsches Widget."));
    return false;
}







/** @brief write_xml
  *
  * @todo: document this function
  */
bool Complex2::write_xml(xmlpp::Element* parent)
{

	xmlpp::Element *e;
	TypedBase* pv;
	string s, name;
	Complex2* pc;

	parent->set_attribute("type", "complex");

	Complex2::iterator it;
	for (it = begin(); it != end(); it++)
	{
		name = (*it).first;
		pv = (*it).second;

        if (Complex2::saveNew)
        {
            e = parent->add_child("PropValue");
            e->set_attribute("name", name);
        }
        else
        {
            e = parent->add_child(name);
        }

		if (pv->get_type() == TypedBase::COMPLEX)
		{
			pc = get_complex(name);
			pc->write_xml(e);
		}
		else
		{
		    pv->write_xml(e);
		}
	}

    return true;
}

/** @brief read_xml
  *
  * @todo: document this function
  */
bool Complex2::read_xml(const xmlpp::Element* element)
{
	xmlpp::Node::NodeList nl;
	xmlpp::Node::NodeList::iterator it;
	string name;
	const xmlpp::Element *e;
	string type;

	nl = element->get_children();

	for (it = nl.begin(); it != nl.end(); it++)
	{
		e = dynamic_cast<const xmlpp::Element*>((*it));
		if (e)
		{
			type = e->get_attribute_value("type");
			if (Complex2::loadNew)
			{
			    name = e->get_attribute_value("name");
			}
			else
			{
                name = e->get_name();
			}

			if ((find(name) == end()) && !allowedXmlToCreateNew())
			{
			    std::cerr << "Auf Property [" << name << "] gestoßen, wird aber nicht erwartet. Wird übersprungen. Das liegt vermutlich an einem alten Dateiformat!" << std::endl;
			    continue;
			}

			try
			{
                if (type == "complex")
                {
                    Complex2 *pc = this->set_complex(name);
                    pc->read_xml(e);
                }
                else
                {
                    if (type == "int")
                    {
                        this->set_int(name);
                        (*this)[name]->read_xml(e);
                    }
                    else if (type == "double")
                    {
                        this->set_double(name);
                        (*this)[name]->read_xml(e);
                    }
                    else if (type == "bool")
                    {
                        this->set_bool(name);
                        (*this)[name]->read_xml(e);
                    }
                    else if (type == "string")
                    {
                        this->set_string(name);
                        (*this)[name]->read_xml(e);
                    }
                    else if (type == "selection")
                    {
                        //std::cout << "selection in complex read xml" << std::endl;
                        this->set_selection(name);
                        (*this)[name]->read_xml(e);
                        //if (sv) sv->read_xml(e);// ist bei selectionvalue nicht wirklich implementiert - was ein durcheinander^^
                    }
                    else
                    {
                        cout << "Complex2::read_xml: ACHTUNG: Unbekannter type='" << type << "'." << endl;
                    }
                } // if complex else
			}
			catch (string& s)
			{
			    cout << s << endl;
			    cerr << s << endl;
			}
			catch (...)
			{
			    cout << "Exception: Complex2::read_xml" << endl;
			}
		}
	}


    return true;
}

















Complex2* Complex2::get_complex_path(const string& path)
{
    TypedBase* pv = get_prop_path(path);
    if ((pv!=NULL) && (pv->get_type() == TypedBase::COMPLEX))
    {
        return ((PropValue2<Complex2>*)pv)->get_pointer();
    }
    else {cout << "PropPath '" << path << "' nicht gefunden!" << endl; return NULL;}
}


// TODO absichern!
TypedBase* Complex2::get_prop_path(const string& path)
{
    vector<string> sp;
    int c,n=0;
    c = splitString(sp, path, '/');

    if (c==0) return NULL;

    Complex2* cmplx = this;
    TypedBase* pv = NULL;


    while (n < c)
    {
        if (cmplx->find(sp[n]) != cmplx->end())
        {
            pv = (*cmplx)[sp[n]];
        }
        else return NULL;
        if (n<c-1)
        {
            if (pv->get_type() == TypedBase::COMPLEX) cmplx = ((PropValue2<Complex2>*)pv)->get_pointer();
            else return NULL;
        }
        n++;
    }

    return pv;
}









Complex2* Complex2::set_complex(const string& name)
{
    return set_pointer<Complex2,TypedBase::COMPLEX>(name);
}

SelectionValue2* Complex2::set_selection(const string& name)
{
    return set_pointer<SelectionValue2,TypedBase::SELECTION>(name);
}

int Complex2::set_int(const std::string& name, int v)
{
    return set_value<int,TypedBase::INT>(name, v);
}

bool Complex2::set_bool(const std::string& name, bool v)
{
    return set_value<bool,TypedBase::BOOL>(name, v);
}

double Complex2::set_double(const std::string& name, double v)
{
    return set_value<double,TypedBase::DOUBLE>(name, v);
}
std::string Complex2::set_string(const std::string& name, const std::string& v)
{
    return set_value<std::string,TypedBase::STRING>(name, v);
}



Complex2* Complex2::get_complex(const string& name)
{
    return get_pointer<Complex2,TypedBase::COMPLEX>(name);
}

SelectionValue2* Complex2::get_selection(const string& name)
{
    return get_pointer<SelectionValue2,TypedBase::SELECTION>(name);
}

int Complex2::get_int(const std::string& name)
{
    return get_value<int,TypedBase::INT>(name, 0);
}
bool Complex2::get_bool(const std::string& name)
{
    return get_value<bool,TypedBase::BOOL>(name, false);
}
double Complex2::get_double(const std::string& name)
{
    return get_value<double,TypedBase::DOUBLE>(name, 0.0f);
}
std::string Complex2::get_string(const std::string& name)
{
    return get_value<std::string,TypedBase::STRING>(name, "");
}






