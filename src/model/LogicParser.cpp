#include "LogicParser.h"

#include "App.h"


char const LogicBin::opAND = '&';
char const LogicBin::opOR = '|';
char const LogicBin::opNOT = '!';
char const LogicBin::opXOR = '^';

char const LogicBin::ops[LogicBin::ops_count] = {'|', '&', '^', '!'};


/*vector<char> LogicBin::ops;
LogicBin::ops.push_back(LogicBin::opOR);
LogicBin::ops.push_back(LogicBin::opAND);
LogicBin::ops.push_back(LogicBin::opXOR);
LogicBin::ops.push_back(LogicBin::opNOT);*/

LogicBin::LogicBin(string& func, LogicBin* p) :
    lho(NULL),
    rho(NULL),
    parent(p),
    op(0)
{
    parse(func);
}

LogicBin::~LogicBin()
{
    if (lho) delete lho;
    if (rho) delete rho;
}

bool LogicBin::eval(VarAssignment& var)
{
    bool lh=false, rh=false;

    if (lho) lh = lho->eval(var);
    if (rho) rh = rho->eval(var);

    switch (op)
    {
        case 0:
            // ist es eine Variable?
            if (var.find(this->var) != var.end())
            {
                return var[this->var];
            }
            else return false;
        case opAND:
            return lh && rh;
        case opOR:
            return lh || rh;
        case opNOT:
            return !rh;
        case opXOR:
            return (!lh & rh)|(lh & !rh);
    }
    return false;
}

bool LogicBin::parse(string& func)
{
    trim_inplace(func);
    if (func.empty()) return false;


    int *level;
    int l;

    level = logic_parser_levels(func);
    if (!level) return false; // Klammerfehler

    l = func.length();

    // von klammer eingeschlossen?
    if ((func[0] == '('))
    {
        int n = 1;
        while ((n<l) && (level[n]!=0))
        {
            n++;
        }
        if ((n==l-1) && (func[n]==')'))
        {
            #ifdef LOGICPARSER_DEBUG
            cout << "klammern" << endl;
            #endif

            func.replace(0,1,"");
            func.replace(func.length()-1,func.length(),"");

            if (level) delete[] level;
            level = logic_parser_levels(func);
            if (!level) return false; // Klammerfehler
            l -= 2;
        }
    }

    char ch;

    for (int i=0; i<ops_count; i++)
    {
        for (int n = l-1; n>=0; n--)
        {
            if (level[n] > 0) continue;

            ch = func[n];
            if (ch == ops[i])
            {
                #ifdef LOGICPARSER_DEBUG
                cout << "Operator found: " << ops[i] << endl;
                #endif
                string lh = func.substr(0, n);
                string rh = func.substr(n+1);

                trim_inplace(lh);
                trim_inplace(rh);

                #ifdef LOGICPARSER_DEBUG
                cout << lh << " ["<<ch<<"] " << rh << endl;
                #endif

                op = ch;

                if (!lh.empty()) lho = new LogicBin(lh, this);
                if (!rh.empty()) rho = new LogicBin(rh, this);

                if (level) delete[] level;
                return true;
             }
        }
    }

    op = 0;
    var = func;

    if (level) delete[] level;

    return true;
} // parse


int LogicBin::depth()
{
    int r = 0, l = 0;
    if (lho) l = lho->depth()+1;
    if (rho) r = rho->depth()+1;
    return ((l>r) ? l : r);
}

int LogicBin::layer()
{
    if (parent == NULL) return 0;
    return parent->layer()+1;
}



int *LogicBin::logic_parser_levels(string& s)
{
    int l = s.length();
    int lvl = 0;
    int *level;
    char ch;
    level = new int[l];
    #ifdef LOGICPARSER_DEBUG
    cout << s << endl;
    #endif
    for (int n = 0; n<l; n++)
    {
        ch = s[n];
        if (ch == ')')
        {
            lvl--;
        }
        level[n] = lvl;
        #ifdef LOGICPARSER_DEBUG
        cout << lvl;
        #endif
        if (ch == '(')
        {
            lvl++;
        }
    }
    #ifdef LOGICPARSER_DEBUG
    cout << endl;
    #endif
    if (lvl==0)
    {
        return level;
    }
    else
    {
        delete[] level;
        return NULL;
    }
}

/** @brief eval
  *
  * @todo: document this function

  a0 & a2 | a0 = a0 & (a2 | a0) | x
                 000000111111100000


  */

bool LogicBin::eval(string func, VarAssignment& var, string& err)
{
    LogicBin lb(func);
    return lb.eval(var);
}

