/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef UNDOITEM_H
#define UNDOITEM_H

#include <string>
using namespace std;

#include <gtkmm.h>


class UndoManager;

class UndoItem
{

    protected:
        UndoItem(string name, UndoManager& m);

        UndoManager& manager;
        string name;
        bool saved;

    public:
        virtual ~UndoItem() {};

        virtual void undo() = 0;
        virtual void clear() = 0;
        const string& getName() {return name;}
        UndoManager& getManager() {return manager;}
        //void setManager(UndoManager *m) {manager = m;}
        bool isSaved() {return saved;}
        void setSaved() {saved = true;}
        void setUnsaved() {saved = false;}
};

#endif // UNDOITEM_H
