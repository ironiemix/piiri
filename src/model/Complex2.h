/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SCHALTUNG_COMPLEX2_H
#define SCHALTUNG_COMPLEX2_H

#include <cstdlib>
#include <iostream>
using namespace std;
#include <map>
#include <vector>
#include <string>
#include <glib-object.h>
#include <libxml++/libxml++.h>
#include <gtkmm.h>

#include "gui/UiText.h"

class Complex2;
//class TypedBase;

template <typename T>
class PropValue2;











/**

typ-Abhängige funktionen:

-initialisieren
-loschen

-wert setzen (typ) // hier wird dann nic ht zwischen SelectionValue* und string* zum beispiel unterschieden weil Selectionvalue bzw. PropValue dann allgemein der daten typ der items wäre
-wert holen (typ)

-Widget erstellen
-widget auslesen

-xml erzeugen
-xml auslesen

*/

//template <typename T>
//class PropValue2;

class TypedBase
{
    public:
        enum TYPE {NONE, INT,DOUBLE,STRING,BOOL,COMPLEX,SELECTION};

        TypedBase(TYPE t) {type = t;}
        virtual ~TypedBase() {}
        TYPE get_type() {return type;};
        virtual std::string get_type_string() = 0;

        virtual void clear() = 0;
        virtual void dump() = 0;

        virtual void clone(TypedBase* pv) = 0;

        //typedef PropValue2<int> int_type;
        //operator int_type*();

        virtual Gtk::Widget& create_widget(std::string& path, ui_text_map& text_map) = 0;
        virtual bool read_widget(Gtk::Widget& w, bool checkOnly) = 0; // return: changed

        virtual bool write_xml(xmlpp::Element *parent) = 0;
        virtual bool read_xml(const xmlpp::Element *element) = 0;

    protected:
        TYPE type;
};

/*
inline TypedBase::operator int_type*()
{
    PropValue2<int>* pv;
//    pv = this;
    return pv;
}*/


template <typename T>
class PropValue2 : public TypedBase
{
    private:
        void *data;

    public:
        PropValue2() :
         TypedBase(NONE),
         data(NULL)
        {init();};

        ~PropValue2(){clear();}



        // typabhängige members

    protected:
        void set_type();
        virtual std::string get_type_string();
        void init();

    public:
        void clear();
        void dump();
        virtual void clone(TypedBase* pv) {_clone((PropValue2<T>*)pv);}
        virtual void _clone(PropValue2<T>* pv);

        std::string get_value_string();
        void value_from_string(std::string& s);
        T get_value();
        T* get_pointer();

        T set_value(T v);
        void set_default();

        virtual Gtk::Widget& create_widget(std::string& path, ui_text_map& text_map);
        virtual bool read_widget(Gtk::Widget& w, bool checkOnly); // return: changed

        bool write_xml(xmlpp::Element *parent);
        bool read_xml(const xmlpp::Element *element);
};


///++++++++++++++++++++++++++++++++++++++++++++++++



class SelectionValue2 //: public TypedBase
{
    public:
        typedef std::string type_property_selection_list_item;
        typedef std::vector<type_property_selection_list_item> type_property_selection_list;

        SelectionValue2():
            m_saveIndex(false)
        {
        }

        void add_option(type_property_selection_list_item o)
        {
            options.push_back(type_property_selection_list_item(o));
        }

        virtual std::string get_type_string() {return "selection";};

        virtual void clear() {};
        virtual void dump()
        {
            std::cout << "SELECTION(";
            bool a = false;
            for (type_property_selection_list::iterator it = options.begin(); it != options.end(); it++)
            {
                if (a) std::cout << ",";
                std::cout << (*it);
                a = true;
            }
            std::cout << ") = " << value << std::endl;
        };

        //virtual void clone(TypedBase* pv) {};
        virtual void clone(SelectionValue2& pv) {
            set_value(pv.get_value());
        };

        virtual Gtk::Widget& create_widget(std::string& path, ui_text_map& text_map);
        virtual bool read_widget(Gtk::Widget& w, bool checkOnly); // return: changed


        //virtual bool write_xml(xmlpp::Element *parent) {return false;};
        //virtual bool read_xml(const xmlpp::Element *element) {return false;};


        void set_value(type_property_selection_list_item v) {value = v;}
        type_property_selection_list_item get_value() {return value;}
        type_property_selection_list_item get_value() const {return value;}

        unsigned int get_value_index() const
        {
            return const_cast<SelectionValue2*>(this)->get_value_index();
        }

        unsigned int get_value_index()
        {
            unsigned int n = 0;
            for (type_property_selection_list::iterator it = options.begin(); it != options.end(); it++)
            {
                if ((*it) == get_value()) return n;
                n++;
            }
            return -1;
        }
        void set_value_index(unsigned int n)
        {
            if ((n>0)&&(n<options.size()))
            {
                set_value(options.at(n));
            }
        }

        type_property_selection_list& get_options() {return options;}

        void setSaveIndex(bool si) {m_saveIndex=si;};
        bool getSaveIndex() {return m_saveIndex;};
        bool getSaveIndex() const {return m_saveIndex;};


    protected:
        type_property_selection_list options;
        type_property_selection_list_item value;
        bool m_saveIndex;
};

std::ostream& operator<<(std::ostream& os, SelectionValue2 const& rhs);
std::istream& operator>>(std::istream& is, SelectionValue2& rhs);


///++++++++++++++++++++++++++++++++++++++++++++++++









typedef std::map<string,TypedBase* > ComplexBase2;

class Complex2 : public ComplexBase2
{
public:
    static bool saveNew;
    static bool loadNew;

	Complex2() :
        allowEdit(true),
        allowXmlToCreateNew(false)
	{
	}

	~Complex2()
	{
		clear();
	}

	/**
	 * Macht nur etwas, wenn der Typ pointer ist: löscht das dahinterliegende Complex2-Objekt
	 */

	void clear();
	void dump();
	void clone(Complex2& src);

    virtual Gtk::Widget& create_widget(std::string& path, ui_text_map& text_map);
    virtual bool read_widget(Gtk::Widget& w, bool checkOnly) {return read_widget(checkOnly);}; // return: changed
    virtual bool read_widget(bool checkOnly); // return: changed


	void setAllowEdit(bool ae) {allowEdit = ae;};
	bool editAllowed() { return allowEdit;};
	bool allowedXmlToCreateNew() {return allowXmlToCreateNew;};
	void setAllowXmlToCreateNew(bool cn) {allowXmlToCreateNew = cn;};

protected:
    template <typename VALUE_TYPE, TypedBase::TYPE T>
    VALUE_TYPE set_value(const std::string& name, VALUE_TYPE v);

    template <typename VALUE_TYPE, TypedBase::TYPE T>
    VALUE_TYPE* set_pointer(const std::string& name);

    template <typename VALUE_TYPE, TypedBase::TYPE T>
    VALUE_TYPE get_value(const string& name, VALUE_TYPE d);

    template <typename VALUE_TYPE, TypedBase::TYPE T>
    VALUE_TYPE* get_pointer(const string& name);

public:
	Complex2* set_complex(const string& name);
    SelectionValue2* set_selection(const string& name);
    int    set_int(const std::string& name, int v=0);
    bool   set_bool(const std::string& name, bool v=false);
    double set_double(const std::string& name, double v=0.0);
    std::string set_string(const std::string& name, const std::string& v=std::string());

	Complex2* get_complex(const string& name);
    SelectionValue2* get_selection(const string& name);
    int    get_int(const std::string& name);
    bool   get_bool(const std::string& name);
    double get_double(const std::string& name);
    std::string get_string(const std::string& name);




	Complex2 *get_complex_path(const string& path);
	bool get_bool_path(const string& path)
	{
        TypedBase* pv = get_prop_path(path);
        if ((pv!=NULL) && (pv->get_type() == TypedBase::BOOL))
        {
            return ((PropValue2<bool>*)pv)->get_value();
        }
        else {cout << "PropPath '" << path << "' nicht gefunden!" << endl; return NULL;}
	}

	virtual bool read_xml(const xmlpp::Element* element);
	virtual bool write_xml(xmlpp::Element* parent);

protected:
    typedef std::map<TypedBase*, Gtk::Widget*> wmap;
    wmap widgets;


private:
    bool allowEdit, allowXmlToCreateNew;

    // TODO absichern!
	TypedBase* get_prop_path(const string& path);

}; // Complex2



template <typename VALUE_TYPE, TypedBase::TYPE T>
VALUE_TYPE Complex2::set_value(const std::string& name, VALUE_TYPE v)
{
    typedef PropValue2<VALUE_TYPE> my_type;
    my_type* pv;
    if (find(name) == end())
    {
        pv = new my_type();
        (*this)[name] = pv;
    }
    else
    {
        pv = (my_type*)((*this)[name]);
        if (pv->get_type() != T) throw (std::string("Property(value) '" +name+"' hat bereits anderen Typ."));
    }
    pv->set_value(v);
    return v;
}

template <typename VALUE_TYPE, TypedBase::TYPE T>
VALUE_TYPE* Complex2::set_pointer(const std::string& name)
{
    typedef PropValue2<VALUE_TYPE> my_type;

    my_type* pv;
    if (find(name) == end())
    {
        pv = new my_type();
        (*this)[name] = pv;
    }
    else
    {
        pv = (my_type*)((*this)[name]);
        if (pv->get_type() != T) throw (std::string("Property(pointer) '" +name+"' hat bereits anderen Typ."));
    }
    return pv->get_pointer();
}


template <typename VALUE_TYPE, TypedBase::TYPE T>
VALUE_TYPE Complex2::get_value(const string& name, VALUE_TYPE d)
{
    typedef PropValue2<VALUE_TYPE> my_type;

    if (find(name) == end()) throw (std::string("Property(value) '" +name+"' nicht vorhanden.")); // set_value<VALUE_TYPE,T>(name, d);

    my_type* pv = (my_type*)((*this)[name]);

    if (pv->get_type() != T) throw (std::string("Property(value) '" +name+"' hat einen anderen Typ."));

    return pv->get_value();
}

template <typename VALUE_TYPE, TypedBase::TYPE T>
VALUE_TYPE* Complex2::get_pointer(const string& name)
{
    typedef PropValue2<VALUE_TYPE> my_type;

    if (find(name) == end()) throw (std::string("Property(pointer) '" +name+"' nicht vorhanden.")); //set_pointer<VALUE_TYPE,T>(name);

    my_type* pv = (my_type*)((*this)[name]);

    if (pv->get_type() != T) throw (std::string("Property(pointer) '" +name+"' hat einen anderen Typ."));

    return pv->get_pointer();
}



///+++++++++++++++++++++++++++++++++++++++++++++++++++++++



template <>
inline void PropValue2<int>::set_type()
{
    type = INT;
}
template <>
inline void PropValue2<double>::set_type()
{
    type = DOUBLE;
}
template <>
inline void PropValue2<Complex2>::set_type()
{
    type = COMPLEX;
}
template <>
inline void PropValue2<bool>::set_type()
{
    type = BOOL;
}
template <>
inline void PropValue2<std::string>::set_type()
{
    type = STRING;
}
template <>
inline void PropValue2<SelectionValue2>::set_type()
{
    type = SELECTION;
}




template <>
inline std::string PropValue2<int>::get_type_string()
{
    return "int";
}
template <>
inline std::string PropValue2<double>::get_type_string()
{
    return "double";
}
template <>
inline std::string PropValue2<Complex2>::get_type_string()
{
    return "complex";
}
template <>
inline std::string PropValue2<bool>::get_type_string()
{
    return "bool";
}
template <>
inline std::string PropValue2<std::string>::get_type_string()
{
    return "string";
}
template <>
inline std::string PropValue2<SelectionValue2>::get_type_string()
{
    return "selection";
}






template <typename T>
void PropValue2<T>::clear()
{
    if (data) delete ((T*)data);
    data = NULL;
}


template <typename T>
void PropValue2<T>::init()
{
    clear();
    data = new T;
    set_type();
}


template <typename T>
T PropValue2<T>::get_value()
{
    return *((T*)data);
}
template <typename T>
inline T* PropValue2<T>::get_pointer()
{
    return ((T*)data);
}



template <typename T>
std::string PropValue2<T>::get_value_string()
{
    ostringstream os;
    os << get_value();
    return os.str();
}
template <>
inline std::string PropValue2<bool>::get_value_string()
{
    return (get_value() ? "true" : "false");
}



template <typename T>
void PropValue2<T>::value_from_string(std::string& s)
{
    istringstream is(s);
    T* v = (T*)data;
    is >> *v;
    //return v;
}
template <>
inline void PropValue2<bool>::value_from_string(std::string& s)
{
    set_value(((s=="true") || (s=="True") || (s=="TRUE")));
}
template <>
inline void PropValue2<std::string>::value_from_string(std::string& s)
{
    set_value(s);
}



template <typename T>
void PropValue2<T>::dump()
{
    std::cout << "(" << get_type_string() << ")" << get_value_string() << std::endl;
}

template <>
inline void PropValue2<Complex2>::dump()
{
    get_pointer()->dump();
}




template <typename T>
T PropValue2<T>::set_value(T v)
{
    *((T*)data) = v;
    return *((T*)data);
}


template <>
inline void PropValue2<int>::set_default()
{
    //int a = 0;
    set_value(0);
}


template <>
inline Gtk::Widget& PropValue2<int>::create_widget(std::string& path, ui_text_map& text_map)
{
    Gtk::Entry* w = Gtk::manage(new Gtk::Entry);
    std::ostringstream os;
    os << get_value();
    w->set_text(os.str());
    return *w;
}
template <>
inline Gtk::Widget& PropValue2<double>::create_widget(std::string& path, ui_text_map& text_map)
{
    Gtk::Entry* w = Gtk::manage(new Gtk::Entry);
    std::ostringstream os;
    os << get_value();
    w->set_text(os.str());
    return *w;
}
template <>
inline Gtk::Widget& PropValue2<std::string>::create_widget(std::string& path, ui_text_map& text_map)
{
    Gtk::Entry* w = Gtk::manage(new Gtk::Entry);
    std::ostringstream os;
    os << get_value();
    w->set_text(os.str());
    return *w;
}
template <>
inline Gtk::Widget& PropValue2<bool>::create_widget(std::string& path, ui_text_map& text_map)
{
    Gtk::CheckButton* w = Gtk::manage(new Gtk::CheckButton);
    w->set_active(get_value());
    return *w;
}
template <class T>
inline Gtk::Widget& PropValue2<T>::create_widget(std::string& path, ui_text_map& text_map)
{
    T* pv = get_pointer();
    return pv->create_widget(path, text_map);
}




template <>
inline bool PropValue2<int>::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    Gtk::Entry* w = dynamic_cast<Gtk::Entry*>(&_w);
    if (w)
    {
        std::istringstream is(w->get_text());
        int n;
        is >> n;
        if (get_value() != n)
        {
            if(!checkOnly) set_value(n);
            return true;
        }
    }
    else throw (std::string("[PropValue2] Falsches Widget."));
    return false;
}
template <>
inline bool PropValue2<bool>::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    Gtk::CheckButton* w = dynamic_cast<Gtk::CheckButton*>(&_w);
    if (w)
    {
        bool b = w->get_active();
        if (get_value() != b)
        {
            if(!checkOnly) set_value(b);
            return true;
        }
    }
    else throw (std::string("[PropValue2] Falsches Widget."));
    return false;
}
template <>
inline bool PropValue2<double>::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    Gtk::Entry* w = dynamic_cast<Gtk::Entry*>(&_w);
    if (w)
    {
        std::istringstream is(w->get_text());
        double d;
        is >> d;
        if (get_value() != d)
        {
            if(!checkOnly) set_value(d);
            return true;
        }
    }
    else throw (std::string("[PropValue2] Falsches Widget."));
    return false;
}
template <>
inline bool PropValue2<std::string>::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    Gtk::Entry* w = dynamic_cast<Gtk::Entry*>(&_w);
    if (w)
    {
//std::cout << "  richtiges widget" << std::endl;

        std::string s = w->get_text();

//std::cout << "  '"<<s<<"'" << std::endl;

        if (get_value() != s)
        {
            set_value(s);
//if(!checkOnly)         std::cout << "  '"<< get_value(s) <<"'" << std::endl;
//std::cout << "  geschrieben" << std::endl;
            return true;
        }
    }
    else throw (std::string("[PropValue2] Falsches Widget."));
    return false;
}
template <class T>
inline bool PropValue2<T>::read_widget(Gtk::Widget& _w, bool checkOnly)
{
    T* pv = get_pointer();
    return pv->read_widget(_w, checkOnly);
}





template <>
inline bool PropValue2<Complex2>::write_xml(xmlpp::Element *parent)
{
    return get_pointer()->write_xml(parent);
}
template <typename T>
bool PropValue2<T>::write_xml(xmlpp::Element *parent)
{
    parent->set_attribute("type", get_type_string());
    parent->add_child_text(get_value_string());

    return true;
}

template <>
inline bool PropValue2<Complex2>::read_xml(const xmlpp::Element* element)
{
    return get_pointer()->read_xml(element);
}
template <typename T>
bool PropValue2<T>::read_xml(const xmlpp::Element* element)
{
    string s;
    if (element->has_child_text())
        s = element->get_child_text()->get_content();
    else
        s = "";


    value_from_string(s);

    //std::cout << "read_xml (" << get_type_string() << ") '" << s << "': " << get_value_string() << std::endl;

    return true;
}


template <typename T>
void PropValue2<T>::_clone(PropValue2<T>* pv)
{
    get_pointer()->clone(*(pv->get_pointer()));
}
template <>
inline void PropValue2<int>::_clone(PropValue2<int>* pv)
{
    set_value(pv->get_value());
}
template <>
inline void PropValue2<bool>::_clone(PropValue2<bool>* pv)
{
    set_value(pv->get_value());
}
template <>
inline void PropValue2<double>::_clone(PropValue2<double>* pv)
{
    set_value(pv->get_value());
}
template <>
inline void PropValue2<std::string>::_clone(PropValue2<std::string>* pv)
{
    set_value(pv->get_value());
}



#endif
