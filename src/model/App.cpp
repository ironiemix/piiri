/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "App.h"

#include "gui/PropertyDialog2.h"
#include "gui/RecoverDlg.h"
#include "iow/IOWFactory.h"


#include <dirent.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <libxml++/libxml++.h>
#include <fstream>
#include <map>


// http://developer.gnome.org/glib/2.30/glib-Miscellaneous-Utility-Functions.html
// http://wiki.ubuntuusers.de/Paketbau

//string App::user_path = g_get_home_dir(); //"/home/stefan/";
// Windows --> ??  C:\Dokumente und einstellungen\Stefan
//string App::user_path = "";
// "/home/stefan/.local/shared";
// Windows --> ??  C:\Dokumente und einstellungen\Stefan\Eigene Dateien
// Schule: H:

std::string App::home_piiri_path = "";
std::string App::cir_path  = "";
std::string App::plan_path = "";
std::string App::etc_piiri_path = "/etc/piiri";
std::string App::system_piiri_path;
std::string App::version   = "4.1";
std::string App::name      = "PiiRi";
Complex2 App::settings;
std::set<std::string> App::missingComponents;
unsigned int App::wiringType = WIRING_PIIRI;

MainWindow* App::pWindow = NULL;
Complex2* App::m_pInternSettings = NULL;

std::ostringstream App::err_os;

std::map<std::string,std::string> App::texts;
std::string App::emptyString;

const char App::target_string[] = "UTF8_STRING";
std::string App::m_ClipboardStore;



/**
Speichern der Programmeinstellungen
*/
bool App::saveSettings()
{
    // Windows und Linux bekommen unterschiedliche Einstellungsdateien
    // das hat sich im Schulbetrieb bewährt.
    #ifdef _WIN32
    std::string propsFile = system_piiri_path + "/settings.xml";
    #else
    std::string propsFile = system_piiri_path + "/.settings.xml";
    #endif

    cout << "Speichere Einstellungen (" << propsFile << ")..." << endl;

    // XML-Dokument erzeugen
    xmlpp::DomParser dom;
    xmlpp::Document *doc = dom.get_document();
	xmlpp::Element *root = doc->create_root_node("piiri");

	// Einstellungen werden im Format 2 gespeichert
	root->set_attribute("version", "2");

    // "Neues" Format verwenden
    bool saveNew = Complex2::saveNew;
    Complex2::saveNew = true;

    try
    {
        settings.write_xml(root);
    }
    catch (std::string& s)
    {
        std::cerr << s << std::endl;
        std::cout << s << std::endl;
    }

    // Format zurücksetzen
    Complex2::saveNew = saveNew;

    // Datei Schreiben
	if (isFileWritable(propsFile))
	{
        doc->write_to_file_formatted(propsFile);
        return true;
	}
	cerr << "Konnte Einstellungen nicht schreiben." << endl;
	return false;
}

/**
Laden der Programmeinstellungen
*/
bool App::loadSettings()
{

    // Windows und Linux bekommen unterschiedliche Einstellungsdateien
    // das hat sich im Schulbetrieb bewährt.
    #ifdef _WIN32
    string propsFile = system_piiri_path + "/settings.xml";
    #else
    string propsFile = system_piiri_path + "/.settings.xml";
    #endif

    cout << "Lade Einstellungen (" << propsFile << ")..." << endl;

    if (FileExists(propsFile))
    {
        xmlpp::DomParser dom(propsFile);
        xmlpp::Document *doc = dom.get_document();
        xmlpp::Element *root = doc->get_root_node();

        // Version der Einstellungen ermitteln
        // ab V2 werden die props anders gespeichert.
        std::string _version = root->get_attribute_value("version");
        int version;
        if (_version.empty()) _version = "1";
        istringstream is(_version);
        is >> version;
        if (version > 1) Complex2::loadNew = true;
        else Complex2::loadNew = false;

        try
        {
            settings.read_xml(root);
        }
        catch (std::string& s)
        {
            std::cerr << s << std::endl;
            std::cout << s << std::endl;
        }

        Complex2::loadNew = false;
    }
    else
    {
        cout << "Konnte Einstellungen nicht lesen. Das ist beim ersten Programmstart i.O." << endl;
    }

    updatePaths();


    return true;
}

/** @brief updateLiveSettings
  *
  * @todo: document this function
  */
void App::updateLiveSettings()
{
    try {
        App::wiringType = settings.get_complex("anzeige")->get_selection("wiring")->get_value_index();
    } catch(std::string s) {std::cerr << s << std::endl;}

    try {
        CircuitChild::timeoutDelta = settings.get_complex("simulation")->get_int("sim_timeout")*CLOCKS_PER_SEC/1000;
    } catch(std::string s) {std::cerr << s << std::endl;}

    try {
        int a = settings.get_complex("simulation")->get_int("step_sim_steptime");
        if (a != (int)CircuitChild::steptime)
        {
            CircuitChild::steptime = a;
            if (getMWp()) {getMWp()->stopStepTimer();getMWp()->startStepTimer();}
        }
    } catch(std::string s) {std::cerr << s << std::endl;}

    try {
        std::string st(settings.get_complex("simulation")->get_selection("typ")->get_value());
        CircuitChild::SimulationTyp cst;

        if (st == "step")
        {
            cst = CircuitChild::ST_STEP;
        }
        else
        {
            cst = CircuitChild::ST_NORMAL;
        }

        if (cst != CircuitChild::simtyp)
        {
            CircuitChild::simtyp = cst;
            if (getMWp())
            {
                switch(cst)
                {
                    case CircuitChild::ST_STEP:
                        getMWp()->startStepTimer();
                    break;
                    default:
                        getMWp()->stopStepTimer();
                }
            }
        }
    } catch(std::string s) {std::cerr << s << std::endl;}


}


void App::updatePaths()
{
    home_piiri_path = settings.get_string("home_piiri_path");
    if (home_piiri_path.empty()) home_piiri_path = system_piiri_path; // TODO mehr testing!

    cir_path  = home_piiri_path + "/cir";
    plan_path = home_piiri_path + "/plan";
    //etc_piiri_path = "../etc/piiri";
}


/** @brief getPropTitle

Stellt dem Einstellungsdialog die Texte zur verfügung.

  */
void App::provideUiText(PropertyDialog2& d)
{
    d.setUiText("/home_piiri_path",
                ui_text(App::t("PiiRi-Pfad"), App::t("Pfad für Schaltungen und Projekte.")));

    d.setUiText("/lang",
                ui_text(App::t("Sprache"), App::t("Sprachdatei wählen.")));

    d.setUiText("/ask_path",
                ui_text(App::t("Pfad erfragen"), App::t("Beim Start nach dem PiiRi-Pfad fragen.")));

    d.setUiText("/editor",
                ui_text(App::t("Texteditor"), App::t("Programm, das als externer Texteditor verwendet wird.")));

    d.setUiText("/step_sim",
                ui_text(App::t("Schritt-Sim."), App::t("Schrittweise Simulation verwenden.")));

    d.setUiText("/update_msg",
                ui_text(App::t("Online-Benachrichtigungen"), App::t("Benachrichtigungen über Neuigkeiten aus dem Internet laden.")));

    d.setUiText("/simulation",
                ui_text(App::t("Simulation"), ""));
    d.setUiText("/simulation/sim_timeout",
                ui_text(App::t("Timeout (ms)"), App::t("Timeout in ms für die Normal-Simluation.")));
    d.setUiText("/simulation/typ",
                ui_text(App::t("Simulation"), App::t("Art der Schaltungs-Simulation.")));
    d.setUiText("/simulation/step_sim_steptime",
                ui_text(App::t("Step-Zeit (ms)"), App::t("Zeitintervalle für Step-Simulation.")));

    d.setUiText("/palette",
                ui_text(App::t("Komponentenpalette"), ""));

    d.setUiText("/palette/function_component",
                ui_text(App::t("Funktionskomponente"), App::t("Funktionskomponente anzeigen")));

    d.setUiText("/palette/ram_component",
                ui_text(App::t("RAM-Komponente"), App::t("RAM-Komponente anzeigen")));

    d.setUiText("/palette/iow_component",
                ui_text(App::t("IOW-Komponente"), App::t("IO-Warrior-Komponente anzeigen")));

    d.setUiText("/anzeige",
                ui_text(App::t("Anzeige"), ""));

    d.setUiText("/anzeige/wiring",
                ui_text(App::t("Verdrahtung"), App::t("Aussehen der Verbindungen.")));

}


void App::init(int argc, char *argv[])
{
    // Thread-Systtem initialisieren
    if(!Glib::thread_supported()) Glib::thread_init();

    // Gtk initialisieren
	Gtk::Main kit(argc, argv);

    g_set_prgname ("PiiRi Schaltungssimulation");

    /*cout << App::user_path << endl;
    cout << "g_get_user_name: " << g_get_user_name() << endl;
    cout << "g_get_real_name: " << g_get_real_name() << endl;
    cout << "g_get_user_data_dir: " << g_get_user_data_dir() << endl;
    */

    // Test-Ausgabe
    cout << "g_get_home_dir: " << g_get_home_dir() << endl;
    cout << "g_get_user_config_dir: " << g_get_user_config_dir() << endl;
    cout << "g_get_user_data_dir: " << g_get_user_data_dir() << endl;
    //cout << "g_get_data_dir: " << g_get_data_dir() << endl;

    #ifdef _WIN32
        // Windows:
        cout << "Windows Version" << endl;
        string user_data_dir = g_get_user_data_dir();
		etc_piiri_path = "../etc/piiri";
    #else
        // Linux:
        cout << "Linux Version" << endl;
        string user_data_dir = g_get_home_dir();

		etc_piiri_path = "/etc/piiri";
		if (!DirectoryExists(etc_piiri_path.c_str()) || FileExists("/etc/piiri/no"))
		{
			etc_piiri_path = "../etc/piiri";
		}
    #endif


    system_piiri_path = user_data_dir + G_DIR_SEPARATOR_S + "piiri";


    //----------- SETTINGS INITIALISIEREN

    settings.set_string("home_piiri_path", system_piiri_path);
    settings.set_bool("ask_path", true);
    settings.set_string("editor", "");
    settings.set_bool("update_msg", false);

    SelectionValue2 *langsel = settings.set_selection("lang");
    std::cout << "Verfügbare Sprachen einlesen:" << std::endl;
    /** verfügbare Sprachen einlesen */
	DIR *dp;
	struct dirent *ep;
    std::string langFile;
	dp = opendir (App::etc_piiri_path.c_str());
	if (dp != NULL)
	{
		while ( (ep = readdir(dp)) ) {
			langFile = ep->d_name;
			if((ep->d_name[0] == '.') || (langFile.length()<=5) || !(langFile.substr(langFile.length()-5, 5) == ".lang")) continue; // dot- und Fremd-Files überspringen
			std::cout << langFile << std::endl;
			langsel->add_option(langFile);
		}
		closedir(dp);
	}
	else
	{
	    std::cerr << "Kann Sprachdateien nicht einlesen." << std::endl;
	}
    langsel->set_value("de.lang");

    Complex2* palette = settings.set_complex("palette");
    palette->set_bool("function_component", false);
    palette->set_bool("ram_component", false);
    palette->set_bool("iow_component", false);


    Complex2* anzeige = settings.set_complex("anzeige");
    SelectionValue2 *wiring = anzeige->set_selection("wiring");
    wiring->add_option("piiri");
    wiring->add_option("4590");
    wiring->set_value("piiri");

    Complex2* simul = settings.set_complex("simulation");
    SelectionValue2 *typ = simul->set_selection("typ");
    typ->add_option("normal");
    typ->add_option("step");
    typ->set_value("normal");

    simul->set_int("sim_timeout", 200);
    simul->set_int("step_sim_steptime", 200);


    //palette->set_list("recent_projects", );
    Complex2* recent_projects = settings.set_complex("recent_projects");
    recent_projects->setAllowEdit(false);
    recent_projects->setAllowXmlToCreateNew(true);

    m_pInternSettings = settings.set_complex("intern");
    m_pInternSettings->setAllowEdit(false);
    //m_pInternSettings->setAllowXmlToCreateNew(true);
    m_pInternSettings->set_int("start_times", 0);
    m_pInternSettings->set_string("current_version", "--");
    m_pInternSettings->set_bool("maximized", false);





    loadSettings();


    App::updateLiveSettings();

    try {
        loadLang(settings.get_selection("lang")->get_value());
    } catch(std::string s) {std::cerr << s << std::endl;}


    /* Evtl. nach dem PiiRi-Pfad fragen    ****************/
    if (settings.get_bool("ask_path"))
    {
        Gtk::Dialog dialog("PiiRi - Arbeitspfad");
        Gtk::VBox* vb = dialog.get_vbox();
        Gtk::Entry entry;
        Gtk::Label l("Kontrollieren Sie den Arbeitspfad:");
        Gtk::CheckButton ask("Beim Start nach Arbeitspfad fragen");
        ask.set_active(settings.get_bool("ask_path"));


        entry.set_text(settings.get_string("home_piiri_path"));

        vb->set_spacing(5);
        vb->pack_start(l, Gtk::PACK_EXPAND_WIDGET);
        vb->pack_start(entry, Gtk::PACK_EXPAND_WIDGET);
        vb->pack_start(ask, Gtk::PACK_EXPAND_WIDGET);

        dialog.add_button("Weiter", Gtk::RESPONSE_OK);
        //dialog.add_button("Zurücksetzen", Gtk::RESPONSE_CANCEL);
        dialog.show_all_children();

        // Enter in Textfeld triggert Dialog-Antwort
        entry.signal_activate().connect(sigc::bind(
            sigc::mem_fun(&dialog, &Gtk::Dialog::response),
        Gtk::RESPONSE_OK));


        dialog.set_default_response(Gtk::RESPONSE_OK);
        dialog.set_default_size(500,100);

        int ok = dialog.run();
        dialog.hide();
        if (ok == Gtk::RESPONSE_OK)
        {
            if (entry.get_text() != settings.get_string("home_piiri_path"))
            {
                settings.set_string("home_piiri_path", entry.get_text());
                updatePaths();
            }
            settings.set_bool("ask_path", ask.get_active());
        }
    }

    /* ******************************************************************/

    // neuen Programmstart merken.
    m_pInternSettings->set_int("start_times", m_pInternSettings->get_int("start_times") + 1);


    std::cout << "etc_piiri_path: " << etc_piiri_path << std::endl;

    // Dateisystem einrichten
    ensureHomeDir();


    std::vector<std::string> files_to_open;



    // --------------- Wiederherstellungsmechanismus
    #define _APP_DELETE_LOCK _APP_DELETE_LOCK

    // wiederherstellungs-indikator
    string lock(home_piiri_path+"/.lock");

    // Wenn diese Datei existiert: Wiederherstellung starten
    if (FileExists(lock))
    {
        cout << "ACHTUNG: Programm wurde nicht ordnungsgemäß beendet." << endl;
        cout << "Leite Wiederherstellungsfunktion ein." << endl;

        // cout << "MANUELLE WIEDERHERSTELLUNG:" << endl;
        // cout << "  Nennen Sie die neuste .tmp-Datei in " << home_piiri_path << " in .cir um und kopieren Sie sie nach " << cir_path <<"."<< endl;
        // cout << "  Diese Datei sollte die vor dem Crash bearbeitete Schaltung vor der letzten Änderung enthalten." << endl;


        std::vector<std::pair<std::string,std::string> > recover_files;


        // gesichertes Projekt aus lock-Datei laden
        Project recPr(lock);

        // Tabs und IDs laden
        std::vector<unsigned long> ids = recPr.getOpenTabIds();
        std::vector<std::string> fns = recPr.getOpenTabs();

        std::vector<unsigned long>::iterator id_it;
        std::vector<std::string>::iterator fn_it = fns.begin();

        ostringstream os;
        for (id_it = ids.begin(); id_it != ids.end(); id_it++)
        {
            int n = 0;
            std::string undofn;
            std::string best;
            time_t best_time = 0;
            struct stat st;
            bool finished = false;

            do
            {
                os << App::home_piiri_path << "/undo-" << (*id_it) << "-" << n << ".tmp";
                undofn = os.str();
                std::cout << "try: " << undofn << std::endl;
                if (FileExists(undofn))
                {
                    stat(undofn.c_str(), &st);
                    time_t t = st.st_mtime;
                    if (t > best_time)
                    {
                        #ifdef _APP_DELETE_LOCK
                        g_unlink(best.c_str());
                        #endif
                        best_time = t;
                        best = undofn;
                    }
                }
                else
                {
                    finished = true;
                    undofn = "";
                }
                n++;


                os.str("");

            } while (!finished);

            std::string recoverd(App::cir_path+"/recovered_"+recPr.getName() + "." + (*fn_it));

            recover_files.push_back(std::pair<std::string,std::string>(best,recoverd));

            fn_it++;
            os.str("");
        } // for über Tab-IDs



        // Dialog anzeigen
        RecoverDlg rec(recover_files);

        bool do_recover = rec.run();

        // Wiederherstellung abarbeiten
        for (std::vector<std::pair<std::string,std::string> >::iterator it = recover_files.begin(); it != recover_files.end(); it++)
        {
            // (best,recoverd)
            if (do_recover)
            {
                GFile* srcf = g_file_new_for_path((    (*it).first    ).c_str());
                GFile* destf = g_file_new_for_path((   (*it).second   ).c_str());
                g_file_copy (
                    srcf,
                    destf,
                    (GFileCopyFlags)(G_FILE_COPY_OVERWRITE|G_FILE_COPY_TARGET_DEFAULT_PERMS), //flags
                    //G_FILE_COPY_OVERWRITE, //flags
                    NULL, //cancellable
                    NULL, //&progressCallback, // progress callback
                    NULL, // progress callback data
                    NULL//  &err
                );
            }

            #ifdef _APP_DELETE_LOCK
            g_unlink((*it).first.c_str());
            #endif
        } // for recover_files
    } // lock vorhanden (Wiederherstellung)


    // -----------------------------------------------------------
    // "Normales" Starten des Hauptfensters
    // files_to_open ist für den Fall dass in Zukunft vllt aus irgendeinem
    // Grund mehrere Schaltungen geöffnet werden sollen

    string filetoopen;

    if ((argc > 1) && argv[1]) filetoopen = argv[1];

    if (!filetoopen.empty())
    {
        files_to_open.push_back(filetoopen);
    }

    if (files_to_open.empty()) files_to_open.push_back(""); // damit zumindest ein leerer tab geöffnet wird


        App::fetchError("Beim Programmstart");


    try
    {
        // Hauptfenster erzeugen
        MainWindow MWnd;
        pWindow = &MWnd;
        pWindow->show();


        for (std::vector<std::string>::iterator it = files_to_open.begin(); it != files_to_open.end(); it++)
        {
            MWnd.openCircuit((*it));
            //MWnd.setUnsaved(); für wiederherstellung
        }

        updateLock();

        Gtk::Main::run(MWnd);
    }
    catch (exception const& e)
    {
        cout << "Exception: " << e.what() << endl;
    }
    catch (string const& s)
    {
        cout << "Exception: " << s << endl;
    }

}

/** @brief updateLock

    Lock datei mit aktuellem Projekt aktualisieren

    erstellt die Datei .lock

  */
void App::updateLock()
{
    #ifdef _APP_DELETE_LOCK
    string lock(home_piiri_path+"/.lock");

    // Aktuelles Projekt holen
    Project* pPr = getMW().getCurrentProject();


    if (!pPr)
    {
        // projektloser Modus:
        // Pseudoprojekt erstellen, einstellen und speichern
        pPr = new Project("", false);
        pPr->setFilename(".lock");
        pPr->setDir(App::home_piiri_path);
        pPr->save();
    }
    else
    {
        // Im Projekt-Modus wird die Projektdatei einfach kopiert
        GFile* srcf = g_file_new_for_path((    pPr->getProjectFilePath()    ).c_str());
        GFile* destf = g_file_new_for_path((    lock   ).c_str());
        g_file_copy (
                     srcf,
                     destf,
                     (GFileCopyFlags)(G_FILE_COPY_OVERWRITE|G_FILE_COPY_TARGET_DEFAULT_PERMS), //flags
                     //G_FILE_COPY_OVERWRITE, //flags
                     NULL, //cancellable
                     NULL, //&progressCallback, // progress callback
                     NULL, // progress callback data
                     NULL//  &err
                     );
    }
    #endif
}




/** @brief end
  *
  * Was gentan werden muss, wenn das Programm ordnungsgemäß beendet wurde.
  */
void App::end()
{
    // Einstellungen sichern
    saveSettings();

    // .lock-Datei löschen
    std::string lock(home_piiri_path+"/.lock");
    #ifdef _APP_DELETE_LOCK
    g_unlink(lock.c_str());
    #endif
}



/**
    Holt den Inhalt aus dem Fehler-Buffer
*/
const std::string App::getErrString(bool reset)
{
    std::string s,s2,ss;
    s = err_os.str();
    if (reset) err_os.str("");
    std::istringstream is(s);

    s2 = s;

    // trim
    while (!s2.empty() && ((s2[0]==' ') || (s2[0]=='\n') || (s2[0]=='\t')))
    {
        s2 = s2.substr(1);
    }

    // Zeilenweise einlesen und in umgekehrter Reihenfolge an Rückgabe-String
    // anhängen: frühere Fehler zuvorderst im String.
    if (!s2.empty())
    {
        is.str(s2);
        is.clear();
        while (!is.eof())
        {
            getline(is, s);
            //if (!s.empty()) ss = ss + "\n - " + s;
            if (!s.empty()) ss = ss + "\n<span weight=\"heavy\">  · </span> " + s + "\n<span size=\"xx-small\"> </span>";
        }

        // Fehlermeldungen werden auch auf Konsole ausgegeben
        cout << "ERROR: " << endl << ss << endl;

        return ss;
    }
    return "";
}



/**
  copyFiles

  Gesamten Inhalt eines Ordners kopieren
  TODO: recursive!

typedef enum {
  G_FILE_COPY_NONE                 = 0,          //< nick=none >
  G_FILE_COPY_OVERWRITE            = (1 << 0),
  G_FILE_COPY_BACKUP               = (1 << 1),
  G_FILE_COPY_NOFOLLOW_SYMLINKS    = (1 << 2),
  G_FILE_COPY_ALL_METADATA         = (1 << 3),
  G_FILE_COPY_NO_FALLBACK_FOR_MOVE = (1 << 4),
  G_FILE_COPY_TARGET_DEFAULT_PERMS = (1 << 5)
} GFileCopyFlags;
*/
bool copyFiles(const string& from, const string& to)
{
    GFile* src = g_file_new_for_path(from.c_str());

    GError *err = NULL;



    GFileEnumerator *fenum = g_file_enumerate_children(src,
                                                 G_FILE_ATTRIBUTE_STANDARD_NAME, // welchte attribute? // developer.gnome.org/gio/stable/GFileInfo.html
                                                 G_FILE_QUERY_INFO_NONE, //GFileQueryInfoFlags flags,
                                                 NULL,
                                                 &err);
    g_object_unref(src);
    if (fenum)
    {
        GFileInfo* fi;
        string fname;
        while ((fi = g_file_enumerator_next_file(fenum, NULL, &err)))
        {
            if (err != NULL)
            {
                cerr << "1" << endl;
                cerr << err->message << endl;
                g_error_free (err);
                err = NULL;
                break;
            }
            fname = g_file_info_get_attribute_as_string(fi, G_FILE_ATTRIBUTE_STANDARD_NAME);
            g_object_unref(fi);
            cout << " copy " << fname;

            GFile* srcf = g_file_new_for_path((from+"/"+fname).c_str());
            GFile* destf = g_file_new_for_path((to+"/"+fname).c_str());

            try
            {

                if (g_file_copy (
                             srcf,
                             destf,
                             (GFileCopyFlags)(G_FILE_COPY_OVERWRITE|G_FILE_COPY_TARGET_DEFAULT_PERMS), //flags
                             //G_FILE_COPY_OVERWRITE, //flags
                             NULL, //cancellable
                             NULL, //&progressCallback, // progress callback
                             NULL, // progress callback data
                             &err
                             ))
                 {
                     cout << " OK" << endl;
                     /*bool b = true;
                     g_file_set_attribute(destf,
                                          "access::can-write",
                                          G_FILE_ATTRIBUTE_TYPE_BOOLEAN,
                                          &b,
                                          G_FILE_QUERY_INFO_NONE,
                                          NULL,
                                          NULL);*/
                 }
                 else
                 {
                     cout << " fail." << endl;

                     if (err->code == G_IO_ERROR_WOULD_RECURSE)
                     {
                         // rekursiver aufruf ...
                         std::cout << from+"/"+fname << "-->" << to+"/"+fname << " muss rekursiv kopiert werden. NYI." << std::endl;
                     }
                     else
                     {
                         cout << "2 " << from+"/"+fname << "-->" << to+"/"+fname << endl;
                         cout << err->message << endl;
                     }
                     g_error_free (err);
                     err = NULL;
                 }

             }
             catch (...)
             {
                 cout << "Exception in g_file_copy" << endl;
             }

             g_object_unref(srcf);
             g_object_unref(destf);

        }



        g_file_enumerator_close(fenum, NULL, NULL);
        g_object_unref(fenum);
        return true;
    }
    else
    {
         cout << "FileEnumerator: " << from << "-->" << to << endl;
         cout << err->message << endl;
         g_error_free (err);
         err = NULL;
    }

    return false;
} // copyFiles


/**
    <home>/piiri richtig einrichten
*/
void App::ensureHomeDir()
{
    // kopiere daten aus /etc/piiri:
    //  - plan/*
    //  - cir/*

    // g_mkdir: http://developer.gnome.org/glib/2.30/glib-File-Utilities.html#g-mkdir


    // copyFiles(etc_piiri_path+"/plan", plan_path);

    if (!DirectoryExists(home_piiri_path.c_str()))
    {
        if(g_mkdir(home_piiri_path.c_str(), 0711) == 0)
        {
			std::cout << home_piiri_path << " erstellt." << std::endl;
        }
    }
    if (!DirectoryExists(plan_path.c_str()))
    {
        if(g_mkdir(plan_path.c_str(), 0711) == 0)
        {
			std::cout << plan_path << " erstellt." << std::endl;
			try
			{
				copyFiles(etc_piiri_path + G_DIR_SEPARATOR_S + "plan", plan_path);
			}
			catch (...)
			{
				std::cout << "Kopieren des Inhalts fehlgeschlagen." << std::endl;
			}
        }
    }
    if (!DirectoryExists(cir_path.c_str()))
    {
        if(g_mkdir(cir_path.c_str(), 0711) == 0)
        {
			std::cout << cir_path << " erstellt." << std::endl;
    		try
			{
				copyFiles(etc_piiri_path + G_DIR_SEPARATOR_S + "cir", cir_path);
			}
			catch (...)
			{
				std::cout << "Kopieren des Inhalts fehlgeschlagen." << std::endl;
			}
        }
    }
    if (!DirectoryExists((home_piiri_path + G_DIR_SEPARATOR_S + "tutorials").c_str()))
    {
        if(g_mkdir((home_piiri_path + G_DIR_SEPARATOR_S + "tutorials").c_str(), 0711) == 0)
        {
			std::cout << home_piiri_path + G_DIR_SEPARATOR_S + "tutorials" << " erstellt." << std::endl;
    		try
			{
				copyFiles((etc_piiri_path + G_DIR_SEPARATOR_S + "tutorials"), (home_piiri_path + G_DIR_SEPARATOR_S + "tutorials"));
			}
			catch (...)
			{
				std::cout << "Kopieren des Inhalts fehlgeschlagen." << std::endl;
			}
        }
    }
    if (!DirectoryExists((home_piiri_path+G_DIR_SEPARATOR_S+"projekte").c_str()))
    {
        if(g_mkdir((home_piiri_path+G_DIR_SEPARATOR_S+"projekte").c_str(), 0711) == 0)
        {
			std::cout << home_piiri_path+G_DIR_SEPARATOR_S+"projekte" << " erstellt." << std::endl;
        }
    }

}



/** @brief getCirFilename

  Dateinamen einer Schaltung aud Namen erzeugen:
  cir-Pfad und Dateiendung anhängen.
*/
string App::getCirFilename(const string& cir)
{
    string s = App::cir_path + G_DIR_SEPARATOR_S + cir + ".cir";
    return s;
}

/// =============================================================
/// =================== Dialoge =================================


/** @brief fetchError
    Erzeugt Fehlermeldung, falls Fehler im Fehler-Stream.
*/
void App::fetchError(const string& title, Gtk::Window& parent)
{
    string s(App::getErrString());
    if (!s.empty())
    {
        error(title, s, "", parent);
    }
}

void App::fetchError(const string& title)
{
    string s(App::getErrString());
    if (!s.empty())
    {
        error(title, s, "");
    }
}

/** @brief error
    Erzeugt Fehlermeldung.
*/
void App::error(const string& title, const string& msg, const string& info, Gtk::Window* parent)
{
    string txt("<big>"+title+"</big>\n\n"+msg);
    if (!info.empty()) txt += "\n\n<i>" + info + "</i>";


    Gtk::MessageDialog d(txt, true, Gtk::MESSAGE_ERROR);
    if (parent) d.set_parent(*parent);
    d.set_position(Gtk::WIN_POS_CENTER_ON_PARENT );
    d.run();
}
void App::error(const string& title, const string& msg, const string& info, Gtk::Window& parent)
{
    App::error(title, msg, info, &parent);
}
/** @brief message
    Erzeugt Info-Meldung
*/
void App::message(const string& title, const string& msg, const string& info, Gtk::Window& parent)
{
    string txt("<big>"+title+"</big>\n\n"+msg);
    if (!info.empty()) txt += "\n\n<i>" + info + "</i>";

    Gtk::MessageDialog d(parent, txt, true, Gtk::MESSAGE_INFO);
    d.set_position(Gtk::WIN_POS_CENTER_ON_PARENT );
    d.run();
}

/** @brief question
    Erzeugt ja/nein Fragedialog.
*/
bool App::question(const string& title, const string& msg, const string& info, Gtk::Window& parent)
{
    string txt("<big>"+title+"</big>\n\n"+msg);
    if (!info.empty()) txt += "\n\n<i>" + info + "</i>";

    Gtk::MessageDialog d(parent, txt, true, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
    d.set_position(Gtk::WIN_POS_CENTER_ON_PARENT);
    return (d.run() == Gtk::RESPONSE_YES);
}

/**
    Erzeugt Frage: Hilfsfunktion die das Hauptfenster als Parent setzt.
*/
bool App::question(const string& title, const string& msg, const string& info)
{
    return question(title, msg, info, getMW());
}


/// =============================================================
/// ===== geöffnete Projekte ====================================


/** @brief addToRecentProjects

    Projekt-Dateipfad zur Liste der kürzlich geöffneten Projekte hinzufügen.

*/
void App::addToRecentProjects(const string& pfn)
{
    /// Die Liste ist in einem speziellen Complex gespeichert:
    Complex2* rp = settings.get_complex("recent_projects");
    if (!rp) return;

    /// Wieviele sind in der Liste?
    Complex2::size_type size = rp->size();

    ostringstream os;

    /// In der Liste soll nicht zweimal hintereinander der gleiche Eintrag stehen.
    /// Nachprüfen, ob das letzte das selbe wie das neue ist:
    /// wenn das der Fall ist - einfach zurückkehren.
    if (size > 0)
    {
        os << "rp_" << (size-1);
        string s = rp->get_string(os.str());
        if (s == pfn) return;
    }

    /// muss die liste beschnitten werden?
    /// Die liste wird maximal MAX_RECENT_PROJECTS lang
    /// da ein neuer Eintrag hinzukommt muss auch bei = gekürzt werden.
    if (size >= MAX_RECENT_PROJECTS)
    {
        /// liste lokal kopieren und
        /// Complex aus den Settings leeren
        std::vector<std::string> rps = getRecentProjects();
        rp->clear();

        /// Complex wieder befüllen
        std::vector<std::string>::iterator it = rps.begin();
        it++; /// erstes Überspringen
        for (; it != rps.end(); it++)
        {
            addToRecentProjects((*it)); // wird wegen automatischer namensgebung so gemacht...
        }
        size = rp->size(); // größe neu ermitteln!
    }

    /// Namen für neuen Eintrag
    os.str("");
    os << "rp_" << size;

    /// Wert (Pfad) setzen
    rp->set_string(os.str(), pfn);
} // addToRecentProjects



/** @brief getRecentProjects
    Kürzlich geöffnete Projekte aus
    Complex auslesen und als einfache Liste von Pfaden zurückgeben.
*/
std::vector<std::string> App::getRecentProjects()
{
    std::vector<std::string> rps;
    Complex2* rp = settings.get_complex("recent_projects");
    if (rp)
    {
        Complex2::size_type size = rp->size();
        std::ostringstream os;
        std::string s;
        for (unsigned int n = 0; n< size; n++)
        {
            os.str("");
            os << "rp_" << n;
            try
            {
                s = rp->get_string(os.str());
                rps.push_back(s);
            }
            catch (...)
            {
                break;
            }
        }
    }
    return rps;
}

/// =============================================================


/** @brief getMissingComponents
    Auffangbecken für nicht gefundene Schaltungen
*/
std::set<std::string> App::getMissingComponents()
{
    set<string> s(missingComponents);
    missingComponents.clear();
    return s;
}



/** @brief t
  *
  * @todo: document this function
  */
const std::string& App::t(const std::string& id)
{
    if (texts.find(id) != texts.end())
    {
        return texts[id];
    }

    if (settings.get_selection("lang")->get_value() != "de.lang")
    {
        // nicht gefunden!
        // tag in Spezial-Datei schreiben
        std::ofstream f("text-misses.txt", ios_base::app);
        f << "<t id=\"" << id << "\">" << id << "</t>" << std::endl;
        f.close();
    }


    return id;
}


/** @brief loadLang
  *
  * @todo: document this function
  */
void App::loadLang(const std::string& filename)
{
    std::string fn(App::etc_piiri_path + "/" + filename);
    if (!FileExists(fn))
    {
        std::cerr << "Lang: " << fn;
        std::cerr << "not found!";
        std::cerr << std::endl;
        return;
    }

    xmlpp::DomParser dom(fn);
    xmlpp::Document *doc = dom.get_document();
    if (!doc) return;
    xmlpp::Element *root = doc->get_root_node();
    if (!root) return;
	xmlpp::Node::NodeList ts = root->get_children("t");
	xmlpp::Node::NodeList::iterator it;
	xmlpp::Element *e;
	for (it = ts.begin(); it != ts.end(); it++)
	{
		e = (xmlpp::Element *)(*it);
		if (e->has_child_text())
		{
            texts[e->get_attribute_value("id")] = e->get_child_text()->get_content();
		}
	}
}

int App::startProgramm(std::string& prg, std::string& param)
{
    #ifdef _WIN32
        std::string cmd("start " + prg + " " + param);
    #else
        std::string cmd(prg + " " + param + " &");
    #endif
    return system(cmd.c_str());
}




void App::on_clipboard_get(Gtk::SelectionData& selection_data, guint)
{
    //info is meant to indicate the target, but it seems to be always 0,
    //so we use the selection_data's target instead.

    //std::cout << "on_clipboard_get" << std::endl;

    const std::string target = selection_data.get_target();

    if(target == App::target_string)
    {
        selection_data.set_text(App::m_ClipboardStore);
    }
    else
    {
        g_warning("ExampleWindow::on_clipboard_get(): Unexpected clipboard target format.");
    }
}

void App::on_clipboard_clear()
{
    std::cout << "on_clipboard_clear" << std::endl;
    //App::m_ClipboardStore.clear();
}


