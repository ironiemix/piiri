#ifndef LOGICSYNTH_H
#define LOGICSYNTH_H

#include <gtkmm.h>

#include "circuit/EditableCircuit.h"
#include "model/LogicParser.h"

class LogicSynth : public Gtk::Dialog
{
    public:
        LogicSynth(Gtk::Window& parent);
        virtual ~LogicSynth();

        int run();
        bool execute(EditableCircuit& obj);

    protected:

        CircuitIO *_synth(EditableCircuit& obj, LogicBin& bin);
        void synth(EditableCircuit& obj, LogicBin& bin);


    private:
        int max_layer;

        Gtk::Entry m_Term;
        Gtk::CheckButton m_Minimize;
};

#endif // LOGICSYNTH_H
