#ifndef EDITABLECIRCUIT_H
#define EDITABLECIRCUIT_H

#include "CircuitContainer.h"

class EditableCircuit : public CircuitContainer
{
    public:
        EditableCircuit(string n, CircuitContainer *p);
        virtual ~EditableCircuit();

    // Manipulation der Schaltung
        bool topmost(CircuitChild* obj); // gibt zurück ob sich was geändert hat
        bool bottommost(CircuitChild* obj);
        bool up(CircuitChild* obj);
        bool down(CircuitChild* obj);
        bool changeElementName(CircuitChild* p, const string& nn);
        bool changeIOName(CircuitIO *p, const string& nn);
        void removeIO(CircuitIO* io);
        void removeComponent(CircuitChild* pObj);

        CircuitChild* clone(CircuitContainer* parent);
        void clone(type_elements& src, CircuitContainer& new_comp);

    protected:
    private:
};

#endif // EDITABLECIRCUIT_H
