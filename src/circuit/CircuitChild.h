#ifndef CIRCUITCHILD_H
#define CIRCUITCHILD_H

#include "circuit/CircuitIO.h"
#include "model/Complex2.h"
#include "gui/UiText.h"

#include <string>
#include <glib.h>
#include <sigc++/sigc++.h>


//#define SIMULATION_DEBUG

#define TIMEOUT_TEST if (CircuitChild::isNormalSim() && (clock() >= CircuitChild::timeoutClock)) throw std::string("Zeitlimit überschritten");


//#include "CircuitContainer.h"

class CircuitContainer;

//typedef std::map<std::string,CircuitInput*> type_inputs;
typedef std::vector<CircuitInput*> type_inputs;
//typedef std::map<std::string,CircuitOutput*> type_outputs;
typedef std::vector<CircuitOutput*> type_outputs;

typedef type_inputs::iterator type_inputs_iterator;
typedef type_outputs::iterator type_outputs_iterator;

/**
hat namen
hat io
Hat properties richtet diese ein
hat parent
ist basisklasse der gates und sonstiger fester komponenten
*/
class CircuitChild
{
    public:
        CircuitChild(std::string n, CircuitContainer* p, const std::string& cn, bool sim = true);
        virtual ~CircuitChild();

        enum CircuitNotifycation
        {
            CN_NONE,
            CN_TIMEOUT_ERROR,
            CN_UPDATE
        };


        /// Statisches für die Simulation

        enum SimulationTyp
        {
            ST_NORMAL,
            ST_STEP
        };

        static deque<CircuitChild*> cir_deque;
        static clock_t timeoutClock;
        static clock_t timeoutDelta;
        static unsigned int steptime;
        static SimulationTyp simtyp;

        static void list_push(CircuitChild* p_cir);
        static void clear_list(CircuitChild* pC = NULL);
        static deque<CircuitChild*>& getDeque();
        static bool isStepSim() { return simtyp == ST_STEP;}
        static bool isNormalSim() { return simtyp == ST_NORMAL;}

        static void step();



    // Properties
        virtual void provideUiText(ui_text_map& textmap);
        //virtual void initProperties();
        virtual void on_propertyChanged() {} // Leere Funktion!!
        Complex2& getProperties() {return m_Properties;};
        bool markedVisible() {return getProperties().get_bool("markedVisible");}
        void markedVisible(bool mv) {getProperties().set_bool("markedVisible", mv);}
        // Wird für die Statuszeile benutzt
        virtual std::string getInfo();
        const std::string& getCirName() {return cir_name;}
        std::string &getName() {return name;}
        int getPosX() {return pos_x;}
        void setPosX(int x) {pos_x = x;}
        int getPosY() {return pos_y;}
        void setPosY(int y) {pos_y = y;}
        void setPos(int x, int y) {pos_x = x; pos_y = y;}
        void setName(const std::string& n) {name = n;}
        virtual void on_input_changed(const std::string& name) {};

        CircuitContainer* getParent() {return m_pParentContainer;}
        CircuitContainer* getParentRecursive();
        std::string getNameRecursive(bool last=true);

    // Simulation
        void list_simulate();
        void step_simulate();


        void setSimulate(bool s) { simulate=s; }
        bool getSimulate() { return simulate; }


    // IO
        CircuitOutput* getOutput(const std::string& out);
        CircuitInput* getInput(const std::string& in);
        type_inputs_iterator getInputIterator(const std::string& in);
        type_outputs_iterator getOutputIterator(const std::string& out);

        type_iovalue getOutputValue(const std::string& out);
        unsigned int getInputValue();
        unsigned int getOutputValue();
        void setInputValue(unsigned int v);
        type_inputs& getInputs() {return inputs;};
        type_outputs& getOutputs() {return outputs;};
        unsigned int getInputPinCount();
        unsigned int getOutputPinCount();
        void clearInputs();
        void clearOutputs();
        void disconnect(bool in=true, bool out=true);
        void setInput(const std::string& in, type_iovalue v);
        type_iovalue getInputValue(const std::string& in);
        virtual void reset(); // wird von container reimplementiert
        void update();
        virtual void inputChanged(const std::string& name, type_iovalue value);
        virtual void outputChanged(const std::string& name, type_iovalue value);

        void setCirName(const std::string& cn) {cir_name = cn;}

        virtual CircuitChild* clone(CircuitContainer* parent);

        virtual bool hasChildren() {return false;};

    protected:
        std::string name;
        std::string cir_name;
        bool visible;
        int pos_x, pos_y;
        bool simulate;

    // IO
        type_inputs inputs;
        type_outputs outputs;

        Complex2 m_Properties;
        CircuitContainer *m_pParentContainer;

        void setParent(CircuitContainer *p) {m_pParentContainer=p;}
        virtual type_iovalue calculateOutputValue(const std::string& out){return 0;};
        virtual void processChildren() {};  // wird von container reimplementiert


        virtual void clear(); // wegen cir_deque und io, wird in container reimplementiert.

    private:
        void ioReset();

    public:
        typedef sigc::signal<void> type_signal_inputs_changed;
        type_signal_inputs_changed signal_inputs_changed() {return m_signal_inputs_changed;}


        typedef sigc::signal<void, const std::string&, type_iovalue> type_signal_input_changed;
        type_signal_input_changed signal_input_changed() {return m_signal_input_changed;}
        typedef sigc::signal<void, const std::string&, type_iovalue> type_signal_output_changed;
        type_signal_output_changed signal_output_changed() {return m_signal_output_changed;}


        typedef sigc::signal<void, int, const std::string&, gpointer> type_signal_circuit_notification;
        type_signal_circuit_notification signal_circuit_notification() {return m_signal_circuit_notification;}
    protected:
        type_signal_inputs_changed m_signal_inputs_changed;

        type_signal_input_changed m_signal_input_changed;
        type_signal_output_changed m_signal_output_changed;

        type_signal_circuit_notification m_signal_circuit_notification;
};


#endif // CIRCUITCHILD_H
