/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "circuit/CircuitVisual.h"
#include "gui/CircuitWidget.h"
#include "circuit/ClockVisual.h"
#include "model/App.h"


/**********************/

// CircuitChildVisualMasterBase

/** @brief ~CircuitChildVisualMasterBase
  *
  * @todo: document this function
  */
CircuitChildVisual::~CircuitChildVisual()
{
    m_signal_size_changed.clear();
    type_visual_map::iterator it;
    for (it = m_childVisuals.begin(); it != m_childVisuals.end(); it++)
    {
    	delete (*it).second;
    }

    m_childVisuals.clear();
}



void CircuitChildVisual::setPos(int x, int y)
{
	if (x>=0) m_pObj->setPosX(x);
	if (y>=0) m_pObj->setPosY(y);
}

int CircuitChildVisual::getPosX()
{
	return m_pObj->getPosX();
}

int CircuitChildVisual::getPosY()
{
	return m_pObj->getPosY();
}




CircuitChildVisual* CircuitContainerVisual::adjoinVisual(CircuitChild* o, bool& neu)
{
    neu = false;
    if (!o) return NULL;
	if (m_childVisuals.find(o) != m_childVisuals.end()) return (*m_childVisuals.find(o)).second; // schon enthalten!
    neu = true;

	// cout << "Create Visual for " << o->getCirName() << " " << o->getName() << endl;

	CircuitChildVisual* v;
	string n = o->getCirName();

	if (o->getParent() == NULL) v = new ToplevelCircuitVisual(dynamic_cast<ToplevelCircuit*>(o)); // wird vermutlich niemals aufgerufen!
	else if (n == "node") v = new NodeVisual(dynamic_cast<NodeComponent*>(o), this);
	else if (n == "text") v = new TextVisual(dynamic_cast<TextComponent*>(o), this);
	else if (n == "clock") v = new ClockVisual(dynamic_cast<ClockComponent*>(o), this);
	else if ((n == "and") || (n == "nand") || (n == "or") || (n == "nor") || (n == "not") || (n == "xor")) { v = new GateVisual(dynamic_cast<GateComponent*>(o), this);}
	else if ((n == "switch") || (n == "8switch")) v = new SwitchVisual(dynamic_cast<Switch*>(o), this);
	else if ((n == "8indicator") || (n == "indicator")) { v = new IndicatorVisual(dynamic_cast<Indicator*>(o), this);}
	else if (n == "led") { v = new LEDVisual(dynamic_cast<LEDComp*>(o), this);}
	else if (n == "digit") v = new DigitalZifferVisual(dynamic_cast<DigitalZiffer*>(o), this);
	else if (n == "digit2") v = new DigitalZiffer2Visual(dynamic_cast<DigitalZiffer2*>(o), this);
	else if (n == "letter") v = new LetterVisual(dynamic_cast<LetterComponent*>(o), this);
 	else if (CircuitContainer* c = dynamic_cast<CircuitContainer*>(o)) v = new CircuitContainerVisual(c, this);
	else v = new CircuitChildVisual(o, this);

	m_childVisuals.insert(pair<CircuitChild*,CircuitChildVisual*>(o, v) );

	return v;
}

CircuitChild* CircuitChildVisual::getParentObj()
{
	if (m_pParentVisual != NULL) return m_pParentVisual->getObj();
	return NULL;
}





void CircuitChildVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
	//int x1=0, y1=0,
	int x2=0, y2=0;

	w = h = 0;

	type_visual_map::iterator it;

    // falls visual nicht vorhanden ??? TODO

    if (m_childVisuals.size() > 0)
    {

        int ChildOffsetX, ChildOffsetY;
        ChildOffsetX = x + m_inWidth; // + m_ioSpace + x;
        ChildOffsetY = y + m_titleHeight; // + y + m_vSpace;

        bool first = true;

        min_x = min_y = 0;

        //for ( it = m_pObj->circuitElements.begin(); it != m_pObj->circuitElements.end(); it++ )
        for ( it = m_childVisuals.begin(); it != m_childVisuals.end(); it++ )
        {
            if (children || (*it).second->getObj()->markedVisible())
            {
                (*it).second->updateSize(cr, ChildOffsetX + (*it).second->getPosX(), ChildOffsetY + (*it).second->getPosY(), false );
            }

            if (((*it).second->getPosX() + (*it).second->getSizeW()) > x2)
                x2 = (*it).second->getPosX()+(*it).second->getSizeW();
            if (((*it).second->getPosY()+(*it).second->getSizeH()) > y2)
                y2 = (*it).second->getPosY()+(*it).second->getSizeH();

            //
            if (!children && (*it).second->getObj()->markedVisible())
            {
                if (((*it).second->getPosX() < min_x) || first)
                {
                    min_x = (*it).second->getPosX();
                }
                if (((*it).second->getPosY() < min_y) || first)
                {
                    min_y = (*it).second->getPosY();
                }
                first = false;
            }
        }
        w = x2 - min_x;
        h = y2 - min_y;
        if (children)
        {
            w += m_ioSpace;
            h += m_vSpace;
        }
    }
    else
    {
        w = 0; //m_ioSpace;
        h = 0; //m_vSpace;
        //if (w < 50) w = 50;
        //if (h < 50) h = 50;
    }
} // CircuitChildVisual::innerSize

void CircuitContainerVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    bool _dummy;
    type_elements::iterator it;
    for ( it = getObj()->getChildren().begin(); it != getObj()->getChildren().end(); it++ )
    {
        //cout << "    " << (*it).second->getName() << endl;
        if (children || (*it)->markedVisible())
        {
            if (m_childVisuals.find((*it)) == m_childVisuals.end()) adjoinVisual((*it), _dummy);
        }
    }

    CircuitChildVisual::innerSize(w, h, cr, x, y, children);
} // CircuitContainerVisual::innerSize





























void CircuitChildVisual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{ // Achtung Dopplung mit ToplevelCircuitVisual

	int w=0, h=0;

	setAbsPos(x,y);


	Cairo::TextExtents extents;
	cr->set_font_size(IO_FONT_SIZE);
	type_outputs::reverse_iterator oit;
	type_inputs::reverse_iterator iit;
	int iw=0, ow=0;
	for ( iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++ ) {
		cr->get_text_extents((*iit)->getNameEx(), extents);
		if (extents.width > iw) iw = extents.width;
	}
	for ( oit = m_pObj->getOutputs().rbegin(); oit != m_pObj->getOutputs().rend(); oit++ ) {
		cr->get_text_extents((*oit)->getNameEx(), extents);
		if (extents.width > ow) ow = extents.width;
	}
	// +5 für die strichelchen
	m_inWidth  = iw + 6 + STRICH;
	m_outWidth = ow + 6 + STRICH; // +((m_pObj->getParent() == NULL) ? 20 : 0 );


	innerSize(w, h, cr, x, y, children);

	int a = max(m_pObj->getInputs().size(), m_pObj->getOutputs().size()) * m_ioHeight;
	w = w + m_inWidth + m_outWidth;
	h = m_titleHeight + max(h, a);

    cr->set_font_size(TITLE_FONT_SIZE);
	cr->get_text_extents(m_pObj->getName(), extents);
	if (extents.width+2*STRICH+4 > w) w = extents.width+2*STRICH +4;

	setSize(w, h);

    updateIOPos();

}

void ToplevelCircuitVisual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{ // Achtung Dopplung mit CircuitChildVisual
	int w=0, h=0;

	setAbsPos(x,y);


	Cairo::TextExtents extents;
	cr->set_font_size(IO_FONT_SIZE);
	type_outputs::reverse_iterator oit;
	type_inputs::reverse_iterator iit;
	int iw=0, ow=0;
	for ( iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++ ) {
		cr->get_text_extents((*iit)->getNameEx(), extents);
		if (extents.width > iw) iw = extents.width;
	}
	for ( oit = m_pObj->getOutputs().rbegin(); oit != m_pObj->getOutputs().rend(); oit++ ) {
		cr->get_text_extents((*oit)->getNameEx(), extents);
		if (extents.width > ow) ow = extents.width;
	}
	// +5 für die strichelchen
	m_inWidth  = iw + 6 + STRICH + 14;
	m_outWidth = ow + 6 + STRICH; // +((m_pObj->getParent() == NULL) ? 20 : 0 );


	innerSize(w, h, cr, x, y, children);

	int a = max(m_pObj->getInputs().size(), m_pObj->getOutputs().size()) * m_ioHeight;
	w = w + m_inWidth + m_outWidth;
	h = m_titleHeight + max(h, a);

    cr->set_font_size(IO_FONT_SIZE);
	cr->get_text_extents(m_pObj->getName(), extents);
	if (extents.width+2*STRICH+4 > w) w = extents.width+2*STRICH +4;

	setSize(w, h);

    updateIOPos();
}

void CircuitChildVisual::updateIOPos()
{
	type_outputs::iterator oit;
	type_inputs::iterator iit;
	int offs = 0;
	for ( oit=m_pObj->getOutputs().begin(); oit != m_pObj->getOutputs().end(); oit++ )
	{
			(*oit)->ps_setToPos(- m_outWidth,   (m_ioHeight/2) + m_titleHeight + offs);
			(*oit)->ps_setFromPos(0,              (m_ioHeight/2) + m_titleHeight + offs);
			offs += m_ioHeight;
	}
	offs = 0;
	for ( iit = m_pObj->getInputs().begin(); iit != m_pObj->getInputs().end(); iit++ )
	{
		(*iit)->ps_setFromPos(0,    (m_ioHeight/2) + m_titleHeight + offs);
		(*iit)->ps_setToPos(0,                  (m_ioHeight/2) + m_titleHeight + offs);
		offs += m_ioHeight;
	}
}

/**
Die Reihenfolge der Componentenkinder ist maßgebend.
Sie werden durchgegangen. Nur wenn ein Visual dafür verfügbar ist wird gezeichnet.
*/

bool CircuitChildVisual::paintAll(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{


	paintSelf(cr, x, y, false);

	// Verbindungen zwischen
	if (children) {
        int px,py,px2,py2;
        double dx1=0,dx2=0,dy2=0;
        bool s45=false;
        CircuitChildVisual *evis;
        unsigned short pc;

		type_elements::iterator it;
		type_outputs::reverse_iterator oit;
		type_inputs::reverse_iterator iit;
		type_forwardings::iterator ioit;
		// Linien von den inputs aus
		cr->save();
        cr->set_line_width(1);
		for ( iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++ )
		{

			px = x + (*iit)->ps_getFromPosX()+m_inWidth;
			py = y + (*iit)->ps_getFromPosY();
            pc = (*iit)->getPinCount();

			for (ioit = (*iit)->getForwardings().begin(); ioit != (*iit)->getForwardings().end(); ioit++)
			{
			    if ((*ioit)->getOwner() == m_pObj) evis = this;
			    else
			    {
			        if (m_childVisuals.find((*ioit)->getOwner()) == m_childVisuals.end()) return false;
			        evis = m_childVisuals[(*ioit)->getOwner()];
			    }
                px2 = evis->getAbsPosX() + (*ioit)->ps_getToPosX();
                py2 = evis->getAbsPosY() + (*ioit)->ps_getToPosY();

                if ((*ioit)->getType() == IO_TYPE_OUT)
                {
                    px2 += evis->getSizeW();
                }



                switch (App::wiringType)
                {
                    case WIRING_4590:
                        if ( ((px2-px)*(px2-px)) > ((py2-py)*(py2-py)) )
                        {
                            // breiter als hoch - in der mitte 45 grad winkel rechts und links gleichmäßig
                            s45 = true;
                            dx2 = (px2-px);
                            dy2 = (py2-py);
                            if (dx2<0) dx2 = -dx2;
                            if (dy2<0) dy2 = -dy2;

                            dx1 = (dx2 - dy2) / 2;
                            dy2 = (py2-py);
                            //if (dy2<0) dy2 = -dy2;
                        }
                        else
                        {
                            // höher als breit: rechtwinklige verbindung dx1 sagt wieviel es erst in x richtung geht bevor es 90 grad hoch oder runter geht
                            dx1 = (px2-px)/2; // mitte
                            //dx1 = px2- ((*iit)->ps_getFromPosY() - this->getAbsPosY()+80)/2;
                            dy2 = (py2-py);
                            s45 = false;
                        }

                    break;
                    default:
                        dx1 = (px2-px)*(px2-px) + (py2-py)*(py2-py)/2;
                        dx1 = sqrt(dx1);
                        dx2 = dx1;
                        if (evis && (evis->getObj()->getCirName() == "node"))
                        {
                            dx2 = dy2 = 0;
                        }
                }




                for (unsigned short pi=0; pi < pc; pi++)
                {
                    switch (App::wiringType)
                    {
                        case WIRING_4590:
                            cr->move_to(px, py -pc/2+pi);
                            if (s45)
                            {
                                cr->rel_line_to(dx1, 0);
                                cr->rel_line_to(dy2<0?-dy2:dy2, dy2);
                                cr->rel_line_to(dx1, 0);
                            }
                            else
                            {
                                cr->rel_line_to(dx1, 0);
                                cr->rel_line_to(0, dy2);
                            }
                            cr->line_to(px2,  py2 -pc/2+pi);
                        break;
                        default:
                            cr->move_to(px, py -pc/2+pi);
                            cr->curve_to(px + dx1*0.2,  py  -pc/2+pi,   px2 - dx2*0.2, py2 -pc/2+pi,    px2,  py2 -pc/2+pi);
                    }

                    if ((*ioit)->isSelected() || (*iit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
                    else if ((*iit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
                    else cr->set_source_rgb(WIRE_COL);

                    cr->stroke();
                }
			}
			//py += m_ioHeight;
		}
	} // if children
	return true;
} // CircuitChildVisual::paintAll










bool CircuitContainerVisual::paintAll(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{

    CircuitChildVisual::paintAll(cr, x, y, children);

	// Verbindungen zwischen
	if (children) {
        int px,py,px2,py2;
        double dx1=0,dx2=0,dy1=0,dy2=0;
        bool s45 = false;
        CircuitChildVisual* svis;
        CircuitChildVisual* evis;
        unsigned short pc;

		type_elements::iterator it;
		type_outputs::reverse_iterator oit;
		type_inputs::reverse_iterator iit;
		type_forwardings::iterator ioit;
        // Linien für alle elemente hinten raus
        cr->restore();
        cr->save();
        cr->set_line_width(1);

        for ( it = getObj()->getChildren().begin(); it != getObj()->getChildren().end(); it++ )
        {
            if (m_childVisuals.find(*it) == m_childVisuals.end()) return false;
            svis = m_childVisuals[*it]; // das vis der komponente des start-io (output)

            for ( oit=(*it)->getOutputs().rbegin(); oit != (*it)->getOutputs().rend(); oit++ )
            {
                px = svis->getAbsPosX() + svis->getSizeW() + (*oit)->ps_getFromPosX();
                py = svis->getAbsPosY() +                    (*oit)->ps_getFromPosY();

                pc = (*oit)->getPinCount();

                for (ioit = (*oit)->getForwardings().begin(); ioit != (*oit)->getForwardings().end(); ioit++)
                {
                    if ((*ioit)->getOwner() == getObj()) evis = this;
                    else
                    {
                        if (m_childVisuals.find((*ioit)->getOwner()) == m_childVisuals.end()) return false;
                        evis = m_childVisuals[(*ioit)->getOwner()];
                    }

                    px2 = evis->getAbsPosX() + (*ioit)->ps_getToPosX();
                    py2 = evis->getAbsPosY() + (*ioit)->ps_getToPosY();
                    if ((*ioit)->getType() == IO_TYPE_OUT)
                    {
                        px2 += evis->getSizeW();
                    }


                    switch (App::wiringType)
                    {
                        case WIRING_4590:
                            if ( ((px2-px)*(px2-px)) > ((py2-py)*(py2-py)) )
                            {
                                // breiter als hoch - in der mitte 45 grad winkel rechts und links gleichmäßig
                                s45 = true;
                                dx2 = (px2-px);
                                dy2 = (py2-py);
                                if (dx2<0) dx2 = -dx2;
                                if (dy2<0) dy2 = -dy2;

                                dx1 = (dx2 - dy2) / 2;
                                dy2 = (py2-py);
                                //if (dy2<0) dy2 = -dy2;
                            }
                            else
                            {
                                // höher als breit: rechtwinklige verbindung dx1 sagt wieviel es erst in x richtung geht bevor es 90 grad hoch oder runter geht
                                dx1 = (px2-px)/2; // mitte
                                //dx1 = px2- ((*oit)->ps_getFromPosY() - this->getAbsPosY()+80)/2;
                                dy2 = (py2-py);
                                s45 = false;
                            }
                            break;

                        default:

                            if (evis == svis) // wenn aus und eingang der selben komponente
                            {
                                dx1 = dx2 = 300;
                                // dy = ((py+py2)/2 - evis->getAbsPosY()) - ( + svis->getSizeH())/2;
                                int m = evis->getAbsPosY() + (evis->getSizeH() - evis->m_titleHeight) / 2;
                                int m2 = (py + py2)/2;
                                if (m2 > m)
                                {
                                    // unten rum
                                    dy1 = evis->getAbsPosY() + evis->getSizeH() - py;
                                    dy2 = evis->getAbsPosY() + evis->getSizeH() - py2;
                                }
                                else
                                {
                                    // oben rum
                                    dy1 = evis->getAbsPosY() - py;
                                    dy2 = evis->getAbsPosY() - py2;
                                }
                                dy1 *= 1.7;
                                dy2 *= 1.7;
                            }
                            else
                            {
                                dx1 = (px2-px)*(px2-px) + (py2-py)*(py2-py)/2;
                                dx1 = sqrt(dx1);
                                dx2 = dx1;
                                dy1 = dy2 = 0;
                            }

                            if (svis->getObj()->getCirName() == "node")
                            {
                                dx1 = dy1 = 0;
                            }
                            if (evis->getObj()->getCirName() == "node")
                            {
                                dx2 = dy2 = 0;
                            }
                    } // case

                    for (unsigned short pi=0; pi<pc; pi++)
                    {
                        //if ((*ioit)->getValue()) cr->set_line_width(WIRE_HOT_WIDTH);
                        //else cr->set_line_width(WIRE_WIDTH);

                        if ((*ioit)->isSelected() || (*oit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
                        else if ((*oit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
                        else cr->set_source_rgb(WIRE_COL);

                        switch (App::wiringType)
                        {
                            case WIRING_4590:
                                cr->move_to(px, py -pc/2+pi);
                                if (evis->getObj()->getCirName() == "node")
                                {
                                    cr->line_to(px2,  py -pc/2+pi);
                                }
                                else
                                {
                                    if (px<px2)
                                    {
                                        if (s45)
                                        {
                                            cr->rel_line_to(dx1, 0);
                                            cr->rel_line_to(dy2<0?-dy2:dy2, dy2);
                                            cr->rel_line_to(dx1, 0);
                                        }
                                        else
                                        {
                                            cr->rel_line_to(dx1-15, 0);
                                            cr->rel_line_to(0, dy2);
                                        }
                                    }
                                }
                                cr->line_to(px2,  py2 -pc/2+pi);
                            break;
                            default:
                                cr->move_to(px, py -pc/2+pi);
                                cr->curve_to(px + dx1*0.2,  py+dy1-pc/2+pi,   px2 - dx2*0.2, py2+dy2  -pc/2+pi,    px2, py2  -pc/2+pi);
                        }

                        cr->stroke();
                    }
                }
            }
        }
        cr->restore();
	} // if children

    type_elements::iterator it;
    int ChildOffsetX, ChildOffsetY;

    CircuitChildVisual *vis;

    for ( it = getObj()->getChildren().begin(); it != getObj()->getChildren().end(); it++ )
    {
        if (m_childVisuals.find((*it)) != m_childVisuals.end())
        {
            vis = m_childVisuals[(*it)];

            ChildOffsetX = x + m_inWidth; // + m_ioSpace + x;
            ChildOffsetY = y + m_titleHeight; // + y + m_vSpace;



            if (children || vis->getObj()->markedVisible())
            {
                vis->paintAll(
                    cr,
                    ChildOffsetX + vis->getPosX() - min_x,
                    ChildOffsetY + vis->getPosY() - min_y,
                    false
                    );
            }
        }
    }



	return true;
} // CircuitContainerVisual::paintAll
































bool CircuitChildVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{

	cr->save();


    // Hintergrund Weiß
    cr->rectangle(x+STRICH, y, getSizeW()-2*STRICH, getSizeH());
    cr->set_source_rgb(CIR_BACK_COL);
    cr->fill();

    // rahmen
    cr->rectangle(x+STRICH, y, getSizeW()-2*STRICH, getSizeH());
    cr->set_line_width(1.0);
    if (selected) cr->set_source_rgb(CIR_FRAME_SEL_COL);
    else cr->set_source_rgb(CIR_FRAME_COL);
    cr->stroke();


    // titel-hintergrund
    cr->rectangle(x+STRICH+1, y+1, getSizeW()-2-2*STRICH, m_titleHeight-2);
    if (getObj()->markedVisible())
        cr->set_source_rgb(CIR_TITLE_BACK_VIS_COL);
    else
        cr->set_source_rgb(CIR_TITLE_BACK_COL);
    cr->fill();

	cr->restore();

	type_outputs::iterator oit;
	type_inputs::iterator iit;

	int offs = 0;

    // io bereiche
    cr->save();
    cr->rectangle(x + 1 + STRICH,              y + m_titleHeight, m_inWidth-1-STRICH,  getSizeH()-m_titleHeight-1);
    cr->rectangle(x + getSizeW()-m_outWidth,   y + m_titleHeight, m_outWidth-1-STRICH, getSizeH()-m_titleHeight-1);
    cr->set_source_rgb(CIR_IO_BACK);
    cr->fill();
    cr->restore();

	// Output strichelchen
	cr->save();
	unsigned short pc, pi;
    cr->set_line_width(1);
	for ( oit=m_pObj->getOutputs().begin(); oit != m_pObj->getOutputs().end(); oit++ )
	{
        pc = (*oit)->getPinCount();
        for (pi=0; pi<pc; pi++)
        {
            if ((*oit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
            else if ((*oit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
            else cr->set_source_rgb(WIRE_COL);

            cr->move_to(x + getSizeW() - STRICH,   y + (m_ioHeight/2) + m_titleHeight + offs -pc/2+pi);
            cr->rel_line_to(STRICH, 0);
            cr->stroke();
        }

        offs += m_ioHeight;
	}


	// Input strichelchen
	offs = 0;
	int c =0;
	for ( iit = m_pObj->getInputs().begin(); iit != m_pObj->getInputs().end(); iit++ )
	{
        pc = (*iit)->getPinCount();
        for (pi=0; pi<pc; pi++)
        {
            cr->move_to(x+STRICH,    y + (m_ioHeight/2) + m_titleHeight + offs -pc/2+pi);
            cr->rel_line_to(-STRICH, 0);

            if ((*iit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
            else if ((*iit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
            else cr->set_source_rgb(WIRE_COL);

            cr->stroke();
        }

		offs += m_ioHeight;
		c++;
	}
	cr->restore();

    Cairo::TextExtents extents;

    // Titel
    // (12)
    cr->set_font_size(TITLE_FONT_SIZE);
    cr->get_text_extents(m_pObj->getName(), extents);
    cr->move_to(x + 2+STRICH, y - extents.y_bearing+2/*+ extents.height + 2*/ ); // WORKING ON

    if (selected) cr->set_source_rgb(CIR_TITLE_SEL_COL);
    else  cr->set_source_rgb(CIR_TITLE_COL);
    cr->show_text(m_pObj->getName());


    paintIOText(cr, x, y);


	return true;
} // CircuitChildVisual::paintSelf




bool ToplevelCircuitVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{

	cr->save();



	type_outputs::iterator oit;
	type_inputs::iterator iit;

	int offs = 0;
	unsigned short pc,pi;
    cr->set_line_width(1.0);
	// Output strichelchen
	cr->save();
	for ( oit=m_pObj->getOutputs().begin(); oit != m_pObj->getOutputs().end(); oit++ )
	{
        pc = (*oit)->getPinCount();
        for (pi=0; pi<pc; pi++)
        {
            if ((*oit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
            else if ((*oit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
            else cr->set_source_rgb(WIRE_COL);

            cr->move_to(x + getSizeW() - STRICH,   y + (m_ioHeight/2) + m_titleHeight + offs  -pc/2+pi);
            cr->rel_line_to(STRICH, 0);
            cr->stroke();

            cr->move_to(x + getSizeW()-m_outWidth,   y + (m_ioHeight/2) + m_titleHeight + offs  -pc/2+pi);
            cr->rel_line_to(-2, 0);
            cr->stroke();
        }

        offs += m_ioHeight;
	}


	// Input strichelchen
	offs = 0;
	int c =0;
	for ( iit = m_pObj->getInputs().begin(); iit != m_pObj->getInputs().end(); iit++ )
	{
        // in-Pfeil
        //cr->arc(x + m_ioHeight/2, y + m_titleHeight + offs + m_ioHeight/2, m_ioHeight/2, 0.0, 2*M_PI);
        cr->move_to(x+1+STRICH, y + m_titleHeight + offs + m_ioHeight-1);
        cr->rel_line_to(0, -(m_ioHeight-2));
        cr->rel_line_to(m_ioHeight-1, (m_ioHeight-2)/2);
        cr->close_path();

        if ((*iit)->getValue() > 0) cr->set_source_rgb(HOT_COL);
        else cr->set_source_rgb(WIRE_COL);
        cr->fill();

        // kleines streichelchen innen
        pc = (*iit)->getPinCount();
        for (pi=0; pi<pc; pi++)
        {
            cr->move_to(x + m_inWidth - 1,    y + (m_ioHeight/2) + m_titleHeight + offs  -pc/2+pi);
            cr->rel_line_to(2, 0);

            if ((*iit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
            else if ((*iit)->getValue() & (1<<pi)) cr->set_source_rgb(HOT_COL);
            else cr->set_source_rgb(WIRE_COL);
            cr->stroke();
        }

		offs += m_ioHeight;
		c++;
	}
	cr->restore();


    paintIOText(cr, x, y);

	return true;
} // ToplevelCircuitVisual::paintSelf





/** @brief paintIOText
  *
  * @todo: document this function
  */
bool CircuitChildVisual::paintIOText(Cairo::RefPtr<Cairo::Context> cr, int x, int y)
{

	type_outputs::iterator oit;
	type_inputs::iterator iit;
    Cairo::TextExtents extents;
    std::string text;
	// kleiner (10)
    cr->set_font_size(IO_FONT_SIZE);

	int px,py;


	// Output text
	cr->save();
	//cr->rectangle(x + getSizeW()-m_outWidth-2, y, m_outWidth, getSizeH());
	//cr->clip();
	px = x + getSizeW()-3-STRICH;
	py = y + m_titleHeight;
	for ( oit = m_pObj->getOutputs().begin(); oit != m_pObj->getOutputs().end(); oit++ ) {
	    text = (*oit)->getNameEx();

		cr->move_to(px, py);
		cr->get_text_extents(text, extents);
		cr->rel_move_to(-extents.width, m_ioHeight + extents.y_bearing/2);

		if ((*oit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
		else if ((*oit)->getValue()) cr->set_source_rgb(HOT_COL);
		else cr->set_source_rgb(WIRE_COL);

		cr->show_text(text);
		py += m_ioHeight;
	}


	// Input text
	px = x - 3 + m_inWidth;
	py = y + m_titleHeight;
	for ( iit = m_pObj->getInputs().begin(); iit != m_pObj->getInputs().end(); iit++ ) {
	    text = (*iit)->getNameEx();

		cr->move_to(px, py);
		cr->get_text_extents(text, extents);
		cr->rel_move_to(-extents.width, m_ioHeight + extents.y_bearing/2);

		if ((*iit)->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
		else if ((*iit)->getValue()) cr->set_source_rgb(HOT_COL);
		else cr->set_source_rgb(WIRE_COL);

		cr->show_text(text);
		py += m_ioHeight;
	}
	cr->restore();

	return true;
}





























/**
 * x,y,xx,yy: Zum Inner-bereich rel. koord.
 */

CircuitChildVisual* CircuitContainerVisual::getVisualFromPoint(int x, int y, int xx, int yy, COMPONENT_AREA& area, list<CircuitChildVisual*>* l)
{
	type_elements::reverse_iterator it;
	//type_visual_map::reverse_iterator it;

	if (l)
	{
		int c;
		if (x>xx) {c=x;x=xx;xx=c;}
		if (y>yy) {c=y;y=yy;yy=c;}
	}

	int x1,x2,y1,y2, rx,ry;
	bool is;
	CircuitChildVisual *vis;

    for (it = getObj()->getChildren().rbegin(); it != getObj()->getChildren().rend(); it++)
    {
        vis = m_childVisuals[(*it)];

        x1 =      vis->getPosX();
        x2 = x1 + vis->getSizeW();
        y1 =      vis->getPosY();
        y2 = y1 + vis->getSizeH();

        if (l==NULL) is = ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2) );
        else is = (x1 >= x) && (y1 >= y) && (x2 <= xx) && (y2 <= yy);

        if (is)
        {
            if (l == NULL)
            {
                vis->parentToComponent(x,y, rx, ry);
                area = vis->areaOnComponent(rx, ry);
                return vis;
            }
            else l->push_back(vis);
        }
    }
	area = CA_NONE;
	return NULL;
} // CircuitContainerVisual::getVisualFromPoint














/**
 * x,y: Zur Componente links oben relative koordinaten
 */

COMPONENT_AREA CircuitChildVisual::areaOnComponent(int x, int y)
{
	//cout << x << " " << y <<  endl;
	COMPONENT_AREA r = CA_NONE;

	if      ((x<0) || (y<0) || (x > getSizeW()) || (y > getSizeH())) return CA_NONE;

	if (y <= (m_titleHeight)) return CA_TITLE;
	else
	{
		if ((x < (getSizeW() - m_outWidth)) && (x > (m_inWidth)))
		{
			return CA_INNER;
		}
		if (x >= (getSizeW() - m_outWidth)) r |= CA_OUT;
		if (x <= (m_inWidth)) r |= CA_IN;
	}
	return r;
}

/**
 * Rechnet parent-koordinaten in koordinaten relativ zur Componente links oben
 * x,y: screen koord
 */

void CircuitChildVisual::parentToComponent(int x, int y, int& relx, int& rely)
{
/*	//cout << "relativePos " << getAbsPosX() << ", " << getAbsPosY() << endl;
	int dx;
	int dy;
    if (m_pParentVisual)
    {
        dx = m_pParentVisual->getAbsPosX();
        dy = m_pParentVisual->getAbsPosY();
    }
    else
    {
        dx = dy = 0;
    }

	relx = x - getAbsPosX() +dx; // changed: +Abs, ++dx
	rely = y - getAbsPosY() +dy;*/
	relx = x - getPosX();
	rely = y - getPosY();
}

/**
 * Berechnet zum angegebenen Bereich rel. koordinaten
 * x,y: zur komponente rel koord.
 */

void CircuitChildVisual::componentToArea(COMPONENT_AREA area, int x, int y, int& relx, int& rely)
{
	int ox=0, oy=0;
	switch (area)
	{
	case CA_INNER:
		ox = m_inWidth;
		oy = m_titleHeight;
		break;
	//case CA_TITLE:
	//	break;
	case CA_IN:
	case (CA_IN|CA_OUT): // sondefall wenn i und o zusammenfallen
		oy = m_titleHeight;
		break;
	case CA_OUT:
		oy = m_titleHeight;
		ox = getSizeW()-m_outWidth;
		break;
	}
	relx = x - ox;
	rely = y - oy;
}


/**
 * x,y: rel. zum in-Bereich
 */

CircuitInput* CircuitChildVisual::getInputFromPoint(int x, int y)
{

	type_inputs::iterator it;
	int num = (y) / m_ioHeight;
	int n=0;
	for (it = m_pObj->getInputs().begin(); it != m_pObj->getInputs().end(); it++)
	{
		if (n == num) return (*it);
		n++;
	}

	return NULL;
}

/**
 * x,y: rel. zum out-Bereich
 */

CircuitOutput* CircuitChildVisual::getOutputFromPoint(int x, int y)
{
	type_outputs::iterator it;
	int num = (y) / m_ioHeight;
	int n=0;
	for (it = m_pObj->getOutputs().begin(); it != m_pObj->getOutputs().end(); it++)
	{
		if (n == num) return (*it);
		n++;
	}
	return NULL;
}



///---------------------------------------------------------------

/// Gate-Viaual


void GateVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;
	Cairo::TextExtents extents;
    string s;
    cr->save();
    cr->set_font_size(GATE_FONT_SIZE);
    s = getGateSymbol();
    cr->get_text_extents(s, extents);
    w = extents.width + 4;
    h = extents.y_bearing + 4;
    cr->restore();
}

bool GateVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;
    CircuitChildVisual::paintSelf(cr, x, y, false);

    cr->save();


	int _x = x;
	int _y = y;

	x = x + m_inWidth;
	y = y + m_titleHeight;


	Cairo::TextExtents extents;
    string s;
    bool _not;

    cr->set_font_size(GATE_FONT_SIZE);

    s = getGateSymbol();

    switch (getObj()->getType())
    {
    	case NAND:
    	case NOR:
    	case NOT:
            _not = true;
            break;
        default:
            _not = false;
    }


    // Gate-Symbol
    cr->set_source_rgb(0,0,0);
    cr->get_text_extents(s, extents);
    //std::cout << extents.y_bearing << std::endl;
    cr->move_to(x -2 + (getSizeW()-m_inWidth-m_outWidth-extents.width)/2, y + (getSizeH()-m_titleHeight -extents.y_bearing)/2);
    cr->show_text(s);

    if (_not)
    {
        CircuitOutput *out = m_pObj->getOutput("x");
        x = _x+getSizeW()+out->ps_getFromPosX()-1;
        y = _y+out->ps_getFromPosY();
        cr->begin_new_sub_path();

        cr->arc(x, y, 4.0,    0.0, 2 * M_PI);
        cr->set_source_rgb(1.0,1.0,1.0);
        cr->fill();

        cr->arc(x, y, 4.0,    0.0, 2 * M_PI);
        cr->set_line_width(1);
        if (out->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
        else if (out->getValue()) cr->set_source_rgb(HOT_COL);
        else cr->set_source_rgb(WIRE_COL);
        cr->stroke();
    }

    cr->restore();

    return true;
}



string GateVisual::getGateSymbol()
{
    string s;
    switch (getObj()->getType())
    {
    	case AND:
            return "&";
    		break;
    	case NAND:
            return "&";
    		break;
    	case OR:
            return "≥1";
    		break;
    	case NOR:
            return "≥1";
    		break;
    	case XOR:
            return "=1";
    		break;
    	case NOT:
            return "1";
    		break;
    }

    return "";
}













































/*************************/



bool DigitalZifferVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;
	CircuitChildVisual::paintSelf(cr, x,y, false);

	x = x + m_inWidth;
	y = y + m_titleHeight;

	cr->save();
	cr->rectangle(x, y, ziffer_size, ziffer_size*2);
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->fill();
	unsigned int num = 0;
	type_inputs::reverse_iterator iit;
	for ( iit = m_pObj->getInputs().rbegin(); iit != m_pObj->getInputs().rend(); iit++ )
	{
		//cout << (*iit).second->getName() << " = " << (*iit).second->getValue() << endl;
		num <<= 1;
		num |= ((*iit)->getValue() ? 1 : 0);
	}
	//cout << " = " << num << endl;

	//   1111111
	//  4        6
	//  4        6
	//   222222222
	//  5        7
	//  5        7
	//   3333333


	int o = 4;
	int z = ziffer_size-o-o;
	// 1
	if ((num!=1) && (num!=4) && (num!=11) && (num!=13)) //  23 567890..
	{
		cr->move_to(x+o,y+o);
		cr->rel_line_to(z, 0);
	}
	// 2
	if ((num!=1) && (num!=7) &&(num!=0) && (num!=12)) //  23456 89
	{
		cr->move_to(x+o,y+o+z);
		cr->rel_line_to(z, 0);
	}
	// 3
	if ((num!=1) && (num!=4) &&(num!=7) && (num!=10) && (num!=15)) //  23 56 789
	{
		cr->move_to(x+o,y+o+z+z);
		cr->rel_line_to(z, 0);
	}

	// 4
	if ((num!=1) && (num!=2) && (num!=3) && (num!=7) && (num!=13)) // 456 890
	{
		cr->move_to(x+o,y+o);
		cr->rel_line_to(0,z);
	}
	// 5
	if ((num==2) || (num==6) || (num==8) || (num==0) || (num>9)) // 2   6 8 0
	{
		cr->move_to(x+o,y+o+z);
		cr->rel_line_to(0,z);
	}

	// 1 2 3 4 5 6 7 8 9 0
	// 6
	if ((num!=5) && (num!=6) && (num!=11) && (num!=12) && (num!=14) && (num!=15)) // 1234  7890
	{
		cr->move_to(x+o+z,y+o);
		cr->rel_line_to(0,z);
	}
	// 7
	if ((num!=2) && (num!=12) && (num!=14) && (num!=15)) // 1 34567890
	{
		cr->move_to(x+o+z,y+o+z);
		cr->rel_line_to(0,z);
	}

	cr->set_line_width(4.0);
	cr->set_source_rgb(0.0, 1.0, 0.7);
	cr->stroke();
	cr->restore();
	return true;
} // DigitalZiffer-paintSelf

bool LetterVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;
	CircuitChildVisual::paintSelf(cr, x,y, false);

	x = x + m_inWidth;
	y = y + m_titleHeight;

	cr->save();
	cr->rectangle(x, y, getSizeW()-m_inWidth-6, getSizeH()-m_titleHeight-1);
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->fill();
	cr->set_line_width(4.0);

    unsigned char ch = (unsigned char)m_pObj->getInputs().front()->getValue();

    if ((ch>='a'&&ch<='z') || (ch>='A'&&ch<='Z'))
    {
        std::string str;
        str += ch;


        Cairo::TextExtents extents;
        cr->set_font_size(letter_size*2);
        cr->get_text_extents(str, extents);

        cr->set_source_rgb(0.0, 1.0, 0.7);
        cr->move_to(x +(getSizeW()-m_inWidth-10-extents.width)/2, y+(getSizeH()-m_titleHeight-1-extents.y_bearing)/2);
        cr->show_text(str);
    }

	cr->restore();
	return true;
} // LetterVisual::paintSelf


bool DigitalZiffer2Visual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;
	CircuitChildVisual::paintSelf(cr, x,y, false);

	x = x + m_inWidth;
	y = y + m_titleHeight;

	cr->save();
	cr->rectangle(x, y, ziffer_size, ziffer_size*2);
	cr->set_source_rgb(0.0, 0.0, 0.0);
	cr->fill();

	//cout << " = " << num << endl;

	//   1111111
	//  4        6
	//  4        6
	//   222222222
	//  5        7
	//  5        7
	//   3333333



	type_inputs::iterator iit;
	iit = m_pObj->getInputs().begin();

	int o = 4;
	int z = ziffer_size-o-o;
	// 1
	if ((*iit)->getValue())
	{
		cr->move_to(x+o,y+o);
		cr->rel_line_to(z, 0);
	}
	iit++;
	// 2
	if ((*iit)->getValue())
	{
		cr->move_to(x+o,y+o+z);
		cr->rel_line_to(z, 0);
	}
	iit++;
	// 3
	if ((*iit)->getValue())
	{
		cr->move_to(x+o,y+o+z+z);
		cr->rel_line_to(z, 0);
	}
	iit++;
	// 4
	if ((*iit)->getValue())
	{
		cr->move_to(x+o,y+o);
		cr->rel_line_to(0,z);
	}
	iit++;
	// 5
	if ((*iit)->getValue())
	{
		cr->move_to(x+o,y+o+z);
		cr->rel_line_to(0,z);
	}
	iit++;
	// 6
	if ((*iit)->getValue())
	{
		cr->move_to(x+o+z,y+o);
		cr->rel_line_to(0,z);
	}
	iit++;
	// 7
	if ((*iit)->getValue())
	{
		cr->move_to(x+o+z,y+o+z);
		cr->rel_line_to(0,z);
	}

	cr->set_line_width(4.0);
	cr->set_source_rgb(0.0, 1.0, 0.7);
	cr->stroke();
	cr->restore();
	return true;
} // DigitalZiffer2-paintSelf



void DigitalZifferVisual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;
	//if (!sizeSet)
	{
		CircuitChildVisual::updateSize(cr, x,y, false);
		setSize(getSizeW()+ziffer_size, max(getSizeH(), ziffer_size*2+m_titleHeight+10));
		sizeSet = true;
	}
}
void LetterVisual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;
	//if (!sizeSet)
	{
		CircuitChildVisual::updateSize(cr, x,y, false);
		setSize(getSizeW()+letter_size, max(getSizeH(), letter_size*2+m_titleHeight+10));
		sizeSet = true;
	}
}
void DigitalZiffer2Visual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;
	//if (!sizeSet)
	{
		CircuitChildVisual::updateSize(cr, x,y, false);
		setSize(getSizeW()+ziffer_size, max(getSizeH(), ziffer_size*2+m_titleHeight+10));
		sizeSet = true;
	}
}






bool NodeVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;

	//x = x + m_inWidth;
	//y = y + m_titleHeight;
	cr->save();
	cr->set_line_width(0.7);


	//cr->rectangle(x,           y,               getSizeW(),   m_titleHeight);
	cr->rectangle(x,           y+m_titleHeight, getSizeW(), m_ioHeight);
	//cr->rectangle(x,           y+m_titleHeight, getSizeW()/2, m_ioHeight);
	//cr->rectangle(x+m_inWidth, y+m_titleHeight, getSizeW()/2, m_ioHeight);
	if (m_pObj->getInput("i")->getValue()) {
		cr->set_source_rgb(0.7, 0.0, 0.0);
	} else {
		cr->set_source_rgb(0.6, 0.6, 0.6);
	}
	cr->fill();

	cr->rectangle(x, y, getSizeW(), getSizeH());
	if (selected) cr->set_source_rgb(0.0, 0.0, 0.8);
	else cr->set_source_rgb(0.6, 0.6, 0.6);
	cr->stroke();

	if (selected) {
		Cairo::TextExtents extents;
        cr->set_font_size(TITLE_FONT_SIZE);
		cr->get_text_extents(m_pObj->getName(), extents);
		cr->move_to(x+(getSizeW()-extents.width)/2, y -2);
		if (selected) cr->set_source_rgb(0.2, 0.2, 0.8);
		else  cr->set_source_rgb(0.6, 0.6, 0.6);
		cr->show_text(m_pObj->getName());
	}

    ostringstream os;
    os << m_pObj->pin->getPinCount();
	cr->move_to(x, y+getSizeH());
	cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->show_text(os.str());

	cr->restore();

	return true;
} // Node-paintSelf

void NodeVisual::updateSize(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;
	setAbsPos(x,y);
	m_inWidth  = 6;
	m_outWidth = m_inWidth;
	m_ioHeight = m_inWidth;
	setSize(m_inWidth*2+2, m_ioHeight+m_titleHeight);

	m_pObj->getInput("i")->ps_setToPos(   getSizeW()/2,   m_ioHeight/2 + m_titleHeight);
	m_pObj->getInput("i")->ps_setFromPos( getSizeW()/2,   m_ioHeight/2 + m_titleHeight);
	m_pObj->getOutput("o")->ps_setFromPos(-getSizeW()/2,   m_ioHeight/2 + m_titleHeight);
	m_pObj->getOutput("o")->ps_setToPos(  -getSizeW()/2,   m_ioHeight/2 + m_titleHeight);

}


bool IOVisual::vertical()
{
	return m_pObj->getProperties().get_bool("vertical");
}

void IOVisual::vertical(bool b)
{
    m_pObj->getProperties().set_bool("vertical", b);
}

void IOVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
	IOComp *comp = (IOComp *)m_pObj;

	w = vertical() ? 20 : 20 * comp->m_pincount + 10;
	h = vertical() ? 20 * comp->m_pincount + 10 : 40;

}
bool IOVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;

	CircuitChildVisual::paintSelf(cr, x,y, false);

	bool vert = vertical();

	x = x + m_inWidth;
	y = y + m_titleHeight;

	IOComp *comp = (IOComp *)m_pObj;

	cr->save();

	int px=x, py=y;
	int w = getSizeW()-m_outWidth-m_inWidth;
	int h = getSizeH()-m_titleHeight;

	int ch, cw, cww, chh;
	if (vert)
	{
		cr->rectangle(px+2, py+2, 20-4, 10-4);
		py += 10;
		h -= 10;
		ch = 20;
		cw = 0;
		cww = 20;
		chh = 20;
	}
	else
	{
		cr->rectangle(px+2, py+2, 10-4, 20-4);
		px += 10;
		w -= 10;
		cw = -20;
		ch = 0;
		px += w;
		px -= 20;
		cww = 20;
		chh = 20;

	}

	bool v;
	string s;
	Cairo::TextExtents extents;

	//if (vertical) cr->set_source_rgb(0,1,1);
	//else cr->set_source_rgb(1,0,1);
	cr->set_line_width(1.0);
	cr->set_source_rgb(0, 0, 0);
	cr->stroke();

    cr->set_font_size(TEXT_FONT_SIZE);


	for (int i = 0; i < comp->m_pincount; i++)
	{
		v = comp->getValue(i);

		if (v) cr->set_source_rgb(HOT_COL);
		else cr->set_source_rgb(WIRE_COL);
		cr->rectangle(px, py, cww, chh);
		cr->fill();

		s = v ? "1" : "0";

		cr->set_source_rgb(1,1,1);
		cr->get_text_extents(s, extents);
		cr->move_to(px + (cww - extents.width)/2, py + (chh + extents.height)/2);
		cr->show_text(s);

		py += ch;
		px += cw;
	}

	if (!vert && (comp->m_pincount > 1))
	{
		ostringstream os;
		// funktioniert so auch mit multipin
		int val=0;
		for (int i = comp->m_pincount-1; i >= 0; i--)
		{
			val <<= 1;
			val += (comp->getValue(i) ? 1 : 0);
		}
		os << "" << val << " = #" << hex << val;
		s = os.str();
		cr->set_source_rgb(0,0,0);
		cr->get_text_extents(s, extents);
		cr->move_to(x-m_inWidth+getSizeW()-m_outWidth-extents.width-5, y + 25 + extents.height);
		cr->show_text(s);
	}



	cr->restore();

	return true;
} // IOVisual-paintSelf



// Area-rel. koord
void SwitchVisual::on_inner_click(int x, int y, CircuitWidget* widg)
{

	//cout << "SwitchVisual on_inner_click x=" << x << " y=" << y << endl;
		Switch* sw = (Switch*)m_pObj;
		int n = -1;
		bool vert = vertical();

		if (vert)
		{
			if ((y < 10) && (y > 0)) n = -3;
			else n = (y-10) / 20;
		}
		else // horizontal
		{
			if ((x < 10) && (x > 0)) n = -3;
			else n = ((getSizeW()-(m_inWidth+m_outWidth))-x) / 20;
		}

		//cout << n << endl;
		if ((n>=0) && (n<sw->m_pincount))
		{
			sw->toogleValue(n);
			m_pObj->getParent()->update();
			widg->on_circuitstate(CSC_SWITCHED);
		}
		else if (n==-3) vertical(!vert);
		widg->update();

}



// Area-rel. koord
void IndicatorVisual::on_inner_click(int x, int y, CircuitWidget* widg)
{
		//Indicator* sw = (Indicator*)m_pObj;
		//int n = -1;
		bool vert = vertical();
		vertical(!vert);
		widg->update();
}








LEDVisual::LEDVisual(LEDComp* o, CircuitContainerVisual* p, Color c) :
    CircuitChildVisual(o, p),
    color(None),
    m_pObj(o)
{
    try
    {
        offImage = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-off.png");
    }
    catch (...)
    {
        offImage.clear();
    }

    m_pSelection = m_pObj->getProperties().get_selection("color");
    setupColor();
    m_inWidth = 5;
}
void LEDVisual::setupColor()
{
    Color c = Bin;

    try
    {
        string s = m_pSelection->get_value();


        if ((s=="blue")||(s=="blau")) c = Blue;
        else if ((s=="green")||(s=="grün")) c = Green;
        else if ((s=="red")||(s=="rot")) c = Red;
        else if ((s=="purple")||(s=="violett")||(s=="lila")||(s=="pink")) c = Purple;
        else if ((s=="yellow")||(s=="gelb")) c = Yellow;
        else if ((s=="orange")||(s=="orange")) c = Orange;
        else c = Bin;
    }
    catch (string& s)
    {
        std::cerr << s << std::endl;;
    }

    if ((c != color))
    {
        //m_setColor = true;
        color = c;
        if (color == Blue)
        {
            _color.set_rgb_p(0, 0, 1);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-blue.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Bin)
        {
            _color.set_rgb_p(1, 1, 1);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-bin.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Yellow)
        {
            _color.set_rgb_p(1, 1, 0);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-yellow.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Red)
        {
            _color.set_rgb_p(1, 0, 0);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-red.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Green)
        {
            _color.set_rgb_p(0, 0.8, 0);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-green.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Purple)
        {
            _color.set_rgb_p(0.8, 0, 1);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-purple.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
        else if (color == Orange)
        {
            _color.set_rgb_p(1, 0.5, 0);
            try
            {
                onImage  = Cairo::ImageSurface::create_from_png(App::etc_piiri_path + "/led/rled-orange.png");
            }
            catch (...)
            {
                onImage.clear();
            }
        }
    }
}
void LEDVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
	w = 45;
	h = 50-m_titleHeight;
	//m_titleHeight = 50;
    m_outWidth = 0;
    m_inWidth = 13;
}
void LEDVisual::updateIOPos()
{
    m_pObj->getInput("i")->ps_setFromPos(0, 25);
    m_pObj->getInput("i")->ps_setToPos(0, 25);
}
bool LEDVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;
	cr->save();


	if (getObj()->markedVisible())
	{
        cr->set_line_width(1.0);
        cr->set_source_rgb(CIR_TITLE_BACK_VIS_COL);
        //cr->rectangle(x-1, y-1, getSizeW()+2, getSizeH()+2);
        cr->begin_new_sub_path();
        cr->arc(x+(getSizeW()+m_inWidth-5)/2, y+getSizeH()/2, 27, 0,2*M_PI);
        cr->stroke();
	}
	if (selected)
	{
        cr->set_line_width(1.0);
        cr->set_source_rgb(CIR_FRAME_SEL_COL);
        cr->rectangle(x, y, getSizeW(), getSizeH());
        cr->stroke();
	}

    CircuitInput *in = m_pObj->getInput("i");

    int xx = in->ps_getToPosX();
    int yy = in->ps_getToPosY();

    if (in->isSelected() || in->isSelected()) cr->set_source_rgb(WIRE_SEL_COL);
    else if (in->getValue()) cr->set_source_rgb(HOT_COL);
    else cr->set_source_rgb(WIRE_COL);
    cr->rectangle(x + xx, y + yy-3, m_inWidth, 6);
    cr->fill();

	y = y + m_titleHeight;
	x = x + m_inWidth;

    setupColor();

    bool b = true;

    if (in->getValue())
    {
        if (onImage) cr->set_source(onImage, 0, 0);
        else
        {
            b = false;
        }
    }
    else
    {
        if (offImage) cr->set_source(offImage, 0, 0);
        else b = false;
    }


    if (b)
    {
        Cairo::RefPtr<Cairo::Pattern> pat = cr->get_source();
        Cairo::Matrix matrix = Cairo::identity_matrix();
        matrix.translate(-x+5, -y+m_titleHeight);
        pat->set_matrix(matrix);
        cr->paint();
    }
    else
    {
        if (offImage)
        {
            cr->set_source(offImage, 0, 0);
            Cairo::RefPtr<Cairo::Pattern> pat = cr->get_source();
            Cairo::Matrix matrix = Cairo::identity_matrix();
            matrix.translate(-x+5, -y+m_titleHeight);
            pat->set_matrix(matrix);
            cr->paint();
        }

        cr->arc(x + (getSizeW()-m_inWidth)/2 -2, y-m_titleHeight + (getSizeH())/2 -1, 16,    0.0, 2 * M_PI);
        if (in->getValue())
        {
            cr->set_source_rgb(_color.get_red_p(), _color.get_green_p(), _color.get_blue_p());
        }
        else
        {
            cr->set_source_rgb(0.3, 0.3, 0.3);
        }
        cr->fill();
    }


	cr->restore();
	return true;
} // LEDVisual-paintSelf

// x,y: rel. zum in-Bereich
CircuitInput* LEDVisual::getInputFromPoint(int x, int y)
{
    int xx = m_pObj->getInput("i")->ps_getToPosX();
    int yy = m_pObj->getInput("i")->ps_getToPosY();
    xx = x-xx;
    yy = yy-m_titleHeight-y;
    yy = yy*yy;
    //cout << xx << " " << yy << endl;
    if ((xx <= m_inWidth) && (yy <= 9)) return m_pObj->getInput("i");
	return NULL;
}







void TextVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return;

    TextComponent *tc = (TextComponent*)getObj();
    string text = tc->getText();

	Cairo::TextExtents extents;
    cr->set_font_size(tc->getFontSize());
    cr->get_text_extents(text, extents);

    w = extents.width+4;
    h = -extents.y_bearing+6;

    m_inWidth = 0;
    m_outWidth = 0;

}

TextVisual::TextVisual(TextComponent* o, CircuitContainerVisual* p) :
 CircuitChildVisual(o, p),
 m_pObj(o)
{
    m_inWidth = 0;
    m_outWidth = 0;
    m_titleHeight = 0;
}

bool TextVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;

    TextComponent *tc = (TextComponent*)getObj();
    string text = tc->getText();

    cr->save();

    if (selected || text.empty())
    cr->rectangle(x,y,getSizeW(), getSizeH());
    cr->set_line_width(1.0);
    if (selected) cr->set_source_rgb(0.0, 0.0, 0.8);
    else cr->set_source_rgb(0.7,0.7,0.7);
    cr->stroke();

    y += 2;
    x += 2;

	Cairo::TextExtents extents;
    cr->set_font_size(tc->getFontSize());
    cr->get_text_extents(text, extents);
    cr->move_to(x, y-extents.y_bearing);
    cr->set_source_rgb(0,0,0);
    cr->show_text(text);

    cr->restore();

    return true;
}




