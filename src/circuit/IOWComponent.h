#ifndef IOWCOMPONENT_H
#define IOWCOMPONENT_H

#include "CircuitChild.h"
#include "iow/IOWarrior.h"

class IOWComponent : public CircuitChild
{
    protected:
        bool writeToDevice;
        IOWarrior* m_pIOW;
        sigc::connection con;
        SelectionValue2* sv_mode;
        unsigned int io_mode;

        enum IO_MODES {IOM_NONE=0, IOM_RW=1, IOM_READ=2, IOM_WRITE=3};
    public:
        IOWComponent(string n, CircuitContainer* p);
        virtual ~IOWComponent();
        type_iovalue calculateOutputValue(const string& out);
        virtual void on_propertyChanged();

        void on_input();
        virtual void on_input_changed(const std::string& name);
        virtual void provideUiText(ui_text_map& textmap);

};


#endif // IOWCOMPONENT_H
