#ifndef RAMCOMPONENT_H
#define RAMCOMPONENT_H

#include "CircuitChild.h"


class RAMComponent : public CircuitChild
{
    protected:
        unsigned int m_addr_bits, m_item_bits, m_item_count;
        unsigned int *m_pItems;
        string m_content;
        bool m_set;
        bool m_multipin;
        unsigned int m_value;

        void on_propertyChanged();
        void createIO();
        void createItems();
        void readContentFromProps(string& content);
        void writeContentToProps();
    public:
        RAMComponent(string n, CircuitContainer* p, unsigned int ab, unsigned int ib);
        virtual ~RAMComponent();
        type_iovalue calculateOutputValue(const string& out);
        virtual void on_input_changed(const string& name);
        virtual void provideUiText(ui_text_map& textmap);
};


#endif // RAMCOMPONENT_H
