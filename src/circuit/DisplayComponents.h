#ifndef DISPLAYCOMPONENTS_H
#define DISPLAYCOMPONENTS_H

#include "CircuitChild.h"

class DigitalZiffer : public CircuitChild {
protected:
	int ziffer_size;
	bool sizeSet;
public:
	DigitalZiffer(string n, CircuitContainer* p) : CircuitChild(n, p,"digit"), ziffer_size(30), sizeSet(false)
	{
		inputs.push_back(new CircuitInput(this, "0"));
		inputs.push_back(new CircuitInput(this, "1"));
		inputs.push_back(new CircuitInput(this, "2"));
		inputs.push_back(new CircuitInput(this, "3"));
		//markedVisible(true);
	}
	virtual ~DigitalZiffer() {
	}

	type_iovalue calculateOutputValue(const string& out) {
		return 0;
	}

};


class LetterComponent : public CircuitChild {
protected:
	int letter_size;
	bool sizeSet;
public:
	LetterComponent(string n, CircuitContainer* p) : CircuitChild(n, p,"letter"), letter_size(30), sizeSet(false)
	{
		inputs.push_back(new CircuitInput(this, "in", 8));
	}
	virtual ~LetterComponent() {
	}

	type_iovalue calculateOutputValue(const string& out) {
		return 0;
	}

};



class DigitalZiffer2 : public CircuitChild {
protected:
	int ziffer_size;
	bool sizeSet;
public:
	DigitalZiffer2(string n, CircuitContainer* p) : CircuitChild(n, p,"digit2"), ziffer_size(30), sizeSet(false)
	{
		inputs.push_back(new CircuitInput(this, "0"));
		inputs.push_back(new CircuitInput(this, "1"));
		inputs.push_back(new CircuitInput(this, "2"));
		inputs.push_back(new CircuitInput(this, "3"));
		inputs.push_back(new CircuitInput(this, "4"));
		inputs.push_back(new CircuitInput(this, "5"));
		inputs.push_back(new CircuitInput(this, "6"));
	}
	virtual ~DigitalZiffer2() {
	}

	type_iovalue calculateOutputValue(const string& out) {
		return 0;
	}

};

#endif // DISPLAYCOMPONENTS_H
