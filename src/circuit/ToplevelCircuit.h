#ifndef TOPLEVELCIRCUIT_H
#define TOPLEVELCIRCUIT_H

#include "EditableCircuit.h"

/**
implementiert properties geschichten neu (und damit das laden aus xml?)
stellt eine editierbare schaltung dar
*/
class ToplevelCircuit : public EditableCircuit
{
    public:
        ToplevelCircuit();
        virtual ~ToplevelCircuit();

    // Properties
        virtual void provideUiText(ui_text_map& textmap);

        virtual void on_propertyChanged();

        Complex2* m_pExtraProperties;

    protected:
        virtual bool loadFromDomDocument(xmlpp::Document *doc);

};

#endif // TOPLEVELCIRCUIT_H
