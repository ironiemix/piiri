/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <iostream>
#include <sstream>

#include "circuit/Components.h"
#include "circuit/CircuitContainer.h"
#include "model/LogicParser.h"
//#include "iow/IOWFactory.h"


//#define COMMAND_LOG






/***************** Gate **********************/

// Logische Gatter ++++++++++++++++++++++++++
bool operator&(CircuitInput& a, CircuitInput& b) {
	return (a.getValue() && b.getValue());
}
bool operator&(bool a, CircuitInput& b) {
	return (a && b.getValue());
}

bool operator|(CircuitInput& a, CircuitInput& b) {
	return (a.getValue() || b.getValue());
}
bool operator|(bool a, CircuitInput& b) {
	return (a || b.getValue());
}

bool operator^(CircuitInput& a, CircuitInput& b) {
	return (a.getValue() && !b.getValue()) || (!a.getValue() && b.getValue());
}
bool operator^(bool a, CircuitInput& b) {
	return (a && !b.getValue()) || (!a && b.getValue());
}

bool operator~(CircuitInput& a) {
	return !a.getValue();
}



/** @brief GateComponent
  *
  * @todo: document this function
  */
GateComponent::GateComponent(string n, CircuitContainer* p, GATE_TYPE t, unsigned int inCount) :
CircuitChild(n,p,""),
type(t)
{
    if (inCount < 2) inCount = 2;

    switch (type)
    {
    	case AND:
            setCirName("and");
    		break;
    	case NAND:
            setCirName("nand");
    		break;
    	case OR:
            setCirName("or");
    		break;
    	case NOR:
            setCirName("nor");
    		break;
    	case XOR:
            setCirName("xor");
            //Die Funktion eines XOR-Gatters mit mehr als zwei Eingängen ergibt sich, indem man zunächst zwei der Eingänge XOR-verknüpft, dann deren Ergebnis mit dem nächsten Eingang XOR-verknüpft, solange bis alle Eingänge berücksichtigt sind.
    		break;
    	case NOT:
            inCount = 1;
            setCirName("not");
    		break;
    }

    if (type != NOT)
    {
        getProperties().set_int("size", inCount);
    }

    outputs.push_back(new CircuitOutput(this, "x"));
    createIO(inCount);
}

void GateComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Breite", "Anzahl der Ein- bzw. Ausgänge.");
}

void GateComponent::on_propertyChanged()
{
    if (type != NOT)
    {
        unsigned int ic, old = inputs.size();
        ic = (unsigned int)getProperties().get_int("size");
        if (ic != old)
        {
            createIO(ic);
        }
    }
}

void GateComponent::createIO(unsigned int ic)
{
    clearInputs();
    switch (type)
    {
    	case NOT:
            inputs.push_back(new CircuitInput(this, "a"));
    		break;
    	default:
            if (ic <= 2)
            {
                inputs.push_back(new CircuitInput(this, "a"));
                inputs.push_back(new CircuitInput(this, "b"));
            }
            else
            {
                ostringstream os;
                for (unsigned int a = 0; a < ic; a++)
                {
                    os << "i" << a;
                    inputs.push_back(new CircuitInput(this, os.str()));
                    os.str("");
                }
            }
    		break;
    }
}


/** @brief calculateOutputValue
  *
  * @todo: document this function
  */
type_iovalue GateComponent::calculateOutputValue(const string& out)
{
    unsigned int ic = inputs.size();

    if (ic<=2)
    {
        switch (type)
        {
            case AND:
                return *getInput("a") & *getInput("b");
            case NAND:
                return !(*getInput("a") & *getInput("b"));
            case OR:
                return *getInput("a") | *getInput("b");
            case NOR:
                return !(*getInput("a") | *getInput("b"));
            case XOR:
                return *getInput("a") ^ *getInput("b");
            case NOT:
                return ~*getInput("a");
        }
    }
    else
    {
        bool erg;
        type_inputs_iterator it = inputs.begin();
        erg = (*it)->getValueB();
        for (it++; it != inputs.end(); it++)
        {
            switch (type)
            {
                case AND:
                case NAND:
                    erg = erg & *(*it);
                    break;
                case OR:
                case NOR:
                    erg = erg | *(*it);
                    break;
                case XOR:
                    erg = erg ^ *(*it);
                    break;
                default:
                    break;
            }
        }


        if ((type == NAND) || (type == NOR) || (type == NOT))
        {
            erg = !erg;
        }

        return erg;
    }
    return false;
}












/************** IO-Komponenten *****************/

/** @brief provideUiText
  *
  * @todo: document this function
  */
void IOComp::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Breite", "Anzahl der Ein- bzw. Ausgänge.");
    textmap["/vertical"] = ui_text("Vertikale Anzeige", "");
    textmap["/multipin"] = ui_text("Multipin", "");
}


Switch::Switch(string n, CircuitContainer* p, int c) : IOComp(n, "switch", p, c)
{
    createIO();
}
void Switch::toogleValue(int n)
{
    if (m_multipin)
    {
        CircuitOutput *out = getOutput("out");
        return out->setValue(out->getValue() ^  (1<<n) );
    }
    else
    {
        ostringstream str;
        str << "o" << n;
        CircuitOutput *out = getOutput(str.str());
        return out->setValue(!out->getValue());
    }
}
type_iovalue Switch::getValue(int n)
{
    if (m_multipin)
    {
        return ((getOutput("out")->getValue() & (1<<n)) != 0);
    }
    else
    {
        ostringstream str;
        str << "o" << n;
        return getOutput(str.str())->getValue();
    }
}
void Switch::on_propertyChanged()
{
    bool cio = false;
	if (m_pincount != getProperties().get_int("size"))
	{
		m_pincount = getProperties().get_int("size");
		cio = true;
	}
	if (m_multipin != getProperties().get_bool("multipin"))
	{
		m_multipin = getProperties().get_bool("multipin");
		cio = true;
	}
	if (cio) createIO();
}
/** @brief createIO
  *
  * @todo: document this function
  */
void Switch::createIO()
{
    clearOutputs();
    if (m_multipin)
    {
        outputs.push_back(new CircuitOutput(this, "out", m_pincount));
    }
    else
    {
        ostringstream str;
        for (int i = 0; i < m_pincount; i++)
        {
            str << "o" << i;
            outputs.push_back(new CircuitOutput(this, str.str()));
            str.str("");
        }
    }
}



Indicator::Indicator(string n, CircuitContainer* p, int c) : IOComp(n, "indicator", p, c)
{
    createIO();
}
type_iovalue Indicator::getValue(int n)
{
    if (m_multipin)
    {
        return ((getInput("in")->getValue() & (1<<n)) != 0);
    }
    else
    {
        ostringstream str;
        str << "i" << n;
        return getInput(str.str())->getValue();
    }
}
void Indicator::on_propertyChanged()
{
    bool cio = false;
	if (m_pincount != getProperties().get_int("size"))
	{
		m_pincount = getProperties().get_int("size");
		cio = true;
	}
	if (m_multipin != getProperties().get_bool("multipin"))
	{
		m_multipin = getProperties().get_bool("multipin");
		cio = true;
	}
	if (cio) createIO();
}
/** @brief createIO
  *
  * @todo: document this function
  */
void Indicator::createIO()
{
    clearInputs();
    if (m_multipin)
    {
        inputs.push_back(new CircuitInput(this, "in", m_pincount));
    }
    else
    {
        ostringstream str;
        for (int i = 0; i < m_pincount; i++)
        {
            str << "i" << i;
            inputs.push_back(new CircuitInput(this, str.str()));
            str.str("");
        }
    }
}





/******************** Text ***********************/

TextComponent::TextComponent(string n, CircuitContainer* p) : CircuitChild(n, p, "text")
{
    getProperties().set_int("font_size", 16);
    font_size = 16;
    getProperties().set_string("text", "Text");
    m_Text = "Text";
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void TextComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/text"] = ui_text("Text", "Der Text, der angezeigt werden soll.");
    textmap["/font_size"] = ui_text("Schriftgröße", "Die Schriftgröße des Textes.");
}



void TextComponent::on_propertyChanged()
{
	font_size = getProperties().get_int("font_size");
	m_Text = getProperties().get_string("text");
}

string & TextComponent::getText()
{
    return m_Text;
}

int TextComponent::getFontSize()
{
    return font_size;
}


/*********** Function *********/


FunctionComponent::FunctionComponent(string n, CircuitContainer* p, int c, string t) : CircuitChild(n, p, "function"),
    count(c), term(t)
{
    getProperties().set_int("size", c);
    getProperties().set_string("term", t);

	outputs.push_back(new CircuitOutput(this, "y"));
    createInputs();
}

type_iovalue FunctionComponent::calculateOutputValue(const string& out)
{
    map<string,bool> assign;
    type_inputs_iterator iit;
    for (iit = inputs.begin(); iit != inputs.end(); iit++)
    {
        assign[(*iit)->getName()] = ((*iit)->getValue()==1);
    }
    string errstr;
    bool b = LogicBin::eval(term, assign, errstr);
    if (!errstr.empty())
    {
        cerr << errstr << endl;
        return false;
    }
    return b;
}


void FunctionComponent::on_propertyChanged()
{
	if (count != getProperties().get_int("size"))
	{
		count = getProperties().get_int("size");
		clearInputs();
		createInputs();
	}
	if (term != getProperties().get_string("term"))
	{
		term = getProperties().get_string("term");
		update();
	}
}

void FunctionComponent::createInputs()
{
    ostringstream str;
    for (int i = 0; i < count; i++)
    {
        str << "x" << i;
        inputs.push_back(new CircuitInput(this, str.str()));
        str.str("");
    }
}

string FunctionComponent::getInfo()
{
    return "y = " + term;
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void FunctionComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Eingangsbreite", "Anzahl der Eingänge.");
    textmap["/term"] = ui_text("Funktionsterm", "Logischer Funktionsterm (Variablen x0, x1 ...).");
}










/************* Adapter *******************/

bool AdapterComponent::DIR_BUNDLE = true;
bool AdapterComponent::DIR_UNBUNDLE = false;
AdapterComponent::AdapterComponent(string n, CircuitContainer* p, unsigned short pc, bool dir) :
 CircuitChild(n, p, "adapter"),
 m_size(pc),
 m_value(0),
 m_direction(dir)
{
    if (m_size<1) m_size = 1;

    getProperties().set_int("size", m_size);
    SelectionValue2* sv = getProperties().set_selection("direction");
    sv->add_option("bundle");
    sv->add_option("unbundle");
    sv->set_value(m_direction==DIR_BUNDLE ? "bundle" : "unbundle");

    createIO();
}

AdapterComponent::~AdapterComponent()
{
}

type_iovalue AdapterComponent::calculateOutputValue(const string& out)
{
    if (m_direction == DIR_BUNDLE)
    {
        //  = -  viele auf eins
        unsigned short v = 0, pi=0;

        for (type_inputs_iterator iit = inputs.begin(); iit != inputs.end(); iit++)
        {
            v |= ((*iit)->getValueB() << pi);
            pi++;
        }
        return v;
    }
    else
    {
        //  - =  eins auf viele
        std::string s(out);
        s.replace(0,3,"");
        istringstream is(s);
        unsigned short num;
        is >> num;
        return (( inputs.front()->getValue() & (1 << num))!=0);
    }
}

void AdapterComponent::createIO()
{
    clearInputs();
    clearOutputs();

    ostringstream str;

    if (m_direction == DIR_BUNDLE)
    {
        // BUNDLE
        // 1-Pin-Eingänge    mehr-Pin-Ausgang
        for (unsigned short i=0; i<m_size; i++)
        {
            str << "in" << i;
            inputs.push_back(new CircuitInput(this, str.str(), 1));
            str.str("");
        }
        outputs.push_back(new CircuitOutput(this, "out", m_size));
    }
    else
    {
        // UNBUNDLE
        // mehr-Pin-Eingang    1-Pin-Ausgänge
        for (unsigned short i=0; i<m_size; i++)
        {
            str << "out" << i;
            outputs.push_back(new CircuitOutput(this, str.str(), 1));
            str.str("");
        }
        inputs.push_back(new CircuitInput(this, "in", m_size));
    }
}

void AdapterComponent::on_propertyChanged()
{
    SelectionValue2* sv = getProperties().get_selection("direction");
    if (sv)
    {
        bool dir = (sv->get_value() == "bundle") ? DIR_BUNDLE : DIR_UNBUNDLE;
        if (dir != m_direction) {
            m_direction = dir;
            createIO();
        }
    }
    unsigned short pc = (unsigned short)getProperties().get_int("size");
    if ((pc<1) || pc>32) pc=1;
    if (pc != m_size)
    {
        m_size = pc;
        createIO();
    }
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void AdapterComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Leitungen", "Anzahl der Leitungen.");
    textmap["/direction"] = ui_text("Richtung", "Bündeln oder entbündeln.");
}



/**************** LED ********************/


string LEDComp::getInfo()
{
    // TODO return getProperties().get_string("color");
    return "";
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void LEDComp::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/color"] = ui_text("Farbe", "Farbe der LED: rot, gelb, grün, blau, violett, orange (oder englisch).");
}


/**************** Node ********************/


NodeComponent::NodeComponent(string n, CircuitContainer* p, unsigned short pc) : CircuitChild(n, p, "node")
{
    pin  = new CircuitInput(this, "i", pc);
    pout = new CircuitOutput(this, "o", pc);

    inputs.push_back(pin);
    outputs.push_back(pout);

    pin->addForwarding(pout);

    getProperties().set_int("size", pc);
}

type_iovalue NodeComponent::calculateOutputValue(const string& out)
{
    return (*getInput("i")).getValue();
}

void NodeComponent::on_propertyChanged()
{
    unsigned short pc = (unsigned short)getProperties().get_int("size");
    setPinCount(pc);
}

void NodeComponent::setPinCount(unsigned short pc)
{
    getProperties().set_int("size", pc);
    pin->setPinCount(pc);
    pout->setPinCount(pc);
    pin->addForwarding(pout);
}
unsigned short NodeComponent::getPinCount()
{
    return pin->getPinCount();
}

void NodeComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Leitungen", "Anzahl der Leitungen.");
}








