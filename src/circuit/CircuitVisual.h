/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SCHALTUNG_CIRCUIT_VISUAL_H
#define SCHALTUNG_CIRCUIT_VISUAL_H

#include <gdkmm/color.h>
#include <cairomm/context.h>
#include <vector>
#include <map>
#include <list>
#include <string>
#include <sigc++/sigc++.h>

#include "CircuitIO.h"
#include "ToplevelCircuit.h"
#include "Components.h"

#include "ClockComponent.h"
#include "DisplayComponents.h"


using namespace std;

//#define CIR_BACK_COL 0.9, 0.9, 0.9
#define CIR_BACK_COL 1.0, 1.0, 1.0
#define MAINCIR_BACK_COL 0.8, 0.8, 0.8

#define CIR_FRAME_SEL_COL 0.0, 0.0, 0.8
#define CIR_FRAME_COL 0.0, 0.0, 0.0
#define CIR_TITLE_BACK_COL 0.8, 0.8, 0.8
#define CIR_TITLE_BACK_VIS_COL 1.0, 0.8, 0.8
#define CIR_IO_BACK 0.9, 0.9, 0.8
#define CIR_TITLE_COL CIR_FRAME_COL
#define CIR_TITLE_SEL_COL CIR_FRAME_SEL_COL

//#define WIRE_SEL_COL 0.9, 0.7, 0.0
#define WIRE_SEL_COL CIR_FRAME_SEL_COL
#define WIRE_COL 0.1, 0.1, 0.1
#define WIRE_WIDTH 1
#define WIRE_HOT_WIDTH 2
#define WIRE_SEL_WIDTH 1
#define HOT_COL 0.8, 0.0, 0.0

#define IO_FONT_SIZE 10
#define GATE_FONT_SIZE 20
#define TEXT_FONT_SIZE 12
#define TITLE_FONT_SIZE 12


#define STRICH 5


/*************************/



class CircuitChild;
class CircuitWidget;
class CircuitChildVisual;
class CircuitContainerVisual;

typedef std::map<CircuitChild*,CircuitChildVisual*> type_visual_map;
typedef std::list<CircuitChildVisual*> type_visual_list;



typedef int COMPONENT_AREA;

#define CA_NONE 0
#define CA_INNER 1
#define CA_TITLE 2
#define CA_IN 4
#define CA_OUT 8
















class CircuitChildVisual
{
    protected:
        CircuitContainerVisual* m_pParentVisual;
        CircuitChild* m_pObj;

    public:
        CircuitChildVisual(CircuitChild* o, CircuitContainerVisual* p = NULL) :
         m_pParentVisual(p),
         m_pObj(o),
         m_titleHeight(15),
         m_ioHeight(15),
         m_inWidth(20),
         m_outWidth(20),
         m_ioSpace(20),
         m_vSpace(10),
         selected(false)
        {
            setSize(m_outWidth + m_inWidth,35);
        }


        virtual ~CircuitChildVisual();



        int m_titleHeight;
        int m_ioHeight;
        int m_inWidth;
        int m_outWidth;
        int m_ioSpace;
        int m_vSpace;
        bool selected;
        type_visual_map m_childVisuals;



        void setSize(int w, int h){bool ch = false;if ((ps_w != w) || (ps_h != h)) ch = true;ps_w = w;ps_h = h; if (ch) m_signal_size_changed.emit(ps_w, ps_h);}
        void setSizeW(int w) {setSize(w, ps_h);}
        void setSizeH(int h) {setSize(ps_w, h);}
        void setAbsPos(int x, int y) {abs_x = x;abs_y = y;}
        int getAbsPosX() {return abs_x;}
        int getAbsPosY() {return abs_y;}
        int getSizeW() {return ps_w;}
        int getSizeH() {return ps_h;}

        virtual void on_inner_click(int x, int y, CircuitWidget* widg) {}; // leer
        CircuitChild* getParentObj();




        virtual CircuitChild* getObj()
        {
            return m_pObj;
        }



        void setPos(int x, int y);
        int getPosX();
        int getPosY();

        int getFromInputPosX(const string& in) {return getPosX();}
        int getFromInputPosY(const string& in)
        {
            int offs=0;
            type_inputs::reverse_iterator iit;
            for ( iit = getObj()->getInputs().rbegin(); iit != getObj()->getInputs().rend(); iit++ ) {
                if ((*iit)->getName() == in) break;
                offs+=m_ioHeight;
            }
            return getPosY()+5+offs;
        }


        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        virtual void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);


        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual bool paintAll(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual bool paintIOText(Cairo::RefPtr<Cairo::Context> cr, int x, int y);


        virtual CircuitInput* getInputFromPoint(int x, int y);
        virtual CircuitOutput* getOutputFromPoint(int x, int y);
        //CircuitIO* getIOFromPoint(int x, int y);

        void componentToArea(COMPONENT_AREA area, int x, int y, int& relx, int& rely);
        void parentToComponent(int x, int y, int& relx, int& rely);

        COMPONENT_AREA areaOnComponent(int x, int y);

        virtual void updateIOPos();

    public:
        //signal accessor:
        typedef sigc::signal<void,int,int> type_signal_size_changed;
        type_signal_size_changed signal_size_changed() {return m_signal_size_changed;}
    protected:
        type_signal_size_changed m_signal_size_changed;

        int ps_w, ps_h, abs_x, abs_y;
        int min_x, min_y; // für marked visible kompaktifizierung

/*        template <class C> void setObj(C* o);
        template <class C> C* getObj();
*/
}; // class CircuitChildVisual

/*
template <class C>
C* CircuitChildVisual::getObj()
{
    return dynamic_cast<C*>(m_pObj);
}

template <class C>
void CircuitChildVisual::setObj(C* o)
{
    m_pObj = o;
}
*/









class CircuitContainerVisual : public CircuitChildVisual
{
    protected:
        CircuitContainer* m_pObj;
    public:

        CircuitContainerVisual(CircuitContainer* o, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         m_pObj(o)
        {
        }

        virtual CircuitContainer* getObj()
        {
            return m_pObj;
        }

        virtual void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        virtual bool paintAll(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);

        CircuitChildVisual* getVisualFromPoint(int x, int y, int xx, int yy, COMPONENT_AREA& area, type_visual_list* l = NULL);
        CircuitChildVisual* adjoinVisual(CircuitChild *o, bool& neu);

        bool isEmpty()
        {
            return !(getObj()->hasChildren() || (getObj()->getInputs().size()>0) || (getObj()->getOutputs().size()>0));
        }

};


class ToplevelCircuitVisual : public CircuitContainerVisual
{
    protected:
        EditableCircuit* m_pObj;
    public:

        ToplevelCircuitVisual(EditableCircuit* o) :
         CircuitContainerVisual(o, NULL),
         m_pObj(o)
        {
        }

        virtual EditableCircuit* getObj()
        {
            return m_pObj;
        }

        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
};


/****************************/


class GateVisual : public CircuitChildVisual
{
    protected:
        string getGateSymbol();
        GateComponent *m_pObj;
    public:
        GateVisual(GateComponent *o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         m_pObj(o) {};

        virtual GateComponent* getObj()
        {
            return m_pObj;
        }

        virtual void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
}; // GateVisual




class DigitalZifferVisual : public CircuitChildVisual {
    protected:
        int ziffer_size;
        bool sizeSet;
        DigitalZiffer* m_pObj;
    public:
        DigitalZifferVisual(DigitalZiffer* o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         ziffer_size(30),
         sizeSet(false),
         m_pObj(o)
        {m_ioSpace = 0;}

        virtual DigitalZiffer* getObj() {return m_pObj;}

        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
}; // DigitalZifferVisual


class LetterVisual : public CircuitChildVisual {
    protected:
        int letter_size;
        bool sizeSet;
        LetterComponent* m_pObj;
    public:
        LetterVisual(LetterComponent* o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         letter_size(30),
         sizeSet(false),
         m_pObj(o)
        {m_ioSpace = 0;}

        virtual LetterComponent* getObj() {return m_pObj;}

        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
}; // LetterVisual


class DigitalZiffer2Visual : public CircuitChildVisual {
    protected:
        int ziffer_size;
        bool sizeSet;
        DigitalZiffer2* m_pObj;
    public:
        DigitalZiffer2Visual(DigitalZiffer2* o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         ziffer_size(30),
         sizeSet(false),
         m_pObj(o)
        {m_ioSpace = 0;}

        virtual DigitalZiffer2* getObj() {return m_pObj;}

        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
}; // DigitalZiffer2Visual




class NodeVisual : public CircuitChildVisual {
    protected:
        NodeComponent *m_pObj;
    public:
        NodeVisual(NodeComponent *o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         m_pObj(o)
        {
            m_titleHeight = 5;
            m_ioHeight = 4;
            m_inWidth = 4;
            m_outWidth = 4;
            m_ioSpace = 0;
        }

        virtual NodeComponent* getObj() {return m_pObj;}

        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        virtual void updateSize(Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
}; // NodeVisual


// Visuals for IO-Components

class IOVisual : public CircuitChildVisual {
    protected:
    public:
        bool vertical();
        void vertical(bool b);
        IOVisual(CircuitChild *o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p)
        {};

        virtual void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);

        virtual void on_inner_click(int x, int y, CircuitWidget* widg) = 0;
}; // IOVisual

class SwitchVisual : public IOVisual {
    protected:
        Switch* m_pObj;
    public:
        SwitchVisual(Switch *o = NULL, CircuitContainerVisual* p = NULL) :
         IOVisual(o, p),
         m_pObj(o)
        {}

        virtual Switch* getObj() {return m_pObj;}

        virtual void on_inner_click(int x, int y, CircuitWidget* widg);
}; // SwitchVisual

class IndicatorVisual : public IOVisual
{
    protected:
        Indicator* m_pObj;
    public:
        IndicatorVisual(Indicator *o = NULL, CircuitContainerVisual* p = NULL) :
         IOVisual(o, p),
         m_pObj(o)
        {}

        virtual Indicator* getObj() {return m_pObj;}

        virtual void on_inner_click(int x, int y, CircuitWidget* widg);
}; // NodeVisual




class LEDVisual : public CircuitChildVisual
{
    protected:
        enum Color {
            Red,
            Yellow,
            Blue,
            Purple,
            Orange,
            Green,
            Bin,
            None
        };

        Gdk::Color _color;

        Cairo::RefPtr<Cairo::ImageSurface> onImage, offImage;
        Color color;
        LEDComp* m_pObj;
        SelectionValue2* m_pSelection;
    public:
        LEDVisual(LEDComp *o = NULL, CircuitContainerVisual* p = NULL, Color c = Blue);

        virtual LEDComp* getObj() {return m_pObj;}

        void setupColor();

        void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
        void updateIOPos();
        CircuitInput* getInputFromPoint(int x, int y);
}; // LEDVisual



class TextVisual : public CircuitChildVisual
{
    protected:
        TextComponent* m_pObj;
    public:
        TextVisual(TextComponent *o = NULL, CircuitContainerVisual* p = NULL);

        virtual TextComponent* getObj() {return m_pObj;}

        void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);
}; // TextVisual



#endif // SCHALTUNG_CIRCUIT_VISUAL_H
