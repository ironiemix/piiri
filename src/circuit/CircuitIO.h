/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef CCIRCUITIO_H
#define CCIRCUITIO_H


#include <string>
#include <iostream>
#include <list>
#include <vector>

#define IOPINMASK ((1 << pinCount)-1)

class CircuitChild;
class CircuitIO;

typedef std::list<CircuitIO*> type_forwardings;
typedef unsigned short type_iovalue;

enum IO_TYPE
{
	IO_TYPE_NONE = 0,
	IO_TYPE_IN = 1,
	IO_TYPE_OUT = 2
};


class CircuitIO
{
    protected:
        CircuitChild *m_pOwner;

        type_iovalue value;
        unsigned short pinCount;

        std::string name;
        bool isset;
        type_forwardings forwardings;
        int ps_fromX, ps_fromY, ps_toX, ps_toY;

        CircuitIO* from;
        IO_TYPE type;
        bool changed; // TODO: used??
        bool selected;
        bool visited;

    public:
        CircuitIO* getFrom() {return from;}
        IO_TYPE getType() {return type;};
        bool isSelected() {return selected;}
        void setSelected(bool s) {selected = s;}
        bool hasChanged() {return changed;}
        unsigned short getPinCount() {return pinCount;};
        void setPinCount(unsigned short pc) {disconnect(); pinCount = pc;};
        //bool isCircular();
        //void resetCircular();


        CircuitIO(CircuitChild* p, const std::string& n, unsigned short pc = 1) :
            m_pOwner(p),
            value(0),
            pinCount(pc>0 ? pc : 1),
            name(n),
            isset(false),
            from(NULL),
            type(IO_TYPE_NONE),
            changed(true),
            selected(false),
            visited(false)
        {
        }
        ~CircuitIO() {}


        virtual void list_setValueB(bool v)
        {
            list_setValue(v?1:0);
        }
        virtual void setValueB(bool v)
        {
            setValue(v?1:0);
        }

        virtual void list_setValue(type_iovalue v) = 0;
        virtual void setValue(type_iovalue v) = 0;

        bool getValueB() {return (value&1)!=0;}
        type_iovalue getValue() {return value;}
        bool isSet() {return (isset);}
        void setUnset() {isset=false;}
        void setSet() {isset=true;}
        const std::string &getName() {return name;}
        const std::string getNameEx();
        void setName(std::string n) {name =n;}
        CircuitChild* getOwner() {return m_pOwner;}


        void dump() {
            std::cout << getName() << " = " << getValue() << (isSet()? "" : "*");
        }

        void addForwarding(CircuitIO* fwd) {
            if ( (fwd->from == NULL) && (getPinCount()==fwd->getPinCount())) {
                forwardings.push_back(fwd);
                fwd->from = this;
            }
        }
        void removeForwarding(CircuitIO* fwd) {
            forwardings.remove(fwd);
            fwd->from = NULL;
        }

        type_forwardings& getForwardings() {
            return forwardings;
        }

        void disconnect(bool to=true, bool bfrom=true, bool off=true);


        //void forward(); // #
        void list_forward(); // #

        void ps_setFromPos(int x, int y) {ps_fromX=x;ps_fromY=y;}
        void ps_setToPos(int x, int y) {ps_toX=x;ps_toY=y;}
        int ps_getFromPosX() {return ps_fromX;}
        int ps_getFromPosY() {return ps_fromY;}
        int ps_getToPosX() {return ps_toX;}
        int ps_getToPosY() {return ps_toY;}
};



class CircuitInput : public CircuitIO
{
    public:
        CircuitInput(CircuitChild* p, const std::string& n, unsigned short pc = 1) : CircuitIO(p, n, pc) {type=IO_TYPE_IN;};
        void list_setValue(type_iovalue v); //#
        virtual void setValue(type_iovalue v);
};



class CircuitOutput : public CircuitIO
{
    protected:
        std::vector<CircuitInput*> connectedInputs;
    public:
        CircuitOutput(CircuitChild* p, const std::string& n, unsigned short pc = 1) : CircuitIO(p, n, pc) {type=IO_TYPE_OUT;};
        void list_setValue(type_iovalue v); //#
        bool connectTo(CircuitInput* pSourceInput);
        void setOwner(CircuitChild* p) {m_pOwner = p;}
        CircuitChild* getOwner() {return m_pOwner;}
        virtual void setValue(type_iovalue v);
};



#endif // CCIRCUITIO_H
