#include "circuit/ToplevelCircuit.h"
#include "model/App.h"

/*

aus "Circuit.cpp


#include <iostream>
#include <fstream>

aus circuit.h

#include <glib-object.h>
#include <sigc++/sigc++.h>

*/


/** @brief ToplevelCircuit
  *
  * @todo: document this function
  */
ToplevelCircuit::ToplevelCircuit() :
 EditableCircuit("- unbenannt -", NULL)
{
    getProperties().clear(); // vorige Props löschen. TODO: nicht so fein!

    getProperties().set_string("description", "");


	Complex2* c = getProperties().set_complex("plan");
	c->set_bool("show", false);
	c->set_bool("over", false);
	c->set_double("alpha", 0.2);
	c->set_double("scale", 1.0);
	c->set_string("file", "");
	c->set_int("x", 0);
	c->set_int("y", 0);

	m_pExtraProperties = getProperties().set_complex("extra");
	m_pExtraProperties->setAllowEdit(false);
	m_pExtraProperties->set_int("clock_intervall", 1000);
	m_pExtraProperties->set_string("clock_input", "");
	m_pExtraProperties->set_bool("clock_active", false);
	m_pExtraProperties->set_double("zoom", 1.0);
}

/** @brief ~ToplevelCircuit
  *
  * @todo: document this function
  */
ToplevelCircuit::~ToplevelCircuit()
{
    //std::cout << "~ToplevelCircuit()" << std::endl;

}

/** @brief on_propertyChanged
  *
  * @todo: document this function
  */
void ToplevelCircuit::on_propertyChanged()
{
}



/** @brief provideUiText
  *
  * @todo: document this function
  */
void ToplevelCircuit::provideUiText(ui_text_map& textmap)
{
    textmap["/description"] = ui_text(App::t("Beschreibung"), App::t("Eine Beschreibung dieser Schaltung."));
    textmap["/plan"] = ui_text(App::t("Hintergrund"), App::t("Ein Hintergrundbild einstellen."));
    textmap["/plan/show"] = ui_text(App::t("Anzeigen"), App::t("Soll das Hintergrundbild angezeigt werden?"));
    textmap["/plan/over"] = ui_text(App::t("Drüberliegend"), App::t("Soll das Hintergrundbild über der Schaltung angezeigt werden?"));
    textmap["/plan/alpha"] = ui_text(App::t("Tranzparenz"), App::t("Tranzparenz des Hintergrundbildes:")+" 0.0 .. 1.0.");
    textmap["/plan/scale"] = ui_text(App::t("Skalierung"), App::t("Skalierungsfaktor des Bildes: Zum Vergrößern oder Verkleinern."));
    textmap["/plan/file"] = ui_text(App::t("Dateiname"), App::t("Der Pfad zur Bild-Datei (relativ zu ") + App::plan_path + ").");
    textmap["/plan/x"] = ui_text(App::t("Position (x)"), App::t("Position der linken oberen Ecke des Bildes."));
    textmap["/plan/y"] = ui_text(App::t("Position (y)"), App::t("Position der linken oberen Ecke des Bildes."));
}




/** @brief loadFromDomDocument
  *
  * @todo: document this function
  */
bool ToplevelCircuit::loadFromDomDocument(xmlpp::Document *doc)
{
    xmlpp::Element *root = doc->get_root_node();
    if (!root) return false;
    xmlpp::Element *circuit  = (xmlpp::Element *)(*root->get_children("circuit").begin());
    if (!circuit) return false;
	xmlpp::Element *settings = (xmlpp::Element *)(*root->get_children("settings").begin());
    if (!settings) return false;

    // Version der Einstellungen ermitteln
    // ab V2 werden die props anders gespeichert.
    std::string _version = root->get_attribute_value("version");
    int version;
    if (_version.empty()) _version = "1";
    istringstream is(_version);
    is >> version;
    bool _ln = Complex2::loadNew;
    if (version > 1) Complex2::loadNew = true;
    else Complex2::loadNew = false;

    //setFilename(fn);


    loadXml(circuit);


    try
    {

        getProperties().read_xml(settings);

    }
    catch (string& s)
    {
        cerr << "Beim Laden der Einstellungen für " << getName() << ": " << s << endl;
    }



    Complex2::loadNew = _ln;

    return true;
}





