/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

// Components.h

#ifndef SCHALTUNG_CIRCUIT_FUNCTIONS_H
#define SCHALTUNG_CIRCUIT_FUNCTIONS_H


#include "CircuitChild.h"
#include "Helpers.h"
//#include "iow/IOWarrior.h"


#include <glibmm.h>


//  http://www.cplusplus.com/reference/stl/list/
// clear;clear; make all




/**
 * http://grooveshark.com/s/Bottom+Of+The+Ocean/2ATVuM?src=5
 *
 */



enum GATE_TYPE
{
    AND,OR,NAND,NOR,XOR,NOT
};



// Logische Gatter ++++++++++++++++++++++++++

class GateComponent : public CircuitChild {
    protected:
        GATE_TYPE type;
    public:
        GateComponent(string n, CircuitContainer* p, GATE_TYPE t, unsigned int inCount = 2);
        virtual ~GateComponent() {}
        type_iovalue calculateOutputValue(const string& out);
        GATE_TYPE getType() {return type;}
        virtual void provideUiText(ui_text_map& textmap);
        virtual void on_propertyChanged();
        void createIO(unsigned int ic);
};


class FunctionComponent : public CircuitChild {
    protected:
        int count;
        string term;

        void on_propertyChanged();
        void createInputs();
    public:
        FunctionComponent(string n, CircuitContainer* p, int c, string t = "");
        virtual ~FunctionComponent() {}
        type_iovalue calculateOutputValue(const string& out);
        virtual string getInfo();
        virtual void provideUiText(ui_text_map& textmap);
};





/*)---
class DisplayComponent : public CircuitChild {
    protected:
        unsigned int addr_bits, item_bits, item_count;
        unsigned int *items;
        string content;
        bool set;
        unsigned int value;

        void on_propertyChanged();
        void createIO();
        void contentFromString(string& content);
    public:
        DisplayComponent(string n, CircuitContainer* p, unsigned int ab, unsigned int ib);
        virtual ~DisplayComponent();
        bool calculateOutputValue(const string& out);
        virtual void on_input_changed(const string& name);
};
*/
// Logische Gatter ++++++++++++++++++++++++++ ENDE














class NodeComponent : public CircuitChild {
protected:
public:
	CircuitInput *pin;
	CircuitOutput *pout;

	NodeComponent(string n, CircuitContainer* p, unsigned short pc);
	virtual ~NodeComponent() {}

    void setPinCount(unsigned short pc);
    unsigned short getPinCount();

	type_iovalue calculateOutputValue(const string& out);
    virtual void on_propertyChanged();
    virtual void provideUiText(ui_text_map& textmap);
};






class AdapterComponent : public CircuitChild {
    protected:
        unsigned short m_size;
        unsigned short m_value; // (?)
        bool m_direction;

        void on_propertyChanged();
        void createIO();
    public:
        static bool DIR_BUNDLE;
        static bool DIR_UNBUNDLE;

        AdapterComponent(string n, CircuitContainer* p, unsigned short pc, bool dir);
        virtual ~AdapterComponent();
        type_iovalue calculateOutputValue(const string& out);
        //virtual void on_input_changed(const string& name);
        virtual void provideUiText(ui_text_map& textmap);
};




class One : public CircuitChild {
protected:
public:
	One(string n, CircuitContainer* p) : CircuitChild(n, p, "one") {
		outputs.push_back(new CircuitOutput(this, "o"));
	}
	virtual ~One() {}
	type_iovalue calculateOutputValue(const string& out) {return 1;}
};

class Zero : public CircuitChild {
protected:
public:
	Zero(string n, CircuitContainer* p) : CircuitChild(n, p, "zero") {
		outputs.push_back(new CircuitOutput(this, "o"));
	}
	virtual ~Zero() {}
	type_iovalue calculateOutputValue(const string& out) {return 0;}
};















// IO-Components

class IOComp : public CircuitChild {
public:
	int m_pincount;
    bool m_multipin;
	IOComp(const string& n, const string& cir, CircuitContainer* p, int c)
	: CircuitChild(n, p, cir), m_pincount(c), m_multipin(false)
	{
		getProperties().set_int("size", c);
        getProperties().set_bool("vertical", false);
        getProperties().set_bool("multipin", false);
	}
	virtual type_iovalue getValue(int n) = 0;
    virtual void provideUiText(ui_text_map& textmap);
    virtual void createIO() = 0;
};

class Switch : public IOComp {
public:
	Switch(string n, CircuitContainer* p, int c);

	void toogleValue(int n);

	virtual type_iovalue getValue(int n);
	type_iovalue calculateOutputValue(const string& out) {
	    CircuitOutput* op = getOutput(out);
	    if (op) return op->getValue();
	    return 0;
    }
	virtual void on_propertyChanged();
    virtual void createIO();
};


class Indicator : public IOComp {
protected:
public:

	Indicator(string n, CircuitContainer* p, int c);

	virtual type_iovalue getValue(int n);
	type_iovalue calculateOutputValue(const string& out) {return 0;}
	virtual void on_propertyChanged();
    virtual void createIO();
};

class LEDComp : public CircuitChild {
public:
	LEDComp(const string& n, CircuitContainer* p) : CircuitChild(n, p, "led")
	{
		inputs.push_back(new CircuitInput(this, "i"));

		SelectionValue2* sv = getProperties().set_selection("color");
		sv->add_option("blau");
		sv->add_option("rot");
		sv->add_option("grün");
		sv->add_option("gelb");
		sv->add_option("violett");
		sv->add_option("orange");
		sv->set_value("blau");


	}
	void on_propertyChanged()
	{
        //string s = getProps()->set_string("color");
	}
    virtual string getInfo();
    virtual void provideUiText(ui_text_map& textmap);
};


class TextComponent : public CircuitChild {
    protected:
        string m_Text;
        int font_size;
    public:
        TextComponent(string n, CircuitContainer* p);
        virtual ~TextComponent() {}
        string& getText();
        int getFontSize();
        virtual void on_propertyChanged();
        virtual void provideUiText(ui_text_map& textmap);
};



#endif // SCHALTUNG_CIRCUIT_FUNCTIONS_H
