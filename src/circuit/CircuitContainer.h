#ifndef CIRCUITCONTAINER_H
#define CIRCUITCONTAINER_H

#include <libxml++/libxml++.h>
#include "CircuitChild.h"


typedef std::list<CircuitChild*> type_elements;
typedef type_elements::iterator type_elements_iterator;


typedef struct _IOThing
{
	CircuitChild* p_circuit;
	CircuitIO* p_io;
} IOThing;


/**
hat kinder: integrierte schaltung
hat dateinamen
ist basisklasse von toplevel, woe aber properties geschichten neu implementiert werden!
*/
class CircuitContainer : public CircuitChild
{
    public:
        CircuitContainer(string n, CircuitContainer *p, bool sim = true);
        virtual ~CircuitContainer();

    // Children
        type_elements& getChildren(){return m_Children;};
        CircuitChild *findChildByName(const std::string& name);
        bool parseIOThingFromString(const std::string& s, IOThing* thing, IO_TYPE type);
        virtual void clear(); // reimlementierung wegen chidren
        virtual bool hasChildren() { return (m_Children.size()>0); };

    // Schaltung Aufbauen
        CircuitInput* addIn(const std::string& name, unsigned short pc = 1, const std::string& prae = std::string(), type_iovalue value = 0);
        CircuitOutput* addOut(const std::string& name, unsigned short pc = 1, const std::string& prae = std::string());
        CircuitChild* addComponent(const std::string& cir, const std::string& name, int x=0, int y=0);
        CircuitChild* addComponent(CircuitChild *neues_element);
        static CircuitChild* createComponent(CircuitContainer* p, const std::string& cir, const std::string& name);
        bool connect(const std::string& from, const std::string& to);

    // Dateioperationen
        const std::string& getFilename() {return filename;}
        void setFilename(const std::string &fn) {filename = fn;}
        bool loadFile(const std::string& fn);
        bool saveFile(const std::string& fn);

        void saveXml(xmlpp::Element* parent);
        void loadXml(xmlpp::Element* parent);


    protected:
        type_elements m_Children;
        std::string filename;

        virtual void reset(); // reimlementierung wegen chidren
        virtual void on_child_notification(int, const std::string&, gpointer);

        virtual void processChildren();

    // Dateioperationen
        bool loadCircuit(const std::string& schaltung);
        virtual bool loadFromDomDocument(xmlpp::Document *doc);

};


#endif // CIRCUITCONTAINER_H
