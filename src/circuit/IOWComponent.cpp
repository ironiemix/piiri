#include "IOWComponent.h"
#include "iow/IOWFactory.h"
#include "CircuitContainer.h"


/** @brief IOWComponent
  *
  * @todo: document this function
  */
IOWComponent::IOWComponent(string n, CircuitContainer* p):
    CircuitChild(n, p, "iow"),
    writeToDevice(false),
    m_pIOW(NULL),
    io_mode(-1)
{

    sv_mode = getProperties().set_selection("mode");
    sv_mode->setSaveIndex(true);
    // Reihenfolge nicht verändern!!
    sv_mode->add_option(" - ");
    sv_mode->add_option("Lesen & Schreiben");
    sv_mode->add_option("Nur Lesen");
    sv_mode->add_option("Nur Schreiben");
    sv_mode->set_value_index(IOM_READ);




    m_pIOW = IOWFactory::getDevice(0, getParent());

    if (!m_pIOW->canRead() && !m_pIOW->canWrite())
    {
        std::cerr << "IOW-Hardware nicht verfügbar." << std::endl;
    }

    if (m_pIOW)
    {
        con = m_pIOW->sig_input.connect(sigc::mem_fun(*this, &IOWComponent::on_input));

        m_pIOW->startThread(); // thread ist sonst schon gestartet
    }
    on_propertyChanged();
}

/** @brief on_propertyChanged
  *
  * @todo: document this function
  */
void IOWComponent::on_propertyChanged()
{
    unsigned int idx = sv_mode->get_value_index();

    if (idx != io_mode)
    {
        io_mode = idx;


        // ermitteln ob der modus möglich ist und ob wir schreiben dürfen
        if (((io_mode == IOM_WRITE) || (io_mode == IOM_RW)))
        {
            if (m_pIOW)
            {
                if (m_pIOW->the_only_writer == NULL)
                {
                    m_pIOW->the_only_writer = this;
                    writeToDevice = true;
                }

                if (m_pIOW->the_only_writer != this)
                {
                    writeToDevice = false;
                }
            }

            if (!writeToDevice) // write war aber angefordert: meldung!
            {
                if (!m_pIOW)
                {
                    //io_mode = IOM_NONE;
                    std::cerr << "Es steht keine Hardware für die IOW-Komponente zur Verfügung!" << std::endl;
                }
                else
                {
                    io_mode = IOM_READ;
                    std::cerr << "Mehr als eine schreibende IOW-Komponente ist nicht erlaubt: Es wird auf 'Nur Lesen' gestellt." << std::endl;
                    sv_mode->set_value_index(io_mode);
                }
            }
        }
        else
        {
            writeToDevice = false; // sowieso

            if (m_pIOW)
            {
                if (m_pIOW->the_only_writer == this)
                {
                    // wenn wir the one and only waren: frei geben!
                    m_pIOW->the_only_writer = NULL;
                }
            }
            else if (io_mode != IOM_NONE)
            {
                //io_mode = IOM_NONE;
                std::cerr << "Es steht keine Hardware für die IOW-Komponente zur Verfügung!" << std::endl;
            }
        }




        // erlaubter modus sollte hier sichergestellt sein!

        // outputs
        if ((io_mode == IOM_READ) || (io_mode == IOM_RW))
        {
            if (outputs.size() == 0)
            {
                // create outputs
                ostringstream str;
                for (unsigned int i = 0; i < 8; i++)
                {
                    str << "out" << i;
                    outputs.push_back(new CircuitOutput(this, str.str()));
                    str.str("");
                }
            }
        }
        else
        {
            clearOutputs();
        }


        // inputs
        if ((io_mode == IOM_WRITE) || (io_mode == IOM_RW))
        {
            if (inputs.size() == 0)
            {
                // create outputs
                ostringstream str;
                for (unsigned int i = 0; i < 8; i++)
                {
                    str << "in" << i;
                    inputs.push_back(new CircuitInput(this, str.str()));
                    str.str("");
                }
            }
        }
        else
        {
            clearInputs();
        }
    }
}




IOWComponent::~IOWComponent()
{
    if (m_pIOW)
    {
        con.disconnect();
        if (m_pIOW->the_only_writer == this) m_pIOW->the_only_writer = NULL;
        IOWFactory::unregisterDevice(0);
    }
}




/** @brief calculateOutputValue
  *
  * @todo: document this function
  */
type_iovalue IOWComponent::calculateOutputValue(const string& out)
{
    if (!m_pIOW) return 0;

    unsigned long val = ~m_pIOW->getInVal();
    //std::cout << std::hex << val << std::endl;

    ostringstream str;
    unsigned int n = 1;
    for (int i=0; i<8; i++)
    {
        str << "out" << i;

        if (str.str() == out)
        {
            return ((val & n) != 0);
        }
        n = n << 1;
        str.str("");
    }
    return 0;
}

/** @brief on_input_changed
  *
  * @todo: document this function
  */
void IOWComponent::on_input_changed(const std::string& name)
{
    if (!m_pIOW) return;

    if (writeToDevice)
    {
        unsigned long val = getInputValue();
        m_pIOW->setOutVal(val);
    }
}





/** @brief on_input
  *
  * @todo: document this function
  */
void IOWComponent::on_input()
{
    update();
    if (getParent()) getParent()->update();
    string s;
    m_signal_circuit_notification.emit(CN_UPDATE, s, NULL);

    //unsigned long val = m_pIOW->getInVal();
    //std::cout << std::hex << val << std::endl;

}


void IOWComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/mode"] = ui_text("Modus", "");
}




