#include "RAMComponent.h"

RAMComponent::RAMComponent(string n, CircuitContainer* p, unsigned int ab, unsigned int ib) :
 CircuitChild(n, p, "ram"),
 m_addr_bits(ab),
 m_item_bits(ib),
 m_item_count(0),
 m_pItems(NULL),
 m_set(false),
 m_multipin(false),
 m_value(0)
{
    m_item_count = 1 << m_addr_bits;

    getProperties().set_int("addr-bits", m_addr_bits);
    getProperties().set_int("item-bits", m_item_bits);
    getProperties().set_bool("writeback", false);
    getProperties().set_string("content", m_content);
    getProperties().set_bool("multipin", false);

    createItems();
    createIO();

    writeContentToProps();
}

RAMComponent::~RAMComponent()
{
    if (m_pItems) delete[] m_pItems;
}

void RAMComponent::createItems()
{
    if (m_pItems) delete[] m_pItems;
    m_pItems = new unsigned int[m_item_count];
	for (unsigned int _i=0u;_i<m_item_count;_i++)
	{
        //cout << m_pItems[_i] << "\t";
	    m_pItems[_i] = 0u;
        //cout << m_pItems[_i] << endl;
	}
}

type_iovalue RAMComponent::calculateOutputValue(const string& out)
{
    if (!m_pItems) return false;

    if (!m_multipin)
    {
        string s(out);
        s.replace(0,3,"");
        istringstream is(s);
        unsigned int num;
        is >> num;
        return ((m_value & (1 << num))!=0) ;
    }
    else
    {
        return m_value;
    }
}

void RAMComponent::on_input_changed(const string& name)
{

//    cout << "on_input_changed" << endl;
    if ((m_addr_bits == 0) || !m_pItems)
    {
        //cout << "noo" << endl;
        return;
    }

    bool s = getInputValue("S");

    ostringstream os;

    unsigned int addr = 0;
    if (!m_multipin)
    {
        for (unsigned int i = 0; i < m_addr_bits; i++)
        {
            os << "A" << (m_addr_bits-i-1);
            addr <<= 1;
            addr |= (     getInputValue(os.str())     ? 1 : 0);
            os.str("");
        }
    }
    else
    {
        addr = getInputValue("Adr");
    }

    if (addr < m_item_count)
    {
        m_value = m_pItems[addr];
    }



    if (m_set && !s) // fallende Flanke?
    {

        //cout << "RAM set ";
        unsigned int in = 0;

        //cout << "at [" << addr << "] ";

        if (!m_multipin)
        {
            for (unsigned int i = 0; i < m_item_bits; i++)
            {
                os << "in" << (m_item_bits-i-1);
                in <<= 1;
                in |= (     getInputValue(os.str())     ? 1 : 0);
                os.str("");
            }
        }
        else
        {
            in = getInputValue("in");
        }

        m_value = in;
        if (addr < m_item_count)
        {
            m_pItems[addr] = m_value;
            if (getProperties().get_bool("writeback")) writeContentToProps();
        }
    }
    m_set = s;
    //cout << "RAM in changed " << name << endl;
}


void RAMComponent::createIO()
{
    if (!m_multipin)
    {
        ostringstream str;
        for (unsigned int i = 0; i < m_addr_bits; i++)
        {
            str << "A" << i;
            inputs.push_back(new CircuitInput(this, str.str()));
            str.str("");
        }
        for (unsigned int i = 0; i < m_item_bits; i++)
        {
            str << "in" << i;
            inputs.push_back(new CircuitInput(this, str.str()));
            str.str("");
        }
        for (unsigned int i = 0; i < m_item_bits; i++)
        {
            str << "out" << i;
            outputs.push_back(new CircuitOutput(this, str.str()));
            str.str("");
        }
    }
    else
    {
        inputs.push_back(new CircuitInput(this, "Adr", m_addr_bits));
        inputs.push_back(new CircuitInput(this, "in", m_item_bits));
        outputs.push_back(new CircuitOutput(this, "out", m_item_bits));
    }

    inputs.push_back(new CircuitInput(this, "S"));
}

void RAMComponent::on_propertyChanged()
{
	if ((getProperties().get_bool("multipin") != m_multipin) || (m_addr_bits != (unsigned int)getProperties().get_int("addr-bits")) || (m_item_bits != (unsigned int)getProperties().get_int("item-bits")))
	{
        m_addr_bits = (unsigned int)getProperties().get_int("addr-bits");
        m_item_bits = (unsigned int)getProperties().get_int("item-bits");
        m_multipin = getProperties().get_bool("multipin");

	    unsigned int c = 1 << m_addr_bits;

	    if (c != m_item_count)
	    {
	        m_item_count = c;
	        createItems();

	        string s = getProperties().get_string("content");
            readContentFromProps(s);
	    }

		clearInputs();
		clearOutputs();
		createIO();

	}

	if (m_content != getProperties().get_string("content"))
	{
        string s = getProperties().get_string("content");
        readContentFromProps(s);
	}
}

void RAMComponent::readContentFromProps(string& content)
{
    m_content = content;

    if (!m_pItems || !m_item_count) return;

    unsigned int u;
    istringstream is;
    is.str(content);

    unsigned int i=0;
    while (!is.eof())
    {
        if (i >= m_item_count) break;
        is >> hex >> u;
        m_pItems[i] = u;
        i++;
    }

    writeContentToProps();
}

/** @brief writeBackToProps
  *
  * @todo: document this function
  */
void RAMComponent::writeContentToProps()
{
    ostringstream os;
    for (unsigned int _i = 0u; _i < m_item_count; _i++)
    {
        os << hex << m_pItems[_i] << " ";
    }
    m_content = os.str();
    getProperties().set_string("content", m_content);
}


/** @brief provideUiText
  *
  * @todo: document this function
  */
void RAMComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/addr-bits"] = ui_text("Adressbits", "Anzahl der Adressbits.");
    textmap["/item-bits"] = ui_text("Inhalt-Bits", "Bits pro Speicherplatz.");
    textmap["/writeback"] = ui_text("Zurückschreiben", "Während der Simulation veränderten Inhalt dauerhaft übernehmen.");
    textmap["/content"] = ui_text("Inhalt", "Speicher-Inhalt: Hexadezimal, Leerzeichengetrennt.");
    textmap["/multipin"] = ui_text("Multipin", "Gebündelte Anschlüsse.");
}

