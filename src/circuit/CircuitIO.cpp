/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "CircuitIO.h"
#include "CircuitChild.h"


void CircuitIO::list_forward() // #
{
	type_forwardings::iterator ioit;

	TIMEOUT_TEST

	if (forwardings.size() > 0)
	{
		#ifdef SIMULATION_DEBUG
		clog << "# forw " << getOwner()->getName() << "." << getName() << "=" << value << endl;
		#endif
		for (ioit = forwardings.begin(); ioit != forwardings.end(); ioit++)
		{
			#ifdef SIMULATION_DEBUG
			clog << " - to " << (*ioit)->getOwner()->getName() << "." << (*ioit)->getName() << " = " << value << endl;
			#endif
			(*ioit)->list_setValue(value);
		}
	}
	else
	{
		#ifdef SIMULATION_DEBUG
		clog << "   " << getOwner()->getName() << "." << getName() << "=" << value << " (no forwardings)" << endl;
		#endif
	}
} // list_forward()

void CircuitInput::setValue(type_iovalue v)
{
    type_iovalue old = value;
    value = v & IOPINMASK;
    if (old != v)
    {
        if (getOwner()) getOwner()->inputChanged(getName(), v);
    }
}

void CircuitOutput::setValue(type_iovalue v)
{
    type_iovalue old = value;
    value = v & IOPINMASK;
    if (old != v)
    {
        if (getOwner()) getOwner()->outputChanged(getName(), v);
    }
}


void CircuitInput::list_setValue(type_iovalue v)
{
	TIMEOUT_TEST

#ifdef SIMULATION_DEBUG
	clog << "set Input " << getOwner()->getName() << "." << getName() << "=" << v << "(" << value << ")" << endl;
#endif
	changed = false;
	type_iovalue old = value;
	v &= IOPINMASK;

	if (v!=value || !isSet())
	{
		changed = true;
		value = v;


		// TODO: benötigt??
		if (getOwner()->getCirName() == "node")
		{
#ifdef SIMULATION_DEBUG
			clog << "Node Spezialbehandlung: " << endl;
#endif
			list_forward();
		}
		else
		{
		    getOwner()->on_input_changed(getName());
			getOwner()->list_push(getOwner());
#ifdef SIMULATION_DEBUG
			clog << getOwner()->getName() << " changed --> L" << endl;
#endif
		}

        if (value != old)
        {
            if (getOwner()) getOwner()->inputChanged(getName(), v);
        }
	}
	setSet();
}


void CircuitOutput::list_setValue(type_iovalue v)
{
	TIMEOUT_TEST

#ifdef SIMULATION_DEBUG
	clog << "set Ouput " << getOwner()->getName() << "." << getName() << "=" << v << "(" << value << ")" << endl;
#endif
	changed = false;
	type_iovalue old = value;

    changed = true;
    value = v & IOPINMASK;

    if (value != old/* || !isSet()*/)
    {
        if (getOwner()) getOwner()->outputChanged(getName(), v);
    }

    list_forward();

	setSet();
} // list_setValue(bool v)

/**
 * Eingehende verbindungen entfernen:
 * beim from
 * Eingang befreien von weiterleitungen, alle forwardings entfernen
 */
void CircuitIO::disconnect(bool to, bool bfrom, bool off)
{
	if (bfrom && this->from)
	{
		this->from->removeForwarding(this);
		//###if (off) this->list_setValue(false);
		if (off)
		{
		    this->setValue(0);
//            getParent()->update();
		}
	}
	if (to) {
		CircuitIO* pio;
		while (forwardings.size() > 0)
		{
			pio = forwardings.front();
			//###if (off) pio->list_setValue(false);
			if (off)
			{
			    pio->setValue(0);
  //              getParent()->update();
			}
			removeForwarding(pio);
		}
	}
}

const std::string CircuitIO::getNameEx()
{
    ostringstream os;
    os << name;
    if (getPinCount() > 1) os << "(" << getPinCount() << ")";
    return os.str();
}
