#ifndef CLOCKVISUAL_H
#define CLOCKVISUAL_H

#include "CircuitVisual.h"
#include "ClockComponent.h"


class ClockVisual : public CircuitChildVisual {
    protected:
        ClockComponent* m_pObj;
    public:
        bool vertical();
        void vertical(bool b);
        ClockVisual(ClockComponent* o = NULL, CircuitContainerVisual* p = NULL) :
         CircuitChildVisual(o, p),
         m_pObj(o)
        {};

        virtual void innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x=0, int y=0, bool children=true);
        virtual bool paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children);

        virtual ClockComponent* getObj() {return m_pObj;}

        virtual void on_inner_click(int x, int y, CircuitWidget* widg);
}; // ClockVisual


#endif // CLOCKVISUAL_H
