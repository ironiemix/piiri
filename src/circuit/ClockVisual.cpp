#include "ClockVisual.h"
#include "gui/CircuitWidget.h"

/** @brief on_inner_click
  *
  * @todo: document this function
  */
void ClockVisual::on_inner_click(int x, int y, CircuitWidget* widg)
{
	ClockComponent* cc = getObj();
	if (!cc) return;

	bool active = cc->getProperties().get_bool("run");
    cc->getProperties().set_bool("run", !active);
    cc->on_propertyChanged();

    cc->getParent()->update();
    //widg->on_circuitstate(CSC_SWITCHED);
    widg->update();
}

/** @brief paintSelf
  *
  * @todo: document this function
  */
bool ClockVisual::paintSelf(Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    if (!cr) return false;

	CircuitChildVisual::paintSelf(cr, x,y, false);

	x = x + m_inWidth;
	y = y + m_titleHeight;

	ClockComponent* cc = getObj();
	if (!cc) return false;

	cr->save();

    cr->rectangle(x,y, 20,20);
    cr->set_source_rgb(0,0,0);
    cr->fill();

	bool active = cc->getProperties().get_bool("run");
    cr->rectangle(x+1,y+1+(active?6:0), 20-2,12);
    if (active) cr->set_source_rgb(0.1,0.8,0.1);
    else cr->set_source_rgb(0.8,0.2,0.2);
    cr->fill();

	cr->restore();
	return true;
}

/** @brief innerSize
  *
  * @todo: document this function
  */
void ClockVisual::innerSize(int& w, int& h, Cairo::RefPtr<Cairo::Context> cr, int x, int y, bool children)
{
    w = 20;
    h = 23;
}
