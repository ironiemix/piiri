#include "CircuitChild.h"
#include "CircuitContainer.h"
#include <iostream>

#include "model/App.h"


deque<CircuitChild*> CircuitChild::cir_deque;

clock_t CircuitChild::timeoutClock;
clock_t CircuitChild::timeoutDelta = CLOCKS_PER_SEC;
unsigned int CircuitChild::steptime = 100;
CircuitChild::SimulationTyp CircuitChild::simtyp = CircuitChild::ST_NORMAL;

/** @brief CircuitChild
  *
  * @todo: document this function
  */
CircuitChild::CircuitChild(std::string n, CircuitContainer *p, const std::string& cn, bool sim) :
 name(n),
 cir_name(cn),
 visible(true),
 simulate(sim),
 m_pParentContainer(p)
{
    pos_x = pos_y = 0;

    //std::cout << "[CircuitChild] init Properties" << std::endl;
    markedVisible(false); // hier wird auch prop markedVisible angelegt!!
    getProperties().set_string("description", "");

}


/** @brief ~CircuitChild
  *
  * @todo: document this function
  */
CircuitChild::~CircuitChild()
{
    //std::cout << "~CircuitChild()" << std::endl;
    m_signal_circuit_notification.clear();
    clear();
}

/** @brief clear
  *
  * @todo: document this function
  */
void CircuitChild::clear()
{
    // std::cout << ((unsigned long)(this))  << " " << this->getName() << " " << (unsigned long)getParent() << std::endl;

    //std::cout << "CircuitChild::clear" << std::endl;

    m_signal_inputs_changed.clear();

    m_signal_input_changed.clear();
    m_signal_output_changed.clear();

    //std::cout << getNameRecursive() << std::endl;

    clear_list(this);

    clearInputs();
    clearOutputs();

}


/** @brief getParentRecursive
  *
  * @todo: document this function
  */
CircuitContainer* CircuitChild::getParentRecursive()
{
    if (m_pParentContainer == NULL) return NULL;
    CircuitContainer* pCc = m_pParentContainer->getParentRecursive();
    if (pCc == NULL) return m_pParentContainer;
    return pCc;
}



/************* Properties ***********/

/** @brief getNameRecursive
  *
  * @todo: document this function
  */
std::string CircuitChild::getNameRecursive(bool last)
{
    std::string s;
	if (getParent()) s = getParent()->getNameRecursive(last);

	if (getParent() || (!getParent() && last))
	{
        if (!s.empty()) s += ">";
        s += getName();
	}
	return s;
}



/** @brief provideUiText
  *
  * @todo: document this function
  */
void CircuitChild::provideUiText(ui_text_map& textmap)
{
    textmap["/description"] = ui_text(App::t("Beschreibung"), App::t("Zum Beispiel: Welche Aufgabe hat diese Komponente?"));
    textmap["/markedVisible"] = ui_text(App::t("Bleibt sichtbar"), App::t("Bleibt sichtbar, wenn die Schaltung als Komponente verwendet wird."));
}

/** @brief initProperties
  *
  * @todo: document this function
  */
/*void CircuitChild::initProperties()
{
}*/

std::string CircuitChild::getInfo()
{
    return "";
}


/************ Simulation *************/


void CircuitChild::update() // #
{
    if (!getSimulate()) return;

#ifdef SIMULATION_DEBUG
	clog << "######################### START UPDATE" << this->getNameRecursive() << endl;
#endif

    if (CircuitChild::isStepSim())
    {
        // invalidate
        list_push(this);
    }
    else
    {
        CircuitChild::timeoutClock = clock() + CircuitChild::timeoutDelta;

        reset(); // zum test

        try
        {
            list_simulate();
        }
        catch (string s)
        {
            deque<CircuitChild*>::iterator dqit;
            string err(s);
            m_signal_circuit_notification.emit(CN_TIMEOUT_ERROR, err, NULL);
        }
    }


#ifdef SIMULATION_DEBUG
	clog << "######################### END UPDATE" << endl;
#endif

}



void CircuitChild::step() // #
{
#ifdef SIMULATION_DEBUG
	clog << "## STEP" << endl;

	deque<CircuitChild*>::iterator dqit;
	clog << "## DEQUE: [";
    for ( dqit = getDeque().begin(); dqit != getDeque().end(); dqit++ )
    {
        clog << (*dqit)->getName() << ", ";
    }
	clog << "]" << endl;
#endif

	CircuitChild* p_obj;
    deque<CircuitChild*> d(CircuitChild::getDeque()); // kopie
    CircuitChild::getDeque().clear(); // leeren
    // kopie abarbeiten
    while(!d.empty())
    {
        p_obj = d.front();
        d.pop_front();
        p_obj->step_simulate();
        //p_obj->update();
        // passender container finden?
        //p_obj->m_signal_circuit_notification.emit(CN_UPDATE, "", NULL);
    }
}

void CircuitChild::step_simulate() // #
{
	type_inputs::iterator iit;
	deque<CircuitChild*>::iterator dqit;

#ifdef SIMULATION_DEBUG
	clog << "Do " << getName() << endl;
#endif

    // eingänge weiterleiten
    for ( iit=inputs.begin(); iit != inputs.end(); iit++ )
    {
        (*iit)->list_forward();
    }

    processChildren();



#ifdef SIMULATION_DEBUG
    for ( dqit = getDeque().begin(); dqit != getDeque().end(); dqit++ )
    {
        clog << (*dqit)->getName() << ", ";
    }
    clog << endl;
#endif

    if (hasChildren())
    {
    }
    else
    {
#ifdef SIMULATION_DEBUG
        clog << getName() << " has NO components" << endl;
        clog << "inputs: ";
        for ( iit=inputs.begin(); iit != inputs.end(); iit++ )
        {
            clog << (*iit)->getName() << "=" << (*iit)->getValue() << "; ";
        }
        clog << endl;
#endif

        type_outputs::iterator oit;
        type_iovalue v;
        // berechnen
        for ( oit=outputs.begin(); oit != outputs.end(); oit++ )
        {
            if ((*oit)->getFrom() != NULL) continue; // wenn dieser ausgang ein signal zugeleitet bekommt
            // v = calculateOutputValue((string&)(*oit));
            v = calculateOutputValue((*oit)->getName());
#ifdef SIMULATION_DEBUG
            clog << "calc OUT " << (*oit)->getOwner()->getName() << "." << (*oit)->getName() << "=" << (v?"1":"0") << endl;
#endif
            (*oit)->list_setValue(v);
        }
    }
}

void CircuitChild::list_simulate() // #
{
	CircuitChild* p_obj;

	type_inputs::iterator iit;
	deque<CircuitChild*>::iterator dqit;

#ifdef SIMULATION_DEBUG
	clog << "Do " << getName() << endl;
#endif

	TIMEOUT_TEST


    // eingänge weiterleiten
    for ( iit=inputs.begin(); iit != inputs.end(); iit++ )
    {
        (*iit)->list_forward();
    }

    processChildren();



#ifdef SIMULATION_DEBUG
    for ( dqit = getDeque().begin(); dqit != getDeque().end(); dqit++ )
    {
        clog << (*dqit)->getName() << ", ";
    }
    clog << endl;
#endif

    if (hasChildren())
    {
        while(!CircuitChild::getDeque().empty())
        {
            p_obj = CircuitChild::getDeque().front();
            CircuitChild::getDeque().pop_front();
            p_obj->list_simulate();
        }
    }
    else
    {
#ifdef SIMULATION_DEBUG
        clog << getName() << " has NO components" << endl;
        clog << "inputs: ";
        for ( iit=inputs.begin(); iit != inputs.end(); iit++ )
        {
            clog << (*iit)->getName() << "=" << (*iit)->getValue() << "; ";
        }
        clog << endl;
#endif

        type_outputs::iterator oit;
        type_iovalue v;
        // berechnen
        for ( oit=outputs.begin(); oit != outputs.end(); oit++ )
        {
            if ((*oit)->getFrom() != NULL) continue; // wenn dieser ausgang ein signal zugeleitet bekommt
            v = calculateOutputValue((string&)(*oit)->getName());
#ifdef SIMULATION_DEBUG
            clog << "calc OUT " << (*oit)->getOwner()->getName() << "." << (*oit)->getName() << "=" << (v?"1":"0") << endl;
#endif
            (*oit)->list_setValue(v);
        }
    }

} // list_simulate

void CircuitChild::list_push(CircuitChild* p_cir)
{
	deque<CircuitChild*>::iterator dqit;
	for ( dqit = CircuitChild::cir_deque.begin(); dqit != CircuitChild::cir_deque.end(); dqit++ ) {
		if ( (*dqit) == p_cir ) return;
	}
	CircuitChild::cir_deque.push_back(p_cir);
}

void CircuitChild::clear_list(CircuitChild* pC)
{

    if (pC == NULL)
    {
        std::cout << "clear_list(NULL) [sollte nicht sein.]" << std::endl;
        CircuitChild::cir_deque.clear();
    }
    else
    {
        std::deque<CircuitChild*>::iterator it = std::find(CircuitChild::cir_deque.begin(), CircuitChild::cir_deque.end(), pC);
        if (it != CircuitChild::cir_deque.end())
        {
            CircuitChild::cir_deque.erase(it);
        }
    }

}

deque<CircuitChild*>& CircuitChild::getDeque()
{
    return CircuitChild::cir_deque;
}




/************ IO ***********/

/**
 * Eingehende und ausgehende verbindungen trennen
 * das element liegt danach frei in der landschaft...
 */
void CircuitChild::disconnect(bool in, bool out)
{
	type_outputs::iterator oit;
	type_inputs::iterator iit;
	if (out)
	{
        for (oit = outputs.begin(); oit != outputs.end(); oit++)
        {
            (*oit)->disconnect(true, false, true);
        }
	}
    if (in)
    {
        for (iit = inputs.begin(); iit != inputs.end(); iit++)
        {
            (*iit)->disconnect(false, true, true);
        }
    }
}

/** @brief getOutputValue
    a(2) =
    b1   =
    b2   =
    -->
    a 0
    a 1
    b1
    b2
    --> b2 b1 a1 a0
        3   2  1  0
  *
  * @todo: document this function
  */
unsigned int CircuitChild::getOutputValue()
{
	unsigned int num = 0;
	type_outputs::reverse_iterator oit;
	for ( oit = outputs.rbegin(); oit != outputs.rend(); oit++ )
	{
		num <<= (*oit)->getPinCount();
		num |= (*oit)->getValue();
	}
	return num;
}

/** @brief getInputValue
  *
  * @todo: document this function
  */
unsigned int CircuitChild::getInputValue()
{
	unsigned int num = 0;
	type_inputs::reverse_iterator iit;
	for ( iit = inputs.rbegin(); iit != inputs.rend(); iit++ )
	{
		num <<= (*iit)->getPinCount();
		num |= (*iit)->getValue();
	}
	return num;
}
/** @brief getInput
  *
  * @todo: document this function
  */
CircuitInput* CircuitChild::getInput(const std::string& in)
{
    type_inputs_iterator it = getInputIterator(in);
    if (it != inputs.end()) return (*it);
    return NULL;
}

/** @brief getOutput
  *
  * @todo: document this function
  */
CircuitOutput* CircuitChild::getOutput(const std::string& out)
{
    type_outputs_iterator it = getOutputIterator(out);
    if (it != outputs.end()) return (*it);
    return NULL;
}

/** @brief getInputIterator
  *
  * @todo: document this function
  */
type_inputs_iterator CircuitChild::getInputIterator(const std::string& in)
{
    type_inputs_iterator it;
    for (it = inputs.begin(); it != inputs.end(); it++)
    {
        if ((*it)->getName() == in) break;
    }
    return it;
}

/** @brief getOutputIterator
  *
  * @todo: document this function
  */
type_outputs_iterator CircuitChild::getOutputIterator(const std::string& out)
{
    type_outputs_iterator it;
    for (it = outputs.begin(); it != outputs.end(); it++)
    {
        if ((*it)->getName() == out) break;
    }
    return it;
}



/** @brief getOutputPinCount
  *
  * @todo: document this function
  */
unsigned int CircuitChild::getOutputPinCount()
{
    unsigned int outbits = 0;
    type_outputs::iterator oit;
    for (oit = outputs.begin(); oit != outputs.end(); oit++)
    {
        outbits += (*oit)->getPinCount();
    }
    return outbits;
}

/** @brief getInputPinCount
  *
  * @todo: document this function
  */
unsigned int CircuitChild::getInputPinCount()
{
    unsigned int inbits = 0;
    type_inputs::iterator iit;
    for (iit = inputs.begin(); iit != inputs.end(); iit++)
    {
        inbits += (*iit)->getPinCount();
    }
    return inbits;
}






/** @brief setInput
  *
  * @todo: document this function
  */
void CircuitChild::setInput(const std::string& in, type_iovalue v)
{
    CircuitInput* i = getInput(in);
    if (i) i->setValue(v);
}


/** @brief getInputValue
  *
  * @todo: document this function
  */
type_iovalue CircuitChild::getInputValue(const std::string& in)
{
    CircuitInput* i = getInput(in);
    if (i) return i->getValue();
    return 0;
}




/** @brief getOutputValue
  *
  * @todo: document this function
  */
type_iovalue CircuitChild::getOutputValue(const string& out)
{
    CircuitOutput* o = getOutput(out);
    if (o) return o->getValue();
    return 0;
}



/** @brief setInputValue
    a(2) =
    b1   =
    b2   =
    -->
    a 0
    a 1
    b1
    b2
    --> b2 b1 a1 a0
        3   2  1  0
    für a mask 3
    für b1 mask 1
    für b2 mask 1
  *
  * @todo: document this function
  */
void CircuitChild::setInputValue(unsigned int v)
{
    // v wird nach rechts durchgeschiftet. dh. der wert steht  immer ganz rechts in den unteren bits
	type_inputs::iterator iit;
	for ( iit = inputs.begin(); iit != inputs.end(); iit++ )
	{
	    // bsp: pincount = 3:
	    // (1 << 3) - 1 = $1000 - 1 = $111
	    unsigned int mask = (1 << (*iit)->getPinCount()) - 1;
	    (*iit)->setValue(v & mask);
		v >>= (*iit)->getPinCount();
	}
}


void CircuitChild::reset()
{
    ioReset();
}

void CircuitChild::ioReset()
{
    {
        type_inputs::iterator it;
        for ( it=inputs.begin(); it != inputs.end(); it++ )
        {
            (*it)->setUnset();
        }
    }
    {
        type_outputs::iterator it;
        for ( it=outputs.begin(); it != outputs.end(); it++ )
        {
            (*it)->setUnset();
        }
    }
}


/** @brief clone
  *
  * @todo: document this function
  */
CircuitChild* CircuitChild::clone(CircuitContainer* parent)
{
    CircuitChild* pch = CircuitContainer::createComponent(parent, this->getCirName(), this->getName());
    pch->getProperties().clone(getProperties());
    pch->on_propertyChanged();
    return pch;
}


/** @brief clearOutputs
  *
  * @todo: document this function
  */
void CircuitChild::clearOutputs()
{
    disconnect(false, true);

    type_outputs::iterator it;
    CircuitOutput* pout;
    for (it = outputs.begin(); it != outputs.end(); it = outputs.begin())
    {
        pout = (*it);
        outputs.erase(it);
        delete pout;
    }
}

/** @brief clearInputs
  *
  * @todo: document this function
  */
void CircuitChild::clearInputs()
{
    disconnect(true, false);

    type_inputs::iterator it;
	CircuitInput* pin;
    for (it = inputs.begin(); it != inputs.end(); it = inputs.begin())
    {
        pin = (*it);
        inputs.erase(it);
        delete pin;
    }
    //inputs.clear();
}


/** @brief testTimeout
  *
  * @todo: document this function
  */
/*
void CircuitChild::testTimeout()
{
    if (clock() >= CircuitChild::timeoutClock) throw std::string("Zeitlimit überschritten:\nDie Schaltung erreichte"
                                                      "innerhalb einer bestimmten Zeit keinen stabilen Zustand.");
}
*/


/** @brief outputChanged
  *
  * @todo: document this function
  */
void CircuitChild::outputChanged(const std::string& name, type_iovalue value)
{
//    std::cout << "OUT " << getName() << "." << name << "\t= " << value << std::endl;
    m_signal_output_changed.emit(name, value);
}

/** @brief inputChanged
  *
  * @todo: document this function
  */
void CircuitChild::inputChanged(const std::string& name, type_iovalue value)
{
//    std::cout << "IN  " << getName() << "." << name << "\t= " << value << std::endl;
    m_signal_input_changed.emit(name, value);
}







