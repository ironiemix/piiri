#include "EditableCircuit.h"

#include "Components.h"


EditableCircuit::EditableCircuit(string n, CircuitContainer *p) :
  CircuitContainer(n,p)
{
    //ctor
}

EditableCircuit::~EditableCircuit()
{
    //dtor
}

/** @brief clone
  *
  * @todo: document this function
  */
CircuitChild* EditableCircuit::clone(CircuitContainer* parent)
{
    // this wird geklont!!
    // ec wird aufgebaut
    EditableCircuit* ec = (EditableCircuit*)CircuitContainer::clone(parent);
    type_inputs_iterator iit;
    type_outputs_iterator oit;
    type_elements_iterator eit;

    ec->clone(getChildren(), *ec);

    CircuitIO* pio;
    for (oit = getOutputs().begin(); oit != getOutputs().end(); oit++)
    {
        ec->addOut((*oit)->getName(), (*oit)->getPinCount());

        // verbindungen zum ausgang
        pio = (*oit)->getFrom();
        if (pio)
        {
            if (pio->getType()==IO_TYPE_OUT)
            {
                ec->connect(pio->getOwner()->getName()+"."+pio->getName(), "out."+(*oit)->getName());
            }
            else
            {
                // wird bei den inputs gemacht
                // hier wäre input nicht vorhanden
                // ec->connect("in."+pio->getName(), "out."+(*oit).second->getName());
            }
        }
    }

    for (iit = getInputs().begin(); iit != getInputs().end(); iit++)
    {
        ec->addIn((*iit)->getName(), (*iit)->getPinCount());

        // benötigte leitungen von diesem eingang aus verlegen
        type_forwardings& fw = (*iit)->getForwardings();
        for (type_forwardings::iterator fit = fw.begin(); fit != fw.end(); fit++)
        {
            CircuitIO* io = (*fit);
            if (io->getType() == IO_TYPE_IN)
            {
                ec->connect("in."+(*iit)->getName(), io->getOwner()->getName()+"."+io->getName());
            }
            else
            {
                ec->connect("in."+(*iit)->getName(), "out."+io->getName());
            }
        }
    }
/*
    type_forwardings::iterator fit;
    CircuitOutput* startout;
    CircuitOutput* endout;
	CircuitChild* pStart;
	CircuitChild* pEnd;

	// Verbindung zu output
    for (eit = getChildren().begin(); eit != getChildren().end(); eit++)
    {
	    pChild = (*it).second; // start
	    // ausgänge durchgehen:
	    for (oit = pChild->getOutputs().begin(); oit != pChild->getOutputs().end(); oit++)
	    {
	        startout = (*oit).second;
            // forwardings durchgehen
            for (fit = pout->getForwardings().begin(); fit != pout->getForwardings().end(); fit++)
            {
                endout = (*fit);
                pEnd = endio->getParent(); // end objekt
                nit = names.find(pEnd->getName());
                if ((endio->getType() == IO_TYPE_IN) && (nit != names.end())) // nur wenn input einer schaltung aus der zu klonenden menge
                {
                    std::string to((*nit).second + "."+endio->getName());
                    nit = names.find(pChild->getName());
                    std::string from((*nit).second+"."+pout->getName());
                    connect(from,  to );
                }
            }
	    }
    }*/
    return ec;
}
#include <set>
/// TODO new_comp als referenz?
void EditableCircuit::clone(type_elements& src, CircuitContainer& new_comp)
{
    // new_comp wird aufgebaut!!
    // src wird geklont
    type_elements_iterator it;
	CircuitChild* pChild;
	CircuitChild* pNew;
    std::map<std::string, std::string> names;
    std::map<std::string, std::string>::iterator nit;
	// alle markierten Container-Komponenten öffnen.
	for (it = src.begin(); it != src.end(); it++)
	{
	    pChild = (*it);
        pNew = pChild->clone(&new_comp);

	    names.insert(std::pair<std::string, std::string>(pChild->getName(), pNew->getName())); // namen zum später suchen merken

        if (pNew)
        {
            new_comp.addComponent(pNew);
            pNew->setPos(pChild->getPosX(), pChild->getPosY());
        }
	}

    type_forwardings::iterator fit;
    type_outputs_iterator oit;
    CircuitOutput* pout;
    CircuitIO* endio;
    CircuitChild* pEnd;
	// Verbindungen zwischen den objekten
	for (it = src.begin(); it != src.end(); it++)
	{
	    pChild = (*it); // start
	    // ausgänge durchgehen:
	    for (oit = pChild->getOutputs().begin(); oit != pChild->getOutputs().end(); oit++)
	    {
	        pout = (*oit);
            // forwardings durchgehen
            for (fit = pout->getForwardings().begin(); fit != pout->getForwardings().end(); fit++)
            {
                endio = (*fit);
                pEnd = endio->getOwner(); // end objekt
                nit = names.find(pEnd->getName());
                if ((endio->getType() == IO_TYPE_IN) && (nit != names.end())) // nur wenn input einer schaltung aus der zu klonenden menge
                {
                    std::string to((*nit).second + "."+endio->getName());
                    nit = names.find(pChild->getName());
                    std::string from((*nit).second+"."+pout->getName());
                    new_comp.connect(from,  to );
                }
            }
	    }
	}
}



bool EditableCircuit::topmost(CircuitChild* obj)
{
	type_elements_iterator i,to;

    if (getChildren().back() != obj)
    {
        getChildren().remove(obj);
        getChildren().push_back(obj);
        return true;
    }
    return false;
}

bool EditableCircuit::bottommost(CircuitChild* obj)
{
	if (getChildren().front() != obj)
	{
        getChildren().remove(obj);
        getChildren().push_front(obj);
        return true;
	}
    return false;
}

bool EditableCircuit::up(CircuitChild* obj)
{
	type_elements_iterator i,to;

	if (getChildren().back() != obj)
	{
		for (i = getChildren().begin(); i != getChildren().end(); i++)
		{
		    if ( (*i) == obj) break;
        }
        if ((i != getChildren().end())) // wenn gefunden
        {
            to = i;
            to++;
            to++;
            if (to != getChildren().end())
            {
                getChildren().splice(to, getChildren(), i);
            }
            else
            {
                getChildren().remove(obj);
                getChildren().push_back(obj);
            }
            return true;
        }
	}
    return false;
}
bool EditableCircuit::down(CircuitChild* obj)
{
	type_elements_iterator i,to;

    if (getChildren().front() != obj)
    {
		for (i = getChildren().begin(); i != getChildren().end(); i++)
		{
		    if ( (*i) == obj) break;
        }
        if ((i != getChildren().end())) // wenn gefunden
        {
            to = i;
            to--;
            getChildren().splice(to, getChildren(), i);
            return true;
        }
    }
    return false;
}


bool EditableCircuit::changeElementName(CircuitChild* p, const string& nn)
{
    if (nn == p->getName()) return true;
    if (findChildByName(nn) == NULL)
    {
        p->setName(nn);
        return true;
    }
	return false;
}

bool EditableCircuit::changeIOName(CircuitIO *p, const string& nn)
{
    // nur io von dieser komponente kann verändert werden!
	if (p->getOwner() != this) return false;

    if (nn == p->getName()) return true;

	if (p->getType() == IO_TYPE_IN)
	{
        if (getInput(nn) == NULL)
        {
            //inputs.erase(p->getName());
            p->setName(nn);
            //inputs[nn] = (CircuitInput *)p;
            m_signal_inputs_changed.emit();
            return true;
        }
	}
	else
	{
        if (getOutput(nn) == NULL)
        {
            //outputs.erase(p->getName());
            p->setName(nn);
            //outputs[nn] = (CircuitOutput *)p;
            return true;
        }
	}
	return false;
}

void EditableCircuit::removeIO(CircuitIO* io)
{
    if (io && (io->getOwner() == this))
    {
        if (io->getType() == IO_TYPE_IN)
        {
            type_inputs_iterator it;
            for (it = inputs.begin(); it != inputs.end(); it++)
            {
                if ((*it) == io)
                {
                    io->disconnect();
                    inputs.erase(it);
                    m_signal_inputs_changed.emit();
                    return;
                }
            }
        }
        else if (io->getType() == IO_TYPE_OUT)
        {
            type_outputs_iterator it;
            for (it = outputs.begin(); it != outputs.end(); it++)
            {
                if ((*it) == io)
                {
                    io->disconnect();
                    outputs.erase(it);
                    return;
                }
            }
        }
    }
}

void EditableCircuit::removeComponent(CircuitChild* pObj)
{
    if (pObj && (pObj->getParent() == this))
    {
        // Node bekommt sonderbehandlung
        CircuitIO *from = NULL;
        type_forwardings::iterator ioit;
        type_forwardings to;

        if (pObj->getCirName() == "node")
        {
            NodeComponent *pe;

            pe = (NodeComponent*)pObj;

            from = pe->pin->getFrom();
            if (from) // nur wenn eine verbindung komplett hierüber geht
            {
                // merken wohin das obj das signal verteilt hat, denn bevor die neuen verbindungen gesetzt werden können, müssen die alten entfernt werden.
                for (ioit = pe->pout->getForwardings().begin(); ioit != pe->pout->getForwardings().end(); ioit++)
                {
                    to.push_back((*ioit));
                }
            }
        }

        // Normales löschen
        pObj->disconnect();
        // nicht machen - wird geziehlt beim destructor bzw in CircuitChild::clear() gemacht.
        //CircuitChild::getDeque().clear(); // leeren ()
        m_Children.remove(pObj);
        delete pObj;

        if (from) // direkte forwardings schalten
        {
            for (ioit = to.begin(); ioit != to.end(); ioit++)
            {
                from->addForwarding((*ioit));
            }

            try
            {
                //###from->list_forward();
                update();
            }
            catch (int i)
            {
                cout << "Exception: " << i << endl;
            }
        }
    }
}

