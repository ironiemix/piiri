#include "circuit/CircuitContainer.h"
#include "circuit/CircuitChild.h"
#include "circuit/Components.h"

#include "RAMComponent.h"
#include "IOWComponent.h"
#include "ClockComponent.h"
#include "DisplayComponents.h"

#include "model/App.h"

#include <cstdio>

CircuitContainer::CircuitContainer(string n, CircuitContainer *p, bool sim) :
 CircuitChild(n, p, "", sim)
{

}

/** @brief ~CircuitContainer
  *
  * @todo: document this function
  */
CircuitContainer::~CircuitContainer()
{
    //std::cout << "~CircuitContainer()" << std::endl;
    clear();
}



void CircuitContainer::reset()
{
    CircuitChild::reset();

	type_elements::iterator eit;
    for ( eit=m_Children.begin(); eit != m_Children.end(); eit++ )
    {
        (*eit)->reset();
    }
}

/** @brief clear
  *
  * @todo: document this function
  */
void CircuitContainer::clear()
{
    //std::cout << "CircuitContainer::clear()" << std::endl;


	CircuitChild* pCh;

	while (m_Children.size() > 0)
	{
	    pCh = m_Children.front();
	    m_Children.pop_front();
	    delete pCh;
	}

    // TODO: beim löschen von compintentw eird das dann doppel ausgeführt weil
    // destruktor von Child das auch nochmal aufruft - kann das hier weg??
    CircuitChild::clear();
}



bool CircuitContainer::parseIOThingFromString(const std::string& s, IOThing* thing, IO_TYPE type)
{
	std::vector<std::string> sp;
	splitString(sp, s, '.');

	//cout << sp[0]<< " "<<sp[1]<<endl;

	if ((sp[0] == "in") || (sp[0] == "out"))
	{
		thing->p_circuit = this;
		type = (sp[0] == "in")? IO_TYPE_IN : IO_TYPE_OUT;
	}
	else
	{
	    thing->p_circuit = findChildByName(sp[0]);
		if (thing->p_circuit == NULL) return false;
	}

	if (type == IO_TYPE_IN)
	{
	    thing->p_io = thing->p_circuit->getInput(sp[1]);
		return (thing->p_io != NULL);
	}
	if (type == IO_TYPE_OUT)
	{
	    thing->p_io = thing->p_circuit->getOutput(sp[1]);
		return (thing->p_io != NULL);
	}
	cerr << "CircuitObject::parseIOThingFromString: Unbekannter IO Typ!" << endl;
	thing->p_io = NULL;
	return false;
} // CircuitObject::parseIOThingFromString

CircuitChild* CircuitContainer::findChildByName(const std::string& name)
{
    type_elements_iterator it;
    for (it = m_Children.begin(); it != m_Children.end(); it++)
    {
        if ((*it)->getName() == name) return (*it);
    }
    return NULL;
}

/** @brief on_child_notification
  *
  * @todo: document this function
  */
void CircuitContainer::on_child_notification(int n, const string& s, gpointer p)
{
    m_signal_circuit_notification.emit(n, s, p);
}


CircuitInput *CircuitContainer::addIn(const string& name, unsigned short pc, const string& prae, type_iovalue value)
{
	string _name;
	if (!name.empty())
	{
		_name = name;
	}
	else
	{
	    string p;
	    if (prae.empty()) p = "i";
	    else p = prae;
// TODO : stringstream
		int num = 0;
		char buf[64];
		do {
			sprintf(buf, "%s%d", p.c_str(), num++);
			_name = buf;
		} while (getInput(_name) != NULL);
	}
	#ifdef COMMAND_LOG
	clog << "Circuit '" << getName() << "' gets new input '" << _name << "'." << endl;
	#endif

	CircuitInput* in = new CircuitInput(this, _name, pc);
	inputs.push_back(in);

	if (getParent() == NULL) in->setValueB(value);

	m_signal_inputs_changed.emit();
	return in;
}

CircuitOutput *CircuitContainer::addOut(const string& name, unsigned short pc, const string& prae)
{
	string _name;
	if (!name.empty())
	{
		_name = name;
	}
	else
	{
	    string p;
	    if (prae.empty()) p = "o";
	    else p = prae;

		int num = 0;
		char buf[64];
		do {
			sprintf(buf, "%s%d", p.c_str(), num++);
			_name = buf;
		} while (getOutput(_name) != NULL);
	}
	#ifdef COMMAND_LOG
	clog << "Circuit '" << getName() << "' gets new output '" << _name << "'." << endl;
	#endif
	CircuitOutput *out = new CircuitOutput(this, _name, pc);
	outputs.push_back(out);

	return out;
}


// static
CircuitChild* CircuitContainer::createComponent(CircuitContainer* p, const std::string& cir, const std::string& name)
{
    //std::cout << "createComponent: (" << p->getName() << ") " << cir << " \"" << name << "\"" << std::endl;
	string _name;

    // namens prefix ist der angegebene name, wenn leer, die cir-id
	string pref;

	pref = (name.empty() ? cir : name);

	// Name des neuen elementes

    int num = 0;
	ostringstream os;

	if (name.empty()) os << pref << "_" << num++; // falls name nicht gegeben fängt die numerierung gleich bei null an
	else os << pref;

	while (p->findChildByName(os.str()) != NULL)
	{
	    os.str("");
	    os << pref << "_" << num++;
	}
	_name = os.str();

	CircuitChild* neues_element = NULL;

	if (false) {
	} else if (cir == "or") {
		neues_element = new GateComponent(_name, p, OR);
	} else if (cir == "and") {
		neues_element = new GateComponent(_name, p, AND);
	} else if (cir == "nor") {
		neues_element = new GateComponent(_name, p, NOR);
	} else if (cir == "nand") {
		neues_element = new GateComponent(_name, p, NAND);
	} else if (cir == "xor") {
		neues_element = new GateComponent(_name, p, XOR);
	} else if (cir == "not") {
		neues_element = new GateComponent(_name, p, NOT);

	//} else if (cir == "rs-intern") {
	//	neues_element = new RSFlipFlop(_name, p);
	} else if (cir == "digit") {
		neues_element = new DigitalZiffer(_name, p);
	} else if (cir == "digit2") {
		neues_element = new DigitalZiffer2(_name, p);
	} else if (cir == "letter") {
		neues_element = new LetterComponent(_name, p);
	} else if (cir == "node") {
		neues_element = new NodeComponent(_name, p, 1);
	} else if (cir == "adapter") {
		neues_element = new AdapterComponent(_name, p, 3, AdapterComponent::DIR_BUNDLE);
	} else if (cir == "clock") {
		neues_element = new ClockComponent(_name, p);
	} else if (cir == "one") {
		neues_element = new One(_name, p);
	} else if (cir == "zero") {
		neues_element = new Zero(_name, p);
	} else if (cir == "text") {
		neues_element = new TextComponent(_name, p);
	} else if (cir == "switch") {
		neues_element = new Switch(_name, p, 1);
	} else if (cir == "led") {
		neues_element = new LEDComp(_name, p);
	} else if (cir == "8switch") {
		neues_element = new Switch(_name, p, 8);
	} else if (cir == "8indicator") {
		neues_element = new Indicator(_name, p,8);
	} else if (cir == "indicator") {
		neues_element = new Indicator(_name, p,1);
	} else if (cir == "function") {
		neues_element = new FunctionComponent(_name, p,2);
	} else if (cir == "ram") {
		neues_element = new RAMComponent(_name, p, 4, 4);
	} else if (cir == "container") {
	    neues_element = new EditableCircuit(_name, p);
	    neues_element->setCirName("container");
	} else if (cir == "iow") {
	    neues_element = new IOWComponent(_name, p);
	} else if (!cir.empty()) {
		CircuitContainer* pCircuit = new CircuitContainer(_name, p);
		neues_element = pCircuit; // = (Circuit*)neues_element;
		if (!pCircuit->loadCircuit(cir)) {
			cerr << "Die Komponente " << p->getName() << " braucht eine " << cir << "-Komponente. Diese Komponente konnte aber nicht erzeugt werden." << endl;
			delete neues_element;
			neues_element = NULL;
		}
	}
	if (neues_element) neues_element->setSimulate(p->getSimulate());
	return neues_element;
}

CircuitChild *CircuitContainer::addComponent(CircuitChild *neues_element)
{
	if (neues_element != NULL)
	{
		m_Children.push_back(neues_element);
		neues_element->signal_circuit_notification().connect(sigc::mem_fun(*this, &CircuitContainer::on_child_notification));
		//lastInserted = neues_element;
		#ifdef COMMAND_LOG
		clog << sp[1] << "-Gatter (" << _name << ") hinzugefügt!" << endl;
		#endif
		return neues_element;
	}
	else
	{
		//lastInserted = NULL;
	}
	return NULL;
}

CircuitChild *CircuitContainer::addComponent(const string& cir, const string& name, int x, int y)
{
    CircuitChild *c = createComponent(this, cir, name);
    if (c)
    {
        c->setPos(x,y);
        addComponent(c);
    }
	return c;
}





/***************** Simulation ********************/
/** @brief processChildren
  *
  * @todo: document this function
  */
void CircuitContainer::processChildren()
{
	type_elements::iterator eit;

    bool isstepsim = CircuitChild::isStepSim();

    // elemente ohne inputs werden auch angestoßen
    for ( eit=m_Children.begin(); eit != m_Children.end(); eit++ )
    {
        if (isstepsim)
        {
            list_push((*eit));
        }
        else
        {
            if ((*eit)->getInputs().size() == 0)
            (*eit)->list_simulate();
        }
    }
}




/************ Schaltung aufbauen ***************/

bool CircuitContainer::connect(const string& from, const string& to)
{
    //std::cout << "connect: " << from << "-->" << to << std::endl;
	IOThing iofrom, ioto;
	bool b1 = parseIOThingFromString(from, &iofrom, IO_TYPE_OUT);
	bool b2 = parseIOThingFromString(to, &ioto, IO_TYPE_IN);
	if (b1 && b2)
	{
	    if (iofrom.p_io->getPinCount() == ioto.p_io->getPinCount())
		{
            iofrom.p_io->addForwarding(ioto.p_io);
		}
	}
	else
	{
	    cerr << "Eine Komponente oder ein Anschluss ist nicht vorhanden." << endl;
	    cerr << "Verbindung zwischen " << from << " und " << to << " konnte nicht hergestellt werden." << endl;
    }
	return false;
}


/********************** Dateioperationen *****************/


void CircuitContainer::loadXml(xmlpp::Element* parent)
{

	xmlpp::Node::NodeList ins = parent->get_children("in");
	xmlpp::Node::NodeList outs = parent->get_children("out");
	xmlpp::Node::NodeList components = parent->get_children("component");
	xmlpp::Node::NodeList connections = parent->get_children("connect");
	xmlpp::Node::NodeList sett;

	xmlpp::Node::NodeList::iterator it;
	xmlpp::Element *e;
	xmlpp::Element *settings;

    inputs.reserve(ins.size());
    type_iovalue v;
	for (it = ins.begin(); it != ins.end(); it++)
	{
		e = (xmlpp::Element *)(*it);
		v = 0;
		if (e->get_attribute_value("value") == "on") v = 1;
		else if (e->get_attribute_value("value") == "off") v = 0;
		else v = atoi(e->get_attribute_value("value").c_str());
		addIn(e->get_attribute_value("name"),  atoi(e->get_attribute_value("pinCount").c_str()), "", v);

	}
    outputs.reserve(outs.size());
	for (it = outs.begin(); it != outs.end(); it++)
	{
		e = (xmlpp::Element *)(*it);
		addOut(e->get_attribute_value("name"), atoi(e->get_attribute_value("pinCount").c_str()));
	}

	std::map<std::string,std::string> umbenennung;
	for (it = components.begin(); it != components.end(); it++)
	{
		e = (xmlpp::Element *)(*it);

		CircuitChild *pobj;

		pobj = addComponent(
			e->get_attribute_value("cirid"),
			e->get_attribute_value("name"),
			atoi(e->get_attribute_value("x").c_str()),
			atoi(e->get_attribute_value("y").c_str())
		);

		if (pobj)
		{

		    if (pobj->getName() != e->get_attribute_value("name"))
		    {
		        umbenennung[e->get_attribute_value("name")] = pobj->getName();
		    }

            sett = e->get_children("settings");
            if (sett.size() > 0)
            {
                settings = (xmlpp::Element *)(*sett.begin());
                pobj->getProperties().read_xml(settings);
                pobj->on_propertyChanged();
            }

            /// Container-Behandlung
            if (e->get_attribute_value("cirid") == "container")
            {
                xmlpp::Element* circuit  = getFirstElement(e, "circuit");
                if (circuit)
                {
                    ((CircuitContainer*)pobj)->loadXml(circuit);
                }
                else
                {
                    std::cerr << "Container enthält keine Schaltung!" << std::endl;
                }
            }
		}
	}

	for (it = connections.begin(); it != connections.end(); it++)
	{
		e = (xmlpp::Element *)(*it);

        std::vector<std::string> sp;

        splitString(sp, e->get_attribute_value("from"), '.');
        if (umbenennung.find(sp[0]) != umbenennung.end()) {
            sp[0] = umbenennung[sp[0]];
        }
        std::string from = sp[0] + "." + sp[1];

        splitString(sp, e->get_attribute_value("to"), '.');
        if (umbenennung.find(sp[0]) != umbenennung.end()) {
            sp[0] = umbenennung[sp[0]];
        }
        std::string to = sp[0] + "." + sp[1];

		connect(from, to);
	}

}





void CircuitContainer::saveXml(xmlpp::Element* parent)
{

	type_inputs::iterator inputs_it;
	type_outputs::iterator outputs_it;
	type_elements::iterator elements_it;
	CircuitChild *pobj;
	xmlpp::Element *e;
	ostringstream os;


	// In / Out
	for ( inputs_it=getInputs().begin(); inputs_it != getInputs().end(); inputs_it++ )
	{
		e = parent->add_child("in");
		e->set_attribute("name", (*inputs_it)->getName());
		os.str("");
		os << (*inputs_it)->getValue();
		e->set_attribute("value", os.str() );
		os.str("");
		os << (*inputs_it)->getPinCount();
		e->set_attribute("pinCount", os.str() );
	}
	for ( outputs_it=getOutputs().begin(); outputs_it != getOutputs().end(); outputs_it++ )
	{
		e = parent->add_child("out");
		e->set_attribute("name", (*outputs_it)->getName());
		os.str("");
		os << (*outputs_it)->getPinCount();
		e->set_attribute("pinCount", os.str() );
	}

	//GValue *settings_value;

	// Elemente
	for ( elements_it=m_Children.begin(); elements_it != m_Children.end(); elements_it++ )
	{
	    pobj = (*elements_it);
		e = parent->add_child("component");
		e->set_attribute("name", pobj->getName());
		e->set_attribute("cirid", pobj->getCirName());
		e->set_attribute("simulate", pobj->getSimulate() ? "1" : "0");
		os.str("");
		os << pobj->getPosX();
		e->set_attribute("x", os.str());
		os.str("");
		os << pobj->getPosY();
		e->set_attribute("y", os.str());

		xmlpp::Element *settings = e->add_child("settings");
		pobj->getProperties().write_xml(settings);

        /// Container-Bahandlung
        if (pobj->getCirName() == "container")
        {
            xmlpp::Element* circuit = e->add_child("circuit");
            ((CircuitContainer*)pobj)->saveXml(circuit);
        }
	}

	// Verbindungen
	type_forwardings::iterator forw_it;
	// ...Input-Forwarding
	for ( inputs_it=getInputs().begin(); inputs_it != getInputs().end(); inputs_it++ )
	{
		type_forwardings& forw = (*inputs_it)->getForwardings();
		for ( forw_it=forw.begin(); forw_it != forw.end(); forw_it++ )
		{
			e = parent->add_child("connect");
			e->set_attribute("from", "in." + (*inputs_it)->getName());

			if ((*forw_it)->getOwner() == this) {
				e->set_attribute("to", "out." + (*forw_it)->getName());
			} else {
				e->set_attribute("to", (*forw_it)->getOwner()->getName() + "." + (*forw_it)->getName());
			}
		}
	}
	// ...von Output aller Elemente
	for ( elements_it=m_Children.begin(); elements_it != m_Children.end(); elements_it++ )
	{
		pobj = (*elements_it);

		for ( outputs_it = pobj->getOutputs().begin(); outputs_it != pobj->getOutputs().end(); outputs_it++ )
		{
			type_forwardings& forw = (*outputs_it)->getForwardings();
			for ( forw_it=forw.begin(); forw_it != forw.end(); forw_it++ ) {
				e = parent->add_child("connect");

				e->set_attribute("from", pobj->getName() + "." + (*outputs_it)->getName());

				if ((*forw_it)->getOwner() == this) {
					e->set_attribute("to", "out." + (*forw_it)->getName());
				} else {
					e->set_attribute("to", (*forw_it)->getOwner()->getName() + "." + (*forw_it)->getName());
				}
			}
		}
	}

} // save xml


/**
    Objekt mit schaltung mit einem bestimmten namen füllen
*/
bool CircuitContainer::loadCircuit(const string& schaltung) {
	std::string fn = App::getCirFilename(schaltung);
	setCirName(schaltung);
	if (loadFile(fn))
	{
        setFilename(fn);
		return true;
	}
	else
	{
	    App::missingComponents.insert(schaltung);
		return false;
	}
	return false;
}


bool CircuitContainer::loadFile(const string& fn) {
    if (FileNotExists(fn))
    {
        cerr << "Datei " << fn << " existiert nicht." << endl;
        return false;
    }
    xmlpp::DomParser dom(fn);
    xmlpp::Document *doc = dom.get_document();
    if (!doc) return false;

    return loadFromDomDocument(doc);
}


/** @brief loadFromDomDocument
  *
  * @todo: document this function
  */
bool CircuitContainer::loadFromDomDocument(xmlpp::Document *doc)
{
    xmlpp::Element *root = doc->get_root_node();
    if (!root) return false;
    xmlpp::Element *circuit  = (xmlpp::Element *)(*root->get_children("circuit").begin());
    if (!circuit) return false;

    // Version der Einstellungen ermitteln
    // ab V2 werden die props anders gespeichert.
    std::string _version = root->get_attribute_value("version");
    int version;
    if (_version.empty()) _version = "1";
    istringstream is(_version);
    is >> version;
    bool _ln = Complex2::loadNew;
    if (version > 1) Complex2::loadNew = true;
    else Complex2::loadNew = false;

    loadXml(circuit);

    Complex2::loadNew = _ln;

    return true;
}


/** @brief saveFile
  *
  * @todo: document this function
  */
bool CircuitContainer::saveFile(const string& fn)
{
	xmlpp::DomParser dom;
	xmlpp::Document *doc = dom.get_document();
    if (!doc) return false;
	xmlpp::Element *root = doc->create_root_node("schaltung");
    if (!root) return false;

	root->set_attribute("name", getName());
	root->set_attribute("version", "2");

    Complex2::saveNew = true;

	// Settings
	xmlpp::Element *settings = root->add_child("settings");
    if (!settings) return false;


	getProperties().write_xml(settings);


	xmlpp::Element *circuit = root->add_child("circuit");
    if (!circuit) return false;
	saveXml(circuit);

    Complex2::saveNew = false;

	if (isFileWritable(fn))
	{
        doc->write_to_file_formatted(fn);
        return true;
	}

	return false;
}





