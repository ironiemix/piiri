#ifndef CLOCKCOMPONENT_H
#define CLOCKCOMPONENT_H

#include "CircuitChild.h"

class ClockComponent : public CircuitChild
{
protected:
    bool value;
    int interval;
    bool run;
    sigc::connection connection;
    bool del;

public:
	ClockComponent(string n, CircuitContainer* p);
	virtual ~ClockComponent();
	type_iovalue calculateOutputValue(const string& out);

	virtual bool timer_callback();
	virtual void on_propertyChanged();
    virtual void provideUiText(ui_text_map& textmap);
private:
    void start();
    void stop();
    void update();
};

#endif // CLOCKCOMPONENT_H
