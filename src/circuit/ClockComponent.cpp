#include "ClockComponent.h"

#include "CircuitContainer.h"

ClockComponent::ClockComponent(string n, CircuitContainer* p) :
    CircuitChild(n, p, "clock"),
    value(false),
    interval(500),
    run(false),
    del(false)
{
    outputs.push_back(new CircuitOutput(this, "o"));

    getProperties().set_int("interval", interval);
    getProperties().set_bool("run", run);
}

ClockComponent::~ClockComponent()
{
    connection.disconnect();
    del = true;
    stop();
}

type_iovalue ClockComponent::calculateOutputValue(const string& out)
{
    return value;
}

bool ClockComponent::timer_callback()
{
    //std::cout << "callb" << std::endl;
    //if (del)
    //return false;
    value = !value;
    //cout << "timer_callback value=" << value << endl;
    update();
    return true;
}

void ClockComponent::update()
{
    if (getParent()) getParent()->update();
    string s;
    m_signal_circuit_notification.emit(CN_UPDATE, s, NULL);
}



void ClockComponent::on_propertyChanged()
{
    bool restart = false;

    bool _r = getProperties().get_bool("run");
    int _i = getProperties().get_int("interval");
    if (_i<20) // soll nicht weniger als 20 sein!
    {
        _i=20;
        getProperties().set_int("interval", _i);
    }

    if (_i != interval)
    {
        restart = true;
        interval = _i;
    }

    if (_r != run)
    {
        restart = true;
        run = _r;
    }

    if (restart)
    {
        stop();
        if (run) start();
    }
}

void ClockComponent::stop()
{
    if (connection.connected())
    {
        connection.disconnect();
        value = false;
        update();
    }
}

void ClockComponent::start()
{
    if (!connection.connected()) connection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &ClockComponent::timer_callback), interval);
}

/** @brief provideUiText
  *
  * @todo: document this function
  */
void ClockComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/interval"] = ui_text("Intervall (ms)", "Intervall in Millisekunden.");
    textmap["/run"] = ui_text("Aktiv", "Taktgeber aktivieren.");
}
