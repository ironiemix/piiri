#include "IOWarrior.h"
#include <cstring>
#ifndef _WIN32
    #include <sys/ioctl.h>
#endif
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <string>




IOWarrior::IOWarrior(int num, void* owner_id) :
m_DevNum(num),
m_ReadHandle(IOW_INVALID_HANDLE),
m_WriteHandle(IOW_INVALID_HANDLE),
refCount(0),
m_timeout(1000), // 1 sek
stop(true),
m_OutVal(0), m_InVal(0xffffffff), m_OutMask(0x0000FFFF), m_InMask(0xFFFF0000),
the_only_writer(NULL),
m_OwnerId(owner_id)
{
    openDevice();

    if (isOpen())
    {
        setOutVal(0x00000000);
    }
    else
    {
        std::cout << "IOW: Hardware nicht geöffnet!" << std::endl;
    }
}


IOWarrior::~IOWarrior()
{
    stopThread();
    this->close();
}

/** @brief close
  *
  * @todo: document this function
  */
void IOWarrior::close()
{
    #ifdef _WIN32
        if (canWrite())
        {
            CloseHandle(m_WriteHandle);
            m_WriteHandle = IOW_INVALID_HANDLE;
        }
        if (canRead())
        {
            CloseHandle(m_ReadHandle);
            CloseHandle(m_ovl.hEvent);
            m_ReadHandle = IOW_INVALID_HANDLE;
        }
    #else
        if (canWrite()) ::close(m_WriteHandle);
        m_WriteHandle = m_ReadHandle = IOW_INVALID_HANDLE;
    #endif
}


/** @brief write
  *
  * @todo: document this function
  */
void IOWarrior::setOutVal(unsigned long val)
{
    m_OutVal = val;
    WriteSimple(m_OutVal);
}

/** @brief run
  *
  * @todo: document this function
  */
void IOWarrior::run()
{
    if (stop) return;

    if (canRead())
    {
        IOWKIT56_IO_REPORT rep56;
        unsigned long rc;

        while(true)
        {
            {//mutex
                Glib::Mutex::Lock lock (mutex);
                if (stop) break;
            }
            //usleep(1000);
            //sleep(1);
            //pause();


            // rc = ReadSimpleNonBlocking(&rep56);
            // std::cout << "Read..." << std::endl;
            rc = ReadSimple(&rep56);

            if (rc)
            {
                {// mutex
                    Glib::Mutex::Lock lock (mutex);
                    memcpy(&m_InVal, &rep56.Bytes[0], sizeof(unsigned long));
                    sig_input.emit();
                }
            }
        }
    }

    std::cout << "IOW: Thread Ende." << std::endl;
    {// mutex
        Glib::Mutex::Lock lock (mutex);
        this->close();
        stop = true;
    }
    // close device
}






/** @brief startThread
  *
  * @todo: document this function
  */
void IOWarrior::startThread()
{
    if (stop==false) return;

    std::cout << "IOW: startThread" << std::endl;
    stop = false;
    thread = Glib::Thread::create(sigc::mem_fun(*this, &IOWarrior::run), true);
}

/** @brief stopThread
  *
  * @todo: document this function
  */
void IOWarrior::stopThread()
{
    if (stop) return;

    std::cout << "IOW: stopThread" << std::endl;
    // thread beenden
    {
        Glib::Mutex::Lock lock (mutex);
        stop = true;
    }
    if (thread) thread->join(); // Here we block to truly wait for the thread to complete
}







/*************************************/



unsigned long IOWarrior::ReadSimpleNonBlocking(IOWKIT56_IO_REPORT *rep56)
{
  memset(rep56, 0x00, IOWKIT56_IO_REPORT_SIZE);
  //switch (IowKitGetProductId(devHandle))
  //  {
  //  case IOWKIT_PRODUCT_ID_IOW40:
      return IowKitReadNonBlocking(IOW_PIPE_IO_PINS, (char*) rep56, IOWKIT40_IO_REPORT_SIZE);
  //  case IOWKIT_PRODUCT_ID_IOW24:
  //    return IowKitReadNonBlocking(devHandle, IOW_PIPE_IO_PINS, (PCHAR) rep56, IOWKIT24_IO_REPORT_SIZE);
  //  case IOWKIT_PRODUCT_ID_IOW56:
  //    return IowKitReadNonBlocking(devHandle, IOW_PIPE_IO_PINS, (PCHAR) rep56, IOWKIT56_IO_REPORT_SIZE);
  //  default:
  //    return 0;
  //  }
}


unsigned long IOWKIT_API IOWarrior::IowKitReadNonBlocking(unsigned long numPipe, char* buffer, unsigned long length)
{
    #ifndef _WIN32
    struct timeval tv;
    fd_set rfds;
    #endif
    unsigned long rbc = 0;
    int siz, extra;

    if (buffer == NULL) return 0;

    siz = m_Info.packet_size;
    switch(numPipe)
    {
        case IOW_PIPE_IO_PINS:
        extra = 1;
        break;
        //case IOW_PIPE_SPECIAL_MODE:
        //extra = 0;
        //break;
        default:
        return 0;
    }

    #ifndef _WIN32
    int result;
    for(rbc = 0; rbc + siz + extra <= length; )
    {
        tv.tv_sec = 0;
        tv.tv_usec = 0;
        FD_ZERO(&rfds);     // clear the read-descriptor for select
        FD_SET(m_ReadHandle, &rfds);  // add the filedescriptor to read-descriptor
        result = select(m_ReadHandle + 1, &rfds, NULL, NULL, &tv);
        if (result > 0 && FD_ISSET(m_ReadHandle, &rfds))
        {
            // put in eventual ReportID 0 to be a Windows lookalike
            *buffer = '\0';
            buffer += extra;

            // there should be some data ready for reading now
            if (::read(m_ReadHandle, buffer, siz) != siz) break;
            //if (numPipe == IOW_PIPE_IO_PINS) memcpy(&dev->lastValue, buffer, sizeof(DWORD));

            buffer += siz;
            rbc += siz + extra;
        }
        else if (result == 0) break;
    } // for
    #else

    #endif

    return rbc;
}


unsigned long IOWarrior::ReadSimple(IOWKIT56_IO_REPORT *rep56)
{
    memset(rep56, 0x00, IOWKIT56_IO_REPORT_SIZE);
    return IowKitRead(IOW_PIPE_IO_PINS, (char*) rep56, IOWKIT40_IO_REPORT_SIZE);
}


unsigned long IOWKIT_API IOWarrior::IowKitRead(unsigned long numPipe, char* buffer, unsigned long length)
{
    #ifndef _WIN32
    struct timeval tv;
    fd_set rfds;
    #endif
    unsigned long rbc = 0;
    int siz, extra;

    if (buffer == NULL)
    return 0;

    // Device holen (struct IowDevice_t)
    //dev = IowiGetDeviceByHandle(devHandle);
    siz = m_Info.packet_size;
    switch(numPipe)
    {
        case IOW_PIPE_IO_PINS:
        extra = 1;
        break;
        case IOW_PIPE_SPECIAL_MODE:
        extra = 0;
        break;
        default:
        return 0;
    }
    #ifndef _WIN32
    int result;
    for(rbc = 0; rbc + siz + extra <= length; )
    {
        tv.tv_sec = (time_t)(m_timeout / 1000);
        tv.tv_usec = (long)((m_timeout % 1000) * 1000);

        FD_ZERO(&rfds);                    // clear the read-descriptor for select
        FD_SET(m_ReadHandle, &rfds);   // add the filedescriptor to read-descriptor
        //if (dev->readTimeoutVal != 0xFFFFFFFF)
        result = select(m_ReadHandle + 1, &rfds, NULL, NULL, &tv);
        //else
        //result = select(m_ReadHandle + 1, &rfds, NULL, NULL, NULL);
        if(result > 0 && FD_ISSET(m_ReadHandle, &rfds))
        {
            // put in eventual ReportID 0 to be a Windows lookalike
            *buffer = '\0';
            buffer += extra;
            // there should be some data ready for reading now
            if (read(m_ReadHandle, buffer, siz) != siz) break;
            //if (numPipe == IOW_PIPE_IO_PINS) memcpy(&dev->lastValue, buffer, sizeof(DWORD));
            buffer += siz;
            rbc += siz + extra;
        }
        else
        // if we encountered a timeout then do not try to read several reports
        if(result == 0)
        break;
    }
    #else



    //std::cout << "try to read..." << std::endl;
    if (!ReadFile(m_ReadHandle, buffer, length, &rbc, &m_ovl))
    {
        if (GetLastError() == ERROR_IO_PENDING)
        {
            //std::cout << "...nix da." << std::endl;
            //std::cout << "warten..." << std::endl;
            DWORD ret = WaitForSingleObject(m_ovl.hEvent, 1000);
            if (ret == WAIT_OBJECT_0) // warten auf Event
            {
                //std::cout << "...da ist was!" << std::endl;
                GetOverlappedResult(m_ReadHandle, &m_ovl, &rbc, false);
                                                           /* ^^ If this parameter is TRUE, and the Internal
                                                           member of the lpOverlapped structure is STATUS_PENDING,
                                                           the function does not return until the operation has been completed.
                                                           If this parameter is FALSE and the operation is still pending,
                                                           the function returns FALSE and the GetLastError function returns
                                                           ERROR_IO_INCOMPLETE.*/
                //std::cout << rbc << " Byte" << std::endl;

            }
            else if (ret == WAIT_TIMEOUT)
            {
                //std::cout << "...timeout" << std::endl;
            }
            else
            {
                std::cout << "...grober mist" << std::endl;
            }
        }
    }

    #endif
    return rbc;
}



bool IOWarrior::WriteSimple(unsigned long value)
{
  IOWKIT_REPORT rep;
  //IOWKIT56_IO_REPORT rep56;

  // Init report
//  switch (IowKitGetProductId(devHandle))
    {
      // Write simple value to IOW40
//    case IOWKIT_PRODUCT_ID_IOW40:
      memset(&rep, 0xff, sizeof(rep));
      rep.ReportID = 0;
      rep.Bytes[3] = (unsigned char) ~value;
      return IowKitWrite(IOW_PIPE_IO_PINS, (char *) &rep, IOWKIT40_IO_REPORT_SIZE) == IOWKIT40_IO_REPORT_SIZE;
      // Write simple value to IOW24
/*    case IOWKIT_PRODUCT_ID_IOW24:
      memset(&rep, 0xff, sizeof(rep));
      rep.ReportID = 0;
      rep.Bytes[0] = (UCHAR) ~value;
      return IowKitWrite(devHandle, IOW_PIPE_IO_PINS,
			 (PCHAR) &rep, IOWKIT24_IO_REPORT_SIZE) == IOWKIT24_IO_REPORT_SIZE;
      // Write simple value to IOW56
    case IOWKIT_PRODUCT_ID_IOW56:
      memset(&rep56, 0xff, sizeof(rep56));
      rep56.ReportID = 0;
      rep56.Bytes[6] = (UCHAR) ~value;
      return IowKitWrite(devHandle, IOW_PIPE_IO_PINS,
			 (PCHAR) &rep56, IOWKIT56_IO_REPORT_SIZE) == IOWKIT56_IO_REPORT_SIZE;
    default:
      return FALSE;*/
    }
}

unsigned long IOWKIT_API IOWarrior::IowKitWrite(unsigned long numPipe, char* buffer, unsigned long length)
{

    if(canWrite())
    {




        if (buffer == NULL) return 0;



        #ifdef _WIN32
            BOOL bOK;
            DWORD written;
            //IOWKIT40_IO_REPORT report;
            //report.ReportID = 0;
            //report.Bytes[3] = (BYTE) 0xAA;

            // 5 wegen IO_PINS Modus (s.o.)
            //bOK = WriteFile(m_Handle, &report, 5, &written, NULL);
            bOK = WriteFile(m_WriteHandle, buffer, 5, &written, NULL);
            if (bOK && (written == 5))
            {
                return length;
            }

            return 0;

        #else

        unsigned long rbc;
        int siz, extra;

        siz = m_Info.packet_size;
        /*switch(numPipe)
        {
            case IOW_PIPE_IO_PINS:*/
            extra = 1;
            /*break;
            case IOW_PIPE_SPECIAL_MODE:
            extra = 0;
            break;
            default:
            return 0;
        }*/


        for(rbc = 0; rbc + siz + extra <= length; rbc += siz + extra)
        {
            buffer += extra;
             if (::write(m_WriteHandle, buffer, siz) != siz) break;
            buffer += siz;
        }
        return rbc;
        #endif
    }
    else
    {
        //std::cout << "Write: nicht offen" << std::endl;
    }


    return 0;
}



void IOWarrior::openDevice()
{
#ifdef _WIN32
	GUID HidGuid;
	HDEVINFO devInfo;
	SP_INTERFACE_DEVICE_DATA devInterfaceData;
	SP_DEVINFO_DATA devData;
	ULONG i;
	ULONG mode;
	UINT NDevs;
	char PathBuffer[IOWI_DEVICEPATH_LEN];
	HIDD_ATTRIBUTES hidAttr;
    PHIDP_PREPARSED_DATA prepData;
    HIDP_CAPS hidCaps;

    HANDLE h;



    //std::cout << "here" << std::endl;
	HidD_GetHidGuid(&HidGuid); // ## hidsdi.h


	// Start HID device enumeration
	// ## http://msdn.microsoft.com/en-us/library/windows/hardware/ff551069%28v=vs.85%29.aspx
	// ## Build a list of all devices that are present in the system but do not belong to an unknown device setup class (Windows Vista and later versions of Windows).
	// ## SetupDiGetClassDevs(GUID *ClassGuid,   PCTSTR Enumerator,HWND hwndParent,  DWORD Flags);
	// ## SetupDiGetClassDevs(&GUID_DEVINTERFACE_VOLUME, NULL, NULL, DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);
	devInfo = SetupDiGetClassDevsA(&HidGuid,             NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
	// Check if error
	if (devInfo == INVALID_HANDLE_VALUE) return;

    std::cout << "Suche nach IO-Warrior ..." << std::endl;

	// Set up DEVICE_DATA for enumeration
	devInterfaceData.cbSize = sizeof(devInterfaceData);
	// Init
	i = 0;
	NDevs = 0;
	// Enumerate IOW devices
	// i (MemberIndex wird hochgezählt, damit wird eine information nach der anderen abgerufen und devInterfaceData abgelegt
	// ## (HDEVINFO DeviceInfoSet, PSP_DEVINFO_DATA DeviceInfoData, GUID *InterfaceClassGuid, DWORD MemberIndex, PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData);
	while (SetupDiEnumDeviceInterfaces(devInfo, NULL,               &HidGuid,                 i,                 &devInterfaceData))
	{

		i++;

		// ## wozu > IowiFindByDevInst. wird von getdevicepath gefüllt??
		devData.cbSize = sizeof(devData);
		// ## pfad holen
		memset(PathBuffer, 0, IOWI_DEVICEPATH_LEN);
		IowiGetDevicePath(devInfo, &devInterfaceData, &devData, PathBuffer); // ## --> ok


		// Open device for HID access
		h =     CreateFile(PathBuffer, GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
		if (h == INVALID_HANDLE_VALUE) // ## alternativer versuch..?
		{
			h = CreateFile(PathBuffer, 0,                         FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
		}

		// Check if we have handle
		if ((h != INVALID_HANDLE_VALUE) && IowiIsIow40Device(h, &hidAttr)) // ## --> ok
		{
            std::cout << "Nr. " << i << ": " << "IOW 40 Device gefunden." << std::endl;

            HidD_GetPreparsedData(h, &prepData);
            HidP_GetCaps(prepData, &hidCaps);
            HidD_FreePreparsedData(prepData);


            mode = (hidCaps.OutputReportByteLength == 5) ? IOWKIT2_PIPE_IO_PINS : IOWKIT2_PIPE_SPECIAL_MODE;
            if (mode == IOWKIT2_PIPE_IO_PINS)
            {
                std::cout << "IO-Pins-Mode" << std::endl;
                std::cout << "DevicePath: " << PathBuffer << std::endl;

                m_Info.packet_size = 4; // woher bekomme ich diesen wert?? GetCommState ??
                m_Info.vendor = hidAttr.VendorID;
                m_Info.product = hidAttr.ProductID;
                m_Info.revision = hidAttr.VersionNumber;
                // mehr infos bekomme ich hier irgendwie nicht ...

                // schließen - dann neu öffnen
                CloseHandle(h);


                memset (&m_ovl, 0, sizeof (OVERLAPPED)); // Struktur mit 0en füllen
                m_ovl.hEvent = CreateEvent (NULL, FALSE, FALSE, NULL); // einen Event setzten


                //m_WriteHandle = CreateFile(PathBuffer, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
                m_WriteHandle = CreateFile(PathBuffer, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

                m_ReadHandle = CreateFile(PathBuffer, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);

                // erfolg

                break;
            }
            else
            {
                std::cout << "Special-Mode. Nicht unterstützt." << std::endl;
            }
		} // handle da

        if (h != INVALID_HANDLE_VALUE) CloseHandle(h);
        h = INVALID_HANDLE_VALUE;
	} // while

	SetupDiDestroyDeviceInfoList(devInfo); // system call
	// ===



#else // win else
// Linux

    std::ostringstream str;
    str << "/dev/usb/iowarrior" << m_DevNum;
    m_strDevice = str.str();

    std::cout << "Öffne IO-Warrior unter " << m_strDevice << " ..." << std::endl;

    // Open device
    {
        Glib::Mutex::Lock lock (mutex);

        m_ReadHandle = open(m_strDevice.c_str(), O_RDWR);
        if(m_ReadHandle != IOW_INVALID_HANDLE)
        {
            std::cout << "... OK." << std::endl;
            //now get the info for this device

            int ioctlret;
            ioctlret = ioctl(m_ReadHandle, IOW_GETINFO, &m_Info);
            if(ioctlret != -1)
            {
                // alles gut
                m_WriteHandle = m_ReadHandle;
                return;
            }
            else
            {
                ::close(m_ReadHandle);
                m_ReadHandle = m_WriteHandle = IOW_INVALID_HANDLE;
            }
        } // if fd!=IOW_INVALID_HANDLE

        std::cout << "... fehlgeschlagen." << std::endl;
    }
#endif // not win
}












#ifdef _WIN32

/** Helpers for Windows */

// ## aus IowiFillInList, kann man so verwenden
// Get device_interface_detail_data for given device
BOOL IowiGetDevicePath(HDEVINFO devInfo, PSP_DEVICE_INTERFACE_DATA devInterfaceData, PSP_DEVINFO_DATA devData, char *PathBuffer)
{
	PSP_DEVICE_INTERFACE_DETAIL_DATA_A devDetails;
	char buffer[IOWI_DEVICEPATH_LEN];
	ULONG len, req_len;

	*PathBuffer = '\0';
	// http://msdn.microsoft.com/en-us/library/windows/hardware/ff551120%28v=vs.85%29.aspx
	// Get length
	SetupDiGetDeviceInterfaceDetailA(devInfo, devInterfaceData, NULL, 0, &len, devData);

	if ((len == 0) || (len > IOWI_DEVICEPATH_LEN)) return FALSE;

	devDetails = (PSP_DEVICE_INTERFACE_DETAIL_DATA_A) buffer/*## char[] */;

	// Get details
	devDetails->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_A);
	SetupDiGetDeviceInterfaceDetailA(devInfo, devInterfaceData, devDetails, len, &req_len, devData);

	strcpy(PathBuffer, devDetails->DevicePath);

	return TRUE;
}


// ## aus IowiFillInList, kann so verwendet werden, achtung: es müssen all diese precompiler macros definiert sein!
BOOL IowiIsIow40Device(HANDLE devHandle, PHIDD_ATTRIBUTES phidAttr)
{
	BOOL bOK;

	// Set up structure for call
	phidAttr->Size = sizeof(HIDD_ATTRIBUTES);
	// Get HID device attributes
	bOK = HidD_GetAttributes(devHandle, phidAttr); // aus hid lib
	if (bOK)
		// Compare vendorID & productID with IoWarrior's
		bOK = ((phidAttr->VendorID == IOWKIT2_VID) && (phidAttr->ProductID == IOWKIT2_PID_IOW40));
	return bOK;
}


#endif // _WIN32
