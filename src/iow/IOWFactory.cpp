
#include "IOWFactory.h"
#include "IOWarrior.h"

#include <iostream>


type_iow_vector IOWFactory::m_IOWs;




/** @brief getDevice
  *
  * @todo: document this function
  */
IOWarrior* IOWFactory::getDevice(int num, void* owner_id)
{
    for (type_iow_vector::iterator it = m_IOWs.begin(); it != m_IOWs.end(); it++)
    {
        if ((*it)->getDevNum() == num)
        {
            if ((*it)->m_OwnerId == owner_id)
            {
                (*it)->incRefCount();
                return (*it);
            }
            else return NULL;
        }
    }

    IOWarrior* iow = new IOWarrior(num, owner_id);
    m_IOWs.push_back(iow);

    iow->incRefCount();
    return iow;
}

void IOWFactory::unregisterDevice(int num)
{
    //std::cout << "unregisterDevice(" << std::endl;
    for (type_iow_vector::iterator it = m_IOWs.begin(); it != m_IOWs.end(); it++)
    {
        if ((*it)->getDevNum() == num)
        {
            //std::cout << "-found" << std::endl;
            (*it)->decRefCount();
            if ((*it)->getRefCount() == 0)
            {
                //std::cout << "-delete" << std::endl;
                m_IOWs.erase(it);
                delete (*it);
            }
            return;
        }
    }
    return;
}

/** @brief getNumDevices
  *
  * @todo: document this function
  */
int IOWFactory::getNumDevices()
{
    return m_IOWs.size();
}


