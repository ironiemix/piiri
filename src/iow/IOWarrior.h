#ifndef IOWARRIOR_H
#define IOWARRIOR_H

//#include "simpel.h"
#include <string>
//#include "iow/iowarrior.h"
#include <gtkmm.h>




/* Version Information */
#define DRIVER_VERSION "v0.5.0"
#define DRIVER_AUTHOR "Christian Lucht <lucht@codemercs.com>, Stefan Beyer"
#define DRIVER_DESC "USB IO-Warrior driver (Linux 2.6.x)"
#define DRIVER_SUPPORTED_DEVICES "IOWarrior USB Devices"

#define USB_VENDOR_ID_CODEMERCS	1984
/* low speed iowarrior */
#define USB_DEVICE_ID_CODEMERCS_IOW40	0x1500
#define USB_DEVICE_ID_CODEMERCS_IOW24	0x1501
#define USB_DEVICE_ID_CODEMERCS_IOWPV1	0x1511
#define USB_DEVICE_ID_CODEMERCS_IOWPV2	0x1512
/* full speed iowarrior */
#define USB_DEVICE_ID_CODEMERCS_IOW56	0x1503

#define CODEMERCS_MAGIC_NUMBER	0xC0	// like COde Mercenaries

/* Define the ioctl commands for reading and writing data */
#define IOW_WRITE	_IOW(CODEMERCS_MAGIC_NUMBER, 1, long)
#define IOW_READ	_IOW(CODEMERCS_MAGIC_NUMBER, 2, long)

/* Define an ioctl command for getting more info on the device */

/* A struct for available device info which is used */
/* in the new ioctl IOW_GETINFO                     */
struct iowarrior_info {
	int vendor;			// vendor id : supposed to be USB_VENDOR_ID_CODEMERCS in all cases
	int product;			// product id : depends on type of chip (USB_DEVICE_ID_CODEMERCS_XXXXX)
	char serial[9];			// the serial number of our chip (if a serial-number is not available this is empty string)
	int revision;			// revision number of the chip
	int speed;			// USB-speed of the device (0=UNKNOWN, 1=LOW, 2=FULL 3=HIGH)
	int power;			// power consumption of the device in mA
	int if_num;			// the number of the endpoint
	unsigned int packet_size;	// size of the data-packets on this interface
};

/*
  Get some device-information (product-id , serial-number etc.)
  in order to identify a chip.
*/
#define IOW_GETINFO _IOR(CODEMERCS_MAGIC_NUMBER, 3, struct iowarrior_info)

/* Get a minor range for your devices from the usb maintainer */
#ifdef CONFIG_USB_DYNAMIC_MINORS
#define IOWARRIOR_MINOR_BASE	0
#else
// SKELETON_MINOR_BASE 192 + 16, not offical yet
#define IOWARRIOR_MINOR_BASE	208
#endif

/* interrupt input queue sizes */
#define MAX_INTERRUPT_BUFFER_IN 16
#define MAX_INTERRUPT_BUFFER_OUT 16

/*********************************/


#ifndef _IOWKIT_BUILD_RUN
#ifdef _IOWKIT2_H_
#error "including both iowkit2.h and iowkit.h is not allowed"
#endif // _IOWKIT2_H_
#endif // _IOWKIT_BUILD_RUN

#ifdef _WIN32

#define IOWKIT_API __stdcall

#else

#define IOWKIT_API

// the following legacy types can only be defined once
#ifndef _IOW_WINTYPES_H_
#define _IOW_WINTYPES_H_

/*
 * Windows specific types and defines
typedef unsigned long          ULONG;
typedef long                   LONG;
typedef unsigned short         USHORT;
typedef unsigned short         WORD;
typedef unsigned char          UCHAR;
typedef unsigned char          BYTE;
typedef char *                 PCHAR;
typedef unsigned short *       PWCHAR;
typedef int                    BOOL;
typedef unsigned char          BOOLEAN;
typedef unsigned long          DWORD;
typedef DWORD *                PDWORD;
typedef void *                 PVOID;
typedef DWORD                  HANDLE;
typedef ULONG *                PULONG;
typedef const char *           PCSTR;
typedef const unsigned short * PWCSTR;

 */
//#define FALSE 0
//#define TRUE 1

#endif // _IOW_WINTYPES_H_

#endif // _WIN32

// IO-Warrior vendor & product IDs
#define IOWKIT_VENDOR_ID         0x07c0
#define IOWKIT_VID               IOWKIT_VENDOR_ID
// IO-Warrior 40
#define IOWKIT_PRODUCT_ID_IOW40  0x1500
#define IOWKIT_PID_IOW40         IOWKIT_PRODUCT_ID_IOW40
// IO-Warrior 24
#define IOWKIT_PRODUCT_ID_IOW24  0x1501
#define IOWKIT_PID_IOW24         IOWKIT_PRODUCT_ID_IOW24
// IO-Warrior PowerVampire
#define IOWKIT_PRODUCT_ID_IOWPV1 0x1511
#define IOWKIT_PID_IOWPV1        IOWKIT_PRODUCT_ID_IOWPV1
#define IOWKIT_PRODUCT_ID_IOWPV2 0x1512
#define IOWKIT_PID_IOWPV2        IOWKIT_PRODUCT_ID_IOWPV2
// IO-Warrior 56
#define IOWKIT_PRODUCT_ID_IOW56  0x1503
#define IOWKIT_PID_IOW56         IOWKIT_PRODUCT_ID_IOW56

// Max number of pipes per IOW device
#define IOWKIT_MAX_PIPES    2

// pipe names
#define IOW_PIPE_IO_PINS      0
#define IOW_PIPE_SPECIAL_MODE 1

// Max number of IOW devices in system
#define IOWKIT_MAX_DEVICES  16
// IOW Legacy devices open modes
#define IOW_OPEN_SIMPLE     1
#define IOW_OPEN_COMPLEX    2

// first IO-Warrior revision with serial numbers
#define IOW_NON_LEGACY_REVISION 0x1010

// Don't forget to pack it!
#pragma pack(push, 1)

typedef struct _IOWKIT_REPORT
 {
  unsigned char ReportID;
  union
   {
    unsigned long Value;
    unsigned char Bytes[4];
   };
 }
  IOWKIT_REPORT, *PIOWKIT_REPORT;

typedef struct _IOWKIT40_IO_REPORT
 {
  unsigned char ReportID;
  union
   {
    unsigned long Value;
    unsigned char Bytes[4];
   };
 }
  IOWKIT40_IO_REPORT, *PIOWKIT40_IO_REPORT;

typedef struct _IOWKIT24_IO_REPORT
 {
  unsigned char ReportID;
  union
   {
    unsigned short Value;
    unsigned char Bytes[2];
   };
 }
  IOWKIT24_IO_REPORT, *PIOWKIT24_IO_REPORT;

typedef struct _IOWKIT_SPECIAL_REPORT
 {
  unsigned char ReportID;
  unsigned char Bytes[7];
 }
  IOWKIT_SPECIAL_REPORT, *PIOWKIT_SPECIAL_REPORT;

typedef struct _IOWKIT56_IO_REPORT
 {
  unsigned char ReportID;
  unsigned char Bytes[7];
 }
  IOWKIT56_IO_REPORT, *PIOWKIT56_IO_REPORT;

typedef struct _IOWKIT56_SPECIAL_REPORT
 {
  unsigned char ReportID;
  unsigned char Bytes[63];
 }
  IOWKIT56_SPECIAL_REPORT, *PIOWKIT56_SPECIAL_REPORT;

#define IOWKIT_REPORT_SIZE sizeof(IOWKIT_REPORT)
#define IOWKIT40_IO_REPORT_SIZE sizeof(IOWKIT40_IO_REPORT)
#define IOWKIT24_IO_REPORT_SIZE sizeof(IOWKIT24_IO_REPORT)
#define IOWKIT_SPECIAL_REPORT_SIZE sizeof(IOWKIT_SPECIAL_REPORT)
#define IOWKIT56_IO_REPORT_SIZE sizeof(IOWKIT56_IO_REPORT)
#define IOWKIT56_SPECIAL_REPORT_SIZE sizeof(IOWKIT56_SPECIAL_REPORT)

#pragma pack(pop)

// Opaque IO-Warrior handle
typedef void* IOWKIT_HANDLE;

// Function prototypes

/*
IOWKIT_HANDLE IOWKIT_API IowKitOpenDevice(void);
void IOWKIT_API IowKitCloseDevice(IOWKIT_HANDLE devHandle);
ULONG IOWKIT_API IowKitWrite(IOWKIT_HANDLE devHandle, ULONG numPipe,
  PCHAR buffer, ULONG length);
ULONG IOWKIT_API IowKitRead(IOWKIT_HANDLE devHandle, ULONG numPipe,
  PCHAR buffer, ULONG length);
*/


/*
BOOL IOWKIT_API IowKitReadImmediate(IOWKIT_HANDLE devHandle, PDWORD value);
ULONG IOWKIT_API IowKitGetNumDevs(void);
IOWKIT_HANDLE IOWKIT_API IowKitGetDeviceHandle(ULONG numDevice);
BOOL IOWKIT_API IowKitSetLegacyOpenMode(ULONG legacyOpenMode);
ULONG IOWKIT_API IowKitGetProductId(IOWKIT_HANDLE devHandle);
ULONG IOWKIT_API IowKitGetRevision(IOWKIT_HANDLE devHandle);
HANDLE IOWKIT_API IowKitGetThreadHandle(IOWKIT_HANDLE devHandle);
BOOL IOWKIT_API IowKitGetSerialNumber(IOWKIT_HANDLE devHandle, PWCHAR serialNumber);
BOOL IOWKIT_API IowKitSetTimeout(IOWKIT_HANDLE devHandle, ULONG timeout);
BOOL IOWKIT_API IowKitSetWriteTimeout(IOWKIT_HANDLE devHandle, ULONG timeout);
BOOL IOWKIT_API IowKitCancelIo(IOWKIT_HANDLE devHandle, ULONG numPipe);
PCSTR IOWKIT_API IowKitVersion(void);
*/



#ifdef _WIN32
extern "C"
{
#include <windows.h>

#if defined(DELETE) && !defined(GTKMM_MACRO_SHADOW_DELETE)
enum { GTKMM_MACRO_DEFINITION_DELETE = DELETE };
#undef DELETE
enum { DELETE = GTKMM_MACRO_DEFINITION_DELETE };
#define DELETE DELETE
#define GTKMM_MACRO_SHADOW_DELETE 1
#endif

//#include <stdlib.h>
//#include <stdio.h>

#include <SetupAPI.h>
//#include <ddk/cfgmgr32.h>
//#include <dbt.h>
#include "ddk/hidsdi.h"


}

// ============================
#define IOWI_DEVICEPATH_LEN 4096

// pipe names
#define IOWKIT2_PIPE_IO_PINS      0
#define IOWKIT2_PIPE_SPECIAL_MODE 1

// IO-Warrior vendor & product IDs
#define IOWKIT2_VENDOR_ID         0x07c0
#define IOWKIT2_VID               IOWKIT2_VENDOR_ID
// IO-Warrior 40
#define IOWKIT2_PRODUCT_ID_IOW40  0x1500
#define IOWKIT2_PID_IOW40         IOWKIT2_PRODUCT_ID_IOW40
// IO-Warrior 24
#define IOWKIT2_PRODUCT_ID_IOW24  0x1501
#define IOWKIT2_PID_IOW24         IOWKIT2_PRODUCT_ID_IOW24
// IO-Warrior PowerVampire
#define IOWKIT2_PRODUCT_ID_IOWPV1 0x1511
#define IOWKIT2_PID_IOWPV1        IOWKIT2_PRODUCT_ID_IOWPV1
#define IOWKIT2_PRODUCT_ID_IOWPV2 0x1512
#define IOWKIT2_PID_IOWPV2        IOWKIT2_PRODUCT_ID_IOWPV2
// IO-Warrior 56
#define IOWKIT2_PRODUCT_ID_IOW56  0x1503
#define IOWKIT2_PID_IOW56         IOWKIT2_PRODUCT_ID_IOW56

// ============================

#define IOW_INVALID_HANDLE INVALID_HANDLE_VALUE
//#define IOW_INVALID_HANDLE -1


BOOL IowiIsIow40Device(HANDLE devHandle, PHIDD_ATTRIBUTES phidAttr);
BOOL IowiGetDevicePath(HDEVINFO devInfo, PSP_DEVICE_INTERFACE_DATA devInterfaceData, PSP_DEVINFO_DATA devData, char *PathBuffer);


#else // win


#define IOW_INVALID_HANDLE -1

#endif // win else




/**********************************/

class IOWarrior
{
    public:
        IOWarrior(int num, void* owner_id);
        virtual ~IOWarrior();
        void close();


        unsigned long getInVal() {return m_InVal;};
        void setOutVal(unsigned long val);

        void startThread();
        void stopThread();

        bool isOpen()  {return (  canRead() &&  canWrite());};
        bool canWrite()  {return (  (m_WriteHandle != IOW_INVALID_HANDLE));};
        bool canRead()  {return (  (m_ReadHandle != IOW_INVALID_HANDLE) );};

        int getRefCount() {return refCount;};
        void incRefCount() {refCount++;};
        void decRefCount() {refCount--;};

        int getDevNum() {return m_DevNum;};


        // http://developer.gnome.org/glibmm/unstable/classGlib_1_1Dispatcher.html
        Glib::Dispatcher sig_input;

    protected:
        void run();

        unsigned long ReadSimpleNonBlocking(IOWKIT56_IO_REPORT *rep56);
        unsigned long ReadSimple(IOWKIT56_IO_REPORT *rep56);
        unsigned long IOWKIT_API IowKitReadNonBlocking(unsigned long numPipe, char* buffer, unsigned long length);
        unsigned long IOWKIT_API IowKitRead(unsigned long numPipe, char* buffer, unsigned long length);

        bool WriteSimple(unsigned long value);
        unsigned long IOWKIT_API IowKitWrite(unsigned long numPipe, char* buffer, unsigned long length);

        //#ifdef _WIN32
        //std::string win_getDevicePath(int num);
        //#endif
        void openDevice();

    private:
        std::string m_strDevice;
        int m_DevNum;

        #ifdef _WIN32
        HANDLE m_ReadHandle;
        HANDLE m_WriteHandle;
        OVERLAPPED m_ovl;
        #else
        int m_ReadHandle;
        int m_WriteHandle;
        #endif

        iowarrior_info m_Info;
        int refCount;
        unsigned long m_timeout;

        Glib::Thread* thread;
        Glib::Mutex mutex;
        bool stop;


        unsigned long m_OutVal, m_InVal, m_OutMask, m_InMask;

    public:
        // wird von der (einzigen) schreibenden componente gesetzt. wenn gesetzt darf nur die componente schreiben die ihn gesetzt hat
        // problem: reihenfolge gesichert?? es ist beim laden von der reihenfolge der erzeugung abhängig - sollte eher von properties amhängig sein!
        void* the_only_writer;

        // wer hat diese schnittstelle für sich beansprucht? eine andere schaltung soll nicht diese schnittstelle mitbenutzen können auch nicht nur im read mode
        void* m_OwnerId;

};

#endif // IOWARRIOR_H
