#ifndef IOWFACTORY_H
#define IOWFACTORY_H

//#include "iowkit.h"
#include <vector>
#include <stdlib.h>
//#include <gtkmm.h>

class IOWarrior;

typedef std::vector<IOWarrior*> type_iow_vector;

class IOWFactory
{
    private:
        IOWFactory() {};
    public:
        ~IOWFactory() {};

        static IOWarrior* getDevice(int num, void* owner_id);
        static void unregisterDevice(int num);
        static int getNumDevices();


    private:
        static type_iow_vector m_IOWs;
};

#endif // IOWFACTORY_H

