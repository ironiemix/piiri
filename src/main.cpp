/***************************************************************************
    PiiRi - Simulating logic Circuits
    Copyright (C) 2012  Stefan Beyer

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "main.h"
#include <iostream>
#include "model/App.h"


/**
 * Die Main-Funktion.
 * Initialisiert Programm

http://developer.gnome.org/gtkmm/2.24/
http://developer.gnome.org/glibmm/2.28/


// URLs
// http://developer.gnome.org/gtkmm/2.24/classGtk_1_1Widget.html
// http://developer.gnome.org/pango/stable/PangoMarkupFormat.html
// http://www.koders.com/cpp/fidDC0C20BD649A7295AD2691F05B8D4DBA7FFC1B9A.aspx?s=Glib+usleep+Gtkmm
// http://developer.gnome.org/glib/2.30/glib-Miscellaneous-Utility-Functions.html#g-path-get-basename
// http://developer.gnome.org/libxml++/stable/
// http://developer.gnome.org/gtkmm/2.24/group__Widgets.html
*/

int main (int argc, char *argv[])
{

    // PiiRi Start

    // Umleitung des Error-Streams
	std::streambuf *orig = std::cerr.rdbuf(App::err_os.rdbuf());

    // PiiRi initialisieren und starten, Hauptfenster öffnen, Gtk-Main-Loop
    App::init(argc, argv);

    // Programm ist beendet
    App::end();

    // die letzten Fehler abholen
	App::getErrString();

    // Error-Stream zurücksetzen.
	std::cerr.rdbuf(orig);

	return 0;

    // ================================================================
    // ================================================================
    // ================================================================
    // ================================================================

    // Tests




/*  // Parser-Test
    map<string,bool> assign;
    assign["x0"] = false;
    assign["x1"] = false;
    assign["x2"] = true;
    // x0&x1|x2 = (x0&x1)|x2
    string parseError;
    bool b = LogicParser::eval("!x0&(x1|x2)", assign, parseError);
    cout << parseError << endl;
    cout << "Ergebnis: "<<(b?"1":"0") << endl;
    return 0;
*/

/* // internationalisation ...
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);

  //                         ${datadir}/locale
//  bindtextdomain("piiri", "../usr/share/locale");
//  bind_textdomain_codeset("piiri", "UTF-8");
//  textdomain("piiri");
*/

/*  // Property test
    try
    {
        PropValue2<int> prop;
        prop.dump();
        prop.set_value(34);
        prop.dump();
        int a = prop.get_value();
        std::cout << a << std::endl;

        Complex2 comp;
        comp.set_int("integer", 55);
        comp.set_bool("bool", false);
        comp.set_double("double", 0.345);
        comp.set_string("string", "Mehr als das!");

        comp.set_bool("bool", true);


        Complex2* c = comp.set_complex("sss");
        c->set_bool("das", false);
        c->set_string("Titel", "Das ist das beste!");

        SelectionValue2* sv = c->set_selection("selection");
        sv->add_option("Hund");
        sv->add_option("Katze");
        sv->add_option("Maus");
        sv->set_value("Maus");


        comp.dump();

        Gtk::Window wnd;

        PropertyDialog2 d("Einstellungs-Test", wnd, comp);

        d.setUiText("/integer", ui_text("Ganzzahl",""));
        d.setUiText("/bool", ui_text("Wahrheitswert",""));
        d.setUiText("/double", ui_text("Kommazahl",""));
        d.setUiText("/string", ui_text("Zeichenkette",""));

        d.setUiText("/sss", ui_text("Zusatz",""));
        d.setUiText("/sss/selection", ui_text("Welches Tier","Wählen Sie ihr Lieblingstier."));
        d.setUiText("/sss/Titel", ui_text("Titel",""));
        d.setUiText("/sss/das", ui_text("Auch was",""));

        if (d.run() == Gtk::RESPONSE_OK)
        {
            if (d.hasChanged())
            {
                cout << "hasChanged, writeback!" << endl;
                d.writeback();
            }
        }

        std::cout << "Nach dem Bearbeiten: " << std::endl;
        comp.dump();


        xmlpp::DomParser dom;
        xmlpp::Document *doc = dom.get_document();
        xmlpp::Element *root = doc->create_root_node("piiri");

        Complex2::saveNew = true;
        Complex2::loadNew = true;
        comp.write_xml(root);

        std::string xml = doc->write_to_string_formatted();
        std::cout << xml << std::endl;

        dom.parse_memory(xml);
        doc = dom.get_document();
        root = doc->get_root_node();

        Complex2 comp2;
        comp2.read_xml(root);

        comp2.dump();


        //cout << comp.get_double("kftr") << endl;
        //cout << c->get_string("Titel") << endl;




    }
    catch (string& s)
    {
        cout << s << endl;
    }

	return 0;
    // Property Test */



} // main

