#ifndef HELPERS_H
#define HELPERS_H

#include <string>
#include <vector>
#include <map>
#include <libxml++/libxml++.h>

using namespace std;

#define DECLARE_ACTION_HANDLER(name) virtual void on_action_##name();
#define DEFINE_ACTION_HANDLER(class, name) void class::on_action_##name()


#define CREATE_ACTION(class, name, lbl, tip) refActionGroup->add( Gtk::Action::create(#name, lbl, tip), sigc::mem_fun(*this, &class::on_action_##name) );
#define CREATE_ACTION_KEY(class, name, lbl, tip, K) refActionGroup->add( Gtk::Action::create(#name, lbl, tip), K, sigc::mem_fun(*this, &class::on_action_##name) );
#define CREATE_ACTION2(class, name, stock, lbl, tip) refActionGroup->add( Gtk::Action::create(#name, stock, lbl, tip), sigc::mem_fun(*this, &class::on_action_##name) );
#define CREATE_ACTION2_KEY(class, name, stock, lbl, tip, K) refActionGroup->add( Gtk::Action::create(#name, stock, lbl, tip), K, sigc::mem_fun(*this, &class::on_action_##name) );




int splitString(vector<string>& l, const string& s, char ch);
void toUpperCase(string& s);
void toLowerCase(string& s);
bool isNumber(const string& s, bool neg=true);

bool DirectoryExists(const char* pzPath );
bool FileExists(const string& fileName );
bool FileNotExists(const string& fileName );
bool isFileWritable(const string& fileName );
string trim(string& s);
void trim_inplace(string& s);

xmlpp::Element* getFirstElement(xmlpp::Element* parent, const std::string& name);

bool xmlParseTestMemory(const Glib::ustring& xml);
bool xmlParseTestFile(const Glib::ustring& filename);


#endif // HELPERS_H
